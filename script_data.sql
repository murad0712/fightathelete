USE [master]
GO
/****** Object:  Database [fightathlete]    Script Date: 3/28/2018 2:30:10 PM ******/
CREATE DATABASE [fightathlete]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'fightathlete', FILENAME = N'D:\RDSDBDATA\DATA\fightathlete.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'fightathlete_log', FILENAME = N'D:\RDSDBDATA\DATA\fightathlete_log.ldf' , SIZE = 1280KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [fightathlete] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [fightathlete].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [fightathlete] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [fightathlete] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [fightathlete] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [fightathlete] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [fightathlete] SET ARITHABORT OFF 
GO
ALTER DATABASE [fightathlete] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [fightathlete] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [fightathlete] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [fightathlete] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [fightathlete] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [fightathlete] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [fightathlete] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [fightathlete] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [fightathlete] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [fightathlete] SET  DISABLE_BROKER 
GO
ALTER DATABASE [fightathlete] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [fightathlete] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [fightathlete] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [fightathlete] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [fightathlete] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [fightathlete] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [fightathlete] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [fightathlete] SET RECOVERY FULL 
GO
ALTER DATABASE [fightathlete] SET  MULTI_USER 
GO
ALTER DATABASE [fightathlete] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [fightathlete] SET DB_CHAINING OFF 
GO
ALTER DATABASE [fightathlete] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [fightathlete] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [fightathlete] SET DELAYED_DURABILITY = DISABLED 
GO
USE [fightathlete]
GO
/****** Object:  User [fightathlete]    Script Date: 3/28/2018 2:30:12 PM ******/
CREATE USER [fightathlete] FOR LOGIN [fightathlete] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [fightathlete]
GO
/****** Object:  Schema [fightathlete]    Script Date: 3/28/2018 2:30:13 PM ******/
CREATE SCHEMA [fightathlete]
GO
/****** Object:  Table [dbo].[ActiveUser]    Script Date: 3/28/2018 2:30:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActiveUser](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[LastLogin] [datetime] NULL,
 CONSTRAINT [PK_ActiveUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Affiliate]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Affiliate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Photo] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Affiliate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Agenda]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Agenda](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[User_Id] [int] NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NOT NULL,
	[Date] [datetime] NULL,
	[LevelId] [int] NULL,
	[DurationId] [int] NULL,
	[FrequencyId] [int] NULL,
	[ObjectiveId] [int] NULL,
	[Week] [int] NULL,
	[ProgramId] [int] NULL,
 CONSTRAINT [PK_Agenda] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompleteBattle]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompleteBattle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BattleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [datetime] NULL,
 CONSTRAINT [PK_CompleteBattle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompleteProgram]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompleteProgram](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [datetime] NULL,
 CONSTRAINT [PK_CompleteProgram] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CompleteWorkout]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompleteWorkout](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [date] NULL,
	[WorkoutName] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_CompleteWorkout] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CurrencyId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Duration]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Duration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Duration] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Exercise]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exercise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[IsRestExercise] [int] NOT NULL,
	[Duration] [nvarchar](50) NULL,
	[Video] [nvarchar](max) NULL,
	[IsDeleted] [int] NULL,
	[IsBasicVersion] [int] NULL,
	[IsNew] [int] NULL,
	[IsActive] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[Thumbnail] [nvarchar](max) NULL,
 CONSTRAINT [PK_Exercise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Exercise_Level]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exercise_Level](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExerciseId] [int] NOT NULL,
	[LevelId] [int] NOT NULL,
 CONSTRAINT [PK_Exercise_Level] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Exercise_MuscleGroup]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exercise_MuscleGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExerciseId] [int] NOT NULL,
	[MuscleGroupId] [int] NOT NULL,
 CONSTRAINT [PK_Exercise_MuscleGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Exercise_Tool]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exercise_Tool](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExerciseId] [int] NOT NULL,
	[ToolId] [int] NOT NULL,
 CONSTRAINT [PK_Exercise_Tool] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExerciseParameter]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExerciseParameter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_ExerciseParameter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FAQ]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQ](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](max) NOT NULL,
	[Answer] [nvarchar](max) NOT NULL,
	[CategoryId] [int] NOT NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FaqCategory]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FaqCategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_FaqCategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FavouriteBattle]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouriteBattle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BattleId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [datetime] NULL,
	[BattleName] [nvarchar](max) NULL,
	[ObjectiveId] [int] NULL,
	[LevelId] [int] NULL,
 CONSTRAINT [PK_FavouriteBattle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FavouriteExercise]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouriteExercise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExerciseId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [datetime] NULL,
	[ExerciseName] [nvarchar](max) NULL,
 CONSTRAINT [PK_FavouriteExercise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FavouriteProgram]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouriteProgram](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [datetime] NULL,
	[ProgramName] [nvarchar](max) NULL,
	[LevelId] [int] NULL,
	[DurationId] [int] NULL,
	[FrequencyId] [int] NULL,
 CONSTRAINT [PK_FavouriteProgram] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FavouriteWorkout]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FavouriteWorkout](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DateTime] [datetime] NULL,
	[WorkoutName] [nvarchar](max) NULL,
	[ObjectiveId] [int] NULL,
	[LevelId] [int] NULL,
 CONSTRAINT [PK_FavouriteWorkout] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Frequency]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Frequency](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Frequency] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HallOfFame]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HallOfFame](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BattleName] [nvarchar](max) NOT NULL,
	[BattleId] [int] NOT NULL,
	[RecordTime] [float] NOT NULL,
	[DateTime] [datetime] NULL,
	[UserId] [int] NOT NULL,
	[IsDeleted] [int] NULL,
	[ObjectiveId] [int] NULL,
	[LevelId] [int] NULL,
 CONSTRAINT [PK_HallOfFame] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Levels]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Levels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Levels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogCircuit]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogCircuit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[Date] [datetime] NULL,
	[WorkoutTime] [float] NULL,
	[UserId] [int] NOT NULL,
	[Point] [int] NOT NULL,
	[ObjectiveId] [int] NULL,
	[LevelId] [int] NULL,
	[Cycle] [int] NULL,
	[DurationID] [int] NULL,
	[FrequencyId] [int] NULL,
	[ProgramId] [int] NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogStrength]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogStrength](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Type] [nvarchar](50) NULL,
	[TypeId] [int] NOT NULL,
	[TypeName] [nvarchar](max) NULL,
	[Date] [datetime] NULL,
	[Point] [int] NOT NULL,
	[ObjectiveId] [int] NULL,
	[LevelId] [int] NULL,
	[Cycle] [int] NULL,
	[DurationId] [int] NULL,
	[FrequencyId] [int] NULL,
	[ProgramId] [int] NULL,
 CONSTRAINT [PK_LogStrength] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogStrengthExercise]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogStrengthExercise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LogStrengthId] [int] NOT NULL,
	[ExerciseName] [nvarchar](max) NULL,
 CONSTRAINT [PK_LogStrengthExercise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LogStrengthSet]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogStrengthSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SetNumber] [int] NULL,
	[Weight] [float] NULL,
	[Reps] [float] NULL,
	[LogStrengthExerciseId] [int] NOT NULL,
 CONSTRAINT [PK_LogStrengthSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MembershipType]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MembershipType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Duration] [int] NOT NULL,
	[Price] [float] NOT NULL,
	[CurrencyId] [int] NOT NULL,
	[Discount] [int] NULL,
 CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MuscleGroup]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuscleGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_MuscleGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Notification]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[Tekst] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Objective]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Objective](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Objective] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OfficialEmail]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OfficialEmail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NULL,
 CONSTRAINT [PK_OfficialEmail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Preference]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Preference](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Preference] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PreferenceParameterType]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PreferenceParameterType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[PreferenceId] [int] NOT NULL,
	[ParameterId] [int] NOT NULL,
 CONSTRAINT [PK_PreferenceParameterType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Privacy]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Privacy](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Privacy] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Profile]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Profile](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Batch] [nvarchar](50) NULL,
	[WorkoutPoint] [int] NULL,
	[ProgramPoint] [int] NULL,
	[BattlePoint] [int] NULL,
 CONSTRAINT [PK_Profile] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Program]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Program](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsBasic] [int] NOT NULL,
	[IsDeleted] [int] NOT NULL,
	[FrequencyId] [int] NOT NULL,
	[DurationId] [int] NOT NULL,
	[IsActive] [int] NULL,
	[IsNew] [int] NULL,
	[Point] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_Program] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Program_Level]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Program_Level](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[LevelId] [int] NOT NULL,
 CONSTRAINT [PK_Program_Level] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Program_Tool]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Program_Tool](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[ToolId] [int] NOT NULL,
 CONSTRAINT [PK_Program_Tool] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramCycle]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramCycle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[ProgramName] [nvarchar](50) NULL,
	[LevelName] [nvarchar](50) NULL,
	[FrequencyName] [nvarchar](50) NULL,
	[Point] [int] NULL,
	[Cycle] [int] NOT NULL,
	[CreatedOn] [datetime] NULL,
	[DurationName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ProgramCycle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProgramWorkout]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProgramWorkout](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProgramId] [int] NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[Day] [int] NOT NULL,
	[Type] [nvarchar](50) NULL,
 CONSTRAINT [PK_ProgramWorkout] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Quote]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Quote](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Tekst] [nvarchar](max) NOT NULL,
	[Author] [nvarchar](max) NULL,
 CONSTRAINT [PK_Quote] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Score]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Score](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Start] [int] NOT NULL,
	[Finish] [int] NOT NULL,
 CONSTRAINT [PK_Score] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TermsOfUse]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TermsOfUse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_TermsOfUse] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Tool]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tool](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](max) NULL,
	[Photo] [nvarchar](max) NULL,
 CONSTRAINT [PK_Tool] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NULL,
	[Gender] [nvarchar](50) NULL,
	[Birthdate] [datetime] NULL,
	[Country] [nvarchar](50) NULL,
	[EmailAddress] [nvarchar](50) NULL,
	[MembershipSince] [datetime] NOT NULL,
	[MembershipTypeId] [int] NOT NULL,
	[Photo] [nvarchar](max) NULL,
	[GcmRegistrationToken] [nvarchar](max) NULL,
	[LastLogin] [datetime] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workout]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workout](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[WorkoutType] [nvarchar](50) NOT NULL,
	[GoalDescription] [nvarchar](max) NULL,
	[Points] [int] NULL,
	[IsBattleWorkout] [int] NOT NULL,
	[Photo] [nvarchar](max) NULL,
	[IsOnBasicVersion] [int] NOT NULL,
	[IsDeleted] [int] NOT NULL,
	[IsActive] [int] NULL,
	[IsNew] [int] NULL,
	[CreatedOn] [datetime] NULL,
 CONSTRAINT [PK_Workout] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workout_Level]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workout_Level](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[LevelId] [int] NOT NULL,
 CONSTRAINT [PK_Workout_Level] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workout_Objective]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workout_Objective](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[ObjectiveId] [int] NOT NULL,
 CONSTRAINT [PK_Workout_Objective] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workout_Tool]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workout_Tool](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[ToolId] [int] NOT NULL,
 CONSTRAINT [PK_Workout_Tool] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkoutExercise]    Script Date: 3/28/2018 2:30:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkoutExercise](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkoutId] [int] NOT NULL,
	[ExerciseId] [int] NOT NULL,
	[Reps] [nvarchar](50) NULL,
	[Sets] [nvarchar](50) NULL,
	[Tempo] [nvarchar](50) NULL,
	[Rest] [nvarchar](50) NULL,
	[Duration] [nvarchar](50) NULL,
	[Distance] [nvarchar](50) NULL,
	[Weight] [nvarchar](50) NULL,
	[RepHigh] [int] NOT NULL,
 CONSTRAINT [PK_WorkoutExercise] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Agenda] ON 

INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (1, 14, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-06-07 00:00:00.000' AS DateTime), 2, 0, 0, 3, 0, NULL)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (78, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-17 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (79, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (80, 10, N'Workout', 1, N'Bench Press', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 2, 0, 0, 2, 2, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (81, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-17 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (82, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (83, 10, N'Workout', 1, N'Bench Press', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 2, 0, 0, 2, 2, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (85, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (86, 10, N'Workout', 17, N'Test Strength', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (87, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-07-05 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (88, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-07-12 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (89, 10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-07-15 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (90, 10, N'Workout', 1, N'Bench Press', CAST(N'2017-07-17 00:00:00.000' AS DateTime), 2, 0, 0, 2, 1, 2)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (92, 10, N'Exercise', 12, N'Burpee', CAST(N'2017-06-13 18:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (93, 17, N'Workout', 19, N'Rest Workout', CAST(N'2017-06-12 18:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (95, 2, N'Workout', 17, N'Test Strength', CAST(N'2017-06-12 22:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (96, 2, N'Workout', 1, N'Bench Press', CAST(N'2017-06-13 22:00:00.000' AS DateTime), 2, 0, 0, 2, 1, 1)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (97, 2, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-15 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 1)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (98, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-06-16 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 1)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (99, 2, N'Exercise', 12, N'Burpee', CAST(N'2017-06-12 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (100, 2, N'Workout', 6, N'Dumbell Workout', CAST(N'2017-06-12 22:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (101, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-12 22:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (102, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-12 22:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (103, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-13 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (104, 2, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-13 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (105, 2, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-16 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (106, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-19 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (107, 2, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-22 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (108, 10, N'Workout', 21, N'Strength 130617', CAST(N'2017-08-15 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (111, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-30 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (112, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-12-01 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (113, 19, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (114, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-21 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (115, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (116, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (117, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (119, 2, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-14 22:00:00.000' AS DateTime), 1, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (120, 2, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 1, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (121, 2, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (122, 29, N'Workout', 21, N'Strength 130617', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (123, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (124, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-08-02 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (126, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-08-06 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (127, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-08-08 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (128, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-08-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (129, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-08-12 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (130, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-09-02 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (131, 19, N'Workout', 22, N'Circuit 130617', CAST(N'2017-09-04 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (132, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-09-06 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (133, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-09-08 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (134, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-09-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (135, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-09-12 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (136, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-07-02 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (137, 19, N'Workout', 22, N'Circuit 130617', CAST(N'2017-07-04 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (138, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-06 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (139, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-07-08 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (140, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (141, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-12 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (142, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-10-02 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (143, 19, N'Workout', 22, N'Circuit 130617', CAST(N'2017-10-04 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (144, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-10-06 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (145, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-10-08 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (146, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-10-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (147, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-10-12 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (148, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-16 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (149, 19, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-18 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (150, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-20 22:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (151, 19, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-22 22:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (152, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-24 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (153, 19, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-26 22:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (154, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-16 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (155, 19, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-16 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (156, 19, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-19 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (157, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-22 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (158, 19, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-25 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (159, 30, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-15 22:00:00.000' AS DateTime), 2, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (160, 30, N'Exercise', 23, N'Sit up', CAST(N'2017-06-15 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (161, 30, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (162, 30, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (163, 30, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-24 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (164, 30, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (165, 30, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-28 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (166, 30, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-30 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (167, 32, N'Exercise', 21, N'Push up', CAST(N'2017-06-17 18:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (168, 17, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-08-21 00:00:00.000' AS DateTime), 1, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (169, 17, N'Workout', 21, N'Strength 130617', CAST(N'2017-08-02 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (170, 17, N'Workout', 22, N'Circuit 130617', CAST(N'2017-08-04 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (171, 17, N'Workout', 25, N'Interval 130617', CAST(N'2017-08-06 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (172, 17, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-08-08 00:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (173, 17, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-08-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (174, 17, N'Workout', 25, N'Interval 130617', CAST(N'2017-08-12 00:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (175, 17, N'Workout', 21, N'Strength 130617', CAST(N'2017-09-01 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (176, 17, N'Battle', 23, N'Battle 130617', CAST(N'2017-09-01 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (177, 17, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-09-04 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (178, 17, N'Workout', 21, N'Strength 130617', CAST(N'2017-09-07 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (179, 17, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-09-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 9)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (182, 36, N'Workout', 29, N'Circuit 150617', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 2, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (183, 36, N'Workout', 28, N'Interval 150617', CAST(N'2017-06-18 22:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (184, 36, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-08-01 00:00:00.000' AS DateTime), 2, 0, 0, 2, 0, 0)
GO
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (185, 36, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-18 22:00:00.000' AS DateTime), 2, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (186, 36, N'Workout', 21, N'Strength 130617', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (187, 36, N'Workout', 22, N'Circuit 130617', CAST(N'2017-07-03 00:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (275, 29, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (276, 46, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 18:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (277, 46, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-25 18:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (278, 46, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-27 18:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (279, 46, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-29 18:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (280, 46, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 18:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (281, 46, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-03 18:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (283, 47, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-25 18:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (284, 47, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (285, 47, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-29 18:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (286, 47, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 18:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (287, 47, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-03 18:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (288, 48, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 18:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (289, 48, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-25 18:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (290, 48, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-27 18:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (291, 48, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-29 18:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (292, 48, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 18:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (293, 48, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-03 18:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (294, 50, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 18:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (295, 50, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-25 18:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (296, 50, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-27 18:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (297, 50, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-29 18:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (298, 50, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-01 18:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (299, 50, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-03 18:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (300, 50, N'Workout', 21, N'Strength 130617', CAST(N'2017-08-01 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (301, 29, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-24 22:00:00.000' AS DateTime), 1, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (302, 29, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 2, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (303, 29, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-29 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (304, 29, N'Workout', 22, N'Circuit 130617', CAST(N'2017-07-01 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (305, 29, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-03 22:00:00.000' AS DateTime), 1, 0, 0, 6, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (306, 29, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-07-05 22:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (307, 29, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-07 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (308, 29, N'Workout', 25, N'Interval 130617', CAST(N'2017-07-09 22:00:00.000' AS DateTime), 1, 0, 0, 6, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (309, 52, N'Exercise', 21, N'Push up', CAST(N'2017-07-02 18:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (310, 29, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-07-07 00:00:00.000' AS DateTime), 2, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (311, 29, N'Workout', 21, N'TETSUYA', CAST(N'2017-07-16 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (312, 29, N'Workout', 35, N'SANSUI 15K', CAST(N'2017-07-18 22:00:00.000' AS DateTime), 1, 0, 0, 3, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (313, 29, N'Workout', 25, N'YUUDAI', CAST(N'2017-07-20 22:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (314, 29, N'Battle', 24, N'ARATA', CAST(N'2017-07-22 22:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (315, 29, N'Workout', 27, N'KICHIROU', CAST(N'2017-07-24 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (316, 29, N'Workout', 25, N'YUUDAI', CAST(N'2017-07-26 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (317, 29, N'Workout', 22, N'HIROKI', CAST(N'2017-07-28 22:00:00.000' AS DateTime), 1, 0, 0, 2, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (318, 29, N'Workout', 36, N'GYO 1K', CAST(N'2017-08-01 22:00:00.000' AS DateTime), 1, 0, 0, 3, 3, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (319, 29, N'Workout', 31, N'TAKUMI', CAST(N'2017-08-05 22:00:00.000' AS DateTime), 1, 0, 0, 2, 3, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (320, 64, N'Exercise', 55, N'MEDBALL SIT UPS', CAST(N'2017-07-17 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (321, 64, N'Exercise', 55, N'MEDBALL SIT UPS', CAST(N'2017-07-17 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (322, 64, N'Workout', 21, N'TETSUYA', CAST(N'2017-07-18 22:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (323, 64, N'Workout', 26, N'HIROYUKI', CAST(N'2017-07-20 22:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (324, 64, N'Battle', 24, N'ARATA-B', CAST(N'2017-07-22 22:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (325, 64, N'Workout', 33, N'SANSUI 5K', CAST(N'2017-07-25 22:00:00.000' AS DateTime), 1, 0, 0, 3, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (326, 64, N'Workout', 38, N'YASUHIRO', CAST(N'2017-07-28 22:00:00.000' AS DateTime), 1, 0, 0, 1, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (327, 64, N'Workout', 41, N'DAICHI', CAST(N'2017-07-30 22:00:00.000' AS DateTime), 1, 0, 0, 4, 2, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (328, 64, N'Workout', 39, N'TOMIO', CAST(N'2017-08-02 22:00:00.000' AS DateTime), 1, 0, 0, 6, 3, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (329, 64, N'Battle', 50, N'HIDEAKI', CAST(N'2017-08-05 22:00:00.000' AS DateTime), 1, 0, 0, 2, 3, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (330, 64, N'Battle', 51, N'HISASHI', CAST(N'2017-08-07 22:00:00.000' AS DateTime), 1, 0, 0, 6, 3, 8)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (331, 75, N'Exercise', 85, N'EXPLOSIVE RUSSIAN TWISTS', CAST(N'2017-07-17 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (332, 75, N'Exercise', 85, N'EXPLOSIVE RUSSIAN TWISTS', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (333, 76, N'Exercise', 61, N'KETTLEBELL CLEAN & PRESSES', CAST(N'2017-07-18 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (334, 77, N'Workout', 25, N'YUUDAI', CAST(N'2017-07-20 22:00:00.000' AS DateTime), 2, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (335, 83, N'Exercise', 104, N'LONG JUMPS', CAST(N'2017-07-20 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (336, 64, N'Workout', 33, N'SANSUI 5K', CAST(N'2017-07-24 22:00:00.000' AS DateTime), 2, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (337, 71, N'Exercise', 105, N'BURPEES', CAST(N'2017-08-08 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (338, 71, N'Exercise', 105, N'BURPEES', CAST(N'2017-09-09 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (339, 85, N'Workout', 38, N'YASUHIRO', CAST(N'2017-08-30 22:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (340, 85, N'Workout', 52, N'AKIHIRO', CAST(N'2017-08-02 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (341, 85, N'Workout', 21, N'TETSUYA', CAST(N'2017-08-28 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (342, 2, N'Workout', 38, N'YASUHIRO', CAST(N'2017-09-06 22:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (343, 64, N'Exercise', 61, N'KETTLEBELL CLEAN & PRESSES', CAST(N'2017-10-03 22:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (344, 88, N'Workout', 25, N'YUUDAI', CAST(N'2017-10-18 23:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (345, 88, N'Workout', 12, N'Sprint workout', CAST(N'2017-10-18 23:00:00.000' AS DateTime), 1, 0, 0, 4, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (346, 85, N'Workout', 41, N'DAICHI', CAST(N'2017-10-21 00:00:00.000' AS DateTime), 1, 0, 0, 4, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (347, 85, N'Workout', 21, N'TETSUYA', CAST(N'2017-11-18 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (348, 85, N'Workout', 26, N'HIROYUKI', CAST(N'2017-11-18 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (349, 91, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-05 23:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (350, 93, N'Workout', 21, N'TETSUYA', CAST(N'2017-12-05 23:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (351, 92, N'Workout', 21, N'TETSUYA', CAST(N'2017-12-05 23:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (352, 92, N'Workout', 26, N'HIROYUKI', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (353, 2, N'Exercise', 105, N'BURPEES', CAST(N'2017-12-04 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (354, 91, N'Strength', 2, N'Bent-Over Row', CAST(N'2017-12-05 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (355, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-12-05 23:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (356, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-12-05 23:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (357, 91, N'Strength', 2, N'Bent-Over Row', CAST(N'2018-03-08 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (358, 91, N'Strength', 2, N'Bent-Over Row', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (359, 85, N'Strength', 21, N'TETSUYA', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (360, 94, N'Circuit', 48, N'ROW 2K', CAST(N'2017-12-10 00:00:00.000' AS DateTime), 1, 0, 0, 3, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (361, 85, N'Circuit', 29, N'NOBORU', CAST(N'2017-12-07 22:42:11.167' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (362, 85, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-07 22:43:30.083' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (363, 85, N'Exercise', 105, N'BURPEES', CAST(N'2017-12-09 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (364, 85, N'Workout', 21, N'TETSUYA', CAST(N'2017-12-09 23:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (365, 85, N'Workout', 26, N'HIROYUKI', CAST(N'2017-12-11 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (366, 85, N'Workout', 29, N'NOBORU', CAST(N'2017-12-14 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (367, 85, N'Workout', 25, N'YUUDAI', CAST(N'2017-12-08 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 11)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (368, 85, N'Workout', 29, N'NOBORU', CAST(N'2017-12-10 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 11)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (369, 93, N'Circuit', 59, N'JIROU', CAST(N'2017-12-08 09:53:45.417' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (370, 93, N'Circuit', 29, N'NOBORU', CAST(N'2017-12-12 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (371, 95, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-08 10:06:47.890' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (372, 95, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-10 00:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
GO
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (373, 95, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-12 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (374, 96, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-08 10:19:09.743' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (375, 96, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-10 00:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (376, 96, N'Circuit', 58, N'IWAO', CAST(N'2017-12-14 00:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (377, 97, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-08 10:25:33.340' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (378, 97, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-12 00:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (379, 97, N'Exercise', 105, N'BURPEES', CAST(N'2017-12-09 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (380, 2, N'Exercise', 105, N'BURPEES', CAST(N'2017-12-14 19:25:23.767' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (381, 97, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-15 16:26:58.280' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (382, 97, N'Workout', 21, N'TETSUYA', CAST(N'2017-12-31 23:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (383, 97, N'Workout', 26, N'HIROYUKI', CAST(N'2018-01-02 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (384, 97, N'Workout', 29, N'NOBORU', CAST(N'2018-01-05 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (385, 97, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-03 11:41:31.203' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (386, 97, N'Strength', 21, N'TETSUYA', CAST(N'2018-01-05 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (387, 97, N'Circuit', 29, N'NOBORU', CAST(N'2018-01-04 10:20:33.803' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (388, 97, N'Strength', 21, N'TETSUYA', CAST(N'2018-01-04 10:21:03.907' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (389, 97, N'Circuit', 39, N'TOMIO', CAST(N'2018-01-04 10:21:31.670' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (390, 97, N'Exercise', 105, N'BURPEES', CAST(N'2018-01-04 10:22:49.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (391, 97, N'Circuit', 30, N'YASUSHI', CAST(N'2018-01-05 16:07:27.173' AS DateTime), 1, 0, 0, 4, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (392, 97, N'Exercise', 105, N'BURPEES', CAST(N'2018-01-08 13:12:54.417' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (393, 2, N'Strength', 21, N'TETSUYA', CAST(N'2018-01-11 20:36:15.913' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (394, 2, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-12 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (395, 2, N'Workout', 21, N'TETSUYA', CAST(N'2018-01-17 23:00:00.000' AS DateTime), 1, 0, 0, 1, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (396, 2, N'Workout', 26, N'HIROYUKI', CAST(N'2018-01-19 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (397, 2, N'Workout', 29, N'NOBORU', CAST(N'2018-01-22 23:00:00.000' AS DateTime), 1, 0, 0, 2, 1, 10)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (398, 102, N'Exercise', 61, N'KETTLEBELL CLEAN & PRESSES', CAST(N'2018-01-24 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (400, 2, N'Strength', 21, N'TETSUYA', CAST(N'2018-02-08 15:23:36.383' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (401, 2, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-02-10 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (402, 2, N'Circuit', 58, N'IWAO', CAST(N'2018-02-14 00:00:00.000' AS DateTime), 1, 0, 0, 6, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (403, 2, N'Circuit', 30, N'YASUSHI', CAST(N'2018-03-07 00:00:00.000' AS DateTime), 1, 0, 0, 4, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (404, 2, N'Strength', 21, N'TETSUYA', CAST(N'2018-03-11 00:00:00.000' AS DateTime), 1, 0, 0, 1, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (405, 2, N'Circuit', 29, N'NOBORU', CAST(N'2018-03-17 00:00:00.000' AS DateTime), 1, 0, 0, 2, 0, 0)
INSERT [dbo].[Agenda] ([Id], [User_Id], [Type], [TypeId], [TypeName], [Date], [LevelId], [DurationId], [FrequencyId], [ObjectiveId], [Week], [ProgramId]) VALUES (406, 2, N'Exercise', 105, N'BURPEES', CAST(N'2018-01-01 00:00:00.000' AS DateTime), 0, 0, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Agenda] OFF
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'1', N'Admin')
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Facebook', N'1524406957600548', N'7280c100-278c-44e6-a833-a4967bd7e40b')
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [UserId]) VALUES (N'Facebook', N'226705207838203', N'52cab500-332f-487f-866b-f4dd9343f60d')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'adc36622-44f6-4364-8b2f-db5e51e52de3', N'1')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'f62a6c95-dc78-49f6-a42d-ae0336b89707', N'1')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'036d7b08-764b-48c7-92e2-fadbd2b5e5f1', N'atish2@bs-23.com', 0, N'ANE7pgSMMjFRzqtJEkBZ27DpMwEQ9r+akdojNJz7KsPRzcDxAN0gmasiHcEEgE1zjg==', N'4d42c695-8451-47d0-944e-bf937b2310ee', NULL, 0, 0, NULL, 0, 0, N'atish2@bs-23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0d9eba4c-0b5d-4bdf-8d42-6435b07713f2', N'dfgf@g.com', 0, N'AGeQIepLICJ6ge0fZIsnbEut4Q7YEhKSMCRKljWkw7Q/haBEzMpC3G9lY21QBgfJZw==', N'cda05204-9b39-4f16-b841-4269e693b070', NULL, 0, 0, NULL, 0, 0, N'dfgf@g.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0fbd80d1-6509-4895-a1dd-e597a9a256a7', N'Test@test.com', 0, N'AJaROkmKMznJ4Gn9iQmGjG0B0xOkZM0iRaa7IhB4LJsE+8F4yN/o5FKT4WVj67klsg==', N'd21958f5-b029-42e3-9b47-d6aa58dc4d31', NULL, 0, 0, NULL, 0, 0, N'Test@test.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'0fc34819-5bcc-4c13-b376-625e1d1b06fe', N'atish.iit0217@gmail.com', 0, N'AAIR1SX/GUT8pH6Nq6t7Gp574j3RaJ0nEZAJfCo7G3DVsEIbqSVes04mAWmuMd6z0g==', N'49db3040-b403-4e9a-aa82-ba5117f2dfe2', NULL, 0, 0, NULL, 0, 0, N'atish.iit0217@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'12507d6a-b441-42bb-836e-78da8281ff03', N'james@bond.com', 0, N'AIc3FCcWmH/JGg6kmv4swAzUh7bMh+gEwZVxgMsE2A4ia7fdNxxKOaDPegVPQndAGw==', N'adbd668d-4ae2-4d51-8a2e-e443251557ad', NULL, 0, 0, NULL, 0, 0, N'james@bond.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'14e65534-5f15-432c-be08-e98edd166013', N'ferry@softwareness.nl', 0, N'ABW/rLRTpGTFCKtc4pZ6ZiwGcPnFzbBSG6nwd23g0omVruYJTq4zt1WiCMARbD1W5Q==', N'a47b561f-454c-4e27-badf-6897d6a8b035', NULL, 0, 0, NULL, 1, 0, N'ferry@softwareness.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1a815183-3a50-4f5e-9be9-41f1ec418b42', N'robbin@gmail.com', 0, N'ACS10VvaRhLqhjnQg/B90NOsR4Z4fpO7Qr+1apCBv09Tv+eYd1j7ot8BJodMIe7kbg==', N'635dbd8e-8fca-4900-b545-b73b9b30cef1', NULL, 0, 0, NULL, 0, 0, N'robbin@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'20d747cc-1ab1-4d30-a7f5-589e4c98b211', N'asdf@gh.com', 0, N'AAlGbNVPzZvjta09aP/wK0sD3fVKurpfPz3XQbFt7+VHBC9mzy6KySPX6P6v4qyscw==', N'5adff046-a079-48e6-a1e6-c4963d3b2575', NULL, 0, 0, NULL, 0, 0, N'asdf@gh.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'23016026-6ba2-44c1-b840-3dd9e37e72c8', N'Ferryvanmaurik@msn.com', 0, N'ANFOuh/ck7h1E/dCPi5FfByeG0/WULHiD7aIms+1z+aAiZYXVplRcvNf6YzDO2FqtA==', N'9b105d76-0b5d-4e6b-9b22-856b5cd130e2', NULL, 0, 0, NULL, 0, 0, N'Ferryvanmaurik@msn.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'242e4d7d-585c-4ec4-9e75-a6968c328596', N'Info@peace360.nl', 0, N'AIms+TdTnccHGCdNawIY+M5lEV8tJ+b3kPktLmcN7pvD30JYajQHsZHkm5OfmWbW9Q==', N'84771221-10c5-474b-ab17-daffda77c538', NULL, 0, 0, NULL, 0, 0, N'Info@peace360.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'29a6b5fd-48ad-46ed-8981-bb811cbc00ca', N'info@fight-athlete.com', 0, N'AO9OcvH9oX/5JAvnkrg+sLjSrrgFwsuh1Vz8vhH0DK6HxecBITnxizKP3wediDeQ1g==', N'17432e4e-feea-4d1a-b079-ca46de662f18', NULL, 0, 0, NULL, 0, 0, N'info@fight-athlete.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'2fbc6bc2-9bc0-4791-9bec-7114be8f2b0d', N'Sjboone@live.nl', 0, N'AGQpJGKRx3NN2ymsrrrsuh1/bSxnk70DZ1G0jzvZFDxm4HzmIdNgqWKpvDhH2OlO4w==', N'de6a06dd-cf9f-481e-9e91-a868a0c07882', NULL, 0, 0, NULL, 0, 0, N'Sjboone@live.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'317d4add-5dcf-4bad-b010-1b460bbd1d39', N'm.n.s@live.nl', 0, N'ABQvpz17Kqxsnu2hO1HRjlO66orfrm9XSfeBo2+TfY3mUH030qwUAoMpIQLPHbydZA==', N'8bf74fcc-96d7-4ebb-bdd3-87b9eb97496b', NULL, 0, 0, NULL, 0, 0, N'm.n.s@live.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'342aab45-676e-4572-9429-f74ace55b2ab', N'baslacor@gmail.com', 0, N'AOiV1JI0/SMtpe+ktjnskGhWAgJOEavK6Oezedsnkvi2xLWzOkv8bl2/3xqq1QOoMQ==', N'854f82a6-905a-4321-a184-a9ebe836ad36', NULL, 0, 0, NULL, 0, 0, N'baslacor@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'3aaac989-d220-45de-9d15-00fa4192c102', N'rob@fight.com', 0, N'AKmyqQlLp47POJvYFaka9o/rcDMseOWN3ePWX2ztmPukJMVBBlLv9CIa8vAS4/tayg==', N'cab165ee-0271-48e8-a9ef-2ccfc6ae27be', NULL, 0, 0, NULL, 0, 0, N'rob@fight.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'403480d0-5612-435e-969b-4cef86c51980', N'Ralph.van.der.meer@kpnmail.nl', 0, N'ABDjGTo4lZp2o3mEhTG1LC0Ys3y22BdFXNv5n1ZnCs1iTVu2YD+2n6T7a3th/4aEcg==', N'f145fdf1-4720-48f2-946e-d0d5edfada28', NULL, 0, 0, NULL, 0, 0, N'Ralph.van.der.meer@kpnmail.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'42786db1-822e-4e78-a460-b209a96a9068', N'gapoog_445@hotmail.com', 0, N'AL5rsZ5/gc/UYTx4k33stTTG7OXbxnCmWeD9pPY2+0riHWuloWo/l6OEDv5asflv5Q==', N'4ba52200-8941-45c2-b76d-ec34e0a8ea00', NULL, 0, 0, NULL, 0, 0, N'gapoog_445@hotmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'42ff0b98-96f3-402a-9612-507844d7eed9', N'asdfh@gmail.com', 0, N'AJS6ip7SCWIkAHi4PP2VAudOAIMmjMK2x0upL7+/qlXBNK4++woic0mcGWb9SVJ3gA==', N'f33bdb33-3dc1-42dc-81fc-1d0f71bcb2e8', NULL, 0, 0, NULL, 0, 0, N'asdfh@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'448af6e1-f1a7-438e-856e-e67939c88769', N'michel.uiterwijk@gmail.com', 0, N'AHBnoRllyg6wc1oLGrOo/WkQ2WH8Lq5U+sO01pGOxITuwUTgc9QxQuSQaGvef/0bhA==', N'5d3a0a69-190f-45d1-8eeb-16002c48f04b', NULL, 0, 0, NULL, 0, 0, N'michel.uiterwijk@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'49699743-9862-4602-a50e-17ef7e38afb9', N'rob@fight.nl', 0, N'AMw3rGGG6yxlYiMg76nnFhuVS3f7NNO9cFhhSro798t+tvx4JWUhtlQ0K0qHByj5ng==', N'84e701c1-4528-46bd-ae7e-1e7daee84471', NULL, 0, 0, NULL, 0, 0, N'rob@fight.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'4f06b8c7-8af8-45ee-a7ba-16ea495805a0', N'paulinaseropian@gmail.com', 0, N'AAG8NmXqZvrIxd+ue4O42qUz4ly+M8f8++/wXQXk8aaeKdnOl26JkGJ+dkTfOjTX1A==', N'5feee9d8-e94e-4120-9695-80ffcfa1f559', NULL, 0, 0, NULL, 0, 0, N'paulinaseropian@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5002e6e2-9305-4bec-aeb8-8222217991be', N'asdf@gmail.com', 0, N'AAaxtNp1nboR4t3i4hW48I58FzoGzc+HchctIuL5KkckQwVDvp1YbvfpVFOZZfxoeA==', N'a0b74320-3ab0-4d4c-be2a-5a8d07c2832c', NULL, 0, 0, NULL, 0, 0, N'asdf@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'52cab500-332f-487f-866b-f4dd9343f60d', N'tareshzayed@gmail.com', 0, NULL, N'1f6a6bcb-0cc9-496b-adcd-f78a304dd913', NULL, 0, 0, NULL, 0, 0, N'tareshzayed@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'55c827e5-02b7-4db5-b659-19dc2e4e2bc2', N'info@happiness360.nl', 0, N'AHRm5Yhmd+woZa7GCf53IgKMDpSoJoEzu2NJtp9ZDA2kH2lEmO2+/HA3Y+F6z51MWA==', N'50b623e5-045b-4b17-a119-ead07fbb3562', NULL, 0, 0, NULL, 0, 0, N'info@happiness360.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'58523985-10d2-476a-b5b3-6428291b06cf', N'ron@ergonline.eu', 0, N'AJtSE4k0/E6xMWIl0TrFHcMgrYeh2CG/qamR0gULeZCSlm8Eg/TxgphqVFG8ePjuTA==', N'aeeabca9-0684-4263-83c4-842e35d6d2aa', NULL, 0, 0, NULL, 0, 0, N'ron@ergonline.eu')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5981d4c7-0507-454a-99ba-b8566ad81752', N'hello@gmail.com', 0, N'ADXPHOYMr6PEEPcDseHhBWgk5BD/awhI4K2yR0KZ2q9+090kb6qr6XvaaUz1d5J5uQ==', N'bab3305d-d245-4b5e-85df-6038b66969e2', NULL, 0, 0, NULL, 0, 0, N'hello@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5b1f3f75-a2d0-49c1-b1c0-18e3271e3262', N'd.mendoza.pt@gmail.com', 0, N'ABay504z9lEB0mgWd5WhHdsd9Z8jtqrXo2VgC+jvWp3Dj9zAMHA3sSeh6t1u+8KSvA==', N'1ea55f49-8cb8-47d1-b80c-0971ffdfe7c3', NULL, 0, 0, NULL, 0, 0, N'd.mendoza.pt@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5ec2f320-a883-476a-82cd-11697d41d102', N'ramizeyada@gmail.com', 0, N'AHeYR5u1QyPKi4TAexxW4Byscxw06GuqGARsUVoRjL8IfUcpfuzMxIFLW86BWScxFg==', N'4656dc5a-44b9-4ea1-9ea0-357e89abf8ca', NULL, 0, 0, NULL, 0, 0, N'ramizeyada@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5ef8004b-b70c-4363-98b7-3d0b17c7821e', N'ron.goene@gmail.com', 0, N'ALz9RM4oFcM8R2leonjpUF4ws8zGbcUYEHQXCdUH8GNF3Z/s/3vGfkMO/V199vKUvw==', N'aac6c8a7-101d-4892-8aab-e2738cc4f843', NULL, 0, 0, NULL, 0, 0, N'ron.goene@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5f37a3b8-9a23-4c2e-8194-3237f5a4ba81', N'mike@ddsw.nl', 0, N'ALuG8T4uiQCT9jhgNerQGp6hSaatsQCEo5R1LAR5p7O1zW5RrZ+B35v3h8kHT6V+Vw==', N'920d7d3d-34e5-4a7a-9ecb-7fba8672f9e1', NULL, 0, 0, NULL, 0, 0, N'mike@ddsw.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5f6ddbe0-2614-4a08-93fc-faeedd8fe170', N'mazadul@ymail.com', 0, N'ACfCR1pxXhLP8mNp5XSkQTCFPRBEwE6yJOgaJTQCpeDi1PtCqZd/b5Ogya2g3cvQlA==', N'8d51fc21-df73-4874-99d2-749a1aec933d', NULL, 0, 0, NULL, 0, 0, N'mazadul@ymail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'63c4df65-10b8-42ca-a84c-e367c3b9a155', N'Willemgouw@hotmail.nl', 0, N'AJd0YFliQ8LxiZO5kVAa1XWhudOiIfTKyFjSD4u0Q2j37qwwW4L7RsbZbk7mGSsQEA==', N'25973b07-40bc-443f-bdf7-13d74f22abc3', NULL, 0, 0, NULL, 0, 0, N'Willemgouw@hotmail.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'6733496e-23f2-4dfb-a4b3-1eaaa183fe8a', N'rob@fightathlete.com', 0, N'ADBZ8dHJQczDLuGRm6Wsep9X8wE7AGzPSEYPP3QeVbwh+miqzSY+fm3xVn1yWeTTBA==', N'f8651564-3529-4928-838f-47676ef4b88d', NULL, 0, 0, NULL, 0, 0, N'rob@fightathlete.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'70096849-09e0-4836-8817-0e302bc65f68', N'atish@bs-23.com', 0, N'AECH3MqcJxJ0WggM60rkv4+3Q4J7QjqpFI3A80w8djjQ7XqJyOg/uuH1VK9aFvKFDQ==', N'91c69622-8d98-4c38-acc4-8297d0ed7866', NULL, 0, 0, NULL, 0, 0, N'atish@bs-23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7026b9d1-4803-4407-ad3e-1ddefe647fa8', N'qweA@c.123', 0, N'AIB+Xap10cVVL65k2U/hacVnSFDxLEhp22DHJPD+DSb6E7OqzPCeNARL7doanRSdHw==', N'132de68f-0cbe-4df8-9604-fb7d2aade22c', NULL, 0, 0, NULL, 0, 0, N'qweA@c.123')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7280c100-278c-44e6-a833-a4967bd7e40b', N'atishiitdu@hotmail.com', 0, NULL, N'23d0415e-e78b-46bf-aeac-c04515366e5b', NULL, 0, 0, NULL, 0, 0, N'atishiitdu@hotmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'77e41577-ae42-45af-8eb3-1afbfb96a12c', N'willex@bu3.nl', 0, N'AJMLxFohGtK6h/VARpdIUF4hGyufV5dWgZFfQZti+fqmqnNT1lFfxDpj09Jc0OjzTQ==', N'4bee9c7b-ae8a-4fd6-9e97-851f4ac9089a', NULL, 0, 0, NULL, 0, 0, N'willex@bu3.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'78e37856-5ba8-4d2d-b28b-e9c4cab7bb86', N'md@g.com', 0, N'AABxcj1Tj1lBo6N0T6Tdp7FPyc+dj8AFn81yf4hFotKRMs2PXGae4vpe259mRitHWg==', N'43a89596-5110-4ecf-bc03-df3add4d2c57', NULL, 0, 0, NULL, 0, 0, N'md@g.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7abe98d2-e7f7-4ddb-83cf-93ad5f3197d0', N'asdfhh@gmail.com', 0, N'AP4+ECMF7IheSTNAuPoxBIKVVQKbhsS7DE+ksz+PjAkPMIBEO5bNJnLhbFoF3EElUA==', N'7b58808c-3c71-4c6d-9ea3-483e1b49f75d', NULL, 0, 0, NULL, 0, 0, N'asdfhh@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7c9c572b-1fe5-4d4a-81e2-5ba1fc941d14', N'atis@bs2-233.com', 0, N'AHCKJkzyeZLJ3TXkJzbAQ95NJ5JoXgFkswc5FUcXz5epzKtM5hvkRuVSHqX/EuV+VQ==', N'14f99ddf-804e-4a89-99ff-e260d7ff0bad', NULL, 0, 0, NULL, 0, 0, N'atis@bs2-233.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7e8f2b34-3638-4c0a-ba3c-5837fd4adc8b', N'nickkraaijenhof190@hotmail.com', 0, N'ANBq6uXmbF9WYmT93NkWesDc1gNDnqYo2DxS+Tvac4KsZF33LGkl147d+K17pJIglg==', N'18e61dad-368c-4aa8-b39d-ee1f88b13e45', NULL, 0, 0, NULL, 0, 0, N'nickkraaijenhof190@hotmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7ed2af00-bab5-4812-831a-ae37869462bb', N'willex@me.com', 0, N'APj631H5pqbQTihIq7MX+RzjBlDotnwlAfHAbyXC+GUiiDyb5EksbM+0xY1z46LoAg==', N'a719621d-c4da-4f8f-ae3b-759229693c11', NULL, 0, 0, NULL, 0, 0, N'willex@me.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'87c3e2f7-b61a-4900-a873-1e84ce32be65', N'Willemgouw@hotmail.com', 0, N'AIj3+91A5CCsS9F7QlQCGVdepXEzAtnY3qHVyDHZ8Y/qimoybL4unxTsWeXL9EgGhQ==', N'31a5216a-c272-46e7-980a-45c52c2b5b83', NULL, 0, 0, NULL, 0, 0, N'Willemgouw@hotmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8e0a63b6-dd78-44ce-ab00-f9cf77959ca0', N'atis@bs2-23.com', 0, N'ABkOk++hbr1FTUQdcWC3FiGd+e2NzUQb00vt26bH1kVg3F9TBxb7sQDiKxaVVcnLxg==', N'ffda1d25-4b65-4382-a2e2-4b6bb6b714d3', NULL, 0, 0, NULL, 0, 0, N'atis@bs2-23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8e9b4a6c-2298-4917-90e7-a05eb95d8498', N'cghh@bs23.com', 0, N'ANSLfhagufeCa6i+KRTPZB0APT3G0gzKvz+sW0uF5LWJZv0bfDIqd70swuaTnzMdoQ==', N'8bcb33e2-c90d-447c-8ccc-7c47d7dd36ea', NULL, 0, 0, NULL, 0, 0, N'cghh@bs23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'8fc7a496-1888-4e78-9a54-bdeaf12c5905', N'mazadul06@gmail.com', 0, N'ADsZUCRBw8Py3g1dBUaskLdZMlJiDiGpid1fId+6siwJMFFt4yGX5bq0hs/u0CghUw==', N'eec93551-9371-4c7b-8dd1-a9cac6ea6484', NULL, 0, 0, NULL, 0, 0, N'mazadul06@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'913532ba-f9c5-493f-ad5f-d909e22b496c', N'first@last.com', 0, N'AD2reDYo1mw0vY3bU/Pf/vgjUYiOvyjkJc1+2NqgK5PMkYLmZM5I5mSbmWR6VqugCg==', N'59702fe6-7aeb-4127-8cb9-d5a81b1c8f9b', NULL, 0, 0, NULL, 0, 0, N'first@last.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'97856393-8372-4c0b-845f-e5c342699645', N'luc.lips@live.nl', 0, N'AIhm4ZpTu/8TavPwRTulKfc1JScL6HV6IVPgziB4+9zgvbstjSBbIvelAX9QdGEsrw==', N'c2e4ac70-fff7-42bd-8cdb-14aaa6f79960', NULL, 0, 0, NULL, 0, 0, N'luc.lips@live.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'97eec0a4-7aac-4198-923d-6382b2f7bc3d', N'info@happido.com', 0, N'AAfzzvESJo6aLxtAFybuWPszomsNbO+wzleM8xhLDx277gKg5F4enXJVZrM7I6dGug==', N'10b55948-400d-450b-aa9b-eac40f2319d0', NULL, 0, 0, NULL, 0, 0, N'info@happido.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a19a886e-55f6-4311-be5d-bf538c36e5f5', N'mazadul@brainstation-23.com', 0, N'APghbx2mgNNY4/jhR/A5iTex9YUccXNw6gItMy9x93SQPwkayBhv6upqszDiEbuIqQ==', N'eb0428e7-4fde-4fae-aae1-ea6a90c31913', NULL, 0, 0, NULL, 0, 0, N'mazadul@brainstation-23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a5712c2f-0117-43d5-8a18-06f4d9e9369c', N'rob@fight2018.com', 0, N'AOKlINVo4tCuQ5aCnRnwzpumd3QJ/+ddQ0LFlth6mWbyEC9KoRVKg21YyZ7APrC9Jg==', N'c4cb7ca3-640d-4892-90ea-716ade9b87ff', NULL, 0, 0, NULL, 0, 0, N'rob@fight2018.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a69c016c-f250-4a58-9f01-2954650c8974', N'ruben_3@live.nl', 0, N'ACn4y8jghfTqqnOOGzj4thr46l/Iv4x5WwYxvLycSpXijmpoq/xLG2h6RPIOoLGrEA==', N'41476759-7986-4631-b24f-cdbdb15d740b', NULL, 0, 0, NULL, 0, 0, N'ruben_3@live.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a7bf2753-c7fa-4052-b780-ef2791ec5767', N'test@g.net', 0, N'APssvUoDAM+Fr8timYsPa5ZNgMEN1ojEGAmrBJxZNrGL1I8Xn4mUN00atLamhgo7Hw==', N'c810539d-6c40-4606-931b-15a04c773a12', NULL, 0, 0, NULL, 0, 0, N'test@g.net')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'adc36622-44f6-4364-8b2f-db5e51e52de3', N'admin@fightAthlete.com', 0, N'AMqOtA6O4xKEKFgWs9igHcTPW2d+vUBGjH0i5bq2LBouz9qz/MpKKt1kZUAkIBuSQQ==', N'12c66c80-dfad-4f11-ac9e-009c33f793de', NULL, 0, 0, NULL, 1, 0, N'admin@fightAthlete.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'add70886-ef93-4dc0-9b5f-a3de9dc04e13', N'info@fightathlete.com', 0, N'AFu7SUnF58nE/ucjm/mb83N1aGJFE3xxG76m1T+NMrYvVFnedBTlKmEZ6NqhV0ak8w==', N'6f965462-fd2e-46bb-820a-99328d7985c8', NULL, 0, 0, NULL, 0, 0, N'info@fightathlete.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ae09b636-cff6-4e9c-b4d9-644708806ce5', N'robbin@zen6es.com', 0, N'AFHR+ney6BPK9YFCvdTyTKZfiUy+9oL+y7bRL2XLlMgV3Cb/rAVtLg1oFzNc4atCzA==', N'704364e6-adf3-441e-8048-4b993024e55f', NULL, 0, 0, NULL, 0, 0, N'robbin@zen6es.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ae113784-3b49-4515-acc3-53962cfdc025', N'Colinwallet@msn.com', 0, N'AKfhatNCwzu1QFeOBjatgEY9tVBTuE4paDZhl4UlUfiZFEJPLyf2q2mDnRKYtMlSqA==', N'15e200cd-03ad-4f90-8d8f-864f54125795', NULL, 0, 0, NULL, 0, 0, N'Colinwallet@msn.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ae538a23-28c4-4856-bdd2-1e371f815d38', N'saskiaschouten@kpnmail.nl', 0, N'ACUEhkfarOPefe7ADhjjzEZV6lSsC7N1t+VQNiEDwauSOKaKhIyN0wr3GuG6AQbZ7w==', N'f09d1b21-9b8b-442a-bb72-efd17528bc2e', NULL, 0, 0, NULL, 0, 0, N'saskiaschouten@kpnmail.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'af4e4d13-bd96-49a8-8324-94a14163ef84', N'asif@gmail.com', 0, N'ADguYmA4Yp8MlIwAEO2VmHucz5xmmrdKsdvqnXJYSiAymJAKMu4YJeqkQ7V18wDwow==', N'675c1e03-12b6-49a7-9304-1e1b205a573e', NULL, 0, 0, NULL, 0, 0, N'asif@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b00527ef-9d1b-44ab-89b5-3bf1aaabe452', N'atish@hbs-23.com', 0, N'AIbTDI7ptPkqYEgTaWRKczwDf04uQVu2vy4PDTeKJHjM/wdjTT8jK85lser/8YyZlg==', N'0481b485-ef0b-456f-8810-7516a6e9c6b6', NULL, 0, 0, NULL, 0, 0, N'atish@hbs-23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b2807c99-5083-47fc-87ff-7bf558bb11fd', N'atish@bs23.com', 0, N'AMrHSe9bBrKHF/CcsaaKU96co6ZFqWE3lOL3nS3uIZtJTsgtBMREX7x7a2xpoESwMA==', N'2c9fcd65-e1e8-47b1-91dc-1cd8217bccea', NULL, 0, 0, NULL, 0, 0, N'atish@bs23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b2bc9ba5-58c9-44b6-9804-3fe86810f26f', N'robbin@fight.com', 0, N'ABE65shKjG/Hq0mXz8i8S+sZyxg/7eTgR/8TCbZvekq7eCneQkw0+NsnOVgQhbHIMA==', N'916b1525-fb76-475c-83e4-99936fef508b', NULL, 0, 0, NULL, 0, 0, N'robbin@fight.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b893f26b-16eb-498d-8c1f-08a1572202e8', N'jori.goene@gmail.com', 0, N'ANCR4r1DfDcSsW5TRf+/uS6bhGC3kFtVKjylAWi1JVLgZFKjVaaRm16YsjBS23YyZg==', N'68d95412-5a98-4544-bd52-897063725675', NULL, 0, 0, NULL, 0, 0, N'jori.goene@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b9454f85-6cdc-40a3-ba98-3b1c572f8a66', N'fight@gmail.com', 0, N'AO4XTO1tWBsIPDPutd6F30QKr39MPQ4kLVTLjpmy9q6w/CrRyKurFS1h28Wus8LYrw==', N'aa0dcb6f-0dee-43f0-b28a-21b8249349bc', NULL, 0, 0, NULL, 0, 0, N'fight@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b9d23cb1-cd6e-4408-be25-2088e3409799', N'ferry.van.maurik@nike.com', 0, N'AIO6f/4ViRCr1pXNgUuC7Pjc3677f30xVcH+KiwRvmuXEdQjX+HBkqd+DAV03BjCqQ==', N'95ce8fae-95c4-4d86-817b-aeafc64d14c0', NULL, 0, 0, NULL, 0, 0, N'ferry.van.maurik@nike.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'bff7678e-85a3-4046-8a62-760ccfe74657', N'infi@jaja.nl', 0, N'AJnKL0Ubv/gP9Gj7osWuXNsR+zmClBs7U9ln/W6cn2jCqcxeokR2ivVuuo4ZGLacLQ==', N'14ad847e-4a64-44a6-a465-e47dec2958ae', NULL, 0, 0, NULL, 0, 0, N'infi@jaja.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c159f45b-964f-4b7b-b0da-3c0346e9e24a', N'qwe@gmail.com', 0, N'AIPtiLRx2nMA2IHDvxuEjrBa4DwdIPELJM5zjx90qz91jzPFuKk4Q+H1qYeBMZm6pQ==', N'ebde42b0-77f4-4530-8dba-b015e6639d21', NULL, 0, 0, NULL, 0, 0, N'qwe@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c22376b8-6f45-453f-b3f7-fc4201d51595', N'info@health360.nl', 0, N'AJo7lSBilGLP4Zu0z00vQl6EAhQU0pJQOHV+596UatY5PLpxfaK9/N/6yY9THvuWNg==', N'2650c44d-ae45-4b12-b306-de4d0abdc77e', NULL, 0, 0, NULL, 0, 0, N'info@health360.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c26b4b30-d5b3-4817-b271-2e1c2b888dc8', N'ron.goene@j3management.nl', 0, N'AI7N2mPYqU4T+nsbGLfJoESPUYKBLYnVEeJ2agw9VtFwfNDtUWPek7jDI2o/Qrpu4Q==', N'9d4eec42-f42c-4d5d-a40c-1e972411beaf', NULL, 0, 0, NULL, 0, 0, N'ron.goene@j3management.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c74d4a0a-6158-4b1d-a0ca-4639189c7de1', N'poiu@gmail.com', 0, N'ALalViAHan11+kZ5yQ1N2rxKGa0z5m7aaO4oJhGGtAgBX8hBtOSLUnK3OVdFL6RI5A==', N'4921a0c0-c4d1-49b4-a37e-551ff3589925', NULL, 0, 0, NULL, 0, 0, N'poiu@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c78f3c95-1673-476e-a718-a62289c116bb', N'Info@zen6es.com', 0, N'ABD7DQxlsQEtPYtsBAx/kAgIZqxzazDr6wjdexGoXbMAcLthu3HthOaI5Hk/4v7R7A==', N'4f0706cc-0a88-40b3-acc3-7adcc124c594', NULL, 0, 0, NULL, 0, 0, N'Info@zen6es.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c819eb8e-696a-4808-baba-79e388ba4c87', N'Jessica@jessica.nl', 0, N'AI6+6Vq6mIkq2GTQWjWnF3GTQY+J2cTQauMmbHwTEJF9hu3e/AuVoiADEAlvBrz/uQ==', N'6ef6b856-2bdb-46c0-b951-07c465324dd7', NULL, 0, 0, NULL, 0, 0, N'Jessica@jessica.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd162b20f-f76f-4fa8-95c1-fa6701a42b73', N'Email@gmail.com', 0, N'AHq07JsOIqMG8S1Q15LCh2JoO3B/D39RL4QVm+zGGGQVJql1lc5jV01GhqfWD6LcZg==', N'20322a1e-0338-4382-b705-ecd00a45d548', NULL, 0, 0, NULL, 0, 0, N'Email@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd2981a32-d150-4797-9de2-376fa8a2fd12', N'fff@bs23.com', 0, N'AHcM7wFmL2ndfhdODgzRbh153xZweTZIzC+ISY3uUoa+7HahUulH+Ym2usm/kfydbg==', N'36835b20-dfba-43bb-a239-33d31387a36c', NULL, 0, 0, NULL, 0, 0, N'fff@bs23.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'd766dc42-821e-4537-b0fe-436d2f34d8ed', N'Susanzwartepoorte@gmail.com', 0, N'AGgqwOrCCYwuZN3Rx5Z7++2Ef92cjyZqGgYhgu6t/nUWaNPZzgjPQZHnI/fWscNdsw==', N'edfcaeab-4fd5-4978-a2bd-3b3e9acc9e39', NULL, 0, 0, NULL, 0, 0, N'Susanzwartepoorte@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'dc472c73-dbd1-482d-995d-170ff4b5d630', N'hossain@gmail.com', 0, N'AMj6f/ZcviIWdVH47FhbT9rZB9ik4esyiPMbqayf+RU5INOZ1hMuV4T+lDnLs3jT1g==', N'742fc353-b370-430f-b358-51890b5c458f', NULL, 0, 0, NULL, 0, 0, N'hossain@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e5b41f0d-7fcf-45b2-adbf-2137fc60f5e9', N'Info@fight.com', 0, N'AO70B12ST+NcGlplhVV9k5Mdc+vbv259GoRgVWcw3W/YUFRY9zP31E4+7ZTJTloagw==', N'1849ec3c-5cb0-4349-b996-33a3324ec55a', NULL, 0, 0, NULL, 0, 0, N'Info@fight.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'eaa957db-7fa6-4717-97d9-ec3702c32d9d', N'r456goene@gmail.com', 0, N'ADu80dqJAcSTbNEFpzwEGraHGBFlKXcecBx3qNf2MzZyEr2AVSCJhBCNHj0sp9wrpA==', N'90afd4b5-b677-4675-be3b-0a3597b2d32b', NULL, 0, 0, NULL, 0, 0, N'r456goene@gmail.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ee6e24ca-4a56-46ba-95b5-71ad4f2b9dc1', N'Rayen@reat.nl', 0, N'APRkZb5JMjGTS+ZCPifGmoCRYQYPmElI3z+7ip429HVc/x+UWqIA8BaRya4uzCPuyQ==', N'6fc428d7-cb3f-4163-b0d8-f510e0a745d4', NULL, 0, 0, NULL, 0, 0, N'Rayen@reat.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'f1ace9d9-6812-4774-a430-946afeb9571d', N'robbin@happido.com', 0, N'AOm3L+01kcBzVPDofDDyLo4r1UhZB+fpD97SaixDy83As59MuQkaJAUHQFmotNNHjg==', N'dfcc9ce1-43cb-4d9d-a540-1a922cef8dec', NULL, 0, 0, NULL, 0, 0, N'robbin@happido.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'f41e6da2-bc04-4671-9721-8fabfcabe05e', N'Amr@gmail', 0, N'AErj1LSaMTByyHOQRVglc6ZetHaAiz+TfQAVKVRVWlq63Bcjt9ajHs5ZPcmWS/lf9g==', N'7ef1e3ee-76d1-4f45-8a99-c41a523dc373', NULL, 0, 0, NULL, 0, 0, N'Amr@gmail')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'f62a6c95-dc78-49f6-a42d-ae0336b89707', N'murad@test.com', 0, N'ANk6hZqlXW2ziZpeofKdnrJL2PYpxoRSWQ8zZ1bQnxsIm95dq8X9tG9rBKlNrdSomA==', N'135ac86f-d04d-4fc1-9709-395cec7f84dc', NULL, 0, 0, NULL, 1, 0, N'murad@test.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fa18d62e-4c77-41ed-9458-0a9512799be4', N'Olledegraaf@live.nl', 0, N'ANNs7/6Epi8+0bBGbT1SpAINcURcyckl9Jb3IKXhiIy50Si4p8FNEungQ9D3+RP9uw==', N'e20122d3-6815-4d14-a919-dacbb1b154f5', NULL, 0, 0, NULL, 0, 0, N'Olledegraaf@live.nl')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fc95d237-36d2-4154-8250-df3bb3699459', N'Roel@mejirogym.nl', 0, N'AHg4+HyZ4Xohtb+mHB1PCqIxxWp8RTzICLiRCw9oxmIRCBUpPQsBQEi6QXAv1OeHww==', N'c1e923f8-8691-4cb7-975a-830b14ae6a75', NULL, 0, 0, NULL, 0, 0, N'Roel@mejirogym.nl')
SET IDENTITY_INSERT [dbo].[Currency] ON 

INSERT [dbo].[Currency] ([Id], [Name]) VALUES (1, N'Dollar')
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (2, N'Euro')
INSERT [dbo].[Currency] ([Id], [Name]) VALUES (3, N'Bat')
SET IDENTITY_INSERT [dbo].[Currency] OFF
SET IDENTITY_INSERT [dbo].[Duration] ON 

INSERT [dbo].[Duration] ([Id], [Name]) VALUES (1, N'1 week')
INSERT [dbo].[Duration] ([Id], [Name]) VALUES (2, N'2 weeks')
INSERT [dbo].[Duration] ([Id], [Name]) VALUES (3, N'3')
INSERT [dbo].[Duration] ([Id], [Name]) VALUES (4, N'4')
SET IDENTITY_INSERT [dbo].[Duration] OFF
SET IDENTITY_INSERT [dbo].[Exercise] ON 

INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (1, N'SQUATS', N'null', 0, N'0', N'/Upload/Squat_NotificationSender.cs', 1, 0, 0, 1, CAST(N'2017-07-10 07:19:06.197' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (2, N'Push up 1', N'null', 0, N'0', N'/Upload/254-Clap push up_1.mp4', 1, 1, 1, 1, CAST(N'2017-06-08 09:18:03.710' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (3, N'Lunge Jump 1', N'null', 1, N'30', N'Unavailable', 1, 1, 1, 1, CAST(N'2017-06-08 09:20:24.097' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (4, N'gfhgfh', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-05-01 16:12:16.683' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (5, N'testttt Ferry', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-05-01 16:12:42.687' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (6, N'Ferry testtyttt', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-05-01 16:17:19.953' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (7, N'No Tools Exercise', NULL, 0, N'0', N'Unavailable', 1, 1, 1, 1, CAST(N'2017-05-02 13:23:17.777' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (8, N'Dumbell Exercise', NULL, 1, N'0', N'/Upload/254-Clap push up_1.mp4', 1, 1, 1, 1, CAST(N'2017-06-13 12:07:25.617' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (9, N'Rest 30 seconds', NULL, 1, N'30', N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:31:09.967' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (10, N'tstt up', N'null', 0, N'0', N'/Upload/tstt up_FightAthlete.Gcm.Notification.csproj', 1, 0, 0, 0, CAST(N'2017-06-08 09:05:36.977' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (11, N'Rest 60 seconds', NULL, 1, N'60', N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:51:02.570' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (12, N'Burpee', NULL, 0, N'0', N'/Upload/Burpee_1.mp4', 1, 1, 1, 1, CAST(N'2017-06-08 09:26:49.257' AS DateTime), N'/Upload/testPic.PNG')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (13, N'CLAP PUSH UPS', N'null', 0, N'0', N'/Upload/Clap push up_FightAthlete.Gcm.Notification.csproj', 1, 0, 1, 1, CAST(N'2017-07-10 07:18:08.470' AS DateTime), N'/Upload/Clap push up_NotificationSender.cs')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (14, N'Rest Exercise', N'null', 0, N'60', N'Unavailable', 1, 0, 0, 0, CAST(N'2017-06-08 09:16:45.623' AS DateTime), N'null')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (15, N'tiytk54', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-06-08 06:26:15.440' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (16, N'test1223', NULL, 0, N'0', N'/Upload/test1223_App.config', 1, 0, 1, 1, CAST(N'2017-06-08 07:41:15.377' AS DateTime), N'/Upload/test1223_App.config')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (17, N'dhdfhdf up', N'null', 0, N'0', N'/Upload/dhdfhdf up_FightAthlete.Gcm.Notification.csproj', 1, 0, 0, 0, CAST(N'2017-06-08 09:04:41.537' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (18, N'Rest Exercise 1', NULL, 1, N'10', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-06-09 10:51:57.810' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (19, N'Rest Exercise 2', NULL, 1, N'20', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-06-09 10:52:26.237' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (20, N'BURPEES', NULL, 0, N'30', N'/Upload/251 Burpee_1.mp4', 1, 0, 1, 1, CAST(N'2017-07-10 07:17:57.547' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (21, N'PUSH UPS', NULL, 0, N'0', N'/Upload/260-Push up_1.mp4', 1, 0, 0, 1, CAST(N'2017-07-10 07:19:29.227' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (22, N'BICYCLE CRUNCHES', NULL, 0, N'0', N'/Upload/BICYCLE CRUNCHES_195-Lunge ''n stretch_1.mp4', 1, 1, 1, 1, CAST(N'2017-07-17 13:00:13.753' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (23, N'SIT UPS', NULL, 0, N'0', N'/Upload/271-Sit up.mp4', 1, 0, 1, 1, CAST(N'2017-07-10 07:18:43.630' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (24, N'PULL UPS', NULL, 0, N'0', N'/Upload/272-Pull up.mp4', 1, 0, 1, 1, CAST(N'2017-07-10 07:18:22.630' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (25, N'Rest 40 seconds', NULL, 1, N'40', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-06-13 13:24:47.623' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (26, N'test exercise', NULL, 0, N'0', N'Unavailable', 1, 1, 1, 1, CAST(N'2017-06-14 15:32:43.490' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (27, N'test exercise 2', NULL, 0, N'0', N'Unavailable', 1, 1, 1, 1, CAST(N'2017-06-14 15:33:08.817' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (28, N'Rest 10 seconds', NULL, 1, N'10', N'Unavailable', 0, 0, 0, 0, CAST(N'2018-01-17 12:05:09.877' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (29, N'RUN 100 METERS', NULL, 0, N'0', N'/Upload/RUN 100 METERS_Running.mp4', 0, 0, 1, 1, CAST(N'2018-01-17 10:53:02.027' AS DateTime), NULL)
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (30, N'Rest 500 seconds', NULL, 1, N'30', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-06-19 14:14:54.190' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (31, N'Rest 100 seconds', NULL, 1, N'30', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-06-21 10:43:11.953' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (32, N'TURKISH GET UP', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-10 07:10:11.397' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (33, N'RUN 5K', NULL, 0, N'0', N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 13:07:30.090' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (34, N'RUN 10K', NULL, 0, N'0', N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 13:08:21.863' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (35, N'RUN 15K', NULL, 0, N'0', N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 13:08:08.857' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (36, N'ROW 1K', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 13:05:51.057' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (37, N'ROW 2K', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 13:06:11.547' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (38, N'ZZZ', NULL, 0, N'0', N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-10 14:28:47.773' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (39, N'vedio testing exercise', NULL, 0, N'0', N'Unavailable', 1, 1, 1, 1, CAST(N'2017-07-11 07:37:01.907' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (40, N'test vedio', NULL, 1, N'20', N'/Upload/test vedio_1 Minute Sciatica Exercises.mp4', 1, 1, 1, 1, CAST(N'2017-07-11 08:23:42.430' AS DateTime), N'/Upload/test vedio_1 Minute Sciatica Exercises.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (41, N'testing', NULL, 0, N'0', N'/Upload/testing_1 Minute Sciatica Exercises.mp4', 1, 0, 1, 1, CAST(N'2017-07-11 08:02:33.637' AS DateTime), N'/Upload/testing_1 Minute Sciatica Exercises.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (42, N'Video test', NULL, 0, N'0', N'/Upload/Video test_Burpee_1.mp4', 1, 0, 1, 1, CAST(N'2017-07-11 09:13:15.087' AS DateTime), N'/Upload/Video test_Burpee_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (43, N'sgsdg', NULL, 0, N'0', N'/Upload/sgsdg_1 Minute Sciatica Exercises.mp4', 1, 0, 1, 1, CAST(N'2017-07-11 09:29:03.340' AS DateTime), N'/Upload/sgsdg_1 Minute Sciatica Exercises.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (44, N'hello', NULL, 0, N'0', N'/Upload/hello_Burpee_1.mp4', 1, 0, 1, 1, CAST(N'2017-07-11 10:58:28.073' AS DateTime), N'/Upload/hello_Burpee_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (45, N'LONG JUMP', NULL, 0, N'0', N'/Upload/Long jump_249 Long jump_1.mp4', 1, 1, 1, 1, CAST(N'2017-07-11 15:45:16.043' AS DateTime), N'/Upload/Long jump_249 Long jump_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (46, N'14 mb video', NULL, 0, N'0', N'/Upload/14 mb video_Moses Sumney -Doomed- (Official Music Video).mp4', 1, 1, 1, 1, CAST(N'2017-07-13 09:57:06.610' AS DateTime), N'/Upload/14 mb video_Moses Sumney -Doomed- (Official Music Video).mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (47, N'30 MB Video', NULL, 0, N'0', N'File size exceed 15360 kb', 1, 1, 0, 1, CAST(N'2017-07-13 10:14:21.240' AS DateTime), N'File size exceed 15360 kb')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (48, N'14 mb video', NULL, 0, N'0', N'/Upload/14 mb video_Moses Sumney -Doomed- (Official Music Video).mp4', 1, 1, 1, 1, CAST(N'2017-07-13 11:15:54.823' AS DateTime), N'File size exceed 15360 kb')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (49, N'TWO ARM SWING', NULL, 0, N'0', N'/Upload/Two arm swing_34 Kettlebell Two arm swing_1.mp4', 1, 0, 1, 1, CAST(N'2017-07-13 11:19:53.447' AS DateTime), N'/Upload/Two arm swing_34 Kettlebell Two arm swing_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (50, N'SQUATS', NULL, 0, N'0', N'/Upload/SQUAT_7 air squat_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:04:34.183' AS DateTime), N'/Upload/SQUAT_7 air squat_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (51, N'MEDBALL TWISTS', NULL, 0, N'0', N'/Upload/MEDBALL TWIST_11 medball twist_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:03:47.480' AS DateTime), N'/Upload/MEDBALL TWIST_11 medball twist_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (52, N'MEDBALL CHOPS', NULL, 0, N'0', N'/Upload/MEDBALL CHOPS_13 Medball chops_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:03:13.783' AS DateTime), N'/Upload/MEDBALL CHOPS_13 Medball chops_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (53, N'MEDBALL KNEELING TWISTS', NULL, 0, N'0', N'/Upload/MEDBALL KNEELING TWIST_14 medball kneeling twist_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:03:22.000' AS DateTime), N'/Upload/MEDBALL KNEELING TWIST_14 medball kneeling twist_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (54, N'MEDBALL RUSSIAN TWISTS', NULL, 0, N'0', N'/Upload/MEDBALL RUSSIAN TWIST_16 medball russian twist_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:03:30.217' AS DateTime), N'/Upload/MEDBALL RUSSIAN TWIST_16 medball russian twist_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (55, N'MEDBALL SIT UPS', NULL, 0, N'0', N'/Upload/MEDBALL SIT UP_17 Medball explosive sit up_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:03:38.423' AS DateTime), N'/Upload/MEDBALL SIT UP_17 Medball explosive sit up_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (56, N'BARBELL HIGH PULL ROLL OUTS', NULL, 0, N'0', N'/Upload/BARBELL HIGH PULL ROLL OUT_31 barbell high pull roll out_2.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 13:37:57.217' AS DateTime), N'/Upload/BARBELL HIGH PULL ROLL OUT_31 barbell high pull roll out_2.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (57, N'ONE ARM BARBELL SNATCHES', NULL, 0, N'0', N'/Upload/ONE ARM BARBELL SNATCHES_33 one arm barbell snatch_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:04:02.543' AS DateTime), N'/Upload/ONE ARM BARBELL SNATCHES_33 one arm barbell snatch_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (58, N'KETTLEBELL TWO ARM SWINGS', NULL, 0, N'0', N'/Upload/KETTLEBELL TWO ARM SWINGS_34 Kettlebell Two arm swing_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:02:55.680' AS DateTime), N'/Upload/KETTLEBELL TWO ARM SWINGS_34 Kettlebell Two arm swing_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (59, N'ONE ARM KETTLEBELL SWINGS', NULL, 0, N'0', N'/Upload/ONE ARM KETTLEBELL SWINGS_35-One arm kettlebell swing_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 13:51:38.457' AS DateTime), N'/Upload/ONE ARM KETTLEBELL SWINGS_35-One arm kettlebell swing_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (60, N'KETTLEBELL CLEANS', NULL, 0, N'0', N'/Upload/KETTLEBELL CLEAN_38-Kettlebell clean_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:02:33.103' AS DateTime), N'/Upload/KETTLEBELL CLEAN_38-Kettlebell clean_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (61, N'KETTLEBELL CLEAN & PRESSES', NULL, 0, N'0', N'/Upload/KETTLEBELL CLEAN & PRESSES_39-Kettlebell clean push press_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:02:13.987' AS DateTime), N'/Upload/KETTLEBELL CLEAN & PRESSES_39-Kettlebell clean push press_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (62, N'KETTLEBELL SQUAT PRESSES', NULL, 0, N'0', N'/Upload/KETTLEBELL SQUAT PRESSES_40-Kettlebell squat press_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:02:42.847' AS DateTime), N'/Upload/KETTLEBELL SQUAT PRESSES_40-Kettlebell squat press_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (63, N'BARBELL CLEAN & PRESSES', NULL, 0, N'0', N'/Upload/BARBELL CLEAN & PRESSES_41 barbell clean and press_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:05:52.757' AS DateTime), N'/Upload/BARBELL CLEAN & PRESSES_41 barbell clean and press_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (64, N'DUMBBELL SHADOW BOXING', NULL, 0, N'0', N'/Upload/DUMBBELL SHADOW BOXING_43 dumbell shadow boxing_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:07:12.507' AS DateTime), N'/Upload/DUMBBELL SHADOW BOXING_43 dumbell shadow boxing_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (65, N'KETTLEBELL SQUATS', NULL, 0, N'0', N'/Upload/KETTLEBELL SQUATS_54 kettlebell squat_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:10:02.113' AS DateTime), N'/Upload/KETTLEBELL SQUATS_54 kettlebell squat_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (66, N'KETTLEBELL LUNGES', NULL, 0, N'0', N'/Upload/KETTLEBELL LUNGES_55 kettlebell lunge_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:11:44.247' AS DateTime), N'/Upload/KETTLEBELL LUNGES_55 kettlebell lunge_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (67, N'KETTLEBELL SNATCHES', NULL, 0, N'0', N'/Upload/KETTLEBELL SNATCHES_56 kettlebell snatch_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:13:49.227' AS DateTime), N'/Upload/KETTLEBELL SNATCHES_56 kettlebell snatch_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (68, N'BARBELL STIFF LEG DEADLIFTS', NULL, 0, N'0', N'/Upload/BARBELL STIFF LEG DEADLIFTS_57 Stiff leg deadlift_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:17:29.843' AS DateTime), N'/Upload/BARBELL STIFF LEG DEADLIFTS_57 Stiff leg deadlift_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (69, N'BARBELL DROP LUNGES', NULL, 0, N'0', N'/Upload/BARBELL DROP LUNGE_59 barbell drop lunge_3_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:19:30.290' AS DateTime), N'/Upload/BARBELL DROP LUNGE_59 barbell drop lunge_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (70, N'BARBELL DROP SQUATS', NULL, 0, N'0', N'/Upload/BARBELL DROP SQUATS_60 barbell drop squat_3.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:22:26.553' AS DateTime), N'/Upload/BARBELL DROP SQUATS_60 barbell drop squat_3.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (71, N'BARBELL KNEELING PRESSES', NULL, 0, N'0', N'/Upload/BARBELL KNEELING PRESSES_62 Barbell kneeling press_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:24:14.433' AS DateTime), N'/Upload/BARBELL KNEELING PRESSES_62 Barbell kneeling press_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (72, N'BARBELL JUMP SQUATS', NULL, 0, N'0', N'/Upload/BARBELL JUMP SQUATS_63 Barbell jump squat_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:25:33.193' AS DateTime), N'/Upload/BARBELL JUMP SQUATS_63 Barbell jump squat_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (73, N'BARBELL SIDE LUNGES', NULL, 0, N'0', N'/Upload/BARBELL SIDE LUNGES_64 Barbell side lunges_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:26:41.387' AS DateTime), N'/Upload/BARBELL SIDE LUNGES_64 Barbell side lunges_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (74, N'SWISSBALL CRUNCHES', NULL, 0, N'0', N'/Upload/SWISSBALL CRUNCHES_65 Swissball crunch_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:27:48.713' AS DateTime), N'/Upload/SWISSBALL CRUNCHES_65 Swissball crunch_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (75, N'SWISSBALL JACKKNIVES', NULL, 0, N'0', N'/Upload/SWISSBALL JACKKNIVES_66 Swissball jacknife_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:29:14.723' AS DateTime), N'/Upload/SWISSBALL JACKKNIVES_66 Swissball jacknife_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (76, N'SWISSBALL ONE LEG JACKKNIVES', NULL, 0, N'0', N'/Upload/SWISSBALL ONE LEG JACKKNIVES_68 Swissball one leg jacknife_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:30:35.943' AS DateTime), N'/Upload/SWISSBALL ONE LEG JACKKNIVES_68 Swissball one leg jacknife_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (77, N'SWISSBALL DUMBBELL ROTATIONS', NULL, 0, N'0', N'/Upload/SWISSBALL DUMBBELL ROTATIONS_71 Swissball dumbell rotation_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:32:23.790' AS DateTime), N'/Upload/SWISSBALL DUMBBELL ROTATIONS_71 Swissball dumbell rotation_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (78, N'SWISSBALL ROLLOUTS', NULL, 0, N'0', N'/Upload/SWISSBALL ROLLOUTS_72 Swissball roll out_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:34:01.397' AS DateTime), N'/Upload/SWISSBALL ROLLOUTS_72 Swissball roll out_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (79, N'SWISSBALL AROUND THE WORLD', NULL, 0, N'0', N'/Upload/SWISSBALL AROUND THE WORLD_73 Swissball around the world_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:35:57.007' AS DateTime), N'/Upload/SWISSBALL AROUND THE WORLD_73 Swissball around the world_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (80, N'SWISSBALL MOUNTAIN CLIMBERS', NULL, 0, N'0', N'/Upload/SWISSBALL MOUNTAIN CLIMBERS_74 Swissball mountain climber_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:37:23.543' AS DateTime), N'/Upload/SWISSBALL MOUNTAIN CLIMBERS_74 Swissball mountain climber_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (81, N'SWISSBALL LEG RAISES', NULL, 0, N'0', N'/Upload/SWISSBALL LEG RAISES_75 Swissball double leg raise_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:38:59.583' AS DateTime), N'/Upload/SWISSBALL LEG RAISES_75 Swissball double leg raise_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (82, N'BOSU ALT. SIDE JUMPS', NULL, 0, N'0', N'/Upload/BOSU ALT. SIDE JUMPS_77 Bosu alternating side jump_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:40:33.563' AS DateTime), N'/Upload/BOSU ALT. SIDE JUMPS_77 Bosu alternating side jump_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (83, N'BOSU PULL OVERS', NULL, 0, N'0', N'/Upload/BOSU PULL OVERS_83 Bosu pull overs_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:42:59.750' AS DateTime), N'/Upload/BOSU PULL OVERS_83 Bosu pull overs_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (84, N'REVERSE CRUNCHES', NULL, 0, N'0', N'/Upload/REVERSE CRUNCHES_86 Reverse crunch_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:43:54.327' AS DateTime), N'/Upload/REVERSE CRUNCHES_86 Reverse crunch_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (85, N'EXPLOSIVE RUSSIAN TWISTS', NULL, 0, N'0', N'/Upload/EXPLOSIVE RUSSIAN TWISTS_88 Explosive russian twist_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:44:43.120' AS DateTime), N'/Upload/EXPLOSIVE RUSSIAN TWISTS_88 Explosive russian twist_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (86, N'ONE SIDE JACK KNIVES', NULL, 0, N'0', N'/Upload/ONE SIDE JACK KNIVES_89 One side jack knife_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:45:55.330' AS DateTime), N'/Upload/ONE SIDE JACK KNIVES_89 One side jack knife_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (87, N'JACK KNIVES', NULL, 0, N'0', N'/Upload/JACK KNIVES_90 Jack knife_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:46:56.127' AS DateTime), N'/Upload/JACK KNIVES_90 Jack knife_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (88, N'BOSU PLANK HOPS', NULL, 0, N'0', N'/Upload/BOSU PLANK HOPS_98 Bosu plank hops_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:50:25.310' AS DateTime), N'/Upload/BOSU PLANK HOPS_98 Bosu plank hops_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (89, N'BOSU FROG JUMPS', NULL, 0, N'0', N'/Upload/BOSU FROG JUMPS_99-Bosu frog jumps_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 14:51:09.277' AS DateTime), N'/Upload/BOSU FROG JUMPS_99-Bosu frog jumps_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (90, N'BARBELL FRONT SQUATS', NULL, 0, N'0', N'/Upload/BARBELL FRONT SQUATS_113 Barbell front squat_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:11:23.977' AS DateTime), N'/Upload/BARBELL FRONT SQUATS_113 Barbell front squat_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (91, N'BARBELL SUMO SQUATS', NULL, 0, N'0', N'/Upload/BARBELL SUMO SQUATS_116 Barbell sumo Squat_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:14:45.930' AS DateTime), N'/Upload/BARBELL SUMO SQUATS_116 Barbell sumo Squat_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (92, N'EXPLOSIVE KNEE RAISES', NULL, 0, N'0', N'/Upload/EXPLOSIVE KNEE RAISES_119 Explosive knee raises_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:16:20.470' AS DateTime), N'/Upload/EXPLOSIVE KNEE RAISES_119 Explosive knee raises_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (93, N'EXPLOSIVE HIGH JUMPS', NULL, 0, N'0', N'/Upload/EXPLOSIVE HIGH JUMPS_120 Explosive high Jumps_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:17:23.503' AS DateTime), N'/Upload/EXPLOSIVE HIGH JUMPS_120 Explosive high Jumps_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (94, N'BODY DROPS', NULL, 0, N'0', N'/Upload/BODY DROPS_121 Body drops_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:18:40.640' AS DateTime), N'/Upload/BODY DROPS_121 Body drops_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (95, N'MEDBALL ONE ARM ROLLS', NULL, 0, N'0', N'/Upload/MEDBALL ONE ARM ROLLS_128 Medball one arm roll_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:20:03.017' AS DateTime), N'/Upload/MEDBALL ONE ARM ROLLS_128 Medball one arm roll_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (96, N'MEDBALL PUSH UPS', NULL, 0, N'0', N'/Upload/MEDBALL PUSH UP_129-Medball explosive push up_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 18:08:30.727' AS DateTime), N'/Upload/MEDBALL PUSH UP_129-Medball explosive push up_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (97, N'BOSU BOUNCE PUSH UPS', NULL, 0, N'0', N'/Upload/BOSU BOUNCE PUSH UPS_137 Bosu bounce push ups -_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:22:53.157' AS DateTime), N'/Upload/BOSU BOUNCE PUSH UPS_137 Bosu bounce push ups -_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (98, N'DUMBBELL PUSH UP ROWS', NULL, 0, N'0', N'/Upload/DUMBBELL PUSH UP ROW_145-Dumbell push up row_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 18:07:50.800' AS DateTime), N'/Upload/DUMBBELL PUSH UP ROW_145-Dumbell push up row_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (99, N'BARBELL OVERHEAD SQUATS', NULL, 0, N'0', N'/Upload/BARBELL OVERHEAD SQUATS_173-Barbell overhead squat_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:28:52.213' AS DateTime), N'/Upload/BARBELL OVERHEAD SQUATS_173-Barbell overhead squat_1.mp4')
GO
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (100, N'BARBELL OVERHEAD SPLIT SQUATS', NULL, 0, N'0', N'/Upload/BARBELL OVERHEAD SPLIT SQUATS_175-Barbell overhead split squat_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:30:28.717' AS DateTime), N'/Upload/BARBELL OVERHEAD SPLIT SQUATS_175-Barbell overhead split squat_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (101, N'BARBELL OVERHEAD LUNGES', NULL, 0, N'0', N'/Upload/BARBELL OVERHEAD LUNGES_176-Barbell overhead lunges_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:35:00.097' AS DateTime), N'/Upload/BARBELL OVERHEAD LUNGES_176-Barbell overhead lunges_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (102, N'KETTLEBELL SNATCHES', NULL, 0, N'0', N'/Upload/KETTLEBELL SNATCH_198-Kettlebell snatch-_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 18:08:15.617' AS DateTime), N'/Upload/KETTLEBELL SNATCH_198-Kettlebell snatch-_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (103, N'ROPE JUMPS', NULL, 0, N'0', N'/Upload/ROPE JUMPS_248-Rope jump_3_1.mp4', 0, 0, 1, 1, CAST(N'2017-11-13 18:50:29.540' AS DateTime), N'/Upload/ROPE JUMPS_248-Rope jump_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (104, N'LONG JUMPS', NULL, 0, N'0', N'/Upload/LONG JUMPS_249 Long jump_3_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:39:53.670' AS DateTime), N'/Upload/LONG JUMPS_249 Long jump_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (105, N'BURPEES', NULL, 0, N'0', N'/Upload/BURPEES_251 Burpee_3_1.mp4', 0, 1, 1, 1, CAST(N'2017-07-17 18:29:12.643' AS DateTime), N'/Upload/BURPEES_251 Burpee_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (106, N'CLAP PUSH UPS', NULL, 0, N'0', N'/Upload/CLAP PUSH UP_254-Clap push up_3_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 18:07:33.857' AS DateTime), N'/Upload/CLAP PUSH UP_254-Clap push up_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (107, N'PUSH UPS', NULL, 0, N'0', N'/Upload/PUSH UPS_260-Push up_3_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:44:05.657' AS DateTime), N'/Upload/PUSH UPS_260-Push up_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (108, N'BICYCLE CRUNCHES', NULL, 0, N'0', N'/Upload/BICYCLE CRUNCHES_267-Explosive bicycle crunch_1_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:44:48.437' AS DateTime), N'/Upload/BICYCLE CRUNCHES_267-Explosive bicycle crunch_1_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (109, N'SIT UPS', NULL, 0, N'0', N'/Upload/SIT UPS_271-Sit up_2_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:45:49.477' AS DateTime), N'/Upload/SIT UPS_271-Sit up_2_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (110, N'PULL UPS', NULL, 0, N'0', N'/Upload/PULL UPS_272-Pull up_2_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:47:14.237' AS DateTime), N'/Upload/PULL UPS_272-Pull up_2_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (111, N'CHIN UPS', NULL, 0, N'0', N'/Upload/CHIN UPS_273-Chin up_2_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 15:48:25.240' AS DateTime), N'/Upload/CHIN UPS_273-Chin up_2_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (112, N'SHADOW BOXING', NULL, 0, N'0', N'/Upload/SHADOW BOXING_242 Shadow boxing_3_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 18:03:48.780' AS DateTime), N'/Upload/SHADOW BOXING_242 Shadow boxing_3_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (113, N'BARBELL ROLLOUTS', NULL, 0, N'0', N'/Upload/BARBELL ROLLOUT_149-Barbell roll out_1.mp4', 0, 0, 1, 1, CAST(N'2017-07-17 18:07:05.857' AS DateTime), N'/Upload/BARBELL ROLLOUT_149-Barbell roll out_1.mp4')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (114, N'RUN 250M', NULL, 0, N'0', N'/Upload/RUN 250M_Running.mp4', 0, 1, 1, 1, CAST(N'2018-01-17 10:53:47.367' AS DateTime), N'Unavailable')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (115, N'ROW 500M', NULL, 0, N'0', N'/Upload/ROW 500M_EX1.mp4', 1, 1, 1, 1, CAST(N'2017-11-28 19:09:44.247' AS DateTime), N'File size exceed 10240kb')
INSERT [dbo].[Exercise] ([Id], [Name], [Description], [IsRestExercise], [Duration], [Video], [IsDeleted], [IsBasicVersion], [IsNew], [IsActive], [CreatedDate], [Thumbnail]) VALUES (116, N'RUN 400M', NULL, 0, N'0', N'/Upload/RUN 400M_Running.mp4', 0, 1, 1, 1, CAST(N'2018-01-17 10:54:07.997' AS DateTime), N'Unavailable')
SET IDENTITY_INSERT [dbo].[Exercise] OFF
SET IDENTITY_INSERT [dbo].[Exercise_Level] ON 

INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (11, 5, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (17, 7, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (18, 7, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (39, 16, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (51, 12, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (52, 12, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (53, 12, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (66, 2, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (67, 3, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (68, 18, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (69, 18, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (70, 19, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (71, 19, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (72, 8, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (73, 8, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (110, 26, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (111, 26, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (112, 27, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (113, 27, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (146, 32, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (147, 32, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (148, 32, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (152, 20, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (153, 20, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (154, 20, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (155, 13, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (156, 24, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (157, 24, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (158, 24, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (159, 23, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (160, 23, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (161, 23, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (162, 1, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (163, 1, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (164, 1, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (165, 21, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (166, 21, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (167, 21, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (193, 39, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (194, 39, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (200, 41, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (201, 41, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (202, 40, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (203, 42, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (204, 43, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (205, 43, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (206, 44, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (207, 44, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (211, 45, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (212, 45, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (213, 45, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (217, 46, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (218, 46, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (219, 47, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (236, 48, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (237, 48, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (241, 49, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (242, 49, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (243, 49, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (247, 22, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (248, 22, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (249, 22, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (253, 36, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (254, 36, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (255, 36, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (256, 37, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (257, 37, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (258, 37, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (268, 33, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (269, 33, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (270, 33, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (273, 35, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (274, 34, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (275, 34, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (296, 56, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (297, 56, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (317, 59, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (318, 59, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (331, 61, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (332, 61, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (333, 60, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (334, 60, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (335, 62, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (336, 62, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (337, 58, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (338, 58, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (339, 52, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (340, 52, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (341, 53, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (342, 53, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (343, 54, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (344, 54, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (345, 55, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (346, 55, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (347, 51, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (348, 51, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (349, 57, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (350, 50, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (351, 50, 2)
GO
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (352, 63, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (353, 63, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (354, 64, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (355, 64, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (356, 65, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (357, 65, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (358, 66, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (359, 66, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (360, 67, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (361, 67, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (362, 68, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (363, 68, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (365, 69, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (366, 70, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (367, 71, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (368, 71, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (369, 72, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (370, 73, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (371, 73, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (372, 74, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (373, 75, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (374, 76, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (375, 77, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (376, 78, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (377, 79, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (378, 79, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (379, 80, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (380, 81, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (381, 82, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (382, 83, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (383, 84, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (384, 84, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (385, 85, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (386, 85, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (387, 86, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (388, 87, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (389, 87, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (390, 88, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (391, 89, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (392, 90, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (393, 90, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (394, 91, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (395, 91, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (396, 92, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (397, 93, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (398, 93, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (399, 94, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (400, 94, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (401, 95, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (404, 97, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (408, 99, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (409, 100, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (410, 101, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (414, 104, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (415, 104, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (420, 107, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (421, 107, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (422, 108, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (423, 108, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (424, 109, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (425, 110, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (426, 110, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (427, 111, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (428, 111, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (429, 112, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (430, 112, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (431, 112, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (433, 113, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (434, 106, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (435, 98, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (436, 102, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (437, 102, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (438, 96, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (439, 96, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (440, 105, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (441, 105, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (442, 105, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (449, 103, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (456, 115, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (457, 115, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (458, 115, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (459, 29, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (460, 29, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (461, 29, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (462, 116, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (463, 116, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (464, 116, 3)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (465, 28, 1)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (466, 28, 2)
INSERT [dbo].[Exercise_Level] ([Id], [ExerciseId], [LevelId]) VALUES (467, 28, 3)
SET IDENTITY_INSERT [dbo].[Exercise_Level] OFF
SET IDENTITY_INSERT [dbo].[Exercise_MuscleGroup] ON 

INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (12, 4, 4)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (13, 4, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (14, 6, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (20, 7, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (21, 7, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (43, 16, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (44, 16, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (59, 12, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (60, 12, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (61, 12, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (62, 12, 4)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (79, 2, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (80, 3, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (82, 2, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (83, 3, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (84, 18, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (85, 18, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (86, 18, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (87, 19, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (88, 19, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (89, 19, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (90, 8, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (91, 8, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (92, 8, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (104, 26, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (105, 26, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (106, 27, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (107, 27, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (130, 32, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (131, 32, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (133, 20, 4)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (134, 20, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (135, 20, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (136, 13, 5)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (137, 13, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (138, 13, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (139, 24, 4)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (140, 24, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (141, 23, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (142, 1, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (143, 1, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (144, 21, 5)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (145, 21, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (146, 21, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (169, 39, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (170, 39, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (171, 39, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (177, 41, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (178, 41, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (179, 40, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (180, 42, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (181, 42, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (182, 42, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (183, 43, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (184, 43, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (185, 44, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (186, 44, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (189, 45, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (190, 45, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (192, 46, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (193, 46, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (194, 46, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (195, 47, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (196, 47, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (213, 48, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (214, 48, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (218, 49, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (219, 49, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (220, 49, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (222, 22, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (226, 36, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (227, 36, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (228, 36, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (229, 37, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (230, 37, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (231, 37, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (235, 33, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (237, 35, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (238, 34, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (248, 56, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (249, 56, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (262, 59, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (263, 59, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (264, 59, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (265, 59, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (266, 59, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (280, 61, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (281, 61, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (282, 61, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (283, 61, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (284, 60, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (285, 60, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (286, 60, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (287, 62, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (288, 62, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (289, 62, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (290, 58, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (291, 58, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (292, 58, 6)
GO
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (293, 58, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (294, 58, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (295, 52, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (296, 53, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (297, 54, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (298, 55, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (299, 51, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (300, 57, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (301, 50, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (302, 50, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (303, 63, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (304, 63, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (305, 63, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (306, 63, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (307, 64, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (308, 65, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (309, 65, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (310, 66, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (311, 66, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (312, 67, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (313, 67, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (314, 68, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (315, 68, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (319, 69, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (320, 69, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (321, 69, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (322, 70, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (323, 70, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (324, 70, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (325, 71, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (326, 71, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (327, 72, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (328, 72, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (329, 72, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (330, 73, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (331, 73, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (332, 74, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (333, 75, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (334, 76, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (335, 77, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (336, 78, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (337, 79, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (338, 80, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (339, 81, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (340, 81, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (341, 82, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (342, 82, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (343, 83, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (344, 83, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (345, 84, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (346, 85, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (347, 86, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (348, 87, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (349, 88, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (350, 88, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (351, 89, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (352, 89, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (353, 90, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (354, 91, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (355, 92, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (356, 92, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (357, 93, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (358, 93, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (359, 94, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (360, 94, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (361, 94, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (362, 95, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (363, 95, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (366, 97, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (370, 99, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (371, 99, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (372, 100, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (373, 100, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (374, 101, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (375, 101, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (379, 104, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (380, 104, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (384, 107, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (385, 108, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (386, 109, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (387, 110, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (388, 111, 4)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (389, 111, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (390, 112, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (392, 113, 6)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (393, 106, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (394, 98, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (395, 98, 3)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (396, 102, 2)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (397, 102, 8)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (398, 96, 1)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (399, 105, 7)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (400, 105, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (404, 103, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (407, 115, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (408, 29, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (409, 114, 10)
INSERT [dbo].[Exercise_MuscleGroup] ([Id], [ExerciseId], [MuscleGroupId]) VALUES (410, 116, 10)
SET IDENTITY_INSERT [dbo].[Exercise_MuscleGroup] OFF
SET IDENTITY_INSERT [dbo].[Exercise_Tool] ON 

INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (9, 4, 2)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (30, 12, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (31, 12, 2)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (37, 18, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (38, 19, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (39, 19, 2)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (40, 8, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (53, 26, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (54, 26, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (55, 27, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (56, 27, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (57, 27, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (64, 31, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (73, 32, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (75, 20, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (76, 13, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (77, 24, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (78, 23, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (79, 21, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (100, 39, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (101, 39, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (102, 39, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (108, 41, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (109, 41, 9)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (110, 40, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (111, 42, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (112, 42, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (113, 43, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (114, 44, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (116, 45, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (118, 46, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (119, 46, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (120, 46, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (121, 47, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (130, 48, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (132, 49, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (134, 22, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (136, 36, 11)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (137, 37, 11)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (141, 33, 10)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (143, 35, 10)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (144, 34, 10)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (152, 56, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (160, 59, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (165, 61, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (166, 60, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (167, 62, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (168, 58, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (169, 52, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (170, 53, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (171, 54, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (172, 55, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (173, 51, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (174, 57, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (175, 50, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (176, 63, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (177, 64, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (178, 65, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (179, 66, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (180, 67, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (181, 68, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (183, 69, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (184, 70, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (185, 71, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (186, 72, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (187, 73, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (188, 74, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (189, 75, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (190, 76, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (191, 77, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (192, 78, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (193, 79, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (194, 80, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (195, 81, 7)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (196, 82, 9)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (197, 83, 9)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (198, 83, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (199, 84, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (200, 85, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (201, 86, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (202, 87, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (203, 88, 9)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (204, 89, 9)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (205, 90, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (206, 91, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (207, 92, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (208, 93, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (209, 94, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (210, 95, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (212, 97, 9)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (215, 99, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (216, 100, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (217, 101, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (220, 104, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (223, 107, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (224, 108, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (225, 109, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (226, 110, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (227, 111, 8)
GO
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (228, 112, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (230, 113, 6)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (231, 106, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (232, 98, 1)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (233, 102, 4)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (234, 96, 5)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (235, 105, 8)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (239, 103, 12)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (240, 29, 10)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (241, 114, 10)
INSERT [dbo].[Exercise_Tool] ([Id], [ExerciseId], [ToolId]) VALUES (242, 116, 10)
SET IDENTITY_INSERT [dbo].[Exercise_Tool] OFF
SET IDENTITY_INSERT [dbo].[FAQ] ON 

INSERT [dbo].[FAQ] ([Id], [Question], [Answer], [CategoryId]) VALUES (1, N'How do I sign up?', N'Just fill in all the necessary fields.', 1)
SET IDENTITY_INSERT [dbo].[FAQ] OFF
SET IDENTITY_INSERT [dbo].[FaqCategory] ON 

INSERT [dbo].[FaqCategory] ([Id], [Name]) VALUES (1, N'Sign up')
INSERT [dbo].[FaqCategory] ([Id], [Name]) VALUES (2, N'Profile and Security')
INSERT [dbo].[FaqCategory] ([Id], [Name]) VALUES (3, N'Payment and Refund')
SET IDENTITY_INSERT [dbo].[FaqCategory] OFF
SET IDENTITY_INSERT [dbo].[FavouriteBattle] ON 

INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (1, 3, 2, CAST(N'2017-04-28 00:00:00.000' AS DateTime), N'Deadlift Battle', 1, 1)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (2, 7, 2, CAST(N'2017-05-23 00:00:00.000' AS DateTime), N'Test 080517', 2, 1)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (3, 3, 10, CAST(N'2017-06-12 00:00:00.000' AS DateTime), N'Deadlift Battle', 1, 1)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (4, 7, 10, CAST(N'2017-06-12 00:00:00.000' AS DateTime), N'Test 080517', 1, 1)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (5, 24, 19, CAST(N'2017-06-16 00:00:00.000' AS DateTime), N'Battle 130617-02', 2, 2)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (6, 23, 36, CAST(N'2017-06-19 00:00:00.000' AS DateTime), N'Battle 130617', 3, 2)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (7, 24, 36, CAST(N'2017-06-21 00:00:00.000' AS DateTime), NULL, 2, 1)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (8, 24, 29, CAST(N'2017-07-01 00:00:00.000' AS DateTime), N'Battle 130617-02', 2, 2)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (9, 51, 76, CAST(N'2017-07-19 00:00:00.000' AS DateTime), N'HISASHI', 6, 1)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (10, 51, 81, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'HISASHI', 6, 3)
INSERT [dbo].[FavouriteBattle] ([Id], [BattleId], [UserId], [DateTime], [BattleName], [ObjectiveId], [LevelId]) VALUES (11, 51, 82, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'HISASHI', 6, 3)
SET IDENTITY_INSERT [dbo].[FavouriteBattle] OFF
SET IDENTITY_INSERT [dbo].[FavouriteExercise] ON 

INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (1, 2, 2, CAST(N'2017-04-28 00:00:00.000' AS DateTime), N'Push up')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (2, 1, 2, CAST(N'2017-05-04 00:00:00.000' AS DateTime), N'Squat')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (3, 12, 2, CAST(N'2017-05-21 00:00:00.000' AS DateTime), N'Burpee')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (4, 21, 19, CAST(N'2017-06-16 00:00:00.000' AS DateTime), N'Push up')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (5, 21, 36, CAST(N'2017-06-19 00:00:00.000' AS DateTime), N'Push up')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (6, 22, 36, CAST(N'2017-06-21 00:00:00.000' AS DateTime), N'Explosive bicycle crunch')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (7, 29, 36, CAST(N'2017-06-21 00:00:00.000' AS DateTime), N'RUN 100 meters')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (8, 21, 29, CAST(N'2017-07-01 00:00:00.000' AS DateTime), N'Push up')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (9, 45, 29, CAST(N'2017-07-13 00:00:00.000' AS DateTime), N'LONG JUMP')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (10, 55, 64, CAST(N'2017-07-18 00:00:00.000' AS DateTime), N'MEDBALL SIT UPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (11, 82, 68, CAST(N'2017-07-18 00:00:00.000' AS DateTime), N'BOSU ALT. SIDE JUMPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (12, 106, 61, CAST(N'2017-07-18 00:00:00.000' AS DateTime), N'CLAP PUSH UPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (13, 98, 61, CAST(N'2017-07-18 00:00:00.000' AS DateTime), N'DUMBBELL PUSH UP ROWS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (14, 85, 75, CAST(N'2017-07-18 00:00:00.000' AS DateTime), N'EXPLOSIVE RUSSIAN TWISTS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (15, 36, 64, CAST(N'2017-07-19 00:00:00.000' AS DateTime), N'ROW 1K')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (16, 58, 81, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'KETTLEBELL TWO ARM SWINGS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (17, 59, 66, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'ONE ARM KETTLEBELL SWINGS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (18, 58, 66, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'KETTLEBELL TWO ARM SWINGS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (19, 52, 66, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'MEDBALL CHOPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (20, 52, 66, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'MEDBALL CHOPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (21, 52, 66, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'MEDBALL CHOPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (22, 60, 29, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'KETTLEBELL CLEANS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (23, 61, 76, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'KETTLEBELL CLEAN & PRESSES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (24, 58, 76, CAST(N'2017-07-20 00:00:00.000' AS DateTime), N'KETTLEBELL TWO ARM SWINGS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (25, 61, 72, CAST(N'2017-07-21 00:00:00.000' AS DateTime), N'KETTLEBELL CLEAN & PRESSES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (26, 58, 72, CAST(N'2017-07-21 00:00:00.000' AS DateTime), N'KETTLEBELL TWO ARM SWINGS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (27, 109, 83, CAST(N'2017-07-21 00:00:00.000' AS DateTime), N'SIT UPS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (28, 91, 71, CAST(N'2017-08-09 00:00:00.000' AS DateTime), N'BARBELL SUMO SQUATS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (29, 87, 71, CAST(N'2017-08-09 00:00:00.000' AS DateTime), N'JACK KNIVES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (30, 37, 2, CAST(N'2017-08-10 00:00:00.000' AS DateTime), N'ROW 2K')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (31, 62, 2, CAST(N'2017-08-17 00:00:00.000' AS DateTime), N'KETTLEBELL SQUAT PRESSES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (32, 66, 2, CAST(N'2017-08-18 00:00:00.000' AS DateTime), N'KETTLEBELL LUNGES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (33, 62, 85, CAST(N'2017-09-17 00:00:00.000' AS DateTime), N'KETTLEBELL SQUAT PRESSES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (34, 58, 85, CAST(N'2017-09-17 00:00:00.000' AS DateTime), N'KETTLEBELL TWO ARM SWINGS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (35, 54, 85, CAST(N'2017-09-17 00:00:00.000' AS DateTime), N'MEDBALL RUSSIAN TWISTS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (36, 105, 85, CAST(N'2017-11-07 00:00:00.000' AS DateTime), N'BURPEES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (37, 90, 85, CAST(N'2017-12-05 00:00:00.000' AS DateTime), N'BARBELL FRONT SQUATS')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (38, 105, 97, CAST(N'2018-01-09 00:00:00.000' AS DateTime), N'BURPEES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (39, 69, 97, CAST(N'2018-01-17 00:00:00.000' AS DateTime), N'BARBELL DROP LUNGES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (40, 61, 102, CAST(N'2018-01-22 00:00:00.000' AS DateTime), N'KETTLEBELL CLEAN & PRESSES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (41, 105, 2, CAST(N'2018-02-08 00:00:00.000' AS DateTime), N'BURPEES')
INSERT [dbo].[FavouriteExercise] ([Id], [ExerciseId], [UserId], [DateTime], [ExerciseName]) VALUES (42, 116, 2, CAST(N'2018-02-08 00:00:00.000' AS DateTime), N'RUN 400M')
SET IDENTITY_INSERT [dbo].[FavouriteExercise] OFF
SET IDENTITY_INSERT [dbo].[FavouriteProgram] ON 

INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (1, 2, 2, CAST(N'2017-05-21 00:00:00.000' AS DateTime), N'NINJA', 1, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (2, 5, 10, CAST(N'2017-06-09 00:00:00.000' AS DateTime), N'WARRIOR', 1, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (3, 8, 19, CAST(N'2017-06-16 00:00:00.000' AS DateTime), N'Program 130617', 2, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (4, 9, 36, CAST(N'2017-06-19 00:00:00.000' AS DateTime), N'Program 130617-02', 1, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (5, 8, 36, CAST(N'2017-06-21 00:00:00.000' AS DateTime), N'Program 130617', 1, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (6, 8, 29, CAST(N'2017-07-01 00:00:00.000' AS DateTime), N'Program 130617', 3, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (7, 9, 29, CAST(N'2017-07-19 00:00:00.000' AS DateTime), N'MASAYOSHI', 2, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (8, 8, 2, CAST(N'2017-09-01 00:00:00.000' AS DateTime), N'YOSHIHIRO', 1, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (9, 11, 97, CAST(N'2018-01-08 00:00:00.000' AS DateTime), N'BUDOKAN', 2, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (10, 10, 2, CAST(N'2018-01-10 00:00:00.000' AS DateTime), N'BUSHIDO', 1, 0, 0)
INSERT [dbo].[FavouriteProgram] ([Id], [ProgramId], [UserId], [DateTime], [ProgramName], [LevelId], [DurationId], [FrequencyId]) VALUES (11, 11, 2, CAST(N'2018-02-08 00:00:00.000' AS DateTime), N'BUDOKAN', 1, 0, 0)
SET IDENTITY_INSERT [dbo].[FavouriteProgram] OFF
SET IDENTITY_INSERT [dbo].[FavouriteWorkout] ON 

INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (1, 2, 2, CAST(N'2017-04-28 00:00:00.000' AS DateTime), N'Bent-Over Row', 3, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (2, 1, 2, CAST(N'2017-04-28 00:00:00.000' AS DateTime), N'Bench Press', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (3, 4, 2, CAST(N'2017-05-05 00:00:00.000' AS DateTime), N'Test circuit', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (4, 6, 10, CAST(N'2017-06-09 00:00:00.000' AS DateTime), N'Dumbell Workout', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (5, 1, 10, CAST(N'2017-06-09 00:00:00.000' AS DateTime), N'Bench Press', 2, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (6, 2, 10, CAST(N'2017-06-09 00:00:00.000' AS DateTime), N'Bent-Over Row', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (7, 4, 10, CAST(N'2017-06-12 00:00:00.000' AS DateTime), N'Test circuit', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (8, 27, 19, CAST(N'2017-06-14 00:00:00.000' AS DateTime), N'Circuit 130617-03', 2, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (9, 26, 2, CAST(N'2017-06-15 00:00:00.000' AS DateTime), N'Circuit 130617-02', 3, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (10, 21, 30, CAST(N'2017-06-16 00:00:00.000' AS DateTime), N'Strength 130617', 1, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (11, 27, 36, CAST(N'2017-06-19 00:00:00.000' AS DateTime), N'Circuit 130617-03', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (12, 21, 36, CAST(N'2017-06-21 00:00:00.000' AS DateTime), N'Strength 130617', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (13, 29, 36, CAST(N'2017-06-21 00:00:00.000' AS DateTime), N'Circuit 150617', 3, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (14, 29, 29, CAST(N'2017-06-25 00:00:00.000' AS DateTime), N'Circuit 150617', 3, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (15, 21, 29, CAST(N'2017-06-27 00:00:00.000' AS DateTime), N'Strength 130617', 1, 3)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (16, 39, 29, CAST(N'2017-07-10 00:00:00.000' AS DateTime), N'TOMIO', 6, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (17, 31, 29, CAST(N'2017-07-13 00:00:00.000' AS DateTime), N'TAKUMI', 2, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (18, 21, 2, CAST(N'2017-07-17 00:00:00.000' AS DateTime), N'TETSUYA', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (19, 21, 80, CAST(N'2017-07-19 00:00:00.000' AS DateTime), N'TETSUYA', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (20, 33, 71, CAST(N'2017-09-07 00:00:00.000' AS DateTime), N'SANSUI 5K', 3, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (21, 2, 85, CAST(N'2017-09-16 00:00:00.000' AS DateTime), N'Bent-Over Row', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (22, 21, 85, CAST(N'2017-09-17 00:00:00.000' AS DateTime), N'TETSUYA', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (23, 52, 85, CAST(N'2017-09-17 00:00:00.000' AS DateTime), N'AKIHIRO', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (24, 53, 85, CAST(N'2017-09-17 00:00:00.000' AS DateTime), N'HAJIME', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (25, 25, 85, CAST(N'2017-10-12 00:00:00.000' AS DateTime), N'YUUDAI', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (26, 2, 91, CAST(N'2017-11-30 00:00:00.000' AS DateTime), N'Bent-Over Row', 1, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (27, 26, 97, CAST(N'2017-12-08 00:00:00.000' AS DateTime), N'HIROYUKI', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (28, 29, 97, CAST(N'2017-12-15 00:00:00.000' AS DateTime), N'NOBORU', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (29, 45, 97, CAST(N'2018-01-03 00:00:00.000' AS DateTime), N'ARATA-A', 2, 3)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (30, 53, 97, CAST(N'2018-01-08 00:00:00.000' AS DateTime), N'HAJIME', 1, 2)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (31, 24, 97, CAST(N'2018-01-09 00:00:00.000' AS DateTime), N'ARATA-B', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (32, 39, 97, CAST(N'2018-01-09 00:00:00.000' AS DateTime), N'TOMIO', 6, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (33, 27, 2, CAST(N'2018-01-10 00:00:00.000' AS DateTime), N'KICHIROU', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (34, 29, 2, CAST(N'2018-01-11 00:00:00.000' AS DateTime), N'NOBORU', 2, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (35, 30, 2, CAST(N'2018-02-08 00:00:00.000' AS DateTime), N'YASUSHI', 4, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (36, 51, 2, CAST(N'2018-02-08 00:00:00.000' AS DateTime), N'HISASHI', 6, 1)
INSERT [dbo].[FavouriteWorkout] ([Id], [WorkoutId], [UserId], [DateTime], [WorkoutName], [ObjectiveId], [LevelId]) VALUES (37, 24, 2, CAST(N'2018-02-08 00:00:00.000' AS DateTime), N'ARATA-B', 2, 1)
SET IDENTITY_INSERT [dbo].[FavouriteWorkout] OFF
SET IDENTITY_INSERT [dbo].[Frequency] ON 

INSERT [dbo].[Frequency] ([Id], [Name]) VALUES (1, N'1 x / week')
INSERT [dbo].[Frequency] ([Id], [Name]) VALUES (2, N'2 x / week')
INSERT [dbo].[Frequency] ([Id], [Name]) VALUES (3, N'3 x / week')
INSERT [dbo].[Frequency] ([Id], [Name]) VALUES (4, N'4 x / week')
SET IDENTITY_INSERT [dbo].[Frequency] OFF
SET IDENTITY_INSERT [dbo].[HallOfFame] ON 

INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (1, N'Deadlift Battle', 3, 8272, CAST(N'2017-05-04 00:00:00.000' AS DateTime), 2, 0, 1, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (2, N'Deadlift Battle', 3, 3228, CAST(N'2017-06-08 00:00:00.000' AS DateTime), 10, 0, 1, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (3, N'Test 080517', 7, 3572, CAST(N'2017-06-09 00:00:00.000' AS DateTime), 10, 0, 1, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (4, N'Battle 130617-02', 24, 14771, CAST(N'2017-06-14 00:00:00.000' AS DateTime), 10, 0, 2, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (5, N'Battle 130617-02', 24, 717, CAST(N'2017-06-14 00:00:00.000' AS DateTime), 19, 0, 2, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (6, N'Battle 130617', 23, 644, CAST(N'2017-06-15 00:00:00.000' AS DateTime), 19, 0, 3, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (7, N'Battle 130617-02', 24, 2453, CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2, 0, 2, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (8, N'Battle 130617', 23, 13905, CAST(N'2017-06-15 00:00:00.000' AS DateTime), 29, 0, 3, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (9, N'Battle 130617-02', 24, 1662, CAST(N'2017-06-15 00:00:00.000' AS DateTime), 29, 0, 2, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (10, N'Battle 130617', 23, 4573, CAST(N'2017-06-19 00:00:00.000' AS DateTime), 2, 0, 3, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (11, N'Battle 130617-02', 24, 2814, CAST(N'2017-06-19 00:00:00.000' AS DateTime), 17, 0, 2, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (12, N'Battle 130617', 23, 2560, CAST(N'2017-06-19 00:00:00.000' AS DateTime), 39, 0, 3, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (13, N'Battle 130617-02', 24, 3085, CAST(N'2017-06-19 00:00:00.000' AS DateTime), 36, 0, 2, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (14, N'Battle 130617', 23, 1763, CAST(N'2017-06-21 00:00:00.000' AS DateTime), 36, 0, 3, 1)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (15, N'HISASHI', 51, 24025, CAST(N'2017-07-18 00:00:00.000' AS DateTime), 64, 0, 6, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (16, N'HISASHI', 51, 7989, CAST(N'2017-07-19 00:00:00.000' AS DateTime), 29, 0, 6, 3)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (17, N'ARATA-I', 44, 3111, CAST(N'2017-10-02 00:00:00.000' AS DateTime), 85, 0, 2, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (18, N'HIDEAKI', 50, 1930, CAST(N'2017-10-02 00:00:00.000' AS DateTime), 85, 0, 2, 2)
INSERT [dbo].[HallOfFame] ([Id], [BattleName], [BattleId], [RecordTime], [DateTime], [UserId], [IsDeleted], [ObjectiveId], [LevelId]) VALUES (19, N'HIDEAKI', 50, 2057, CAST(N'2017-10-03 00:00:00.000' AS DateTime), 2, 0, 2, 2)
SET IDENTITY_INSERT [dbo].[HallOfFame] OFF
SET IDENTITY_INSERT [dbo].[Levels] ON 

INSERT [dbo].[Levels] ([Id], [Name], [Description]) VALUES (1, N'Beginner', NULL)
INSERT [dbo].[Levels] ([Id], [Name], [Description]) VALUES (2, N'Intermediate', NULL)
INSERT [dbo].[Levels] ([Id], [Name], [Description]) VALUES (3, N'Advanced', NULL)
SET IDENTITY_INSERT [dbo].[Levels] OFF
SET IDENTITY_INSERT [dbo].[LogCircuit] ON 

INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (1, N'Workout', 1, N'Bench Press', CAST(N'2017-04-28 00:00:00.000' AS DateTime), 8627, 2, 50, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (2, N'Program', 1, N'Bench Press', CAST(N'2017-04-30 00:00:00.000' AS DateTime), 28837, 2, 50, 1, 1, 1, 0, 0, 1)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (3, N'Workout', 1, N'Bench Press', CAST(N'2017-05-01 00:00:00.000' AS DateTime), 6772, 2, 50, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (4, N'Program', 1, N'Bench Press', CAST(N'2017-05-02 00:00:00.000' AS DateTime), 8040, 2, 50, 2, 1, 2, 0, 0, 1)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (5, N'Workout', 6, N'Dumbell Workout', CAST(N'2017-05-04 00:00:00.000' AS DateTime), 3772, 2, 20, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (6, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-05-04 00:00:00.000' AS DateTime), 10092, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (7, N'Program', 1, N'Bench Press', CAST(N'2017-05-05 00:00:00.000' AS DateTime), 75915, 2, 50, 2, 2, 3, 0, 0, 2)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (8, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-05-05 00:00:00.000' AS DateTime), 9128, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (9, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-05-05 00:00:00.000' AS DateTime), 26614, 2, 30, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (10, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-05-05 00:00:00.000' AS DateTime), 8272, 2, 30, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (12, N'Program', 1, N'Bench Press', CAST(N'2017-05-05 00:00:00.000' AS DateTime), 28951, 2, 50, 2, 2, 4, 0, 0, 2)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (13, N'Workout', 1, N'Bench Press', CAST(N'2017-05-05 00:00:00.000' AS DateTime), 4398, 2, 50, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (14, N'Workout', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 64961, 2, 50, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (15, N'Workout', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 17282, 2, 50, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (16, N'Workout', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 3862, 2, 50, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (17, N'Workout', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 24988, 2, 50, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (18, N'Workout', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 37540, 2, 50, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (19, N'Program', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 4178, 2, 50, 2, 2, 5, 0, 0, 2)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (20, N'Program', 1, N'Bench Press', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 22701, 2, 50, 2, 2, 6, 0, 0, 2)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (21, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 35636, 2, 0, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (22, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 29878, 2, 0, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (23, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-05-14 00:00:00.000' AS DateTime), 4810, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (24, N'Program', 3, N'Deadlift Battle', CAST(N'2017-05-14 00:00:00.000' AS DateTime), 2439, 2, 30, 1, 1, 1, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (25, N'Workout', 4, N'Test circuit', CAST(N'2017-05-17 00:00:00.000' AS DateTime), 13137, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (26, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-05-17 00:00:00.000' AS DateTime), 20563, 2, 0, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (27, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-05-18 00:00:00.000' AS DateTime), 32616, 2, 0, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (28, N'Workout', 6, N'Dumbell Workout', CAST(N'2017-05-22 00:00:00.000' AS DateTime), 1150, 2, 20, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (29, N'Workout', 4, N'Test circuit', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 6810, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (30, N'Program', 4, N'Test circuit', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 5622, 2, 30, 1, 1, 1, 0, 0, 6)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (31, N'Program', 3, N'Deadlift Battle', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 2478, 2, 30, 1, 1, 2, 0, 0, 6)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (32, N'Program', 4, N'Test circuit', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 4391, 2, 30, 1, 1, 2, 0, 0, 6)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (33, N'Program', 4, N'Test circuit', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 7378, 2, 30, 1, 1, 3, 0, 0, 6)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (37, N'Program', 4, N'Test circuit', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 4189, 2, 30, 1, 1, 1, 0, 0, 7)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (38, N'Program', 4, N'Test circuit', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 5103, 2, 30, 1, 1, 2, 0, 0, 7)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (39, N'Program', 1, N'Bench Press', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 2316, 2, 50, 2, 2, 1, 0, 0, 7)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (40, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-05 00:00:00.000' AS DateTime), 5406, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (41, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-05 00:00:00.000' AS DateTime), 7037, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (42, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-06 00:00:00.000' AS DateTime), 9458, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (43, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-06 00:00:00.000' AS DateTime), 7526, 2, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (44, N'Battle', 3, N'Deadlift Battle', CAST(N'2017-06-08 00:00:00.000' AS DateTime), 3228, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (45, N'Workout', 4, N'Test circuit', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 25602, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (46, N'Program', 3, N'Deadlift Battle', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 2001, 10, 30, 1, 1, 1, 0, 0, 5)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (47, N'Battle', 7, N'Test 080517', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 3572, 10, 100, 1, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (48, N'Battle', 7, N'Test 080517', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 5838, 10, 100, 1, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (49, N'Battle', 7, N'Test 080517', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 4800, 10, 100, 1, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (50, N'Workout', 4, N'Test circuit', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 8162, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (51, N'Workout', 4, N'Test circuit', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 10650, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (52, N'Workout', 4, N'Test circuit', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 12458, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (53, N'Workout', 4, N'Test circuit', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 6660, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (54, N'Workout', 4, N'Test circuit', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 7285, 10, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (55, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-06-10 00:00:00.000' AS DateTime), 28124, 10, 0, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (56, N'Program', 3, N'Deadlift Battle', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 5306, 10, 30, 1, 1, 2, 0, 0, 2)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (57, N'Workout', 4, N'Test circuit', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 17572, 17, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (58, N'Workout', 4, N'Test circuit', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 7864, 17, 30, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (59, N'Workout', 6, N'Dumbell Workout', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 3114, 17, 20, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (60, N'Workout', 6, N'Dumbell Workout', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 5381, 10, 20, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (61, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 24163, 10, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (62, N'Workout', 6, N'Dumbell Workout', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 12106, 10, 20, 1, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (63, N'Workout', 13, N'Sprint workout 2', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 21894, 10, 0, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (64, N'Program', 3, N'Deadlift Battle', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 13670, 2, 30, 1, 1, 3, 0, 0, 2)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (65, N'Workout', 19, N'Rest Workout', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 22219, 2, 50, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (66, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 11169, 2, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (67, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 11387, 2, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (68, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 11807, 2, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (69, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 14771, 10, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (70, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 5337, 19, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (71, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 7315, 19, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (72, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 7731, 19, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (73, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 1039, 19, 0, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (74, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 24789, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (75, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 6225, 19, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (76, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1120, 19, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (77, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 8856, 19, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (78, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1629, 19, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (79, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 8148, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (80, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 3131, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (81, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 35613, 19, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (82, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 6924, 19, 100, 3, 1, 1, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (83, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1184, 19, 0, 2, 1, 1, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (84, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 702, 19, 100, 6, 1, 1, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (85, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 717, 19, 100, 2, 1, 1, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (86, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 15239, 19, 100, 3, 1, 1, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (87, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2859, 19, 0, 2, 1, 2, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (88, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1303, 19, 0, 2, 1, 3, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (89, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 644, 19, 100, 3, 1, 1, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (90, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 5822, 19, 100, 3, 1, 2, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (91, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2459, 19, 100, 3, 1, 3, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (92, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2190, 19, 100, 3, 1, 2, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (93, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1183, 19, 100, 3, 1, 4, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (94, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2182, 19, 100, 3, 1, 5, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (95, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2754, 19, 100, 3, 1, 3, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (96, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 9589, 19, 100, 3, 1, 4, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (97, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 98181, 19, 100, 3, 1, 5, 0, 0, 9)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (98, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 934, 19, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (99, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 3296, 19, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (100, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2421, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (101, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 8040, 2, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (102, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2453, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (103, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 9945, 2, 100, 3, 1, 0, 0, 0, NULL)
GO
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (104, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 6512, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (105, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 4335, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (106, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 20358, 29, 100, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (107, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 13905, 29, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (108, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 6840, 29, 0, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (109, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2862, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (110, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 2295, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (111, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 1850, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (112, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 4425, 19, 134, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (113, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 9828, 19, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (114, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 33557, 19, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (115, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 23678, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (116, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 18125, 19, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (117, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 11574, 19, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (118, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 6436, 19, 100, 6, 1, 2, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (119, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 19623, 30, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (120, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 24079, 17, 134, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (121, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 66936, 17, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (122, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 8313, 2, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (123, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 10634, 2, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (124, N'Workout', 19, N'Rest Workout', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 6010, 17, 50, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (125, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 4573, 2, 100, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (126, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 1921, 2, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (127, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 7989, 2, 100, 3, 3, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (128, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 2814, 17, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (129, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 6071, 39, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (130, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 2560, 39, 100, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (131, N'Workout', 28, N'Interval 150617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 48936, 39, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (132, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 36688, 36, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (133, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 6911, 36, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (134, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 3085, 36, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (135, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 36767, 36, 100, 3, 1, 1, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (136, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 59832, 36, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (137, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 7177, 36, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (138, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 8744, 36, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (139, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-20 00:00:00.000' AS DateTime), 5687, 36, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (140, N'Battle', 23, N'Battle 130617', CAST(N'2017-06-21 00:00:00.000' AS DateTime), 1763, 36, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (141, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-21 00:00:00.000' AS DateTime), 18861, 29, 134, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (142, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 39956, 29, 100, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (143, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 3723, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (144, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 41646, 36, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (145, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 33441, 47, 100, 3, 1, 1, 0, 0, 8)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (146, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 31924, 48, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (147, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 37026, 50, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (148, N'Workout', 22, N'Circuit 130617', CAST(N'2017-06-25 00:00:00.000' AS DateTime), 44219, 29, 100, 3, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (149, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-25 00:00:00.000' AS DateTime), 8199, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (150, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-25 00:00:00.000' AS DateTime), 1662, 29, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (151, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 13311, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (152, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 3782, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (153, N'Workout', 26, N'Circuit 130617-02', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 11226, 29, 100, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (154, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-26 00:00:00.000' AS DateTime), 9071, 2, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (155, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 3471, 29, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (156, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 7304, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (157, N'Battle', 24, N'Battle 130617-02', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 3253, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (158, N'Workout', 25, N'Interval 130617', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 3603, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (159, N'Workout', 28, N'Interval 150617', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 27899, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (160, N'Workout', 29, N'Circuit 150617', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 6646, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (161, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-06-27 00:00:00.000' AS DateTime), 5102, 29, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (162, N'Workout', 28, N'Interval 150617', CAST(N'2017-06-29 00:00:00.000' AS DateTime), 27205, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (163, N'Workout', 30, N'Interval 190617', CAST(N'2017-06-29 00:00:00.000' AS DateTime), 57008, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (164, N'Workout', 30, N'Interval 190617', CAST(N'2017-06-29 00:00:00.000' AS DateTime), 56223, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (165, N'Workout', 30, N'Interval 190617', CAST(N'2017-06-29 00:00:00.000' AS DateTime), 56205, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (166, N'Workout', 28, N'Interval 150617', CAST(N'2017-06-29 00:00:00.000' AS DateTime), 97130, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (167, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-05 00:00:00.000' AS DateTime), 19327, 29, 0, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (168, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-06 00:00:00.000' AS DateTime), 6790, 29, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (169, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-07 00:00:00.000' AS DateTime), 34266, 29, 0, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (170, N'Workout', 29, N'Circuit 150617', CAST(N'2017-07-07 00:00:00.000' AS DateTime), 3458, 29, 134, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (171, N'Workout', 27, N'Circuit 130617-03', CAST(N'2017-07-09 00:00:00.000' AS DateTime), 34135, 29, 0, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (172, N'Workout', 22, N'HIROKI', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 28516, 29, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (173, N'Workout', 29, N'NOBORU', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 6507, 29, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (174, N'Workout', 31, N'TAKUMI', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 11108, 29, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (175, N'Workout', 27, N'KICHIROU', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 15669, 29, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (176, N'Workout', 34, N'SANSUI 10K', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 4095, 29, 100, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (177, N'Workout', 28, N'YUTAKA', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 29136, 29, 100, 4, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (178, N'Workout', 26, N'HIROYUKI', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 9741, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (179, N'Workout', 39, N'TOMIO', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 3851, 29, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (180, N'Workout', 28, N'YUTAKA', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 25139, 29, 100, 4, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (181, N'Workout', 39, N'TOMIO', CAST(N'2017-07-10 00:00:00.000' AS DateTime), 6017, 29, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (182, N'Workout', 27, N'KICHIROU', CAST(N'2017-07-11 00:00:00.000' AS DateTime), 9339, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (183, N'Workout', 41, N'DAICHI', CAST(N'2017-07-11 00:00:00.000' AS DateTime), 38011, 29, 100, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (184, N'Workout', 27, N'KICHIROU', CAST(N'2017-07-11 00:00:00.000' AS DateTime), 4692, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (185, N'Workout', 31, N'TAKUMI', CAST(N'2017-07-13 00:00:00.000' AS DateTime), 23944, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (186, N'Workout', 28, N'YUTAKA', CAST(N'2017-07-13 00:00:00.000' AS DateTime), 114366, 29, 100, 4, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (187, N'Workout', 28, N'YUTAKA', CAST(N'2017-07-13 00:00:00.000' AS DateTime), 37764, 29, 100, 4, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (188, N'Workout', 26, N'HIROYUKI', CAST(N'2017-07-14 00:00:00.000' AS DateTime), 15931, 29, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (189, N'Workout', 27, N'KICHIROU', CAST(N'2017-07-17 00:00:00.000' AS DateTime), 15996, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (190, N'Workout', 58, N'IWAO', CAST(N'2017-07-18 00:00:00.000' AS DateTime), 13542, 29, 200, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (191, N'Workout', 59, N'JIROU', CAST(N'2017-07-18 00:00:00.000' AS DateTime), 25676, 29, 200, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (192, N'Workout', 25, N'YUUDAI', CAST(N'2017-07-18 00:00:00.000' AS DateTime), 19791, 29, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (193, N'Battle', 51, N'HISASHI', CAST(N'2017-07-18 00:00:00.000' AS DateTime), 24025, 64, 300, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (194, N'Workout', 29, N'NOBORU', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 12163, 66, 150, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (195, N'Workout', 58, N'IWAO', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 22755, 29, 200, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (196, N'Workout', 26, N'HIROYUKI', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 40015, 29, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (197, N'Battle', 51, N'HISASHI', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 7989, 29, 300, 6, 3, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (198, N'Workout', 41, N'DAICHI', CAST(N'2017-07-20 00:00:00.000' AS DateTime), 58655, 66, 100, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (199, N'Workout', 26, N'HIROYUKI', CAST(N'2017-08-09 00:00:00.000' AS DateTime), 37567, 71, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (200, N'Workout', 27, N'KICHIROU', CAST(N'2017-08-29 00:00:00.000' AS DateTime), 18522, 85, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (201, N'Workout', 39, N'TOMIO', CAST(N'2017-09-17 00:00:00.000' AS DateTime), 12395, 85, 100, 6, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (202, N'Battle', 44, N'ARATA-I', CAST(N'2017-10-02 00:00:00.000' AS DateTime), 3111, 85, 500, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (203, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-02 00:00:00.000' AS DateTime), 1930, 85, 300, 2, 2, 0, 0, 0, NULL)
GO
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (204, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-02 00:00:00.000' AS DateTime), 6016, 85, 300, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (205, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-03 00:00:00.000' AS DateTime), 2057, 2, 300, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (206, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-03 00:00:00.000' AS DateTime), 6623, 2, 300, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (207, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-03 00:00:00.000' AS DateTime), 9865, 85, 300, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (208, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-04 00:00:00.000' AS DateTime), 5785, 85, 300, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (209, N'Battle', 50, N'HIDEAKI', CAST(N'2017-10-13 00:00:00.000' AS DateTime), 3121, 85, 300, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (210, N'Workout', 25, N'YUUDAI', CAST(N'2017-10-17 00:00:00.000' AS DateTime), 20229, 85, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (211, N'Workout', 27, N'KICHIROU', CAST(N'2017-10-18 00:00:00.000' AS DateTime), 14252, 85, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (212, N'Workout', 29, N'NOBORU', CAST(N'2017-11-07 00:00:00.000' AS DateTime), 46994, 85, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (213, N'Workout', 29, N'NOBORU', CAST(N'2017-11-08 00:00:00.000' AS DateTime), 44272, 85, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (214, N'Workout', 26, N'HIROYUKI', CAST(N'2017-11-17 00:00:00.000' AS DateTime), 9147, 85, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (215, N'Circuit', 39, N'TOMIO', CAST(N'2017-11-30 00:00:00.000' AS DateTime), 6542, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (216, N'Circuit', 39, N'TOMIO', CAST(N'2017-11-30 00:00:00.000' AS DateTime), 7785, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (217, N'Circuit', 39, N'TOMIO', CAST(N'2017-11-30 00:00:00.000' AS DateTime), 18734, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (218, N'Circuit', 39, N'TOMIO', CAST(N'2017-11-30 00:00:00.000' AS DateTime), 9580, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (219, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-01 00:00:00.000' AS DateTime), 10415, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (220, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-04 00:00:00.000' AS DateTime), 10132, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (221, N'Circuit', 29, N'NOBORU', CAST(N'2017-12-07 00:00:00.000' AS DateTime), 19593, 85, 150, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (222, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-07 00:00:00.000' AS DateTime), 17888, 85, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (223, N'Circuit', 30, N'YASUSHI', CAST(N'2017-12-07 00:00:00.000' AS DateTime), 34802, 85, 100, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (224, N'Circuit', 30, N'YASUSHI', CAST(N'2017-12-07 00:00:00.000' AS DateTime), 4422, 85, 100, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (225, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-07 00:00:00.000' AS DateTime), 8944, 85, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (226, N'Circuit', 60, N'EIJI', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 6702, 85, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (227, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 2524, 91, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (228, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 14273, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (229, N'Circuit', 61, N'Test 1752', CAST(N'2017-12-08 00:00:00.000' AS DateTime), 3382, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (230, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-12 00:00:00.000' AS DateTime), 20537, 91, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (231, N'Circuit', 61, N'Test 1752', CAST(N'2017-12-12 00:00:00.000' AS DateTime), 11242, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (232, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-14 00:00:00.000' AS DateTime), 23860, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (233, N'Circuit', 51, N'HISASHI', CAST(N'2017-12-14 00:00:00.000' AS DateTime), 4408, 97, 300, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (234, N'Circuit', 60, N'EIJI', CAST(N'2017-12-14 00:00:00.000' AS DateTime), 6014, 97, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (235, N'Circuit', 29, N'NOBORU', CAST(N'2017-12-15 00:00:00.000' AS DateTime), 4875, 97, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (236, N'Circuit', 50, N'HIDEAKI', CAST(N'2017-12-15 00:00:00.000' AS DateTime), 2497, 97, 300, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (237, N'Circuit', 39, N'TOMIO', CAST(N'2017-12-19 00:00:00.000' AS DateTime), 15003, 2, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (238, N'Circuit', 26, N'HIROYUKI', CAST(N'2017-12-31 00:00:00.000' AS DateTime), 10022, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (239, N'Circuit', 24, N'ARATA-B', CAST(N'2018-01-02 00:00:00.000' AS DateTime), 8017, 97, 500, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (240, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-03 00:00:00.000' AS DateTime), 5539, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (241, N'Circuit', 50, N'HIDEAKI', CAST(N'2018-01-03 00:00:00.000' AS DateTime), 2619, 97, 300, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (242, N'Circuit', 61, N'Test 1752', CAST(N'2018-01-08 00:00:00.000' AS DateTime), 5715, 97, 100, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (243, N'Circuit', 50, N'HIDEAKI', CAST(N'2018-01-08 00:00:00.000' AS DateTime), 7780, 97, 300, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (244, N'Circuit', 39, N'TOMIO', CAST(N'2018-01-08 00:00:00.000' AS DateTime), 4849, 97, 100, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (245, N'Circuit', 60, N'EIJI', CAST(N'2018-01-09 00:00:00.000' AS DateTime), 3736, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (246, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-09 00:00:00.000' AS DateTime), 19062, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (247, N'Circuit', 24, N'ARATA-B', CAST(N'2018-01-09 00:00:00.000' AS DateTime), 3334, 97, 500, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (248, N'Circuit', 60, N'EIJI', CAST(N'2018-01-09 00:00:00.000' AS DateTime), 7393, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (249, N'Circuit', 24, N'ARATA-B', CAST(N'2018-01-09 00:00:00.000' AS DateTime), 998, 97, 500, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (250, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-11 00:00:00.000' AS DateTime), 12018, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (251, N'Circuit', 29, N'NOBORU', CAST(N'2018-01-11 00:00:00.000' AS DateTime), 5785, 2, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (252, N'Circuit', 44, N'ARATA-I', CAST(N'2018-01-11 00:00:00.000' AS DateTime), 2782, 2, 500, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (253, N'Circuit', 44, N'ARATA-I', CAST(N'2018-01-11 00:00:00.000' AS DateTime), 1683, 2, 500, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (254, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-11 00:00:00.000' AS DateTime), 9192, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (255, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-11 00:00:00.000' AS DateTime), 21880, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (256, N'Circuit', 29, N'NOBORU', CAST(N'2018-01-12 00:00:00.000' AS DateTime), 9463, 2, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (257, N'Circuit', 60, N'EIJI', CAST(N'2018-01-17 00:00:00.000' AS DateTime), 13696, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (258, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-17 00:00:00.000' AS DateTime), 44186, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (259, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-17 00:00:00.000' AS DateTime), 30831, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (260, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-01-18 00:00:00.000' AS DateTime), 10943, 97, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (261, N'Circuit', 60, N'EIJI', CAST(N'2018-01-19 00:00:00.000' AS DateTime), 1613, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (262, N'Circuit', 60, N'EIJI', CAST(N'2018-01-19 00:00:00.000' AS DateTime), 2133, 2, 100, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (263, N'Circuit', 24, N'ARATA-B', CAST(N'2018-01-19 00:00:00.000' AS DateTime), 1482, 2, 500, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (264, N'Workout', 30, N'YASUSHI', CAST(N'2018-01-22 00:00:00.000' AS DateTime), 104243, 102, 100, 4, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (265, N'Circuit', 29, N'NOBORU', CAST(N'2018-02-06 00:00:00.000' AS DateTime), 9133, 2, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (266, N'Circuit', 44, N'ARATA-I', CAST(N'2018-02-06 00:00:00.000' AS DateTime), 8556, 2, 500, 2, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (267, N'Circuit', 29, N'NOBORU', CAST(N'2018-02-08 00:00:00.000' AS DateTime), 15252, 2, 150, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (268, N'Circuit', 24, N'ARATA-B', CAST(N'2018-02-08 00:00:00.000' AS DateTime), 6504, 2, 500, 2, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (269, N'Circuit', 51, N'HISASHI', CAST(N'2018-02-08 00:00:00.000' AS DateTime), 9552, 2, 300, 6, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogCircuit] ([Id], [Type], [TypeId], [TypeName], [Date], [WorkoutTime], [UserId], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationID], [FrequencyId], [ProgramId]) VALUES (270, N'Circuit', 26, N'HIROYUKI', CAST(N'2018-02-16 00:00:00.000' AS DateTime), 21782, 2, 100, 2, 1, 0, 0, 0, NULL)
SET IDENTITY_INSERT [dbo].[LogCircuit] OFF
SET IDENTITY_INSERT [dbo].[LogStrength] ON 

INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (1, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-04-28 00:00:00.000' AS DateTime), 70, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (2, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-04-30 00:00:00.000' AS DateTime), 70, 1, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (3, 2, N'Program', 2, N'Bent-Over Row', CAST(N'2017-04-30 00:00:00.000' AS DateTime), 70, 1, 1, 1, 0, 0, 1)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (4, 2, N'Program', 2, N'Bent-Over Row', CAST(N'2017-04-30 00:00:00.000' AS DateTime), 70, 1, 1, 2, 0, 0, 2)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (5, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-05-01 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (6, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-05-02 00:00:00.000' AS DateTime), 70, 3, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (8, 2, N'Program', 2, N'Bent-Over Row', CAST(N'2017-05-08 00:00:00.000' AS DateTime), 70, 1, 1, 3, 0, 0, 2)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (9, 2, N'Program', 2, N'Bent-Over Row', CAST(N'2017-05-09 00:00:00.000' AS DateTime), 70, 1, 1, 4, 0, 0, 1)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (10, 2, N'Program', 2, N'Bent-Over Row', CAST(N'2017-05-09 00:00:00.000' AS DateTime), 70, 1, 1, 5, 0, 0, 1)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (11, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-05-14 00:00:00.000' AS DateTime), 70, 1, 2, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (12, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-05-16 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (13, 2, N'Workout', 2, N'Bent-Over Row', CAST(N'2017-05-17 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, NULL)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (14, 2, N'Workout', 17, N'Test Strength', CAST(N'2017-05-18 00:00:00.000' AS DateTime), 20, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (15, 2, N'Workout', 17, N'Test Strength', CAST(N'2017-05-18 00:00:00.000' AS DateTime), 20, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (16, 2, N'Workout', 17, N'Test Strength', CAST(N'2017-05-19 00:00:00.000' AS DateTime), 20, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (22, 2, N'Program', 17, N'Test Strength', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 20, 1, 1, 1, 0, 0, 6)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (23, 2, N'Program', 17, N'Test Strength', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 20, 1, 1, 2, 0, 0, 6)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (24, 2, N'Program', 17, N'Test Strength', CAST(N'2017-05-25 00:00:00.000' AS DateTime), 20, 1, 1, 1, 0, 0, 7)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (25, 10, N'Workout', 17, N'Test Strength', CAST(N'2017-06-09 00:00:00.000' AS DateTime), 20, 2, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (26, 10, N'Workout', 17, N'Test Strength', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 20, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (27, 10, N'Workout', 17, N'Test Strength', CAST(N'2017-06-12 00:00:00.000' AS DateTime), 20, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (28, 2, N'Workout', 17, N'Test Strength', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 20, 2, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (29, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-13 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (30, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-14 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (31, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (32, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1000, 1, 1, 1, 0, 0, 9)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (33, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (34, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1000, 1, 1, 2, 0, 0, 9)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (35, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-15 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (36, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (37, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (38, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 1000, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (39, 19, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-16 00:00:00.000' AS DateTime), 1000, 1, 1, 3, 0, 0, 9)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (40, 17, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (41, 2, N'Workout', 17, N'Test Strength', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 20, 2, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (42, 36, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 1000, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (43, 36, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-19 00:00:00.000' AS DateTime), 1000, 1, 1, 2, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (44, 29, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 1000, 1, 2, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (45, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 1000, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (46, 2, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-22 00:00:00.000' AS DateTime), 1000, 1, 1, 2, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (47, 36, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (48, 36, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (49, 46, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1000, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (50, 48, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1000, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (51, 48, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (52, 50, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-23 00:00:00.000' AS DateTime), 1000, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (53, 29, N'Workout', 21, N'Strength 130617', CAST(N'2017-06-25 00:00:00.000' AS DateTime), 1000, 1, 2, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (54, 64, N'Workout', 38, N'YASUHIRO', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 250, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (55, 64, N'Workout', 21, N'TETSUYA', CAST(N'2017-07-19 00:00:00.000' AS DateTime), 250, 1, 1, 1, 0, 0, 8)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (56, 85, N'Workout', 38, N'YASUHIRO', CAST(N'2017-08-13 00:00:00.000' AS DateTime), 250, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (57, 88, N'Workout', 38, N'YASUHIRO', CAST(N'2017-10-19 00:00:00.000' AS DateTime), 250, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (58, 85, N'Workout', 21, N'TETSUYA', CAST(N'2017-11-17 00:00:00.000' AS DateTime), 250, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (59, 91, N'Strength', 2, N'Bent-Over Row', CAST(N'2017-12-01 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (60, 91, N'Strength', 2, N'Bent-Over Row', CAST(N'2017-12-12 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (61, 2, N'Strength', 2, N'Bent-Over Row', CAST(N'2017-12-19 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (62, 2, N'Strength', 2, N'Bent-Over Row', CAST(N'2017-12-19 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (63, 97, N'Strength', 53, N'HAJIME', CAST(N'2018-01-08 00:00:00.000' AS DateTime), 200, 1, 2, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (64, 97, N'Strength', 53, N'HAJIME', CAST(N'2018-01-09 00:00:00.000' AS DateTime), 200, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (65, 2, N'Strength', 2, N'Bent-Over Row', CAST(N'2018-01-19 00:00:00.000' AS DateTime), 70, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (66, 2, N'Strength', 21, N'TETSUYA', CAST(N'2018-02-02 00:00:00.000' AS DateTime), 250, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (67, 2, N'Strength', 21, N'TETSUYA', CAST(N'2018-02-06 00:00:00.000' AS DateTime), 250, 1, 1, 0, 0, 0, 0)
INSERT [dbo].[LogStrength] ([Id], [UserId], [Type], [TypeId], [TypeName], [Date], [Point], [ObjectiveId], [LevelId], [Cycle], [DurationId], [FrequencyId], [ProgramId]) VALUES (68, 2, N'Strength', 21, N'TETSUYA', CAST(N'2018-02-07 00:00:00.000' AS DateTime), 250, 1, 1, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[LogStrength] OFF
SET IDENTITY_INSERT [dbo].[LogStrengthExercise] ON 

INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (1, 1, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (2, 1, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (3, 2, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (4, 2, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (5, 3, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (6, 3, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (7, 4, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (8, 4, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (9, 5, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (10, 5, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (11, 6, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (12, 6, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (13, 7, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (14, 7, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (15, 8, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (16, 8, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (17, 9, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (18, 9, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (19, 10, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (20, 10, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (21, 11, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (22, 11, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (23, 12, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (24, 12, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (25, 13, N'Squat')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (26, 13, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (27, 14, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (28, 14, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (29, 15, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (30, 15, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (31, 16, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (32, 16, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (33, 17, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (34, 17, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (35, 18, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (36, 18, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (37, 19, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (38, 19, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (39, 20, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (40, 20, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (41, 21, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (42, 21, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (43, 22, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (44, 22, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (45, 23, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (46, 23, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (47, 24, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (48, 24, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (49, 25, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (50, 25, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (51, 26, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (52, 26, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (53, 27, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (54, 27, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (55, 28, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (56, 28, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (57, 29, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (58, 30, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (59, 30, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (60, 30, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (61, 31, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (62, 31, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (63, 31, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (64, 32, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (65, 32, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (66, 32, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (67, 33, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (68, 33, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (69, 33, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (70, 34, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (71, 34, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (72, 34, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (73, 35, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (74, 35, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (75, 35, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (76, 36, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (77, 36, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (78, 36, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (79, 37, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (80, 37, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (81, 37, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (82, 38, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (83, 38, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (84, 38, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (85, 39, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (86, 39, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (87, 39, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (88, 40, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (89, 40, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (90, 40, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (91, 41, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (92, 41, N'Dumbell Exercise')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (93, 42, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (94, 42, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (95, 42, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (96, 43, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (97, 43, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (98, 43, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (99, 44, N'Burpee')
GO
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (100, 44, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (101, 44, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (102, 45, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (103, 45, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (104, 45, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (105, 46, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (106, 46, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (107, 46, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (108, 47, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (109, 47, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (110, 47, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (111, 48, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (112, 48, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (113, 48, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (114, 49, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (115, 49, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (116, 49, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (117, 50, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (118, 50, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (119, 50, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (120, 51, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (121, 51, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (122, 51, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (123, 52, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (124, 52, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (125, 52, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (126, 53, N'Burpee')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (127, 53, N'Push up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (128, 53, N'Pull up')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (129, 54, N'BARBELL DROP SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (130, 54, N'BARBELL CLEAN & PRESSES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (131, 54, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (132, 54, N'DUMBBELL PUSH UP ROWS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (133, 54, N'CHIN UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (134, 55, N'BARBELL FRONT SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (135, 55, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (136, 55, N'MEDBALL PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (137, 55, N'SWISSBALL DUMBBELL ROTATIONS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (138, 55, N'BOSU PULL OVERS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (139, 56, N'BARBELL DROP SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (140, 56, N'BARBELL CLEAN & PRESSES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (141, 56, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (142, 56, N'DUMBBELL PUSH UP ROWS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (143, 56, N'CHIN UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (144, 57, N'BARBELL DROP SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (145, 57, N'BARBELL CLEAN & PRESSES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (146, 57, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (147, 57, N'DUMBBELL PUSH UP ROWS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (148, 57, N'CHIN UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (149, 58, N'BARBELL FRONT SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (150, 58, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (151, 58, N'MEDBALL PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (152, 58, N'SWISSBALL DUMBBELL ROTATIONS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (153, 58, N'BOSU PULL OVERS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (154, 59, N'SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (155, 59, N'Push up 1')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (156, 60, N'SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (157, 60, N'Push up 1')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (158, 61, N'SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (159, 61, N'Push up 1')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (160, 62, N'SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (161, 62, N'Push up 1')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (162, 63, N'BARBELL DROP SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (163, 63, N'BARBELL SIDE LUNGES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (164, 63, N'CHIN UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (165, 63, N'JACK KNIVES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (166, 63, N'CLAP PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (167, 64, N'BARBELL DROP SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (168, 64, N'BARBELL SIDE LUNGES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (169, 64, N'CHIN UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (170, 64, N'JACK KNIVES')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (171, 64, N'CLAP PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (172, 65, N'SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (173, 65, N'Push up 1')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (174, 66, N'BARBELL FRONT SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (175, 66, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (176, 66, N'MEDBALL PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (177, 66, N'SWISSBALL DUMBBELL ROTATIONS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (178, 66, N'BOSU PULL OVERS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (179, 67, N'BARBELL FRONT SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (180, 67, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (181, 67, N'MEDBALL PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (182, 67, N'SWISSBALL DUMBBELL ROTATIONS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (183, 67, N'BOSU PULL OVERS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (184, 68, N'BARBELL FRONT SQUATS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (185, 68, N'BARBELL STIFF LEG DEADLIFTS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (186, 68, N'MEDBALL PUSH UPS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (187, 68, N'SWISSBALL DUMBBELL ROTATIONS')
INSERT [dbo].[LogStrengthExercise] ([Id], [LogStrengthId], [ExerciseName]) VALUES (188, 68, N'BOSU PULL OVERS')
SET IDENTITY_INSERT [dbo].[LogStrengthExercise] OFF
SET IDENTITY_INSERT [dbo].[LogStrengthSet] ON 

INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (1, 1, 20, 5, 1)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (2, 1, 10, 5, 2)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (3, 2, 10, 3, 2)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (4, 1, 15, 5, 3)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (5, 2, 12, 4, 3)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (6, 1, 12, 4, 4)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (7, 1, 15, 5, 5)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (8, 1, 12, 4, 6)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (9, 2, 15, 3, 6)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (10, 1, 5, 5, 7)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (11, 1, 54, 5, 8)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (12, 1, 15, 10, 9)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (13, 1, 20, 8, 10)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (14, 2, 15, 5, 10)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (15, 1, 0, 0, 11)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (16, 1, 0, 0, 12)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (17, 1, 10, 15, 13)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (18, 1, 15, 5, 14)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (19, 1, 25, 8, 15)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (20, 1, 15, 10, 16)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (21, 1, 15, 5, 17)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (22, 1, 15, 5, 18)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (23, 1, 15, 5, 19)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (24, 1, 15, 5, 20)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (25, 1, 0, 0, 21)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (26, 1, 12, 34, 22)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (27, 2, 34, 22, 22)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (28, 3, 545, 65, 22)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (29, 1, 11, 11, 23)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (30, 2, 10, 15, 23)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (31, 1, 0, 25, 24)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (32, 2, 0, 0, 24)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (33, 1, 10, 5, 25)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (34, 1, 18, 3, 26)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (35, 2, 20, 5, 26)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (36, 1, 15, 5, 27)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (37, 1, 15, 5, 28)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (38, 2, 20, 4, 28)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (39, 1, 12, 5, 29)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (40, 2, 14, 6, 29)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (41, 1, 22, 3, 30)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (42, 1, 30, 7, 31)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (43, 2, 22, 9, 31)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (44, 1, 24, 12, 32)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (45, 2, 45, 30, 32)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (46, 1, 20, 5, 33)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (47, 2, 15, 3, 33)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (48, 1, 15, 5, 34)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (49, 1, 5, 5, 35)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (50, 1, 5, 5, 36)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (51, 1, 10, 5, 37)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (52, 1, 10, 5, 38)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (53, 1, 10, 5, 39)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (54, 1, 10, 5, 40)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (55, 1, 15, 5, 41)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (56, 1, 15, 5, 42)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (57, 1, 15, 5, 43)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (58, 1, 15, 5, 44)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (59, 1, 20, 7, 45)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (60, 1, 20, 7, 46)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (61, 1, 20, 5, 47)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (62, 1, 20, 5, 48)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (63, 1, 0, 0, 49)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (64, 1, 0, 0, 50)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (65, 1, 15, 5, 51)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (66, 2, 20, 10, 51)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (67, 1, 15, 12, 52)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (68, 2, 20, 14, 52)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (69, 1, 80, 21, 53)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (70, 1, 20, 21, 54)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (71, 1, 15, 12, 55)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (72, 1, 15, 8, 56)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (73, 1, 100, 19, 57)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (74, 1, 10, 4, 58)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (75, 1, 55, 4, 59)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (76, 1, 25, 3, 60)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (77, 2, 23, 23, 60)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (78, 1, 10, 2, 61)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (79, 1, 15, 5, 62)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (80, 1, 20, 3, 63)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (81, 1, 55, 88, 64)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (82, 1, 55, 5, 65)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (83, 1, 5, 5, 66)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (84, 1, 10, 5, 67)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (85, 1, 15, 5, 68)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (86, 1, 20, 2, 69)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (87, 1, 2, 2, 70)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (88, 1, 3, 3, 71)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (89, 1, 1, 1, 72)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (90, 1, 100, 50, 73)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (91, 1, 0, 0, 74)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (92, 1, 0, 0, 75)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (93, 1, 50, 24, 76)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (94, 1, 0, 30, 77)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (95, 1, 0, 25, 78)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (96, 1, 25, 0, 79)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (97, 1, 50, 0, 80)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (98, 1, 60, 0, 81)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (99, 1, 0, 0, 82)
GO
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (100, 1, 0, 0, 83)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (101, 1, 0, 0, 84)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (102, 1, 0, 0, 85)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (103, 1, 0, 0, 86)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (104, 1, 0, 0, 87)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (105, 1, 15, 5, 88)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (106, 1, 15, 5, 89)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (107, 1, 15, 5, 90)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (108, 1, 0, 0, 91)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (109, 2, 0, 0, 91)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (110, 3, 25, 8, 91)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (111, 1, 35, 8, 92)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (112, 1, 0, 0, 93)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (113, 1, 0, 0, 94)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (114, 1, 0, 0, 95)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (115, 1, 100, 0, 96)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (116, 1, 50, 0, 97)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (117, 1, 50, 0, 98)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (118, 2, 60, 0, 98)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (119, 1, 100, 0, 99)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (120, 1, 0, 0, 100)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (121, 1, 0, 0, 101)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (122, 1, 80, 12, 102)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (123, 2, 0, 0, 102)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (124, 3, 0, 0, 102)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (125, 1, 0, 0, 103)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (126, 1, 0, 0, 104)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (127, 1, 0, 12, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (128, 2, 0, 14, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (129, 3, 0, 18, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (130, 4, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (131, 5, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (132, 6, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (133, 7, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (134, 8, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (135, 9, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (136, 10, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (137, 11, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (138, 12, 0, 0, 105)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (139, 1, 0, 0, 106)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (140, 1, 0, 0, 107)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (141, 2, 0, 0, 107)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (142, 3, 0, 0, 107)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (143, 1, 10, 4, 108)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (144, 1, 10, 2, 109)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (145, 2, 20, 1, 109)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (146, 1, 20, 3, 110)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (147, 1, 22, 4, 111)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (148, 1, 44, 3, 112)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (149, 2, 22, 2, 112)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (150, 1, 44, 1, 113)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (151, 1, 22, 4, 114)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (152, 1, 44, 3, 115)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (153, 1, 22, 3, 116)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (154, 1, 22, 4, 117)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (155, 1, 44, 2, 118)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (156, 1, 22, 2, 119)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (157, 1, 20, 2, 120)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (158, 1, 10, 4, 121)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (159, 1, 10, 3, 122)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (160, 1, 10, 2, 123)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (161, 1, 44, 2, 124)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (162, 1, 20, 2, 125)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (163, 1, 120, 21, 126)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (164, 1, 125, 23, 127)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (165, 1, 130, 25, 128)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (166, 1, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (167, 2, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (168, 3, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (169, 4, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (170, 5, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (171, 6, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (172, 7, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (173, 8, 0, 0, 129)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (174, 1, 0, 0, 130)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (175, 1, 0, 0, 131)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (176, 1, 0, 0, 132)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (177, 1, 0, 0, 133)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (178, 1, 80, 5, 134)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (179, 1, 0, 0, 135)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (180, 1, 0, 0, 136)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (181, 1, 0, 0, 137)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (182, 1, 0, 0, 138)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (183, 1, 58, 5, 139)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (184, 1, 36, 12, 140)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (185, 1, 14, 2, 141)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (186, 1, 25, 1, 142)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (187, 1, 56, 4, 143)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (188, 1, 0, 0, 144)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (189, 2, 0, 0, 144)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (190, 3, 5, 12, 144)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (191, 1, 0, 0, 145)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (192, 1, 0, 0, 146)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (193, 1, 0, 0, 147)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (194, 1, 0, 0, 148)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (195, 1, 50, 12, 149)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (196, 2, 50, 10, 149)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (197, 0, 0, 0, 149)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (198, 1, 80, 10, 150)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (199, 0, 0, 0, 150)
GO
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (200, 1, 0, 0, 151)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (201, 0, 0, 0, 151)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (202, 1, 0, 0, 152)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (203, 0, 0, 0, 152)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (204, 1, 0, 0, 153)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (205, 0, 0, 0, 153)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (206, 1, 10, 20, 154)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (207, 2, 0, 0, 154)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (208, 1, 30, 40, 155)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (209, 1, 20, 10, 156)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (210, 2, 10, 30, 156)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (211, 0, 0, 0, 156)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (212, 1, 20, 30, 157)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (213, 0, 0, 0, 157)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (214, 1, 9.09090909090909, 11, 158)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (215, 1, 22.727272727272727, 100, 159)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (216, 1, 13.636363636363635, 50, 160)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (217, 1, 40.909090909090907, 100, 161)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (218, 1, 50, 15, 162)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (219, 2, 50, 12, 162)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (220, 1, 0, 0, 163)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (221, 1, 0, 0, 164)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (222, 1, 0, 0, 165)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (223, 1, 0, 0, 166)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (224, 1, 55, 12, 167)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (225, 2, 55, 14, 167)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (226, 3, 55, 15, 167)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (227, 4, 0, 0, 167)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (228, 1, 75, 8, 168)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (229, 2, 75, 10, 168)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (230, 1, 0, 20, 169)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (231, 1, 0, 0, 170)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (232, 1, 0, 0, 171)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (233, 1, 0, 0, 172)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (234, 1, 0, 0, 173)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (235, 1, 45.454545454545453, 10, 174)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (236, 2, 0, 0, 174)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (237, 1, 90.9090909090909, 20, 175)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (238, 1, 136.36363636363635, 30, 176)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (239, 1, 181.81818181818181, 40, 177)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (240, 1, 227.27272727272725, 50, 178)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (241, 1, 22.727272727272727, 12, 179)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (242, 1, 0, 0, 180)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (243, 1, 0, 0, 181)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (244, 1, 0, 0, 182)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (245, 1, 0, 0, 183)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (246, 1, 45.454545454545453, 10, 184)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (247, 2, 54.54545454545454, 10, 184)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (248, 1, 0, 0, 185)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (249, 1, 0, 0, 186)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (250, 1, 0, 0, 187)
INSERT [dbo].[LogStrengthSet] ([Id], [SetNumber], [Weight], [Reps], [LogStrengthExerciseId]) VALUES (251, 1, 0, 0, 188)
SET IDENTITY_INSERT [dbo].[LogStrengthSet] OFF
SET IDENTITY_INSERT [dbo].[MembershipType] ON 

INSERT [dbo].[MembershipType] ([Id], [Name], [Duration], [Price], [CurrencyId], [Discount]) VALUES (1, N'Basic', 12, 0, 1, 0)
INSERT [dbo].[MembershipType] ([Id], [Name], [Duration], [Price], [CurrencyId], [Discount]) VALUES (2, N'Exclusive', 6, 99.99, 1, 50)
INSERT [dbo].[MembershipType] ([Id], [Name], [Duration], [Price], [CurrencyId], [Discount]) VALUES (3, N'Admin', 120, 0, 1, 0)
SET IDENTITY_INSERT [dbo].[MembershipType] OFF
SET IDENTITY_INSERT [dbo].[MuscleGroup] ON 

INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (1, N'Chest', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (2, N'Shoulders', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (3, N'Back', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (4, N'Biceps', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (5, N'Triceps', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (6, N'Abs', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (7, N'Legs', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (8, N'Glutes', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (9, N'Full body', NULL)
INSERT [dbo].[MuscleGroup] ([Id], [Name], [Description]) VALUES (10, N'Cardio', NULL)
SET IDENTITY_INSERT [dbo].[MuscleGroup] OFF
SET IDENTITY_INSERT [dbo].[Objective] ON 

INSERT [dbo].[Objective] ([Id], [Name], [Description]) VALUES (1, N'Basic strength', NULL)
INSERT [dbo].[Objective] ([Id], [Name], [Description]) VALUES (2, N'General conditioning', NULL)
INSERT [dbo].[Objective] ([Id], [Name], [Description]) VALUES (3, N'Cardio endurance', NULL)
INSERT [dbo].[Objective] ([Id], [Name], [Description]) VALUES (4, N'Anaerobic endurance', NULL)
INSERT [dbo].[Objective] ([Id], [Name], [Description]) VALUES (6, N'Core strength', NULL)
SET IDENTITY_INSERT [dbo].[Objective] OFF
SET IDENTITY_INSERT [dbo].[OfficialEmail] ON 

INSERT [dbo].[OfficialEmail] ([Id], [Email]) VALUES (1, N'atish@bs-23.com')
SET IDENTITY_INSERT [dbo].[OfficialEmail] OFF
SET IDENTITY_INSERT [dbo].[Privacy] ON 

INSERT [dbo].[Privacy] ([Id], [Description]) VALUES (1, N'Privacy statement
The protection of your personal data is of particular concern to us at FightAthlete. Therefore, we comply with the legal requirements when we collect and process your personal data. Below you will find extensive information about the scope and purpose of collecting data.

Version: Oktober 2017

Principle of anonymous data use
In principle, our offers (FightAthlete) can be used without providing personal data. The use of individual services on our website can therefore entail divergent regulations which in this case are separately explained below. The legal basis for data protection can be found in the Dutch Data Protection Act (Wbp).
When you access our website or App, some information, such as IP address, is transferred. You are also providing information about the terminal device used (PC, smartphone, tablet, etc.), the browser used (Internet Explorer, Safari, Firefox, etc.), time of visit to the website, the so-called referrer and quantity of data transferred.

We cannot use these data to identify an individual user. We only use this information to determine how attractive our offer is and to improve its performance or contents, if necessary, and make its design even more interesting for you. However, we would like to point out that in the case of static IP address personal identification is possible by RIPE query in individual cases, although we do not perform this. Nevertheless, this website is accessible for both static and dynamic IP addresses assigned.

Personal data
The term “personal data” is defined in the Dutch Data Protection Act. Thereby, it is described as details of personal or material circumstances regarding a specific or identifiable natural person. These include, for example, your real name, address, phone number or date of birth.

Collecting and processing personal data
Personal data are collected by us, only if you supply them to us, for example when you contact us, in particular by registering a FightAthlete account, placing an order, requesting information or publishing personal data in our FightAthlete App in your profile.
We use the personal data provided by you only to the extent that your data are necessary for rendering or processing our services. We save your data for as long as it is necessary to reach the envisaged objectives or until you remove your account or for as long as legal storage periods require the data to be retained. Finally, your data are removed or blocked according to the legal regulations.

Registration of FightAthlete accounts
By means of our login system you can create your FightAthlete account, which you can use after your initial registration to subscribe to the Exclusive membership. In the process we use cookies, small files in your browser to identify you. All data which you store on your account are saved in a data base of FightAthlete. 

We require only the following data for registration:
•	name and surname
•	Gender
•	Country of residence
•	e-mail address
•	password

Moreover, you must acknowledge our Privacy Statement and accept our General Terms and Conditions of Business and cancellation conditions. 
Further, we offer you the possibility to create your FightAthlete account via your Facebook account or link it to your Facebook profile. You can register or log in with us by your Facebook account, if you simply use the Facebook button when registering your FightAthlete account instead of other possibilities. You will then be directed to the Facebook page (where you must log in or you need to have an account) and you will be told which data on Facebook we need from you, that is, first of all, your public profile information, such as name, surname, gender and e-mail address which you have saved there. This is necessary for identification, so that a secure FightAthlete account can be created for you. This also helps us show you which of your friends have already joined FightAthlete. Your Facebook profile and your FightAthlete account are permanently linked by the e-mail address. We save your e-mail data here and we will contact you at this address with information, if necessary. We also take note of your registration with us via Facebook. As soon as you log in to Facebook, you can also log in with us. Without your consent, we will not forward any of your data to Facebook. Important: We do not find out your Facebook access data at any point and we have no possibility to post anything on your Facebook profile, without it having been approved by you separately. You can find out how Facebook handles privacy settings by consulting the Facebook Data Protection Policy (https://www.facebook.com/policy.php) and Terms and Conditions of Use (https://www.facebook.com/legal/terms); there you will also find the valid provisions for all above-mentioned possibilities of logging in and registering with us. Other Facebook functions, such as the "Like" button, which you will find in the following chapter of this Privacy Statement have got nothing to do with this login possibility and are displayed separately from your FightAthlete account.

Data collection, processing and use within the framework of FightAthlete App
General provisions
To use our FightAthlete App you must have a FightAthlete account. You have already found out above what data are collected for this purpose (cf. Point 4). To provide you with the best FightAthlete experience possible, our approach is partly based on publishing specific information of our users, i.e. even your information. We are going to introduce our programme to you in more detail below, so that you can decide for yourself whether and which data you want to publish.

This introduction includes the following information, in particular:
•	public profile (photo, name, surname, training location, motivation)
•	condition (estimated fitness level, desired objective)
•	training information (period and type of training unit, total number of training sessions, note, photo)
•	followed by/follows (persons who follow the user, persons followed by the user)

You become visible in the FightAthlete App with the data in your public profile. This information helps other users to find you in FightAthlete App. At the same time, other users can see your name and surname (if provided), fitness level and your profile photo, and they can recognise you by this information, if necessary.
The FightAthlete App saves all your successfully completed training you have consumed as well as related information, such as self-uploaded photos or notes. Other users can see these data and can consider them as incentive for themselves, leave comments or decide whether they want to follow you.
Moreover, our FightAthleteApp also provides the users with the possibility to be followed ("Followed by") to support or encourage them in their training experience, or to compete with one another. For this purpose, users can be searched for by the name registered in the public profile or, after linking of a FightAthlete account to a Facebook account, via their Friends list. You will be notified about new followers on the website or mobile App by push message.
Thus, some of your information is also available to other users in FightAthlete App, irrespective of whether it is accessed on the website or in the relevant mobile App. Our aim is that nobody is left alone with their training. Instead, users’ performance is appreciated and can become an incentive for new members.
If you do not want to make it possible to link your performance to yourself, you are free to refrain from providing any personal data on your profile or training sessions and linking your account to Facebook. Moreover, you should not save any training photos or enter any notes in this case. For this reason, we have ensured that each user can change their personal information which can be viewed by other users and each user is free to use their own name or a fictitious name in their FightAthlete App.

FightAthlete Personal Trainer
Personalised training
If you opted for FightAthlete Personal Trainer, you will receive suitable training every week, based on your successfully completed training. For this purpose, we analyse the completed training performance (type of training, time required for each set, total time required) using a complex algorithm and we evaluate this in relation to the training performance of other users, so that we can offer perfectly tailored training.

Evaluation of location data
Within the framework of our FightAthlete App, it is possible to map the runs. For this purpose, we need access to location data. These data help us to calculate the distances covered at the relevant time and thereby determine the end correctly for pre-defined distances or determine the distances covered. To determine in which parts of the world most of the users are based, we evaluate your location data statistically. For this purpose, we draw on the positioning data saved on iOS devices under "Frequent locations". These data are forwarded to us via the use of the App without any reference to your person being made. We only evaluate all positioning data statistically. You will find further information about the positioning function "Frequent locations" of iOS devices and the possibility of deactivating it here: http://support.apple.com/de-de/HT5594

Access rights
We need these access options and information to ensure the technical functioning of our App and provision of the services offered within the App, in particular to be able to access your camera or your photos, to determine your running or to send you push messages to inform you about new followers or comments. During the installation procedure or before you use the App for the first time, we request permission to access individual functions and information. We only access these functions if you give us your approval. You can manually revoke the access rights in the settings for each operating system. You can find out how this works in the manufacturer instructions for your mobile OS. However, please note that you can use the App only to a limited extent or you cannot use it at all without the relevant approval. 

Before you use the App for the first time, we will request the following rights for the purpose described below:
Right > Purpose: Camera > taking photos for profile/feed, Photo library > selection of photos for profile/feed, Location tracing > calculation of running distance and Statistical evaluation, Delivery of push messages > receipt of push messages, Mobile data/WLAN > use of internet and downloading new contents

Contact form
You can contact us by e-mail or by an online contact form. Obviously, we will use the personal data provided to us in this way only for the purpose for which you make them available to us when you contact us. 

Obviously, you can revoke your declarations of consent for the future at any time. Please refer to the section mentioned at the end of this statement to find out how to do this.

Newsletter
You can subscribe to our newsletter, if you want to receive regular news updates or information from us about selected topics and products. We need your valid e-mail address for subscription. To ensure that you actually want to receive information from us, we use the so-called double opt-in procedure. After the successful subscription, you receive a link by e-mail, which you can use to activate the newsletter service.

Obviously, you can unsubscribe from the newsletter at any time and revoke your given consent for the future. To do this, please click the relevant button in the newsletter you received or refer to the contact data provided below.

Cookies and tracking pixels
To improve our web service and to make your use as comfortable as possible, we use cookies. Cookies are small text files which are saved on your PC when you visit our website. They facilitate the repeated allocation of your browser. Cookies save information, such as your language settings, duration of the visit to our website or the entries you made there. This means that the required data does not need to be entered again each time the service is used. Moreover, cookies help us to recognise your preferences and adjust our website to your areas of interest.

Most browsers accept cookies automatically. If you want to prevent cookies from being saved, you can select the "Accept no cookies" option in your browser settings. To find out exactly how this works, you can consult the instructions of your browser manufacturer. You can delete the cookies, which have already been saved on your PC at any time. However, we would like to point out that our website service can only be used to a limited extent without cookies.

Moreover, every time our website is loaded, we record how often it is visited and clicked on by using tags on our website, so-called tracking pixels, likewise without any interference and intervention for your PC.

Google Analytics
According to the agreement between Google and the Den-Haag-based authorised representative for data protection and freedom of information (Dutch DPA), Google Analytics can be used under certain conditions in compliance with data protection and without leading to any complaints. In particular, we would like to point out that Google Analytics on our website has been expanded by relevant IP masking function to ensure the anonymous collection of IP addresses. Please also pay attention to the following information about the application of Google Analytics:

This website uses Google Analytics, a web analysis service of Google Inc. ("Google"). Google Analytics uses the so-called "cookies". The information about your use of this website generated by cookies is usually forwarded to and saved on a Google server in the USA. However, due to activating IP anonymisation on this website, your IP address is shortened beforehand by Google in member states of the European Union or other states which are parties to the Agreement on the European Economic Area. Only in exceptional cases is the full IP address forwarded to a Google server in the USA, and shortened there. On behalf of the providers of this website, Google uses this information to evaluate your use of the website, compile reports about the website activities and supply other services related to the website use and internet use to the website provider.

The IP address transferred from your browser within the framework of Google Analytics is not combined with other Google data. You can prevent the cookies from being saved by the relevant settings of your browser software. However, we would like to point out that in this case it may not be possible to fully use all functions of this website. Moreover, you can prevent Google from collecting cookie-generated data about your use of the website (including your IP address) and from processing these data, if you download and install the browser plug-in available at the following link (http://tools.google.com/dlpage/gaoptout?hl=en).
You will find more information at https://tools.google.com/dlpage/gaoptout or at https://www.google.de/intl/en/policies/ (general information about Google Analytics and data protection). You can find more information here www.google.com/privacy/privacy-policy.html.

DoubleClick by Google
DoubleClick by Google is a service of Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA ("Google"). DoubleClick by Google uses cookies to show you the advertisements which are relevant for you. In the process, a pseudonym identification number (ID) is assigned to your browser to check which advertisements were inserted in your browser and what advertisements were seen. The cookies contain no personal data. The use of DoubleClick cookies only helps Google and our partner websites to place the advertisements based on previous visits to our or other websites on the internet. The information generated by cookies is forwarded by Google to a server in the USA, and saved there for analysis. Google transfers data to third parties only based on legal regulations or within the framework of the commissioned data processing. Google will never combine your data with other data collected by Google.
You can prevent Google from collecting the cookie-generated data about your use of the website and from processing these data, if you download and install the browser plug-in available in the section on DoubleClick deactivation extension: https://www.google.com/settings/ads/onweb/

Google Tag Manager
For the sake of transparency, we would like to point out that we use Google Tag Manager. Google Tag Manager itself does not collect any personal data. Tag Manager simplifies our integration and management of tags. Tags are small code components which are used for, among other things, measuring traffic and visitor behaviour, identifying the effects of online advertisement and social channels, setting up remarketing and organising target groups, as well as testing and optimising the websites. We only use Google Analytics, DoubleClick and HubSpot. If you have performed deactivation, Google Tag Manager will take this deactivation into account. You will find more information about Google Tag Manager at: https://www.google.com/intl/de/tagmanager/use-policy.html

Google AdWords
To draw attention to our services, we place advertisements in Google AdWords. They are inserted according to search queries in Google. On the page with our contact forms, cookies are used to register how many users found us thanks to one of our advertisements. We can optimise our advertisements thanks to the anonymous statistics acquired in this way. Cookies are saved by Google when the advertisement is clicked on and can be prevented by the settings of your browser. In this case, your visit to our website is not included in the anonymous user statistics. You can find more information in Google website statistics.

Google Dynamic Remarketing
We use dynamic remarketing on our website. This is a function of Google AdWords, a service of Google Inc (1600 Amphitheatre Parkway, Mountain View, CA 94043, USA; "Google"). The technology helps us to place automatically generated target-group-oriented advertisements after your visit to our website. The advertisements are focused on the products and services you clicked on or viewed during your last visit on to our website. Google uses cookies to generate interest-based advertisements. If you do not want to receive any user-based advertisements from Google, you can deactivate the placement of advertisements by Google advertisement settings.

You can find more information on how Google uses cookies in the Data Protection Policy of Google (https://www.google.com/intl/en/policies/privacy/).

Facebook Custom Audience
Custom Audiences, a product of Facebook (Facebook Custom Audiences 1601 S. California Avenue, Palo Alto, CA, 94304), is also used on the website within the framework of use-based online advertisement. In principle, a non-reversible and not personal hash total (hash value) from your use data is generated in the process, which can be transferred to Facebook for analysis and marketing purposes. A Facebook tracking pixel is also set up on our website to this end. Information about your activities on the website (e.g. surfing behaviour, sub-pages visited, etc.) is collected in the process. Your IP address is saved and used for the geographical adaptation of advertisements.

You will find more information about the purpose and scope of data collection and subsequent data processing and use, as well as privacy settings in the Data Protection Policy of Facebook (https://www.facebook.com/policy.php)

Twitter Advertising
Tracking Code (pixel) by Twitter (Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA) is also set up on the website within the framework of use-based online advertisement. In principle, a non-reversible and not personal hash total (hash value) from your use data is generated in the process, which can be transferred to Twitter for analysis and marketing purposes. At the same time, a Twitter cookie is set up. Information about your activities on the website (e.g. surfing behaviour, sub-pages visited, etc.) is collected in the process. Your IP address is saved and used for the geographical adaptation of advertisements.
You will find more information about the purpose and scope of data collection and subsequent data processing and use, as well as privacy settings in the Data Protection Policy of Twitter (https://twitter.com/privacy).

New Relic
Data are collected and saved on our website by New Relic of New Relic Inc., New Relic Inc., 188 Spear Street, Suite 1200 San Francisco, CA 94105, USA. User profiles are generated from these data by means of pseudonymised user profiles for the purpose of web analysis. These user profiles are used for the analysis of visitor behaviour and are evaluated for the purpose of improvement and need-based design of our offer. Likewise, technical performance data (e.g., response and loading times) and application data (hardware and software employed) are analysed by measurements to improve our server performance. Cookies can be set up for this purpose. These are text files which are saved on your PC and help to analyse your use of the website. The pseudonymised user profiles are not combined with personal data about the pseudonym owner without an express and separate consent to be given by the person concerned. You can object to the collection and saving of data for the purpose of web analysis at any time with future effect by deactivating cookies in your browser settings. You will find more information at the beginning of Point 8 or in the help section of your browser. The detailed New Relic Data Protection Policy can be found at: https://newrelic.com/privacy.

Optimizely
We set up Optimizely on our website, a service of Optimizely, Inc. ("Optimizely"). Optimizely offers a test method for improvement of website design. Hereby, the original version of our website is tested in comparison to the changed version to find out which one is more effective.
When you visit our website, technical information is evaluated by Optimizely using the data transferred by your browser in the process (for example, type/version of browser, operating system used, our websites visited, including the time spent, previously visited websites). Your IP address is processed only in an anonymised form. You can object to tracking at any time with future effect by deactivating it at the following link: https://www.optimizely.com/en/opt_out/.
You can review the Data Protection Policy of Optimizely here: http://www.optimizely.com/privacy

Pingdom
This website uses Pingdom, a monitoring service supplied by a Swedish company called Pingdom AB, Kopparbergsvägen 8, 722 13, Västerås, Sweden. This service sets up cookies which help to analyse your access, load behaviour ("Performance") and website availability, to improve the load behaviour and presentation of content on the website. If you want to prevent the saving of cookies and their related evaluation by Pingdom, please deactivate the cookies in your browser settings accordingly. You will find more information at the beginning of Point 8.

Social Plugins
This website uses social plug-ins of the following providers
•	Facebook (operator: Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA)
•	Twitter (operator: Twitter Inc., 795 Folsom St., Suite 600, San Francisco, CA 94107, USA)
•	Google+ (operator: Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA)

These plug-ins usually collect data from you by default and transfer them to the server of the relevant provider. After activation, the plug-ins also collect personal data, such as your IP address, and send them to the server of the relevant provider where they are saved. Moreover, activated social plug-ins set up a cookie with a unique identifier when you visit the relevant website. Thus, the providers can also create profiles about your use behaviour. This happens, even if you are not a member of the social network of the relevant provider. If you are a member of a social network of the provider and you are logged in to the social network when you visit this website, your data and information about your visit to this website can be linked to your profile on the social network. We have no influence over the exact scope of data collected about you by the relevant provider. You will find more information about the scope, type and purpose of data processing as well as rights and possible settings to protect your privacy in the data protection policies of relevant providers of social networks. 

These are available at the following addresses:
•	Facebook: http://www.facebook.com/policy.php
•	Twitter: http://twitter.com/privacy/
•	Google+: http://www.google.com/intl/en/privacy/

Pinterest
The "Pin it" button plug-in of Pinterest (Pinterest Inc., 635 High Street, Palo Alto,CA, 94301, USA) is used on this website. When you visit this website, a direct connection is created between your browser and the Pinterest server by the plug-in. Thus, Pinterest can receive information that you have visited this website with your IP address. When you click the "Pin it" button while you are logged in to your Pinterest user account on Pinterest, you can share the contents on this website in the Pinterest network. Thus, Pinterest can allocate the visit to these websites to your user account. We would like to point out that as the website provider we do not learn about the content of the data transferred and their use by Pinterest. To find the Data Protection Policy of the Pinterest service, please visit the following link: http://en.about.pinterest.com/privacy/.

You can change the settings for saving your data, also without having a Pinterest account, here: https://help.pinterest.com/entries/25010303-How-does-Pinterest-use-data-about-other-websites-I-visit-.

AppFlyer
The FightAthlete App uses AppFlyer, an analysis service of AppFlyer Ltd. The information about the use of FightAthlete App generated by AppFlyer is usually transferred to a server outside Germany and saved there. We have activated IP anonymisation in the code of FightAthlete App, so that the IP address of the user is shortened before it is saved by AppFlyer Ltd. Google and AppFlyer Ltd. use the anonymous information on our behalf to evaluate the use of FightAthlete App and to prepare reports for us about your activities in FightAthlete App. You can limit the way in which Google Analytics and AppFlyer function even further in the data protection settings of your device. Moreover, you can send an e-mail to privacy@appflyer.com, if you do not want to allow AppFlyer. You can find more information about data protection at Google here (https://www.google.de/intl/en/policies/privacy/).
Transfer of data to third parties

In principle, your data are not transferred to third parties, unless we have a legal obligation to do so, or forwarding data is necessary for the performance of the contractual relationship, or you have given us express prior consent to forward your data. External service providers and partner companies, such as online payment providers or the shipping companies commissioned with delivery, receive your data only if it is required for execution of your order. But in these cases the scope of the transferred data is limited to the necessary minimum. If our service providers come into contact with your personal data, we guarantee that they observe the regulations of data protection law in the same way. Please also note the relevant data protection policies of the providers.

The relevant service provider is responsible for the content of third-party services, whereby we perform reasonable checks to find out if the service complies with the statutory requirements.

Data security
We have introduced extensive technical and operational safeguards to protect your data against accidental or deliberate manipulations, loss, destruction or access by unauthorised persons. Our security procedures are regularly tested and adjusted to technological progress.

Links to other websites
Our websites may contain links to the websites of other providers. We would like to point out that this Privacy Statement applies only to the website of FightAthlete. We have no influence on or control over the compliance of other providers with the applicable data protection regulations.

Amendments to the Privacy Statement
We reserve the right to amend or adjust this Privacy Statement at any time subject to compliance with the applicable data protection regulations.

Information/revocation/removal and responsible office
Based on the legal regulations in the Dutch Data Protection Act, you may contact us free of charge with questions about the collection, processing and use of your personal data and their correcting, blocking or deleting, or revoking the consent given. We shall respond to all reasonable queries free of charge and as soon as possible based on the applicable law. Moreover, we would like to point out that you have the right to correct false data and delete personal data, if this is not contrary to any statutory retention obligation.

These compiled data protection regulations or the Privacy Statement shall be continuously adjusted in the course of further development of the internet. Amendments shall be announced on this website beforehand. To stay informed about current data protection regulations, you should regularly visit this website.
You can send your queries free of charge to the following responsible office:

FightAthlete
Grote Koppel 37, Amersfoort, The Netherlands
e-mail: support@FightAthlete.training
represented by Company Director Robbin Morsink')
SET IDENTITY_INSERT [dbo].[Privacy] OFF
SET IDENTITY_INSERT [dbo].[Program] ON 

INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (1, N'GLADIATOR', 1, 1, 1, 1, 1, 1, 150, CAST(N'2017-05-08 13:23:35.160' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (2, N'NINJA', 1, 1, 1, 1, 1, 1, 110, CAST(N'2017-04-28 08:57:42.807' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (3, N'WARRIOR', 0, 1, 2, 1, 0, 1, 70, CAST(N'2017-04-29 12:34:08.190' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (4, N'test fewrrry', 0, 1, 2, 2, 1, 1, 0, CAST(N'2017-05-01 16:14:22.517' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (5, N'WARRIOR', 0, 1, 1, 2, 1, 1, 370, CAST(N'2017-05-23 12:53:23.420' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (6, N'Test Complete Program', 1, 1, 1, 1, 1, 1, 80, CAST(N'2017-05-25 15:48:30.457' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (7, N'Complete Program', 1, 1, 1, 1, 1, 1, 100, CAST(N'2017-05-25 18:37:08.843' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (8, N'YOSHIHIRO', 0, 0, 2, 3, 1, 1, 2000, CAST(N'2017-07-17 20:25:30.727' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (9, N'MASAYOSHI', 0, 0, 2, 3, 1, 1, 1950, CAST(N'2017-07-17 20:27:02.210' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (10, N'BUSHIDO', 1, 0, 1, 1, 1, 1, 500, CAST(N'2017-11-15 16:31:32.423' AS DateTime))
INSERT [dbo].[Program] ([Id], [Name], [IsBasic], [IsDeleted], [FrequencyId], [DurationId], [IsActive], [IsNew], [Point], [CreatedOn]) VALUES (11, N'BUDOKAN', 1, 0, 2, 1, 1, 0, 250, CAST(N'2017-11-15 16:37:17.483' AS DateTime))
SET IDENTITY_INSERT [dbo].[Program] OFF
SET IDENTITY_INSERT [dbo].[Program_Level] ON 

INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (5, 2, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (6, 2, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (13, 3, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (14, 3, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (15, 4, 3)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (16, 1, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (17, 1, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (18, 5, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (19, 5, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (20, 6, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (21, 6, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (22, 7, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (23, 7, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (42, 8, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (43, 8, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (44, 8, 3)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (45, 9, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (46, 9, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (47, 9, 3)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (48, 10, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (49, 10, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (50, 10, 3)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (51, 11, 1)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (52, 11, 2)
INSERT [dbo].[Program_Level] ([Id], [ProgramId], [LevelId]) VALUES (53, 11, 3)
SET IDENTITY_INSERT [dbo].[Program_Level] OFF
SET IDENTITY_INSERT [dbo].[Program_Tool] ON 

INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (5, 2, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (6, 2, 2)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (13, 3, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (14, 3, 2)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (15, 1, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (16, 1, 2)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (17, 5, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (18, 5, 2)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (19, 6, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (20, 6, 2)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (21, 7, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (22, 7, 2)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (29, 8, 6)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (30, 8, 5)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (31, 8, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (32, 8, 9)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (33, 8, 8)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (34, 8, 4)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (35, 8, 10)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (36, 8, 7)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (37, 9, 8)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (38, 9, 9)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (39, 9, 6)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (40, 9, 5)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (41, 9, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (42, 9, 10)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (43, 9, 4)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (44, 10, 6)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (45, 10, 5)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (46, 10, 1)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (47, 10, 9)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (48, 10, 8)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (49, 11, 8)
INSERT [dbo].[Program_Tool] ([Id], [ProgramId], [ToolId]) VALUES (50, 11, 9)
SET IDENTITY_INSERT [dbo].[Program_Tool] OFF
SET IDENTITY_INSERT [dbo].[ProgramCycle] ON 

INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (1, 1, 2, N'GLADIATOR', N'Beginner', N'1', 150, 1, CAST(N'2017-04-30 15:12:15.133' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (2, 1, 2, N'GLADIATOR', N'Intermediate', N'1', 150, 0, CAST(N'2017-05-08 13:52:11.513' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (3, 1, 2, N'GLADIATOR', N'Beginner', N'1', 150, 2, CAST(N'2017-05-09 10:17:12.973' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (8, 6, 2, N'Test Complete Program', N'Beginner', N'1', 80, 1, CAST(N'2017-05-25 12:22:26.950' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (9, 6, 2, N'Test Complete Program', N'Beginner', N'1', 80, 2, CAST(N'2017-05-25 12:22:26.950' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (10, 6, 2, N'Test Complete Program', N'Beginner', N'1', 80, 2, CAST(N'2017-05-25 12:22:26.950' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (11, 6, 2, N'Test Complete Program', N'Beginner', N'1', 80, 2, CAST(N'2017-05-25 12:22:26.950' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (12, 7, 2, N'Complete Program', N'Beginner', N'1', 100, 1, CAST(N'2017-05-25 13:19:10.547' AS DateTime), N'1')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (13, 9, 19, N'Program 130617-02', N'Beginner', N'2', 2200, 1, CAST(N'2017-06-15 09:35:52.833' AS DateTime), N'3')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (14, 9, 19, N'Program 130617-02', N'Beginner', N'2', 2200, 2, CAST(N'2017-06-15 10:15:06.840' AS DateTime), N'3')
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (15, 8, 19, NULL, NULL, NULL, 0, 1, CAST(N'2017-06-16 13:11:12.803' AS DateTime), NULL)
INSERT [dbo].[ProgramCycle] ([Id], [ProgramId], [UserId], [ProgramName], [LevelName], [FrequencyName], [Point], [Cycle], [CreatedOn], [DurationName]) VALUES (16, 9, 19, NULL, NULL, NULL, 0, 3, CAST(N'2017-06-16 13:12:20.813' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[ProgramCycle] OFF
SET IDENTITY_INSERT [dbo].[ProgramWorkout] ON 

INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (8, 2, 3, 2, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (9, 2, 3, 5, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (10, 2, 1, 7, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (18, 3, 2, 2, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (19, 1, 1, 1, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (20, 1, 3, 3, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (21, 1, 2, 4, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (22, 5, 3, 1, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (23, 5, 2, 3, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (24, 5, 1, 5, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (25, 5, 6, 5, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (26, 5, 7, 11, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (27, 5, 12, 12, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (28, 6, 4, 1, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (29, 6, 17, 4, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (30, 6, 3, 6, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (31, 7, 4, 2, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (32, 7, 1, 3, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (33, 7, 17, 6, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (79, 8, 21, 1, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (80, 8, 26, 3, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (81, 8, 24, 5, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (82, 8, 33, 8, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (83, 8, 38, 11, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (84, 8, 41, 13, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (85, 8, 39, 16, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (86, 8, 50, 19, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (87, 8, 51, 21, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (88, 9, 25, 1, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (89, 9, 26, 4, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (90, 9, 21, 7, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (91, 9, 23, 9, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (92, 9, 29, 11, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (93, 9, 27, 13, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (94, 9, 38, 15, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (95, 9, 36, 17, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (96, 9, 45, 19, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (97, 9, 50, 21, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (98, 10, 21, 1, N'Strength')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (99, 10, 26, 3, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (100, 10, 29, 6, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (101, 11, 25, 1, N'Circuit')
INSERT [dbo].[ProgramWorkout] ([Id], [ProgramId], [WorkoutId], [Day], [Type]) VALUES (102, 11, 29, 3, N'Circuit')
SET IDENTITY_INSERT [dbo].[ProgramWorkout] OFF
SET IDENTITY_INSERT [dbo].[Quote] ON 

INSERT [dbo].[Quote] ([Id], [Tekst], [Author]) VALUES (1, N'HE WHO CONQUERS HIMSELF IS THE MIGHTIEST WARRIOR', N'Confusius')
INSERT [dbo].[Quote] ([Id], [Tekst], [Author]) VALUES (2, N'HOLD UP YOUR HEAD, YOU WERE MADE FOR VICTORY', N'Anne Gilchrist')
INSERT [dbo].[Quote] ([Id], [Tekst], [Author]) VALUES (3, N'BE STRONG
BE BRAVE
BE HUMBLE', N'George A. Sheehan')
INSERT [dbo].[Quote] ([Id], [Tekst], [Author]) VALUES (5, N'EXCUSES ARE USELESS RESULTS ARE PRICELESS', N'Unknown')
INSERT [dbo].[Quote] ([Id], [Tekst], [Author]) VALUES (6, N'GREATNESS IS A CHOICE', N'Unknown')
SET IDENTITY_INSERT [dbo].[Quote] OFF
SET IDENTITY_INSERT [dbo].[Score] ON 

INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (1, N'Newie', 1, 5000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (2, N'Rookie-A', 5001, 15000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (3, N'Rookie-B', 15001, 30000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (4, N'Rookie-C', 30001, 45000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (5, N'Rookie-D', 45001, 60000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (6, N'Intermediate-A', 60001, 75000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (7, N'Intermediate-B', 75001, 90000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (8, N'Intermediate-C', 90001, 105000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (9, N'Intermediate-D', 105001, 120000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (10, N'Advanced-A', 120001, 135000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (11, N'Advanced-B', 135001, 150000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (12, N'Advanced-C', 150001, 165000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (13, N'Advanced-D', 165001, 180000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (14, N'Expert-A', 180001, 195000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (15, N'Expert-B', 195001, 210000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (16, N'Expert-C', 210001, 225000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (17, N'Expert-D', 225001, 240000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (18, N'Elite-A', 240001, 255000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (19, N'Elite-B', 255001, 270000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (20, N'Elite-C', 270001, 285000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (21, N'Elite-D', 285001, 300000)
INSERT [dbo].[Score] ([Id], [Title], [Start], [Finish]) VALUES (22, N'Master', 300001, 1000000)
SET IDENTITY_INSERT [dbo].[Score] OFF
SET IDENTITY_INSERT [dbo].[TermsOfUse] ON 

INSERT [dbo].[TermsOfUse] ([Id], [Description]) VALUES (1, N'GENERAL TERMS AND CONDITIONS

1. Introduction
We, from FightAthlete want to support any athlete, worldwide, to become strong, lean and mean. We provide killer exercises, game changing workouts and bad ass programs. FightAthlete demands and promotes physical fitness and mental willpower and self-confidence. We are looking forward to welcoming you as part of the FightAthlete community. 

The following General Terms and Conditions of Business set out the legal framework for using FightAthlete and the services that we offer. Therefore, please read these General Terms and Conditions of Business carefully.

2. Scope
2.1 Parties to the contract and subject matter of the contract
These General Terms and Conditions of Business form the basis of the user contract being formed between you and us, FightAthlete (hereinafter referred to as "us" or "we"). The subject matter of this contract is the use, free of charge or for a fee, of the services we offer under the name FightAthlete via our website www.fightathlete.training, other FightAthlete websites or via our software applications (hereinafter referred to individually as "FightAthlete Service" or collectively as “FightAthlete Services” or in general “FightAthlete”). If you would like to have a mobile app to use the FightAthlete Services, please check our website www.fightathlete.training to see if we have a mobile app for your end device and your end device''s operating system.

2.2 Terms and conditions for participating
A condition for opening a user account and using the FightAthlete Services is that you are at least 18 years of age and have full legal capacity. FightAthlete is intended exclusively for consumers. The legal definition of a consumer is every natural person that enters into a legal transaction for reasons that cannot be chiefly attributed to either their commercial or their self-employed occupation. Use of FightAthlete for commercial purposes of any kind is expressly prohibited.

2.3 Additional terms and conditions
We reserve the right to agree to additional terms and conditions for individual FightAthlete Services. We will, however, notify you of this in good time prior to use.

3. FightAthlete Services and Prices
3.1 Services free of charge or for a fee
The scope of the services included in FightAthlete and the FightAthlete Services and available for use by you depends on the type of FightAthlete Service and whether you use the FightAthlete Services free of charge or for a fee. If you use it free of charge you only have access to certain basic functions and information of the respective FightAthlete Service. A more extensive range of functions is available to you if you enable the Exclusive part of the respective FightAthlete Service. 

Please note that in order to use some of the FightAthlete Services to the full extent, certain equipment and training tools may be required. These are not part of the FightAthlete Services and need to be provided or purchased by you separately at your own costs. 

3.2 Prices
Please consult the website www.fightathlete.training for information on the respective current pricing and subscription models and the services that these include. All prices stated include the applicable VAT.

4. Your Health
4.1 Terms and conditions with regard to your health
Use of the FightAthlete Services is at your own risk. 
In any case a condition for the use of the FightAthlete Services is that you must be in a good general state of health. If you have knowledge of any pre-existing medical conditions we advise you to seek medical advice from a doctor urgently before you start the FightAthlete Services (such as trainings or coachings). This applies in particular if you have knowledge of one or more of the following medical complaints/conditions/procedures: (i) cardiovascular disease, (ii) lung or respiratory disease (including asthma), (iii), spinal and/or joint problems, (iv) neuromuscular disease, (v) surgical procedures, (vi) any other health issues. 

In case of FightAthlete Services related to pregnant women, female athletes should note that breast-feeding mothers should not do the trainings and coachings offered by us. 

The following general rules apply: Listen to what your body is telling you. Before using the FightAthlete Services for the first time or while using FightAthlete, if you have any doubts about your health (e.g. because you are experiencing considerable pain, a general malaise, shortness of breath, nausea or dizziness) consult your doctor before starting or continuing with FightAthlete.

4.2 No substitute for medical advice
The services and information offered by FightAthlete and the FightAthlete Services do not constitute medical advice or a doctor''s advice. Nor are they a substitute for a medical examination or treatment by a doctor.

4.3 Training Methods
Fitness advice is subject to constantly evolving knowledge in relation to health science, nutritional science and sports science. Although we base our trainings and nutritional tips on current studies and knowledge, we do not guarantee that these reflect the most up to date research findings or knowledge.

5. User Account
5.1 Registration process
In order to use the FightAthlete Services you must first register and open a user account to use FightAthlete. You can open a user account either directly online at www.fightathlete.training or via our mobile app. We will ask you to accept these General Terms and Conditions of Business and our privacy policy during the registration process. After you register, for security reasons we will first send you an email in which we ask you to verify the registration by clicking on the "Confirm account" field. After you have clicked on this field you will be redirected to a website where we will finally confirm your registration. Only then will the registration process be complete. 

Alternatively you can open a user account by using your Facebook account. The registration process is completed, once you have entered your Facebook account details and clicked the “Confirm” button.

6. Conclusion of a Contract
How the respective contract is formed depends on the method by which you register for FightAthlete for the first time and whether you sign up for additional fee-based services.

6.1 Online registration on the website www.fightathlete.training
When registering on our website www.fightathlete.training, the user contract between you and us is formed after the registration process is fully completed.

6.2 Registration via mobile apps
When registering via mobile apps, the formation of the user contract depends on the rules of the app store supplier (for example Apple, Google, etc.). The contract is generally formed when you click on the "Install" field in the relevant app store and, where necessary, enter your password in question. Please note that in order to use the FightAthlete Services it is still necessary to open a cost-free user account with us.

6.3 Conclusion of a contract for one off additional services for a fee or for subscriptions
You can purchase individual additional services as part of a subscription. If you purchase the additional service via your mobile app, the contract is formed when you click on the field "Buy now", or a similar field, as part of an in-app purchase and, where necessary, enter your password for the app store in question.

6.4 Correction of input errors
If you would like to purchase a subscription via our mobile app as part of an in-app purchase, we will not ask you for any further billing or payment details because you will purchase the service via your account with your app-store supplier. Please contact the relevant app-store supplier if you would like to correct any input errors.

7. Term of Validity
7.1 User contract
The user contract concluded between you and us once you register your account is valid for an indefinite period.

7.2 Subscriptions
Our subscriptions are automatically renewed for the same minimum term that has been selected until you or we cancel them. 

In order to avoid any misunderstanding, please note that the term of a subscription is determined by calendar and is independent from your use or extent of your use of the respective FightAthlete Service.

8. Terms and Conditions of Payment
8.1 Collection of fees
The fee is collected for the relevant minimum term when the contract is concluded for the purchase of a subscription. If the subscription is renewed automatically, the fee is collected via iTunes 24 hours before the start of the respective billing period.

8.2 Payment default
We reserve the right to assert further claims for late payments.

9. Payment Methods
If you purchase FightAthlete services for a fee via in-app purchases, the respective app-store supplier will bill you. Please consult this supplier to find out what payment methods are available. 

If we incur costs and/or expenses because a payment is declined and this is your fault (e.g. because there are insufficient funds in the account or the credit card limit has already been exhausted), then we are entitled to bill you for the actual costs and/or expenses incurred. 
Where there is a legitimate reason, we reserve the right for each purchase to refrain from offering certain payment methods and to specify alternative payment methods.

10. Right to Cancel
10.1 Cancellation policy
If you have entered into a contract for the use of FightAthlete or purchased a subscription, in each case, you are entitled to the following right of withdrawal. You have the right to cancel the contract within 14 days without stating any reasons. The cancellation period runs for 14 days from the conclusion of the contract. 

Consequences of cancellation 
If you cancel the contract we are obliged to refund all the payments that we have received from you, including the delivery costs (except for any additional costs incurred as a result of your choosing a different delivery method to the least expensive standard delivery method that we offer), promptly and at the latest within 14 days from the day on which we received the notification of your cancellation of the contract. For this refund we will use the same payment method that you used for the original transaction unless a different arrangement has been expressly agreed with you; under no circumstances will you be charged any fees in respect of this refund. 

If you asked for the services to start during the cancellation period you must pay us an appropriate amount, equal to the proportion of the services that have already been provided by the time you inform us that you are exercising your right to cancel this contract compared to the full scope of the services covered by the contract. 

End of the cancellation policy.

10.2 Lapse of the right of cancellation
In the case of a contract for the provision of services the right of cancellation lapses if we have provided the service in full and only began to perform the service after you gave your express approval and simultaneously confirmed that you were aware that you would lose your right of cancellation if we had completely fulfilled the contract. 

In the case of a contract for the delivery of digital content that is not stored on a physical data carrier the right of cancellation also lapses if we have begun to perform the contract after you gave your express approval and simultaneously confirmed that you were aware that you would lose your right of cancellation once we had begun to perform the contract.

10.3 Model form for your cancellation

11. Liability for Defects
11.1 Statutory Provisions
Statutory provisions apply to claims due to defective services. Your consumer rights remain unaffected in any case.

11.2 Disclaimer of guarantees
We do not make any representations or guarantees that the use of the FightAthlete Services will bring the training- or other result intended by you. We do not promise a concrete success. Also, the actual training result will depend on factors which cannot be influenced, such as, for example, physical disposition and preconditions. Consequently, results may vary strongly between individuals despite the same use of the FightAthlete Services.

12. Liability
12.1 General
Insofar as you are provided with guides or instructions in connection with the FightAthlete Services it is imperative that you follow them. Otherwise you risk being injured and your general health. 

Insofar as you use equipment or training tools it is your responsibility to ensure that such equipment and tools are in good working condition and installed and/or set up properly. 

You need to observe and respect our health safety notices in clause 4.

12.2 Liability for services provided free of charge
For services provided free of charge, we will be liable, regardless of the legal basis, exclusively for damage due to wilful conduct or gross negligence or the absence of a guaranteed feature. Our liability is not limited for wilful misconduct. In the event of gross negligence or the absence of a guaranteed feature our liability is limited to reasonable, foreseeable damage. Otherwise, our liability is excluded.

12.3 Liability for services provided for a fee
In the case of services provided for a fee we have, regardless of the legal basis, unlimited liability in principle for damage due to wilful conduct or gross negligence or the absence of a guaranteed feature. 
If we breach a material contractual obligation as a result of slight negligence, our liability is limited to reasonable, foreseeable damage. A material contractual obligation is any obligation that is necessary to fulfil the purpose of the contract, and on the fulfilment of which you as the consumer can rely or ought to be able to rely. 

Our liability in the event of any injury to life, limb or health that is our fault remains unaffected by the above-mentioned limitations. 
Otherwise, our liability is excluded.

12.4 Liability of our employees
To the extent that our liability is excluded or limited, this exclusion or limitation also applies to our employees and agents.

12.5 Product liability
Claims under the Dutch Product Liability Act remain unaffected by the above-mentioned liability exclusions or limitations.

13. Rights of Use over FightAthlete Content
Depending on which services have been enabled for you, or which services you have purchased, the services we offer contain content which is protected by copyright or otherwise and we hold the respective rights. We grant you a non-exclusive and non-transferable right to use this content in a non-commercial form within the scope of the contractual provisions. Purely for the avoidance of doubt we draw your attention to the fact that in particular distributing our content or making it publicly available, e.g. on websites other than FightAthlete websites, is not permitted. The right of use will lapse when your access to the respective service is no longer enabled (e.g. after you have cancelled your subscription) or when your user contract ends.

14. Responsibility for User-Generated Content
14.1 Disclaimer of responsibility for third party content
You are solely responsible for content that you post within the FightAthlete Services. We accept no responsibility for this content, nor do we monitor it.

14.2 Compliance with statutory provisions
When supplying your own content you are obliged to comply with all the applicable laws and other legislation of the Federal Republic of The Netherlands. Regardless of whether or not it constitutes a criminal offence, it is prohibited to supply content of a pornographic, sexual, violent, racist, seditious, discriminatory, offensive and/or defamatory nature. 

In addition you are also obliged to refrain from infringing any third-party rights. This applies in particular to personality rights of third parties as well as to third-party intellectual property rights (such as, for example, copyrights and trademark rights). In particular you must also hold the necessary rights over your profile picture or any other picture you post. 

We are entitled to delete or remove any content that is unlawful or that infringes the above-mentioned principles at any time. If you infringe the above-mentioned principles we are entitled to give you a warning or to temporarily block your user account or to cancel the user contract for good cause in accordance with clause 
15.3.

14.3 Indemnification
If you infringe the principles mentioned in clause 14.2 and that this is your fault (i.e. because you acted either negligently or with intent), you are obliged to indemnify us against any third-party claims arising from such infringement. We reserve the right to assert claims for damages and other claims.

15. Ending the Contract
15.1 User contract
You have the right to cancel your user account at any time, without stating any reasons, thereby also ending your user contract. To do this you simply have to select the necessary settings in your profile. Please note that after you have cancelled your user account we will or may delete all the content and training results that you added, and you will no longer have access to content that you already purchased. If at the time of deleting your account you still have a current subscription or have booked an additional service that has not yet expired, any sum that you have already paid will not be refunded, not even on a pro rata basis. 

We are entitled to cancel the user contract without stating any reasons by giving two (2) weeks'' written notice, but no earlier than at the end of the minimum contractual term or at the end of the respective renewal period of your.

15.2 Subscription
Every subscription for a FightAthlete Service must be cancelled individually. You can cancel the respective subscription without stating any reasons at any time effective at the end of the minimum contractual term or at the end of the respective renewal period. Subscriptions purchased via in-app purchase must be cancelled using the settings in the respective app store. If your subscription fee is collected via iTunes a notice period of 24 hours before the end of the minimum contractual term or before the end of the respective renewal period applies for technical reasons. Your user account and any other subscriptions will continue to exist after you have cancelled your subscription. 

We are entitled to cancel your subscription with effect from the end of the minimum contractual term or with effect from the end of the respective renewal period by giving two (2) weeks'' written notice.

15.3 Cancellation for good cause
The right to cancel for good cause remains unaffected in the case of either party. In particular we are entitled to cancel your user contract or your subscription with immediate effect, and to cancel your user account, if you seriously or repeatedly breach the provisions of the user contract and/or these General Terms and Conditions of Business, or if you are in arrears with your payment obligations.

16. Personal Data
We process your personal data in accordance with our privacy policy; you can always retrieve the current version of these at https://www.fightathlete.training/privacy. In particular these data protection provisions govern and explain the extent to which your personal data can be seen by other users and what options you have for controlling its disclosure to other users.

17. Changes to the General Terms and Conditions 
We reserve the right to change or modify these General Terms and Conditions with future effect. We will advise you of the changes by email no later than two (2) weeks before the new version of the General Terms and Conditions is scheduled to enter into force. If you do not object to the validity of the new General Terms and Conditions of Business within such period and continue to use FightAthlete, then the new General Terms and Conditions will be deemed to have been accepted. In the event that you do object, we expressly reserve our rights of ordinary cancellation. We will also advise you again separately of your right to object, the deadline to do so and the legal consequences of your objection or failure to object.

18. Final Provisions
18.1 Applicable law
The relationship between the parties is governed exclusively by Dutch law under exclusion of the UN Convention on Contracts for the International Sale of Goods (CISG). In relation to business transactions with consumers within the European Union, the law of the consumer’s place of residence may also be applicable where such law contains consumer law provisions that it is mandatory to apply.

18.2 Place of jurisdiction
If you do not have a place of general jurisdiction in The Netherlands or in another EU Member State, or if you have moved your permanent place of residence to a country outside the EU after these General Terms and Conditions of Business have entered into effect, or if your permanent place of residence or usual place of residence at the time the complaint is filed is not known, then the exclusive place of jurisdiction for all disputes arising from this contract will be our place of business.

18.3 Language of the contract
The language of the contract is English.

18.4 Severability clause
Should any individual provisions of these General Terms and Conditions be or become invalid in whole or in part, this will not affect the validity of the remaining provisions.

19. Information about the Supplier

FightAthlete is a trade name of:
Happido BV
Email: info@fightathlete.training 
Managing Director: Robbin Morsink')
SET IDENTITY_INSERT [dbo].[TermsOfUse] OFF
SET IDENTITY_INSERT [dbo].[Tool] ON 

INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (1, N'DUMBBELLS', NULL, N'Unavailable')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (4, N'Kettlebells', NULL, N'/Upload/tool_kettlebell.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (5, N'Medicine ball', NULL, N'/Upload/tool_med_ball.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (6, N'Barbell', NULL, N'/Upload/tool_barbell.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (7, N'Swissball', NULL, N'/Upload/tool_swissball.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (8, N'Bodyweight', NULL, N'/Upload/tool_bodyweight.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (9, N'Bosu', NULL, N'/Upload/tool_bosu.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (10, N'Running', NULL, N'/Upload/tool_running.png')
INSERT [dbo].[Tool] ([Id], [Name], [Description], [Photo]) VALUES (12, N'Jump rope', NULL, N'/Upload/tool_jump_rope.png')
SET IDENTITY_INSERT [dbo].[Tool] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (1, N'Admin', N'FightAthlete', N'Male', CAST(N'2017-04-27 00:00:00.000' AS DateTime), N'Bangladesh', N'admin@fightAthlete.com', CAST(N'2017-04-27 00:00:00.000' AS DateTime), 3, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (2, N'James', N'Bond', N'Female', CAST(N'2017-04-27 18:00:00.000' AS DateTime), N'USA', N'james@bond.com', CAST(N'2017-04-28 08:15:36.910' AS DateTime), 1, N'/Upload/Users/2/profile_2.JPG', NULL, CAST(N'2018-03-05 09:52:19.167' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (3, N'Kerstman', N'Kerst', N'Male', CAST(N'1976-01-07 00:00:00.000' AS DateTime), NULL, N'ferry@softwareness.nl', CAST(N'2017-05-01 00:00:00.000' AS DateTime), 2, N'Unavailable', NULL, NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (5, N'First', N'Last', N'Female', CAST(N'2017-05-17 18:00:00.000' AS DateTime), NULL, N'Email@gmail.com', CAST(N'2017-05-18 09:04:28.203' AS DateTime), 1, N'/Upload/Users/5/Atish.JPG', N'c9sHPGMVtAI:APA91bGjUdy16TjgkZeZhKcFtdYSM2OAcaxUP3VGDnqhiyUQfZO1krF1lR7L_OOZuAVVMODd_-v2y7e3KpWGvZ46XuV4kQ3cgCAClUOxxgHUnpwiEbFgT6ttL9izh_Cm4CzW6EIWAizg', CAST(N'2017-05-13 00:00:00.000' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (10, N'atish', N'dipongkor', N'Gender : Male', CAST(N'2017-06-06 00:00:00.000' AS DateTime), N'USA', N'atish@bs-23.com', CAST(N'2017-06-06 09:05:42.127' AS DateTime), 1, N'/Upload/Users/10/profile_10.JPG', NULL, CAST(N'2017-06-14 11:45:40.697' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (15, N'bs', N'test', N'        Male', CAST(N'2017-06-09 00:00:00.000' AS DateTime), N'Netherlands', N'atish2@bs-23.com', CAST(N'2017-06-09 13:15:35.620' AS DateTime), 1, NULL, N'f8jzlDtwTzw:APA91bGOiHthaGTux24wrGcEsS5v9F5TDxj6UBRECSCgHs6ET7E0P16iLobl4u4r9AhQDpifgpGbpuJXjH9xxGKhO5FYeHlcljWbxvKWkerQAs0JMGHRIuvopa2IsYCgGXdAyhxgpjzm', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (16, N'Rubel', N'hossain', N'        Male', CAST(N'2017-06-09 00:00:00.000' AS DateTime), N'Netherlands', N'hossain@gmail.com', CAST(N'2017-06-09 13:31:03.053' AS DateTime), 1, NULL, N'fx_NlREhxOQ:APA91bFvLqjTqD-qbu1apN7wXuQ_nAfV0COncLCy6fBEH6hItyV9CYtCEMMRru02AB8PXUcVuFT77jMCE37_YptEGPfeNJv1xEdh8iqXz2SrynKuiQDzYR1YN8Z94cDAohDX-YxVQbnh', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (18, N'gfds', N'asdf', N'Female', CAST(N'2017-06-13 18:00:00.000' AS DateTime), N'Netherlands', N'asdf@gmail.com', CAST(N'2017-06-14 07:33:14.727' AS DateTime), 1, NULL, N'eSxAbzJYoBE:APA91bGPUaGZ4i6fQbDgt3gICGWLa21UoO5XzDxA2m8ZcnEZjJzmOAdR40HVe1YHDLzilQ6PIg5kaOrFfbF_nD6XeUXLEjqYxWK68qm67Er2daThEs-Hsrj4WC1Es_gItc-2PKkXFEyR', CAST(N'2017-06-14 07:30:49.923' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (19, N'ami', N'asif', N'Gender : Female', CAST(N'2017-06-13 18:00:00.000' AS DateTime), N'USA', N'asif@gmail.com', CAST(N'2017-06-14 11:35:04.140' AS DateTime), 1, N'/Upload/Users/19/profile_19.JPG', NULL, CAST(N'2017-06-16 14:22:45.517' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (20, N'ak', N'bser', N'Male', CAST(N'2017-06-14 18:00:00.000' AS DateTime), N'Netherlands', N'atish@bs23.com', CAST(N'2017-06-15 08:24:09.230' AS DateTime), 1, NULL, N'cIvV_BvdnAM:APA91bEumMb4c5Ygzqx2IBIxsyZdoC0JFWbc_T7-r03-RuAyH5caZ8y6qQiCqBRfHj_JZ9YMV8EgrFS97UZtDDCFfwxS8GqQ_Lk-nUUALAIBy7PCCeIN5Q6z71coSp3rPcoTdLWsP4xd', CAST(N'2017-06-15 08:21:40.897' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (27, N'Atish', N'Dipongkor', N'male', CAST(N'1992-08-01 00:00:00.000' AS DateTime), N'Dhaka, Bangladesh', N'atishiitdu@hotmail.com', CAST(N'2017-06-15 14:27:18.420' AS DateTime), 1, NULL, N'enxMKLVgIYs:APA91bF4xGJ7TFdGxrsDGMgu-DP2PMPDIcwnHuIlNlmMHmItnu74HY9YYVmOOULd0XF7PQLZ6nBNTCbqg_zBFATzv9O70h9VzHO1HitlCbQNGHy6_Yu6Ld2_2OBoH8z5bvFI9g7yRB-S', CAST(N'2017-09-07 09:16:33.143' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (28, N'dgh', N'dfbj', N'Male', CAST(N'2017-06-14 18:00:00.000' AS DateTime), N'Netherlands', N'atish@hbs-23.com', CAST(N'2017-06-15 15:13:33.393' AS DateTime), 1, NULL, N'dLfmu0N4c_k:APA91bFLnEbHPNF2n5JwCFuMW0c9j8_hTWGlVxqfU11yyP0aJdk2PvjxffwZutgnIj5GZ5UU3AwDSTrFjW4B1MTMarVYOj4gYF_rnMtUNVwULnAQKlxI-acdVMtf5Wy16L6_qhf_h7jS', CAST(N'2017-06-15 15:12:42.923' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (29, N'Robbin ', N'Morsink ', N'Female', CAST(N'2017-06-14 22:00:00.000' AS DateTime), N'Netherlands', N'info@happiness360.nl', CAST(N'2017-06-15 21:06:26.847' AS DateTime), 1, N'/Upload/Users/29/profile_3.JPG', NULL, CAST(N'2017-08-08 09:55:32.817' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (31, N'mazadul', N'Islam', N'Male', CAST(N'2017-06-17 18:00:00.000' AS DateTime), N'Netherlands', N'mazadul@brainstation-23.com', CAST(N'2017-06-18 04:53:45.010' AS DateTime), 1, NULL, N'eH5YjisKGv4:APA91bGxw8U4wxqFjVJWoma0lvA6kZstoCmaoEtvgKKakTSu_SEIrLuiFZDOWpZ6DHEDD5SDewzXFf1BJ_NfjuChnWTGx7D-DmcyI-xCi1Ej-LnU0EqpjScvUQqE-bYbimqxDU38eHej', CAST(N'2017-06-18 04:52:54.820' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (32, N'mazadul', N'islam', N'Male', CAST(N'2017-06-17 18:00:00.000' AS DateTime), N'Netherlands', N'mazadul@ymail.com', CAST(N'2017-06-18 05:04:20.897' AS DateTime), 1, NULL, N'', CAST(N'2017-06-18 05:03:29.477' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (36, N'Fight', N'Athlete', N'Male', CAST(N'2017-06-18 18:00:00.000' AS DateTime), N'USA', N'fight@gmail.com', CAST(N'2017-06-19 11:58:01.970' AS DateTime), 1, N'/Upload/Users/36/profile_1.JPG', NULL, CAST(N'2017-06-23 11:47:37.217' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (37, N'fhh', N'fgg', N'Male', CAST(N'2017-06-18 18:00:00.000' AS DateTime), N'157', N'fff@bs23.com', CAST(N'2017-06-19 12:01:03.367' AS DateTime), 1, NULL, N'cSmedyvOHdQ:APA91bEsfGE51-cw5JjVzqgOBXKFHopTO-Qu3r1MGxZkJeQROy74WxpPiUO7ysCdxSvGn7Tar5tSq44y0f2-dXaC5LXgBMBtO53mv7xlchrb_OJe_lX8kKO1jwJwABgCnTPgWJB0yyBk', CAST(N'2017-06-19 12:01:18.210' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (38, N'ghh', N'fgg', N'Male', CAST(N'2017-06-18 18:00:00.000' AS DateTime), N'157', N'cghh@bs23.com', CAST(N'2017-06-19 12:05:33.017' AS DateTime), 1, NULL, N'cSmedyvOHdQ:APA91bEsfGE51-cw5JjVzqgOBXKFHopTO-Qu3r1MGxZkJeQROy74WxpPiUO7ysCdxSvGn7Tar5tSq44y0f2-dXaC5LXgBMBtO53mv7xlchrb_OJe_lX8kKO1jwJwABgCnTPgWJB0yyBk', CAST(N'2017-06-19 12:05:33.487' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (39, N'Robbin', N'Morsink ', N'Male', CAST(N'2017-06-18 22:00:00.000' AS DateTime), N'Netherland', N'info@fight-athlete.com', CAST(N'2017-06-19 13:47:27.180' AS DateTime), 1, NULL, N'cHkGvijjRVw:APA91bG8JHA2c-PJKddj4-9h2YazIQhL-8alCTe2PAmHZ6fzth54GAZ0zEJw3D-eLHjxdWbMuWfWrO1TnqULJYxdebCFTqZD45vKIcTF_qC94VMqLOY2chW4dhZBye5DDdBInFTpAduu', CAST(N'2017-06-19 13:47:24.767' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (40, N'hr', N'mr', N'Female', CAST(N'2017-06-19 18:00:00.000' AS DateTime), N'Netherlands', N'md@g.com', CAST(N'2017-06-20 09:19:09.303' AS DateTime), 1, NULL, N'', CAST(N'2017-06-20 09:18:55.617' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (41, N'test', N'user', N'Male', CAST(N'2017-06-19 18:00:00.000' AS DateTime), N'Netherlands', N'test@g.net', CAST(N'2017-06-20 12:40:30.747' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (44, N'qwer', N'asdf', N'Male', CAST(N'2017-06-22 18:00:00.000' AS DateTime), N'Netherlands', N'qwe@gmail.com', CAST(N'2017-06-23 11:27:22.140' AS DateTime), 1, NULL, N'e-n3F9BfjE0:APA91bGTYqaDm853Vutf7eIlK0oX7L3XEBhg_v0tilroRYWHr_dLCL-k33Z7YDL1N3D3Pd_LnIr_r7njq7QCJ6PusqNXDi7qbOcfs59pgdvleG_SFjL4HctwW0L6Nf9XHvXioTOmI4X5', CAST(N'2017-06-23 11:27:00.883' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (46, N'gfgvv', N'dfgh', N'Male', CAST(N'2017-06-22 18:00:00.000' AS DateTime), N'Netherlands', N'dfgf@g.com', CAST(N'2017-06-23 12:07:34.450' AS DateTime), 1, N'/Upload/Users/46/profile_2.JPG', NULL, CAST(N'2017-06-23 12:10:01.807' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (47, N'asdf', N'asdf', N'Male', CAST(N'2017-06-22 18:00:00.000' AS DateTime), N'Netherlands', N'asdfh@gmail.com', CAST(N'2017-06-23 12:32:54.630' AS DateTime), 1, N'/Upload/Users/47/profile_3.JPG', NULL, NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (48, N'asdf', N'asdf', N'Male', CAST(N'2017-06-22 18:00:00.000' AS DateTime), N'Bangladesh', N'asdfhh@gmail.com', CAST(N'2017-06-23 12:40:28.607' AS DateTime), 1, N'/Upload/Users/48/profile_3.JPG', NULL, NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (50, N'poiz', N'poiu', N'Male', CAST(N'2017-06-22 18:00:00.000' AS DateTime), N'USA', N'poiu@gmail.com', CAST(N'2017-06-23 15:21:43.597' AS DateTime), 1, N'/Upload/Users/50/profile_2.JPG', NULL, NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (51, N'first', N'last', N'Male', CAST(N'2017-07-02 18:00:00.000' AS DateTime), N'Netherlands', N'first@last.com', CAST(N'2017-07-03 09:56:24.260' AS DateTime), 1, NULL, N'', CAST(N'2017-07-03 11:26:06.103' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (52, N'mazadul', N'Islam', N'Male', CAST(N'2017-07-02 18:00:00.000' AS DateTime), N'Netherlands', N'mazadul06@gmail.com', CAST(N'2017-07-03 10:31:47.270' AS DateTime), 1, NULL, N'dF_MiploS8I:APA91bHMqwucFL_dmCeeL8I0UtbwrUxNd_VpJFeYDHFjw6-rWfXSn8UGUPfnSEC1xyxGGS2x4U5gqlgLrBm8MoGziYkij3dO6e8KSePzheJpL9hmqoNEIWzq7EY8-iurEqdiYmAzol0-', CAST(N'2017-07-04 04:33:21.310' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (58, N'Atish', N'Dipongkor', N'male', CAST(N'1992-08-01 00:00:00.000' AS DateTime), N'Dhaka, Bangladesh', N'atishiitdu@hotmail.com', CAST(N'2017-07-04 12:04:45.593' AS DateTime), 1, NULL, N'e6GBfu-Yrvw:APA91bFDYGCw2pJLPNdNLi87cfYoFJ9o1RpQBQXmDehEtr7h32CZD5szF2AA2sycUdPPFfVtYONIaeSxM15ckJPHr4qg0IzLNmbnk1cnzLbyDIBn6wTs3caIs6W5jTltOiWd21u7xAlS', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (59, N'Michel', N'Uiterwijk', N'Male', CAST(N'2017-07-11 22:00:00.000' AS DateTime), N'Netherlands', N'mike@ddsw.nl', CAST(N'2017-07-12 13:23:43.377' AS DateTime), 1, NULL, N'dxE-Ai6KhJY:APA91bHi_zElfxS7c65uK3XDufUaWpimMgKyp2Hvs0b3dJA82xWDBezSmIsY4eZL6d3X8XqSdi4xqR11DIrf0pWatdrnGC_sDxpNQ9qxVg2GCojtlBFczf_Vpr40B88nl5iMpv378B3N', CAST(N'2017-07-12 13:23:34.323' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (61, N'Saskia', N'Schouten', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'saskiaschouten@kpnmail.nl', CAST(N'2017-07-18 13:57:15.143' AS DateTime), 1, NULL, N'', CAST(N'2017-08-18 23:07:56.980' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (62, N'Olle ', N'De Graaf', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'Olledegraaf@live.nl', CAST(N'2017-07-18 15:06:26.980' AS DateTime), 2, NULL, N'', CAST(N'2017-07-24 06:45:47.810' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (64, N'Ferry', N'van Maurik', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'Ferryvanmaurik@msn.com', CAST(N'2017-07-18 15:35:36.683' AS DateTime), 2, NULL, N'', CAST(N'2017-09-21 16:08:14.813' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (65, N'darryll', N'mendoza', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'd.mendoza.pt@gmail.com', CAST(N'2017-07-18 16:59:36.777' AS DateTime), 2, NULL, N'dJqIT6nlYZg:APA91bH6GIqKCNyoF831CebCm6-cSD2HPi8thCyLkXMbLaKgvv9dCEBsBmdTNYomyNa8Cvct71UuNKXBVKRjAqhZHSyCIHhZQkXufnS20r1f3iriEW7FTv_TZLkj4KX2yEgbu6mGLCZZ', CAST(N'2017-11-10 22:29:31.577' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (66, N'Jori', N'Goené', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'jori.goene@gmail.com', CAST(N'2017-07-18 17:11:54.347' AS DateTime), 2, NULL, N'fIk4coHkVu0:APA91bFLwGdR8bbdiE4wCewwqmFKA-ZvFSyu6ctQGLQ8NxrBCqknp8Tbh6MqrnYkOPAUGoExvG2cLLsHTC2YVDahk-BvyHyTg8oqrIO8oPWf6D3fR9INa_trXdcaVz-zKxOsFb7A6nQY', CAST(N'2017-07-31 09:39:47.487' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (67, N'marije', N'spanninga', N'Female', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'm.n.s@live.nl', CAST(N'2017-07-18 17:40:02.977' AS DateTime), 2, NULL, N'cNbiW1BcVxo:APA91bFuQmVMrCavxJFq6SBZJXJxDT8FeXAN98twt_f6EbjtdQcv5Hx8PeqDJ9pFvPAaz5gZJhHv8OG-4CkMamfaxwnadafYmOKqyKIvaE61LpKPhKtrE_h0masom3sDc-yp_cA72daA', CAST(N'2017-07-21 07:35:35.587' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (68, N'Willex', N'Duijst', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'willex@me.com', CAST(N'2017-07-18 17:44:41.427' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (69, N'luc', N'lips', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'luc.lips@live.nl', CAST(N'2017-07-18 19:13:01.457' AS DateTime), 1, NULL, N'cxIyyTx-n10:APA91bGwLaLjd-9I0C18YK2OTR5kzwqwNm0EBDmjaJXYZZBbDEmdHhX0K1IKpSRE0PuUFC4ohD-Rq54X-CvLP5b_7110pwDXYU8mUpeiDM0MYecRO7YsiapiJ6f6VE9RL0NyUF1rkBFY', CAST(N'2017-07-18 19:12:54.470' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (70, N'Nick', N'Kraaijenhof ', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'nickkraaijenhof190@hotmail.com', CAST(N'2017-07-18 19:36:33.290' AS DateTime), 1, NULL, N'dk27s8QH4rk:APA91bEmgVKN4Qj6jdCDycmEAcJfsn_y943ITSmzjJ_lov4J7Qc8EJQ73-AiiU_K0b4gQ6b0USoyFdYRn8Es5VYftaDv04dURt_rsX_NyjkNhJhUCTgiIASMuw2hT00nzWNph7vHu6te', CAST(N'2017-07-24 07:54:55.030' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (71, N'Bas', N'Lacor', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'baslacor@gmail.com', CAST(N'2017-07-18 20:03:53.457' AS DateTime), 2, NULL, N'', CAST(N'2017-09-07 12:44:17.187' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (72, N'Jessica', N'Lacor', N'Female', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'Jessica@jessica.nl', CAST(N'2017-07-18 20:11:58.820' AS DateTime), 2, NULL, N'', CAST(N'2017-07-21 05:28:12.173' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (73, N'Ralph', N'van der Meer', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'Ralph.van.der.meer@kpnmail.nl', CAST(N'2017-07-18 20:29:45.930' AS DateTime), 1, NULL, N'', CAST(N'2017-07-18 20:29:37.693' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (74, N'Ruben', N'Kruithof ', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'ruben_3@live.nl', CAST(N'2017-07-18 20:55:17.797' AS DateTime), 1, NULL, N'dBFELAiIDdQ:APA91bEnnPTzMQq02iorccjf4a3TqR_Qind8_-DoSP3OSxPs5iR4MnhPb9HsPw1xokBhBRmgX4lUDjEDav7fVFJZFEQnZV1vY0NxK6K8RwjyIKWz2TFoASiHIp12cEU5zCt5UkMQiM3M', CAST(N'2017-08-09 06:32:23.727' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (75, N'Paulina', N'Seropian', N'Female', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'paulinaseropian@gmail.com', CAST(N'2017-07-18 21:16:04.250' AS DateTime), 1, NULL, N'', CAST(N'2017-07-30 17:43:30.793' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (76, N'Rayen', N'Bindraban', N'Male', CAST(N'2017-07-17 22:00:00.000' AS DateTime), N'Netherlands', N'Rayen@reat.nl', CAST(N'2017-07-18 21:45:16.023' AS DateTime), 1, NULL, N'', CAST(N'2017-08-03 08:33:19.233' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (77, N'Colin', N'Wallet', N'Male', CAST(N'2017-07-18 22:00:00.000' AS DateTime), N'Netherlands', N'Colinwallet@msn.com', CAST(N'2017-07-19 09:16:58.213' AS DateTime), 1, NULL, N'', CAST(N'2017-07-21 05:54:12.107' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (78, N'Rami', N'Zeyada', N'Male', CAST(N'2017-07-18 22:00:00.000' AS DateTime), N'Netherlands', N'ramizeyada@gmail.com', CAST(N'2017-07-19 09:25:45.117' AS DateTime), 1, NULL, N'', CAST(N'2017-07-19 11:27:42.920' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (79, N'Bas', N'Boone', N'Male', CAST(N'2017-07-18 22:00:00.000' AS DateTime), N'Netherlands', N'Sjboone@live.nl', CAST(N'2017-07-19 13:56:06.943' AS DateTime), 1, NULL, N'', CAST(N'2017-07-19 15:42:57.543' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (80, N'Roël ', N'Mannaart', N'Male', CAST(N'2017-07-18 22:00:00.000' AS DateTime), N'Netherlands', N'Roel@mejirogym.nl', CAST(N'2017-07-19 14:05:28.413' AS DateTime), 1, NULL, N'', CAST(N'2017-07-19 14:05:18.107' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (81, N'Willem', N'Gouw', N'Male', CAST(N'2017-07-19 22:00:00.000' AS DateTime), N'Netherlands', N'Willemgouw@hotmail.com', CAST(N'2017-07-20 18:57:20.143' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (82, N'S', N'Zwartepoorte', N'Female', CAST(N'2017-07-19 22:00:00.000' AS DateTime), N'Netherlands', N'Susanzwartepoorte@gmail.com', CAST(N'2017-07-20 18:58:12.183' AS DateTime), 1, NULL, N'', CAST(N'2017-07-20 21:14:33.993' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (83, N'Willex', N'Duijst', N'Female', CAST(N'2017-07-20 22:00:00.000' AS DateTime), N'Netherlands', N'willex@bu3.nl', CAST(N'2017-07-21 12:08:05.573' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (84, N'Rob', N'Test ', N'Male', CAST(N'2017-08-07 22:00:00.000' AS DateTime), N'Netherlands', N'Info@peace360.nl', CAST(N'2017-08-08 11:21:09.390' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (85, N'Rob', N'Mors', N'Male', CAST(N'2017-08-09 22:00:00.000' AS DateTime), N'Netherlands', N'Info@zen6es.com', CAST(N'2017-08-10 16:29:03.247' AS DateTime), 1, N'/Upload/Users/85/profile_3.JPG', NULL, CAST(N'2017-12-08 09:40:25.577' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (86, N'mike', N'uit', N'Male', CAST(N'2017-10-10 22:00:00.000' AS DateTime), N'Netherlands', N'michel.uiterwijk@gmail.com', CAST(N'2017-10-11 14:51:45.723' AS DateTime), 2, NULL, N'dGjSDt6vK6Y:APA91bEx2m9prIWnXmc6_gVqcjSvJFmnW-v6zh7ICAD0CUzs6vn7fuezut5xrlEUjrupslNOgBtQcnGTRcl2Rb59HDtuh8Aof2TCdhso7mEUqyjT0cx2mxqiS9Cv4GMiogRVgHjEFYRg', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (87, N'Ferry', N'van Maurik', N'Male', CAST(N'2017-10-11 22:00:00.000' AS DateTime), N'Netherlands', N'ferry.van.maurik@nike.com', CAST(N'2017-10-12 09:39:31.157' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (89, N'Rob', N'Mors', N'Female', CAST(N'2017-10-19 22:00:00.000' AS DateTime), N'Netherlands', N'robbin@happido.com', CAST(N'2017-10-20 13:03:44.283' AS DateTime), 2, NULL, N'', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (90, N'Ron', N'Goene', N'Male', CAST(N'2017-11-19 23:00:00.000' AS DateTime), N'Netherlands', N'ron@ergonline.eu', CAST(N'2017-11-20 15:48:48.467' AS DateTime), 1, NULL, N'ce7cFvd_Xrs:APA91bELE6HJleMX-vCJNpA8SOK8JFb9P3hcP9jRs357h_TF-sRR3nseRCGDUlgQ2aNO_HWN-cEv6z1yMZE4QViAt3p6gC7aFIE70yc-IK5hl4p99enHJva6KW5Su_k4GJESbeDDw5_U', CAST(N'2017-11-20 15:48:48.113' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (91, N'Test', N'Back', N'Male', CAST(N'2017-11-27 23:00:00.000' AS DateTime), N'Afghanistan', N'Test@test.com', CAST(N'2017-11-28 08:54:17.560' AS DateTime), 1, NULL, NULL, CAST(N'2017-12-12 10:35:09.683' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (94, N'Piyawan', N'Luangpiboon', N'Female', CAST(N'2017-12-06 23:00:00.000' AS DateTime), N'Netherlands', N'gapoog_445@hotmail.com', CAST(N'2017-12-07 21:48:37.517' AS DateTime), 2, NULL, N'', CAST(N'2017-12-07 21:49:23.170' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (95, N'Robbin', N'Morsink', N'Male', CAST(N'2017-12-07 23:00:00.000' AS DateTime), N'Netherlands', N'robbin@fight.com', CAST(N'2017-12-08 10:04:03.687' AS DateTime), 1, NULL, N'', CAST(N'2017-12-08 10:05:08.443' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (97, N'Rob', N'Mors', N'Male', CAST(N'2017-12-07 23:00:00.000' AS DateTime), N'Netherlands', N'rob@fight.nl', CAST(N'2017-12-08 10:24:37.697' AS DateTime), 1, N'/Upload/Users/97/profile_1.JPG', NULL, CAST(N'2018-01-21 15:52:00.173' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (98, N'Rob', N'Mors', N'Male', CAST(N'2018-01-04 23:00:00.000' AS DateTime), N'Netherlands', N'rob@fight2018.com', CAST(N'2018-01-05 14:03:09.547' AS DateTime), 1, NULL, N'', CAST(N'2018-01-05 14:03:04.617' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (99, N'Rib', N'morsink', N'Male', CAST(N'2018-01-08 23:00:00.000' AS DateTime), N'Netherlands', N'rob@fightathlete.com', CAST(N'2018-01-09 13:34:45.203' AS DateTime), 2, NULL, N'', CAST(N'2018-01-09 13:41:24.313' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (100, N'ghffg', N'ghh', N'Male', CAST(N'2018-01-10 23:00:00.000' AS DateTime), N'Netherlands', N'infi@jaja.nl', CAST(N'2018-01-11 20:47:03.373' AS DateTime), 2, NULL, N'f3ogJE3mXos:APA91bFWj5f6CFx8xnUUDJXRL647zAB2ecWBS7TUyV5NboiOXTjFezRJHjivejFB1c4lHnmnldRUAp_bDtGz--oqECH5nuk46YCGNbs0A6TLseXNuOf6jZiTLWP_kptf25rayy4Atz4u', NULL)
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (101, N'Ron', N'Goene', N'Male', CAST(N'2018-01-16 23:00:00.000' AS DateTime), N'Netherlands', N'r456goene@gmail.com', CAST(N'2018-01-17 18:04:39.327' AS DateTime), 1, N'/Upload/Users/101/profile_3.JPG', N'cn37ZjCVxVI:APA91bF_Z4ZebHcNm-JYFdOrgphLJ4rwHel2pL1FISI9sIq8A5F7WNskBQWkMc3l8XwL7B3H5yexXeQX4o1WzTcXMX07Dw8ISxkCpAHKLJJN-G6tRQdlhZ-Tde_fySNN7FsooN_hsgBc', CAST(N'2018-01-17 18:05:11.107' AS DateTime))
INSERT [dbo].[User] ([Id], [Name], [Surname], [Gender], [Birthdate], [Country], [EmailAddress], [MembershipSince], [MembershipTypeId], [Photo], [GcmRegistrationToken], [LastLogin]) VALUES (102, N'asd', N'lkj', N'Male', CAST(N'2018-01-21 18:00:00.000' AS DateTime), N'Netherlands', N'asdf@gh.com', CAST(N'2018-01-22 12:45:44.567' AS DateTime), 1, NULL, N'cLfWOJJTLic:APA91bFoSeNLw9aSd44hjSrgA0OsuHD32XqOS-QSYYXSk1-3JV3IFyq22w1-OGWlGUiDnM6FOsnWtC7Qhr4ZaceqMD73undau9jjjNUG_2SaEKZ8N1yZcuSFhyfWs84gOz9hgw43hpUG', CAST(N'2018-01-22 12:45:44.067' AS DateTime))
SET IDENTITY_INSERT [dbo].[User] OFF
SET IDENTITY_INSERT [dbo].[Workout] ON 

INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (1, N'Bench Press', N'Circuit', N'Bench Press as quick as possible', 50, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-05-01 12:16:59.657' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (2, N'Bent-Over Row', N'Strength', N'Bent-Over Row to highest limit', 70, 0, N'Unavailable', 1, 0, 1, 0, CAST(N'2017-05-19 20:25:21.420' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (3, N'Deadlift Battle', N'Circuit', N'Take it as a challenge and push the body to the limit', 30, 1, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-04-29 10:11:49.150' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (4, N'Test circuit', N'Circuit', N'Test the circuit workout', 30, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-05-17 20:15:03.927' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (5, N'test ferry', N'Strength', N'rhrth', 0, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-04-05 16:13:23.477' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (6, N'Dumbell Workout', N'Circuit', NULL, 20, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-05-02 13:26:39.657' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (7, N'Test 080517', N'Circuit', N'As fast as possible', 100, 1, N'Unavailable', 1, 1, 1, 0, CAST(N'2017-05-23 15:50:07.803' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (8, N'Test 080517-2', N'Circuit', NULL, 0, 0, N'Unavailable', 0, 0, 1, 0, CAST(N'2017-05-08 14:18:54.447' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (9, N'Test 080517-3', N'Circuit', NULL, 0, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:19:32.153' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (10, N'Test 080517-4', N'Circuit', NULL, 1000, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:19:51.370' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (11, N'Test 080517-5', N'Circuit', NULL, 0, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:20:13.643' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (12, N'Sprint workout', N'Circuit', NULL, 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:34:41.697' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (13, N'Sprint workout 2', N'Circuit', NULL, 0, 0, N'Unavailable', 0, 1, 1, 1, CAST(N'2017-05-08 14:40:21.887' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (14, N'Sprint workout 3', N'Circuit', NULL, 0, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-05-08 14:42:59.660' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (16, N'Sprint workout 4', N'Circuit', N'goal goal', 40, 0, N'/Upload/1-burpee_1.jpg', 0, 1, 1, 1, CAST(N'2017-05-08 14:54:16.070' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (17, N'Test Strength', N'Strength', NULL, 20, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-05-18 17:14:28.717' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (18, N'TEST BATTLE', N'Circuit', NULL, 1000, 1, N'Unavailable', 0, 1, 1, 0, CAST(N'2017-05-24 21:17:18.713' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (19, N'Rest Workout', N'Circuit', NULL, 50, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-06-07 15:11:46.100' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (20, N'Rest workout', N'Circuit', N'test rest', 30, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-06-09 10:54:49.757' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (21, N'TETSUYA', N'Strength', N'BECOME STRONG', 250, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 19:17:52.927' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (22, N'HIROKI', N'Circuit', N'TEST', 100, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-07-10 07:29:08.250' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (23, N'AKIRA-B', N'Circuit', N'TEST', 100, 1, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 18:29:58.333' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (24, N'ARATA-B', N'Circuit', N'TEST', 500, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 18:34:26.237' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (25, N'YUUDAI', N'Circuit', N'RUN', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 19:49:25.873' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (26, N'HIROYUKI', N'Circuit', N'TEST', 100, 0, N'Unavailable', 1, 0, 1, 0, CAST(N'2017-11-08 12:32:06.450' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (27, N'KICHIROU', N'Circuit', NULL, 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 19:04:20.520' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (28, N'YUTAKA', N'Circuit', N'RUN AS FAST AS POSSIBLE', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 20:21:38.887' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (29, N'NOBORU', N'Circuit', N'BE FAST', 150, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 19:09:50.037' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (30, N'YASUSHI', N'Circuit', N'Finish asap', 100, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 19:25:17.100' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (31, N'TAKUMI', N'Circuit', N'FINISH THE CIRCUIT AS QUICK AS POSSIBLE. FOCUS ON GOOD TECHNIQUE.', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 19:12:35.193' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (32, N'TAMOTSU', N'Circuit', N'Finish it!!', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-10 08:01:28.510' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (33, N'SANSUI 5K', N'Circuit', N'RUN 5K AS FAST AS POSSIBLE', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-10 07:55:32.357' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (34, N'SANSUI 10K', N'Circuit', N'RUN 10K AS FAST AS POSSIBLE', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-10 07:56:16.367' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (35, N'SANSUI 15K', N'Circuit', N'RUN 15K AS FAST AS POSSIBLE', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-10 07:56:45.850' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (36, N'GYO 1K', N'Circuit', N'ROW 5K AS FAST AS POSSIBLE', 100, 0, N'Unavailable', 0, 1, 1, 1, CAST(N'2017-07-10 07:59:13.037' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (37, N'GYO 2K', N'Circuit', N'ROW 2K AS FAST AS POSSIBLE', 0, 0, N'Unavailable', 0, 1, 1, 1, CAST(N'2017-07-10 07:59:46.863' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (38, N'YASUHIRO', N'Strength', N'INCREASE THE WEIGHT OF EACH EXERCISE', 250, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 18:21:27.137' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (39, N'TOMIO', N'Circuit', N'FINISH THE CIRCUIT AS QUICK AS POSSIBLE', 100, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 19:21:37.347' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (40, N'rest workout', N'Circuit', N'rest', 20, 0, N'Unavailable', 1, 1, 1, 1, CAST(N'2017-07-11 09:08:58.883' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (41, N'DAICHI', N'Circuit', N'FINISH THE WORKOUT AS QUICK AS POSSIBLE', 100, 0, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 19:06:07.570' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (42, N'AKIRA-I', N'Circuit', N'FINISH THE CIRCUIT AS QUICK AS POSSIBLE. PAY ATTENTION TO PROPER TECHNIQUE.', 200, 1, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-17 18:28:21.647' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (43, N'AKIRA-A', N'Circuit', N'FINISH THE CIRCUIT AS QUICK AS POSSIBLE.', 300, 1, N'Unavailable', 0, 0, 1, 1, CAST(N'2017-07-18 09:25:45.673' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (44, N'ARATA-I', N'Circuit', N'FINISH THE CIRCUIT AS QUICK AS POSSIBLE', 500, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 18:36:20.073' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (45, N'ARATA-A', N'Circuit', N'FINISH THE CIRCUIT AS QUICK AS POSSIBLE.', 500, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 18:37:30.017' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (46, N'RUN 5K', N'Circuit', N'RUN 5K AS FAST AS POSSIBLE', 200, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 19:58:45.990' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (47, N'RUN 10K', N'Circuit', N'RUN 10K AS FAST AS POSSIBLE', 200, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 19:59:57.903' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (48, N'ROW 2K', N'Circuit', N'ROW 2K AS FAST AS POSSIBLE', 200, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 20:17:09.637' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (49, N'ROW 1K', N'Circuit', N'ROW 1K AS FAST AS POSSIBLE', 200, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 20:05:43.517' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (50, N'HIDEAKI', N'Circuit', N'FINISH THE REPS AS QUICK AS POSSIBLE', 300, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 20:13:54.417' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (51, N'HISASHI', N'Circuit', N'FINISH THE BATTLE AS QUICK AS POSSIBLE', 300, 1, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-17 20:15:32.413' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (52, N'AKIHIRO', N'Strength', N'TRY TO GRADUALLY INCREASE THE WEIGHT OF EACH EXERCISE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 09:46:08.300' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (53, N'HAJIME', N'Strength', N'GRADUALLY INCREASE THE WEIGHT OF EACH EXERCISE.', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 09:56:08.293' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (54, N'HARUO', N'Circuit', N'RUN THE INTERVALS AS FAST AS POSSIBLE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 10:02:53.970' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (55, N'IZANAGI', N'Circuit', N'ROW THE INTERVALS AS FAST AS POSSIBLE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 10:09:25.143' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (56, N'JUNICHI', N'Circuit', N'ROW THE INTVERVALS AS FAST AS POSSIBLE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 10:16:03.903' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (57, N'KAEDE', N'Circuit', N'RUN THE INTERVALS AS FAST AS POSSIBLE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 10:24:41.163' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (58, N'IWAO', N'Circuit', N'FINISH THE WORKOUT AS FAST AS POSSIBLE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 10:38:21.967' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (59, N'JIROU', N'Circuit', N'FINISH THE CIRCUIT AS FAST AS POSSIBLE', 200, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-07-18 10:49:45.857' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (60, N'EIJI', N'Circuit', N'FINISH ALL THE EXERCISES IN THE WORKOUT AS FAST AS POSSIBLE. MAKE SURE YOU USE PROPER FORM.', 100, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-12-08 09:08:22.337' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (61, N'Test 1752', N'Circuit', N'Test', 100, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2017-12-08 16:54:09.077' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (62, N'Test 2008', N'Circuit', N'Read this carefully, so you can workout safely.

Short workout description:
6x (10 burpees - 10 seconds rest)

1) Master proper form for each exercise.
2) Increase speed
3) Increase weight of equipment', 350, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2018-01-15 19:00:37.873' AS DateTime))
INSERT [dbo].[Workout] ([Id], [Name], [WorkoutType], [GoalDescription], [Points], [IsBattleWorkout], [Photo], [IsOnBasicVersion], [IsDeleted], [IsActive], [IsNew], [CreatedOn]) VALUES (63, N'Test 1527', N'Strength', N'Read this carefully, so you can workout safely.

Short workout description:
Exercises:     Reps:     Sets:     Rest:
Squat            8-12       3           60 sec

1) The goal is to increase the weight for each exercise', 100, 0, N'Unavailable', 1, 0, 1, 1, CAST(N'2018-01-17 14:21:53.213' AS DateTime))
SET IDENTITY_INSERT [dbo].[Workout] OFF
SET IDENTITY_INSERT [dbo].[Workout_Level] ON 

INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (15, 3, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (19, 5, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (34, 1, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (37, 6, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (38, 6, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (45, 8, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (46, 8, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (47, 8, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (48, 9, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (49, 9, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (50, 9, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (51, 10, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (52, 10, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (53, 10, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (54, 11, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (55, 11, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (56, 11, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (60, 12, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (61, 12, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (62, 12, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (63, 13, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (64, 13, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (65, 13, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (66, 14, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (67, 14, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (68, 14, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (69, 16, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (70, 16, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (71, 4, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (72, 17, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (73, 17, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (74, 2, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (75, 2, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (76, 7, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (77, 7, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (78, 7, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (82, 18, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (83, 18, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (84, 18, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (85, 19, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (86, 19, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (87, 20, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (88, 20, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (181, 22, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (182, 22, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (183, 22, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (199, 33, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (200, 33, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (201, 33, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (202, 34, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (203, 34, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (204, 34, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (205, 35, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (206, 35, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (207, 35, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (208, 36, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (209, 36, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (210, 36, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (211, 37, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (212, 37, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (213, 37, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (214, 32, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (215, 32, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (216, 32, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (238, 40, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (239, 40, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (243, 38, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (244, 38, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (245, 38, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (247, 42, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (248, 23, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (250, 24, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (251, 44, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (252, 45, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (256, 27, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (257, 27, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (258, 27, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (259, 41, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (260, 41, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (261, 41, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (262, 29, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (263, 29, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (264, 29, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (265, 31, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (266, 31, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (267, 31, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (268, 21, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (269, 21, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (270, 21, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (271, 39, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (272, 39, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (273, 39, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (274, 30, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (275, 30, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (276, 30, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (277, 25, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (278, 25, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (279, 25, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (286, 46, 1)
GO
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (287, 46, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (288, 46, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (289, 47, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (290, 47, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (291, 47, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (295, 49, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (296, 49, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (297, 49, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (298, 50, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (299, 50, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (300, 50, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (304, 51, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (305, 51, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (306, 51, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (307, 48, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (308, 48, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (309, 48, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (310, 28, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (311, 28, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (312, 28, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (313, 43, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (320, 52, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (321, 52, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (322, 52, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (329, 53, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (330, 53, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (331, 53, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (332, 54, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (333, 54, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (334, 54, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (335, 55, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (336, 55, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (337, 55, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (338, 56, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (339, 56, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (340, 56, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (341, 57, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (342, 57, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (343, 57, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (344, 58, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (345, 58, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (346, 58, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (347, 59, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (348, 59, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (349, 59, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (350, 26, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (357, 60, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (358, 60, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (359, 60, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (360, 61, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (361, 61, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (362, 61, 3)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (363, 62, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (364, 63, 1)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (365, 63, 2)
INSERT [dbo].[Workout_Level] ([Id], [WorkoutId], [LevelId]) VALUES (366, 63, 3)
SET IDENTITY_INSERT [dbo].[Workout_Level] OFF
SET IDENTITY_INSERT [dbo].[Workout_Objective] ON 

INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (23, 3, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (24, 3, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (30, 5, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (31, 5, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (51, 1, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (52, 1, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (55, 6, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (56, 6, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (59, 8, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (60, 8, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (61, 8, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (62, 8, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (63, 9, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (64, 9, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (65, 9, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (66, 9, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (67, 10, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (68, 10, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (69, 10, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (70, 10, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (71, 11, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (72, 11, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (73, 11, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (74, 11, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (79, 12, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (80, 13, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (81, 13, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (82, 13, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (83, 13, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (84, 14, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (85, 16, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (86, 16, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (87, 4, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (88, 4, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (89, 17, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (90, 17, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (91, 17, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (92, 2, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (93, 2, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (94, 7, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (95, 7, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (96, 7, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (98, 18, 6)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (99, 19, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (100, 19, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (101, 19, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (102, 20, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (103, 20, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (104, 20, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (137, 22, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (143, 33, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (144, 34, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (145, 35, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (146, 36, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (147, 37, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (148, 32, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (156, 40, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (157, 40, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (159, 38, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (161, 42, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (162, 23, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (164, 24, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (165, 44, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (166, 45, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (168, 27, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (169, 41, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (170, 29, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (171, 31, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (172, 21, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (173, 39, 6)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (174, 30, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (175, 25, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (178, 46, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (179, 47, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (181, 49, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (182, 50, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (184, 51, 6)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (185, 48, 3)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (186, 28, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (187, 43, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (190, 52, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (193, 53, 1)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (194, 54, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (195, 55, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (196, 56, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (197, 57, 4)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (198, 58, 6)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (199, 59, 6)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (200, 26, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (203, 60, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (204, 61, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (205, 62, 2)
INSERT [dbo].[Workout_Objective] ([Id], [WorkoutId], [ObjectiveId]) VALUES (206, 63, 1)
SET IDENTITY_INSERT [dbo].[Workout_Objective] OFF
SET IDENTITY_INSERT [dbo].[Workout_Tool] ON 

INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (19, 3, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (20, 3, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (43, 1, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (44, 1, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (47, 6, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (54, 12, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (55, 13, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (56, 13, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (57, 16, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (58, 4, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (59, 4, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (60, 17, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (61, 2, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (62, 7, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (63, 7, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (66, 18, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (67, 18, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (68, 19, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (69, 19, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (70, 20, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (71, 20, 2)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (135, 22, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (141, 33, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (142, 34, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (143, 35, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (144, 36, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (145, 37, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (154, 40, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (156, 38, 6)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (157, 38, 4)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (158, 38, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (159, 38, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (161, 23, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (163, 24, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (164, 44, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (165, 45, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (167, 27, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (168, 27, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (169, 41, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (170, 29, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (171, 31, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (172, 31, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (173, 21, 6)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (174, 21, 5)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (175, 21, 1)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (176, 21, 9)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (177, 39, 7)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (178, 39, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (179, 30, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (180, 30, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (181, 25, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (182, 25, 9)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (185, 46, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (186, 47, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (188, 49, 11)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (189, 50, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (191, 51, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (192, 48, 11)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (193, 43, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (200, 52, 6)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (201, 52, 9)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (206, 53, 6)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (207, 53, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (208, 54, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (209, 56, 11)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (210, 57, 10)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (211, 58, 9)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (212, 58, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (213, 58, 5)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (214, 58, 7)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (215, 59, 6)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (216, 59, 5)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (217, 59, 9)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (218, 59, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (219, 26, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (224, 60, 4)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (225, 60, 8)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (226, 61, 6)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (227, 61, 7)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (228, 62, 4)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (229, 63, 4)
INSERT [dbo].[Workout_Tool] ([Id], [WorkoutId], [ToolId]) VALUES (230, 63, 6)
SET IDENTITY_INSERT [dbo].[Workout_Tool] OFF
SET IDENTITY_INSERT [dbo].[WorkoutExercise] ON 

INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (19, 3, 2, N'5', N'0', N'0', N'0', N'0', N'0', N'12', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (20, 3, 1, N'6', N'0', N'0', N'0', N'0', N'1', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (49, 1, 2, N'5', N'0', N'0', N'0', N'0', N'0', N'10', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (50, 1, 1, N'10', N'0', N'0', N'0', N'0', N'0', N'10', 15)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (53, 6, 8, N'5', N'0', N'0', N'0', N'0', N'0', N'20', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (63, 12, 1, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (64, 13, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (65, 13, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (66, 13, 3, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (67, 13, 2, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (68, 13, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (69, 13, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (70, 13, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (71, 16, 12, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (72, 16, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (73, 4, 3, N'30', N'0', N'0', N'0', N'30', N'0', N'20', 40)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (74, 4, 1, N'50', N'0', N'0', N'0', N'0', N'0', N'20', 60)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (75, 4, 2, N'0', N'0', N'0', N'0', N'0', N'0', N'20', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (76, 4, 3, N'45', N'0', N'0', N'0', N'30', N'2', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (77, 4, 1, N'25', N'0', N'0', N'0', N'0', N'4', N'0', 30)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (78, 17, 12, N'10', N'5', N'10', N'60', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (79, 17, 8, N'15', N'10', N'12', N'120', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (80, 2, 1, N'5', N'3', N'10', N'120', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (81, 2, 2, N'5', N'5', N'10', N'60', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (82, 7, 8, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (83, 7, 3, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (84, 7, 1, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (89, 18, 12, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (90, 18, 13, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (91, 18, 3, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (92, 18, 2, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (93, 19, 12, N'0', N'0', N'0', N'0', N'0', N'0', N'15', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (94, 19, 14, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (95, 19, 1, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (96, 20, 12, N'10', N'0', N'0', N'0', N'0', N'0', N'15', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (97, 20, 18, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (98, 20, 8, N'20', N'0', N'0', N'0', N'60', N'0', N'25', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (99, 20, 19, N'0', N'0', N'0', N'0', N'20', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (100, 20, 12, N'0', N'0', N'0', N'0', N'0', N'2', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (245, 22, 20, NULL, N'0', N'0', N'0', N'30', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (246, 22, 28, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (247, 22, 20, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (248, 22, 28, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (249, 22, 20, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (277, 33, 33, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (278, 34, 34, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (279, 35, 35, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (280, 36, 36, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (281, 37, 37, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (312, 40, 21, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (313, 40, 28, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (314, 40, 20, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (315, 40, 25, N'0', N'0', N'0', N'0', N'40', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (316, 40, 36, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (317, 40, 25, N'0', N'0', N'0', N'0', N'40', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (318, 40, 37, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (329, 38, 70, N'4', N'3', N'4110', N'90 seconds', N'0', N'0', N'0', 8)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (330, 38, 63, N'4', N'3', N'4110', N'90 seconds', N'0', N'0', N'0', 8)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (331, 38, 68, N'4', N'3', N'4110', N'90 seconds', N'0', N'0', N'0', 8)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (332, 38, 98, N'4', N'3', N'4110', N'90 seconds', N'0', N'0', N'0', 8)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (333, 38, 111, N'4', N'3', N'4110', N'90 seconds', N'0', N'0', N'0', 8)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (335, 23, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (337, 24, 107, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (338, 44, 107, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (339, 45, 107, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 100)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (356, 27, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (357, 27, 50, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (358, 27, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (359, 27, 110, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (360, 27, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (361, 27, 107, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (362, 27, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (363, 27, 109, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (364, 27, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (365, 41, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (366, 41, 28, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (367, 41, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (368, 41, 28, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (369, 41, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (370, 41, 28, N'0', N'0', N'0', N'0', N'10', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (371, 41, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (372, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (373, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (374, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (375, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (376, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (377, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (378, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (379, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (380, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (381, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (382, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (383, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (384, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (385, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (386, 29, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (387, 29, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (388, 31, 24, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (389, 31, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
GO
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (390, 31, 21, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (391, 31, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (392, 31, 23, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (393, 31, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (394, 21, 90, N'8', N'3', N'4010', N'60 seconden', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (395, 21, 68, N'8', N'3', N'4010', N'60 seconden', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (396, 21, 96, N'8', N'3', N'4010', N'60 seconden', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (397, 21, 77, N'8', N'3', N'4010', N'60 seconden', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (398, 21, 83, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (399, 39, 79, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (400, 39, 108, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (401, 39, 109, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (402, 39, 80, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (403, 39, 84, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (404, 30, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (405, 30, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 30)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (406, 30, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (407, 30, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 30)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (408, 30, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (409, 30, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 30)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (410, 30, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (411, 30, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 30)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (412, 30, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (413, 30, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 30)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (414, 30, 29, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (415, 25, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (416, 25, 103, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (417, 25, 82, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (418, 25, 104, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (419, 25, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (420, 25, 92, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (423, 46, 33, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (424, 47, 34, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (426, 49, 36, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (427, 50, 110, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (429, 51, 109, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 100)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (430, 48, 37, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (431, 43, 105, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 100)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (445, 52, 90, N'4', N'3', N'41x1', N'90 seconds', N'0', N'0', N'0', 6)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (446, 52, 56, N'4', N'3', N'41X1', N'90 seconds', N'0', N'0', N'0', 6)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (447, 52, 71, N'4', N'3', N'41X1', N'90 seconds', N'0', N'0', N'0', 6)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (448, 52, 57, N'4', N'3', N'41X1', N'90 seconds', N'0', N'0', N'0', 6)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (449, 52, 97, N'4', N'3', N'41X1', N'90 seconds', N'0', N'0', N'0', 6)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (459, 53, 70, N'8', N'3', N'30x0', N'60 seconds', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (460, 53, 73, N'8', N'3', N'30X0', N'60 seconds', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (461, 53, 111, N'8', N'3', N'30X0', N'60 seconds', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (462, 53, 87, NULL, N'3', N'30X0', N'60 seconds', N'0', N'0', N'0', 15)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (463, 53, 106, NULL, N'3', N'30X0', N'60 seconds', N'0', N'0', N'0', 15)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (464, 54, 114, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (465, 54, 9, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (466, 54, 114, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (467, 54, 9, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (468, 54, 114, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (469, 54, 9, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (470, 54, 114, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (471, 54, 9, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (472, 54, 114, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (473, 54, 9, N'0', N'0', N'0', N'0', N'30', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (474, 54, 114, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (475, 56, 115, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (476, 56, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (477, 56, 115, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (478, 56, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (479, 56, 115, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (480, 56, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (481, 56, 115, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (482, 56, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (483, 56, 115, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (484, 57, 116, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (485, 57, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (486, 57, 116, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (487, 57, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (488, 57, 116, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (489, 57, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (490, 57, 116, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (491, 57, 11, N'0', N'0', N'0', N'0', N'60', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (492, 57, 116, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (493, 58, 88, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (494, 58, 85, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (495, 58, 84, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (496, 58, 53, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (497, 58, 80, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (498, 59, 113, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (499, 59, 54, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (500, 59, 89, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (501, 59, 108, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (502, 59, 109, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (503, 59, 113, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (504, 59, 54, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (505, 59, 89, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (506, 59, 108, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (507, 59, 109, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (508, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (509, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (510, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (511, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (512, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (513, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (514, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (515, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
GO
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (516, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (517, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (518, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (519, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (520, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (521, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (522, 26, 94, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (523, 26, 112, N'0', N'0', N'0', N'0', N'0', N'0', N'0', 25)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (530, 60, 67, NULL, N'0', N'0', N'0', N'0', N'0', N'0', 5)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (531, 60, 60, NULL, N'0', N'0', N'0', N'0', N'0', N'0', 5)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (532, 60, 105, NULL, N'0', N'0', N'0', N'0', N'0', N'0', 10)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (533, 61, 56, N'0', N'0', N'0', N'0', N'0', N'0', N'100', 20)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (534, 61, 79, N'0', N'0', N'0', N'0', N'0', N'0', N'100', 50)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (535, 62, 63, N'10', N'0', N'0', N'0', N'0', N'0', N'0', 0)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (536, 63, 63, N'8', N'3', N'30X0', N'60 sec', N'0', N'0', N'0', 12)
INSERT [dbo].[WorkoutExercise] ([Id], [WorkoutId], [ExerciseId], [Reps], [Sets], [Tempo], [Rest], [Duration], [Distance], [Weight], [RepHigh]) VALUES (537, 63, 69, N'4', N'3', N'40X0', N'90 sec', N'0', N'0', N'0', 8)
SET IDENTITY_INSERT [dbo].[WorkoutExercise] OFF
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [fightathlete] SET  READ_WRITE 
GO

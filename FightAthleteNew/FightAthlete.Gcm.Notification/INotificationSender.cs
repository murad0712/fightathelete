﻿using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace FightAthlete.Gcm.Notification
{
    public interface INotificationSender
    {
        Task SendToAll(JObject message);
        Task Send(JObject message, string to);
    }
}
﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace FightAthlete.Gcm.Notification
{
    public class NotificationSender : INotificationSender
    {
        public string ApiKey { get; set; }
        public string GcmUrl { get; set; }

        public NotificationSender(string apiKey, string gcmUrl)
        {
            ApiKey = apiKey;
            GcmUrl = gcmUrl;
        }

        public async Task SendToAll(JObject message)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.TryAddWithoutValidation(
                    "Authorization", "key=" + ApiKey);
                var gcmData = new JObject {{"to", "/topics/global"}, {"data", message}};
                var response = await client.PostAsync(new Uri(GcmUrl), new StringContent(gcmData.ToString(), Encoding.Default, "application/json"));

                if (!response.IsSuccessStatusCode)
                {
                    throw new NotSupportedException();
                }
            }
        }

        public async Task Send(JObject message, string to)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.TryAddWithoutValidation(
                    "Authorization", "key=" + ApiKey);
                var gcmData = new JObject { { "to", to }, { "data", message } };
                var response = await client.PostAsync(new Uri(GcmUrl), new StringContent(gcmData.ToString(), Encoding.Default, "application/json"));

                if (!response.IsSuccessStatusCode)
                {
                    throw new NotSupportedException();
                }
            }
        }
    }
}
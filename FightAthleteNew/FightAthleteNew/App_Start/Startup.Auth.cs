﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using FightAthlete.Web.Configuration;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using FightAthleteNew.Providers;
using FightAthleteNew.Models;
using Microsoft.Owin.Security.Facebook;

namespace FightAthleteNew
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            var facebookOptions = new FacebookAuthenticationOptions
            {
                AppId = FightAthleteWebConfig.Current.FacebookAppId,
                AppSecret = FightAthleteWebConfig.Current.FacebookAppSecret,
                UserInformationEndpoint = FightAthleteWebConfig.Current.FacebookUserInformationEndpoint,
                Provider = new FacebookAuthenticationProvider
                {
                    OnAuthenticated = context =>
                    {
                        context.Identity.AddClaim(new Claim("FacebookAccessToken", context.AccessToken));
                        context.Identity.AddClaim(new Claim(ClaimTypes.Name, context.Name));
                        context.Identity.AddClaim(new Claim(ClaimTypes.Email, context.Email));
                        context.Identity.AddClaim(new Claim("ExternalUserData", context.User.ToString().Trim()));
                        return Task.FromResult(0);
                    }
                },
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie
            };
            facebookOptions.Scope.Add(FightAthleteWebConfig.Current.FacebookUserProfileScope);
            app.UseFacebookAuthentication(facebookOptions);

            app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions
            {
                ClientId = "437757279557-kfigu25j339hec3tov3d7mc6pibu4pg2.apps.googleusercontent.com",
                ClientSecret = "cM6PgEdWj80hxaJiz1zWkXyc"
            });
        }
    }
}

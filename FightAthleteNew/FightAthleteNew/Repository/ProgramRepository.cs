﻿using System.Collections.Generic;
using System.Linq;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Repository
{
    public class ProgramRepository : Repository<Program>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        public IQueryable<ProgramViewModel> GetAllPrograms()
        {
            var programVm = (from program in _db.Programs
                select new ProgramViewModel
                {
                    Id = program.Id,
                    Name = program.Name,
                    IsBasic = program.IsBasic,
                    FrequencyId = program.FrequencyId,
                    DurationId = program.DurationId,
                    Point = program.Point,
                    IsActive = program.IsActive,
                    IsDeleted = program.IsDeleted,
                    IsNew = program.IsNew,
                    Levels = (from programLevel in _db.ProgramLevels
                        join level in _db.Levels on programLevel.LevelId equals level.Id
                        where programLevel.ProgramId == program.Id
                        select level
                        ).ToList(),
                    Tools = (from programTools in _db.ProgramTools
                        join tool in _db.Tools on programTools.ToolId equals tool.Id
                        where programTools.ProgramId == program.Id
                        select tool
                        ).ToList(),
                    ProgramWorkouts = (from programWorkout in _db.ProgramWorkouts
                        join workout in _db.Workouts on programWorkout.WorkoutId equals workout.Id
                        where programWorkout.ProgramId == program.Id
                        select programWorkout
                        ).ToList()
                });
            return programVm;
        }

        public void DeleteProgram(int id)
        {
            _db.Programs.Find(id).IsDeleted = 1;
            _db.SaveChanges();
        }

        public List<ProgramDTO> GetProgramByLevel(int id)
        {
            var programs = (from program in _db.Programs
                             join programLevel in _db.ProgramLevels on program.Id equals programLevel.ProgramId

                             where program.IsDeleted == 0 && program.IsActive == 1 && programLevel.LevelId == id
                             select new ProgramDTO()
                             {
                                 Id = program.Id,
                                 Name = program.Name,
                                 CreatedOn = program.CreatedOn,
                                 IsNew = program.IsNew,
                                 FrequencyId = program.FrequencyId,
                                 Tools = (from programTool in _db.ProgramTools
                                          join tool in _db.Tools on programTool.ToolId equals tool.Id
                                          where programTool.ProgramId == program.Id
                                          select tool
                                  ).ToList()
                             }).ToList();

            return programs;
        }
    }
}
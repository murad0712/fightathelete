﻿using System.Collections.Generic;
using System.Linq;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Repository
{
    public class ToolRepository : Repository<Tool>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        public List<ToolDTO> GetToolByWorkout(int id)
        {
            var tools = (from tool in _db.Tools
                         join workoutTools in _db.WorkoutTools on tool.Id equals workoutTools.ToolId

                         where workoutTools.WorkoutId == id
                         select new ToolDTO
                         {
                             Id = tool.Id,
                             Name = tool.Name,
                             Photo = tool.Photo
                         }).ToList();

            return tools;
        }
    }
}
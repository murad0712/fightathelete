﻿using System.Collections.Generic;

namespace FightAthleteNew.Repository
{
    public interface IRepository<T>
    {
        T Get(int id);
        IEnumerable<T> GetAll();
        void Add(T entity);
        void Update(T entity);
        void Delete(int id);
        void Save();
        /*IQueryable<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate);*/


    }
}

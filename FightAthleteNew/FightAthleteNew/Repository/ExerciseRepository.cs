﻿using System.Collections.Generic;
using System.Linq;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Repository
{
    public class ExerciseRepository:Repository<Exercise>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public int Create(Exercise exercise)
        {
            _db.Exercises.Add(exercise);
            _db.SaveChanges();
            return exercise.Id;
        }

        public IQueryable<ExerciseViewModel> GetAllExercise()
        {
            var exercisesVm = (from exercise in _db.Exercises
                             select new ExerciseViewModel
                             {
                                 Id = exercise.Id,
                                 Name = exercise.Name,
                                 Description = exercise.Description,
                                 IsRestExercise = exercise.IsRestExercise,
                                 IsBasicVersion = exercise.IsBasicVersion,
                                 IsNew = exercise.IsNew,
                                 IsActive = exercise.IsActive,
                                 IsDeleted = exercise.IsDeleted,
                                 Duration = exercise.Duration,
                                 CreatedDate = exercise.CreatedDate,
                                 Levels = (from exerciseLevel in _db.ExerciseLevels
                                     join level in _db.Levels on exerciseLevel.LevelId equals level.Id
                                     where exerciseLevel.ExerciseId == exercise.Id
                                     select level
                                  ).ToList(),
                                 MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                           join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                           where exerciseMuscleGroup.ExerciseId == exercise.Id
                                           select muscleGroup
                                  ).ToList(),
                                 Tools = (from exerciseTool in _db.ExerciseTools
                                     join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                                     where exerciseTool.ExerciseId == exercise.Id
                                     select tool
                                  ).ToList(),
                                 Video = exercise.Video
                             }
                );
            return exercisesVm;
        }

        public void DeleteExercise(int id)
        {
            _db.Exercises.Find(id).IsDeleted = 1;
            _db.SaveChanges();
        }

        public List<ExerciseDTO> GetExerciseByLevel(int id)
        {
            var exercises = (from exercise in _db.Exercises
                join exerciseLevel in _db.ExerciseLevels on exercise.Id equals exerciseLevel.ExerciseId
            
                where exercise.IsDeleted == 0 && exercise.IsActive == 1 && exerciseLevel.LevelId == id
                select new ExerciseDTO
                {
                    Id = exercise.Id,
                    Name = exercise.Name,
                    Video = exercise.Video,
                    IsNew = exercise.IsNew,
                    Tools = (from exerciseTool in _db.ExerciseTools
                             join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                             where exerciseTool.ExerciseId == exercise.Id
                             select tool
                                  ).ToList(),
                    MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                    join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                    where exerciseMuscleGroup.ExerciseId == exercise.Id
                                    select muscleGroup
                                  ).ToList()
                }).ToList();

            return exercises;
        }

        public List<ExerciseDTO> GetExerciseByMusclegroup(int id)
        {
            var exercises = (from exercise in _db.Exercises
                             join exerciseMuscleGroup in _db.ExerciseMuscleGroups on exercise.Id equals exerciseMuscleGroup.ExerciseId

                             where exercise.IsDeleted == 0 && exercise.IsActive == 1 && exerciseMuscleGroup.MuscleGroupId == id
                             select new ExerciseDTO
                             {
                                 Id = exercise.Id,
                                 Name = exercise.Name,
                                 IsNew = exercise.IsNew,
                                 Video = exercise.Video
                             }).ToList();

            return exercises;
        }

        public List<ExerciseDTO> GetExerciseByWorkout(int id)
        {
            var exercises = (from exercise in _db.Exercises
                             join workoutExercises in _db.WorkoutExercises on exercise.Id equals workoutExercises.ExerciseId

                             where exercise.IsDeleted == 0 && exercise.IsActive == 1 && workoutExercises.WorkoutId == id && exercise.IsRestExercise == 0
                             select new ExerciseDTO
                             {
                                 Id = exercise.Id,
                                 Name = exercise.Name,
                                 IsNew = exercise.IsNew,
                                 Video = exercise.Video
                             }).Distinct().ToList();

            return exercises;
        }

        public List<ExerciseDTO> GetExerciseByFilter(int level, int muscle, int tool)
        {
            var exercises = (from exercise in _db.Exercises
                             join exerciseMuscleGroup in _db.ExerciseMuscleGroups on exercise.Id equals exerciseMuscleGroup.ExerciseId
                             join exerciseLevel in _db.ExerciseLevels on exercise.Id equals exerciseLevel.ExerciseId
                             join exercisetool in _db.ExerciseTools on exercise.Id equals exercisetool.ExerciseId
                             where exercise.IsDeleted == 0 && 
                                    exercise.IsActive == 1 && 
                                    exerciseLevel.LevelId == level && 
                                    exerciseMuscleGroup.MuscleGroupId == muscle &&
                                    exercisetool.ToolId == tool
                             select new ExerciseDTO
                             {
                                 Id = exercise.Id,
                                 Name = exercise.Name,
                                 IsNew = exercise.IsNew,
                                 Video = exercise.Video,
                                 CreatedDate = exercise.CreatedDate
                             }).ToList();

            return exercises;
        }
    }
}
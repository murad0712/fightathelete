﻿using System.Collections.Generic;
using System.Linq;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Repository
{
    public class WorkoutRepository : Repository<Workout>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        public int Create(Workout workout)
        {
            _db.Workouts.Add(workout);
            _db.SaveChanges();
            return workout.Id;
        }

        public IQueryable<WorkoutViewModel> GetAllWorkouts()
        {
            var workoutVm = (from workout in _db.Workouts
                select new WorkoutViewModel
                {
                    WorkoutId = workout.Id,
                    Name = workout.Name,
                    Type = workout.WorkoutType,
                    Goal = workout.GoalDescription,
                    Points = workout.Points,
                    IsBattleWorkout = workout.IsBattleWorkout,
                    IsBasicVersion = workout.IsOnBasicVersion,
                    IsNew = workout.IsNew,
                    IsActive = workout.IsActive,
                    IsDeleted = workout.IsDeleted,
                    Photo = workout.Photo,
                    CreatedOn = workout.CreatedOn,
                    Levels = (from workoutLevel in _db.WorkoutLevels
                        join level in _db.Levels on workoutLevel.LevelId equals level.Id
                        where workoutLevel.WorkoutId == workout.Id
                        select level
                     ).ToList(),
                    Objectives = (from workoutObjective in _db.WorkoutObjectives
                        join objective in _db.Objectives on workoutObjective.ObjectiveId equals objective.Id
                        where workoutObjective.WorkoutId == workout.Id
                        select objective
                     ).ToList(),
                    Tools = (from workoutTool in _db.WorkoutTools
                                  join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                  where workoutTool.WorkoutId == workout.Id
                                  select tool
                     ).ToList(),
                    Exercises = (from workoutExercise in _db.WorkoutExercises
                        join exercise in _db.Exercises on workoutExercise.ExerciseId equals exercise.Id
                        where workoutExercise.WorkoutId == workout.Id
                        select new WorkoutExerciseViewModel
                        {
                            Id = exercise.Id,
                            Name = exercise.Name,
                            Description = exercise.Description,
                            IsRestExercise = exercise.IsRestExercise,
                            Video = exercise.Video,
                            Reps = workoutExercise.Reps,
                            RepHigh = workoutExercise.RepHigh,
                            Sets = workoutExercise.Sets,
                            Tempo = workoutExercise.Tempo,
                            Rests = workoutExercise.Rest,
                            Duration = workoutExercise.Duration,
                            Distance = workoutExercise.Distance,
                            Weight = workoutExercise.Weight,
                            Levels = (from exerciseLevel in _db.ExerciseLevels
                                      join level in _db.Levels on exerciseLevel.LevelId equals level.Id
                                      where exerciseLevel.ExerciseId == exercise.Id
                                      select level
                                  ).ToList(),
                            MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                            join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                            where exerciseMuscleGroup.ExerciseId == exercise.Id
                                            select muscleGroup
                                  ).ToList(),
                            Tools = (from exerciseTool in _db.ExerciseTools
                                     join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                                     where exerciseTool.ExerciseId == exercise.Id
                                     select tool
                                  ).ToList()
                        }).ToList()
    
                });
            return workoutVm;
        }

        public void DeleteWorkout(int id)
        {
            var workout = _db.Workouts.Find(id);
            if (workout != null) workout.IsDeleted = 1;
            _db.SaveChanges();
        }

        public IQueryable<WorkoutViewModel> GetAllBattles()
        {
            var workoutVm = (from workout in _db.Workouts
                             where workout.IsBattleWorkout == 1
                             select new WorkoutViewModel
                             {
                                 WorkoutId = workout.Id,
                                 Name = workout.Name,
                                 Type = workout.WorkoutType,
                                 Goal = workout.GoalDescription,
                                 Points = workout.Points,
                                 IsBattleWorkout = workout.IsBattleWorkout,
                                 IsBasicVersion = workout.IsOnBasicVersion,
                                 IsNew = workout.IsNew,
                                 IsActive = workout.IsActive,
                                 IsDeleted = workout.IsDeleted,
                                 Photo = workout.Photo,
                                 CreatedOn = workout.CreatedOn,
                                 Levels = (from workoutLevel in _db.WorkoutLevels
                                           join level in _db.Levels on workoutLevel.LevelId equals level.Id
                                           where workoutLevel.WorkoutId == workout.Id
                                           select level
                                  ).ToList(),
                                 Objectives = (from workoutObjective in _db.WorkoutObjectives
                                               join objective in _db.Objectives on workoutObjective.ObjectiveId equals objective.Id
                                               where workoutObjective.WorkoutId == workout.Id
                                               select objective
                                  ).ToList(),
                                 Tools = (from workoutTool in _db.WorkoutTools
                                          join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                          where workoutTool.WorkoutId == workout.Id
                                          select tool
                                  ).ToList(),
                                 Exercises = (from workoutExercise in _db.WorkoutExercises
                                              join exercise in _db.Exercises on workoutExercise.ExerciseId equals exercise.Id
                                              where workoutExercise.WorkoutId == workout.Id
                                              select new WorkoutExerciseViewModel
                                              {
                                                  Id = exercise.Id,
                                                  Name = exercise.Name,
                                                  Description = exercise.Description,
                                                  IsRestExercise = exercise.IsRestExercise,
                                                  Video = exercise.Video,
                                                  Reps = workoutExercise.Reps,
                                                  RepHigh = workoutExercise.RepHigh,
                                                  Sets = workoutExercise.Sets,
                                                  Tempo = workoutExercise.Tempo,
                                                  Rests = workoutExercise.Rest,
                                                  Duration = workoutExercise.Duration,
                                                  Distance = workoutExercise.Distance,
                                                  Weight = workoutExercise.Weight,
                                                  Levels = (from exerciseLevel in _db.ExerciseLevels
                                                            join level in _db.Levels on exerciseLevel.LevelId equals level.Id
                                                            where exerciseLevel.ExerciseId == exercise.Id
                                                            select level
                                                        ).ToList(),
                                                  MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                                                  join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                                                  where exerciseMuscleGroup.ExerciseId == exercise.Id
                                                                  select muscleGroup
                                                        ).ToList(),
                                                  Tools = (from exerciseTool in _db.ExerciseTools
                                                           join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                                                           where exerciseTool.ExerciseId == exercise.Id
                                                           select tool
                                                        ).ToList()
                                              }).ToList()

                             });
            return workoutVm;
        }

        //for all users
        public List<WorkoutDTO> GetAllWorkoutsByObjLvl(int lvl, int obj)
        {
            var workouts = (from workout in _db.Workouts
                            join workoutLevel in _db.WorkoutLevels on workout.Id equals workoutLevel.WorkoutId
                            join workoutobj in _db.WorkoutObjectives on workout.Id equals workoutobj.WorkoutId
                            where workout.IsDeleted == 0 &&
                                   workout.IsActive == 1 &&
                                  workout.IsBattleWorkout == 0 &&
                                   workoutLevel.LevelId == lvl &&
                                   workoutobj.ObjectiveId == obj
                            select new WorkoutDTO()
                            {
                                Id = workout.Id,
                                Name = workout.Name,
                                WorkoutType = workout.WorkoutType,
                                CreatedOn = workout.CreatedOn,
                                IsNew = workout.IsNew,
                                Tools = (from workoutTool in _db.WorkoutTools
                                         join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                         where workoutTool.WorkoutId == workout.Id
                                         select tool
                                 ).ToList()
                            }).ToList();

            return workouts;

        }

        //for different user type
        public List<WorkoutDTO> GetAllWorkoutsByObjLvl(int lvl, int obj, int userId)
        {
            var user = _db.Userses.FirstOrDefault(_ => _.Id == userId);

            if (user != null && user.MembershipTypeId == 1)
            {
                var workouts = (from workout in _db.Workouts
                                join workoutLevel in _db.WorkoutLevels on workout.Id equals workoutLevel.WorkoutId
                                join workoutobj in _db.WorkoutObjectives on workout.Id equals workoutobj.WorkoutId
                                where workout.IsDeleted == 0 &&
                                      workout.IsActive == 1 &&
                                      workout.IsBattleWorkout == 0 &&
                                      workout.IsOnBasicVersion == 1 &&
                                      workoutLevel.LevelId == lvl &&
                                      workoutobj.ObjectiveId == obj
                                select new WorkoutDTO()
                                {
                                    Id = workout.Id,
                                    Name = workout.Name,
                                    WorkoutType = workout.WorkoutType,
                                    CreatedOn = workout.CreatedOn,
                                    IsNew = workout.IsNew,
                                    Tools = (from workoutTool in _db.WorkoutTools
                                             join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                             where workoutTool.WorkoutId == workout.Id
                                             select tool
                                     ).ToList()
                                }).ToList();

                return workouts;
            }
            else
            {
                var workouts = (from workout in _db.Workouts
                                join workoutLevel in _db.WorkoutLevels on workout.Id equals workoutLevel.WorkoutId
                                join workoutobj in _db.WorkoutObjectives on workout.Id equals workoutobj.WorkoutId
                                where workout.IsDeleted == 0 &&
                                      workout.IsActive == 1 &&
                                      workout.IsBattleWorkout == 0 &&
                                      workoutLevel.LevelId == lvl &&
                                      workoutobj.ObjectiveId == obj
                                select new WorkoutDTO()
                                {
                                    Id = workout.Id,
                                    Name = workout.Name,
                                    WorkoutType = workout.WorkoutType,
                                    CreatedOn = workout.CreatedOn,
                                    IsNew = workout.IsNew,
                                    Tools = (from workoutTool in _db.WorkoutTools
                                             join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                             where workoutTool.WorkoutId == workout.Id
                                             select tool
                                     ).ToList()
                                }).ToList();

                return workouts;
            }
        }

        public List<WorkoutDTO> GetAllBattlesByObjLvl(int lvl, int obj)
        {
            var workouts = (from workout in _db.Workouts
                            join workoutLevel in _db.WorkoutLevels on workout.Id equals workoutLevel.WorkoutId
                            join workoutobj in _db.WorkoutObjectives on workout.Id equals workoutobj.WorkoutId
                            where workout.IsDeleted == 0 &&
                                   workout.IsActive == 1 &&
                                   workout.IsBattleWorkout == 1 &&
                                   workoutLevel.LevelId == lvl &&
                                   workoutobj.ObjectiveId == obj
                            select new WorkoutDTO()
                            {
                                Id = workout.Id,
                                Name = workout.Name,
                                WorkoutType = workout.WorkoutType,
                                CreatedOn = workout.CreatedOn,
                                IsNew = workout.IsNew,
                                Tools = (from workoutTool in _db.WorkoutTools
                                         join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                         where workoutTool.WorkoutId == workout.Id
                                         select tool
                                  ).ToList()
                            }).ToList();

            return workouts;
        }

        public IQueryable<WorkoutViewModel> GetWorkoutsForFilter(int lvl, int obj)
        {
            var workoutVm = (from workout in _db.Workouts
                join workoutLevel in _db.WorkoutLevels on workout.Id equals workoutLevel.WorkoutId
                join workoutObjective in _db.WorkoutObjectives on workout.Id equals workoutObjective.WorkoutId
            where workoutLevel.LevelId == lvl && workoutObjective.ObjectiveId == obj
            
                             select new WorkoutViewModel
                             {
                                 WorkoutId = workout.Id,
                                 Name = workout.Name,
                                 Type = workout.WorkoutType,
                                 Goal = workout.GoalDescription,
                                 Points = workout.Points,
                                 IsBattleWorkout = workout.IsBattleWorkout,
                                 IsBasicVersion = workout.IsOnBasicVersion,
                                 IsNew = workout.IsNew,
                                 IsActive = workout.IsActive,
                                 IsDeleted = workout.IsDeleted,
                                 Photo = workout.Photo,
                                 CreatedOn = workout.CreatedOn,
                                 /*Levels = (from workoutLevel in _db.WorkoutLevels
                                           join level in _db.Levels on workoutLevel.LevelId equals level.Id
                                           where workoutLevel.WorkoutId == workout.Id && level.Id == lvl
                                           select level
                                  ).ToList(),
                                 Objectives = (from workoutObjective in _db.WorkoutObjectives
                                               join objective in _db.Objectives on workoutObjective.ObjectiveId equals objective.Id
                                               where workoutObjective.WorkoutId == workout.Id && objective.Id == obj
                                               select objective
                                  ).ToList(),*/
                                  Levels = new List<Level>(),
                                  Objectives = new List<Objective>(),
                                 Tools = (from workoutTool in _db.WorkoutTools
                                          join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                          where workoutTool.WorkoutId == workout.Id
                                          select tool
                                  ).ToList(),
                                 Exercises = (from workoutExercise in _db.WorkoutExercises
                                              join exercise in _db.Exercises on workoutExercise.ExerciseId equals exercise.Id
                                              where workoutExercise.WorkoutId == workout.Id
                                              select new WorkoutExerciseViewModel
                                              {
                                                  Id = exercise.Id,
                                                  Name = exercise.Name,
                                                  Description = exercise.Description,
                                                  IsRestExercise = exercise.IsRestExercise,
                                                  Video = exercise.Video,
                                                  Reps = workoutExercise.Reps,
                                                  RepHigh = workoutExercise.RepHigh,
                                                  Sets = workoutExercise.Sets,
                                                  Tempo = workoutExercise.Tempo,
                                                  Rests = workoutExercise.Rest,
                                                  Duration = workoutExercise.Duration,
                                                  Distance = workoutExercise.Distance,
                                                  Weight = workoutExercise.Weight,
                                                  Levels = (from exerciseLevel in _db.ExerciseLevels
                                                            join level in _db.Levels on exerciseLevel.LevelId equals level.Id
                                                            where exerciseLevel.ExerciseId == exercise.Id
                                                            select level
                                                        ).ToList(),
                                                  MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                                                  join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                                                  where exerciseMuscleGroup.ExerciseId == exercise.Id
                                                                  select muscleGroup
                                                        ).ToList(),
                                                  Tools = (from exerciseTool in _db.ExerciseTools
                                                           join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                                                           where exerciseTool.ExerciseId == exercise.Id
                                                           select tool
                                                        ).ToList()
                                              }).ToList()

                             });
            return workoutVm;
        }

        public List<ProgramWorkoutViewModel> GetWorkoutsByProgramId(int id)
        {
            var proWorkout = (from program in _db.Programs
                join programWorkout in _db.ProgramWorkouts on program.Id equals programWorkout.ProgramId
                join workout in _db.Workouts on programWorkout.WorkoutId equals workout.Id
                where program.Id == id
                select new ProgramWorkoutViewModel
                {
                    WorkoutId = workout.Id,
                    WorkoutName = workout.Name,
                    IsBattle = workout.IsBattleWorkout,
                    Day = programWorkout.Day,
                    Levels = (from level in _db.Levels
                        join workoutLevel in _db.WorkoutLevels on level.Id equals workoutLevel.LevelId
                     where workoutLevel.WorkoutId == workout.Id
                     select level).ToList(),
                    Objectives = (from objective in _db.Objectives
                        join workoutObjective in _db.WorkoutObjectives on objective.Id equals workoutObjective.ObjectiveId
                     where workoutObjective.WorkoutId == workout.Id
                     select objective).ToList()
                }).ToList();
            return proWorkout;
        }
    }
}
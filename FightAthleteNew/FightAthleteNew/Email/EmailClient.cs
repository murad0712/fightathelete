﻿using System.Net;
using System.Net.Mail;
using FightAthlete.Web.Configuration;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Email
{
    public class EmailClient
    {
        public void SendEmail(EmailTemplate emailTemplate)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.Host = FightAthleteWebConfig.Current.SmtpServer;
                smtpClient.Port = int.Parse(FightAthleteWebConfig.Current.SmtpPort);
                smtpClient.Credentials = new NetworkCredential(FightAthleteWebConfig.Current.SmtpServerAdmin,
                    FightAthleteWebConfig.Current.SmtpServerAdminPassword);
                var mail = new MailMessage
                {
                    From = new MailAddress(FightAthleteWebConfig.Current.SmtpServerAdmin),
                    Subject = emailTemplate.Subject,
                    Body = emailTemplate.Body
                };
                mail.To.Add(emailTemplate.To ?? FightAthleteWebConfig.Current.DefaultEmail);
                smtpClient.Send(mail);
            }
        }
    }
}
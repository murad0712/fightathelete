﻿using System;
using System.Collections.Generic;
using System.Linq;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;
using FightAthleteNew.Repository;

namespace FightAthleteNew.Services
{
    public class ExerciseService
    {
        private ExerciseRepository _exerciseRepository;
        private ExerciseLevelRepo _exerciseLevelRepo;
        private ExerciseMuscleGroupRepo _exerciseMuscleGroupRepo;
        private ExerciseToolRepo _exerciseToolRepo;
        protected readonly ApplicationDbContext _context;
        public ExerciseService()
        {
            _exerciseRepository = new ExerciseRepository();
            _exerciseLevelRepo = new ExerciseLevelRepo();
            _exerciseMuscleGroupRepo = new ExerciseMuscleGroupRepo();
            _exerciseToolRepo = new ExerciseToolRepo();
            _context = ApplicationDbContext.Create();
        }

        public IQueryable<ExerciseViewModel> GetAllExercise()
        {
            return _exerciseRepository.GetAllExercise();
        }

        public void AddNewExercise(ExerciseViewModel exerciseVm)
        {
            Exercise exercise = new Exercise();
            exercise.Name = exerciseVm.Name;
            exercise.Description = exerciseVm.Description;
            exercise.IsRestExercise = exerciseVm.IsRestExercise;
            exercise.IsBasicVersion = exerciseVm.IsBasicVersion;
            exercise.Duration = exerciseVm.Duration;
            exercise.Video = exerciseVm.Video;
            exercise.IsDeleted = 0;
            exercise.CreatedDate = DateTime.UtcNow;
            var exerciseId = _exerciseRepository.Create(exercise);

            AddLevelMuscleTool(exerciseVm,exerciseId);
        }

        public void AddLevelMuscleTool(ExerciseViewModel exerciseVm,int exerciseId)
        {
            ExerciseLevel exerciseLevel = new ExerciseLevel();
            ExerciseMuscleGroup exerciseMuscleGroup = new ExerciseMuscleGroup();
            ExerciseTool exerciseTool = new ExerciseTool();

            if (exerciseVm.Levels != null)
            {
                foreach (var exerciseLevelData in exerciseVm.Levels)
                {
                    exerciseLevel.ExerciseId = exerciseId;
                    exerciseLevel.LevelId = exerciseLevelData.Id;
                    _exerciseLevelRepo.Add(exerciseLevel);
                }
            }

            if (exerciseVm.MuscleGroups != null)
            {
                foreach (var exerciseMuscleGroupData in exerciseVm.MuscleGroups)
                {
                    exerciseMuscleGroup.ExerciseId = exerciseId;
                    exerciseMuscleGroup.MuscleGroupId = exerciseMuscleGroupData.Id;
                    _exerciseMuscleGroupRepo.Add(exerciseMuscleGroup);
                }
            }

            if (exerciseVm.Tools != null)
            {
                foreach (var exerciseToolData in exerciseVm.Tools)
                {
                    exerciseTool.ExerciseId = exerciseId;
                    exerciseTool.ToolId = exerciseToolData.Id;
                    _exerciseToolRepo.Add(exerciseTool);
                }
            }

        }

        public void Delete(int id)
        {
            _exerciseRepository.DeleteExercise(id);
        }

        public void EditExercise(ExerciseViewModel exerciseVm)
        {
            Exercise exercise = new Exercise();
            exercise.Id = exerciseVm.Id;
            exercise.Name = exerciseVm.Name;
            exercise.Description = exerciseVm.Description;
            exercise.IsRestExercise = exerciseVm.IsRestExercise;
            exercise.IsBasicVersion = exerciseVm.IsBasicVersion;
            exercise.Duration = exerciseVm.Duration;
            exercise.Video = exerciseVm.Video;
            exercise.IsDeleted = 0;
            _exerciseRepository.Update(exercise);


            var deleteExerciseLevel =
                _context.ExerciseLevels.Where(levels => levels.ExerciseId == exercise.Id);
            if (deleteExerciseLevel.Any())
            {
                foreach (var level in deleteExerciseLevel)
                {
                    _exerciseLevelRepo.Delete(level.Id);
                }
            }

            var deleteExerciseMuscleGroup =
               from muscles in _context.ExerciseMuscleGroups
               where muscles.ExerciseId == exercise.Id
               select muscles;
            if (deleteExerciseMuscleGroup.Any())
            {
                foreach (var muscle in deleteExerciseMuscleGroup)
                {
                    _exerciseMuscleGroupRepo.Delete(muscle.Id);
                }
            }
            
            var deleteTool =
               from tools in _context.ExerciseTools
               where tools.ExerciseId == exercise.Id
               select tools;
            if (deleteTool.Any())
            {
                foreach (var tool in deleteTool)
                {
                    _exerciseToolRepo.Delete(tool.Id);
                }
            }

            AddLevelMuscleTool(exerciseVm, exercise.Id);

            
        }

        public List<ExerciseDTO> GetExerciseByLevel(int id) => _exerciseRepository.GetExerciseByLevel(id);

        public List<ExerciseDTO> GetExerciseByMusclegroup(int id) => _exerciseRepository.GetExerciseByMusclegroup(id);
        public List<ExerciseDTO> GetExerciseByWorkout(int id) => _exerciseRepository.GetExerciseByWorkout(id);

        public List<ExerciseDTO> GetExerciseByFilter(int level, int muscle, int tool) => _exerciseRepository.GetExerciseByFilter(level, muscle, tool);



        public List<CircuitWorkoutViewModel> GetCircuitExerciseByWorkoutId(int id)
        {
            var circuit = (from workoutExercise in _context.WorkoutExercises
                           join exercise in _context.Exercises on workoutExercise.ExerciseId equals exercise.Id
                           where workoutExercise.WorkoutId == id
                           select new CircuitWorkoutViewModel()
                           {
                               ExerciseId = exercise.Id,
                               ExerciseName = exercise.Name,
                               Reps = workoutExercise.Reps,
                               RepHigh = workoutExercise.RepHigh,
                               Weight = workoutExercise.Weight,
                               Distance = workoutExercise.Distance,
                               IsRestExercise = exercise.IsRestExercise,
                               Duration = exercise.Duration
                           });
            return circuit.ToList();
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Repository;

namespace FightAthleteNew.Services
{
    public class ObjectsService
    {
        private LevelRepository _levelRepository;

        public ObjectsService()
        {
            this._levelRepository = new LevelRepository();
        }

        public List<Level> GetLevels()
        {
            return _levelRepository.GetAll().ToList();
        }
    }
}
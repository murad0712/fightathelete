﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace FightAthleteNew.Models
{
    public class CustomPasswordValidator : IIdentityValidator<string>
    {
        private const string PasswordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$";
        private const int PasswordMinLength = 8;
        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (string.IsNullOrEmpty(item) || item.Length < PasswordMinLength)
            {
                return Task.FromResult(IdentityResult.Failed("Password must have at least 8 characters, a capital and a number."));
            }

            return Task.FromResult(!Regex.IsMatch(item, PasswordPattern) ? 
                IdentityResult.Failed("Password must have at least 8 characters, a capital and a number.") : IdentityResult.Success);
        }
    }
}
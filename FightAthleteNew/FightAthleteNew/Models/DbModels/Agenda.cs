﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Agenda")]
    public class Agenda
    {
        public int Id { get; set; }
        public int User_Id { get; set; }
        public string Type { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public DateTime Date { get; set; }

        public int? ObjectiveId { get; set; }
        public int? LevelId { get; set; }
        public int? DurationId { get; set; }
        public int? FrequencyId { get; set; }

        public int Week { get; set; }
        public int? ProgramId { get; set; }
    }
}
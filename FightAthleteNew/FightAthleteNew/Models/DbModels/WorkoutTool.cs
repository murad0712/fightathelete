﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Workout_Tool")]
    public class WorkoutTool
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public int ToolId { get; set; }
    }
}
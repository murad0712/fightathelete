﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("HallOfFame")]
    public class HallOfFame
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string BattleName { get; set; }
        public int BattleId { get; set; }
        public int ObjectiveId { get; set; }
        public double RecordTime { get; set; }
        public DateTime DateTime { get; set; }
        public int IsDeleted { get; set; }

        public int? LevelId { get; set; }
    }
}
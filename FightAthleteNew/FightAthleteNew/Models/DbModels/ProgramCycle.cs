﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("ProgramCycle")]
    public class ProgramCycle
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int UserId { get; set; }
        public string ProgramName { get; set; }
        public string LevelName { get; set; }
        public string FrequencyName { get; set; }
        public string DurationName { get; set; }
        public int Point { get; set; }
        public int Cycle { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("FavouriteExercise")]
    public class FavouriteExercise
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public string ExerciseName { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }
    }
}
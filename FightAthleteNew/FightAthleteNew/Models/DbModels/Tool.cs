﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Tool")]
    public class Tool
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        public string Photo { get; set; }
    }

    public class ToolDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Workout_Level")]
    public class WorkoutLevel
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public int LevelId { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Affiliate")]
    public class Affiliate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
    }
}
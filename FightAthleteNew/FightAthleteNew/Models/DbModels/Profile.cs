﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Profile")]
    public class Profile
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Batch { get; set; }
        public int WorkoutPoint { get; set; }
        public int ProgramPoint { get; set; }
        public int BattlePoint { get; set; }
    }
}
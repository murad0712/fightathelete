﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Workout_Objective")]
    public class WorkoutObjective
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public int ObjectiveId { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("FAQ")]
    public class Faq
    {
        public int Id { get; set; }
        [Display(Name = "Category)")]
        public int CategoryId { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public string Answer { get; set; }


        [ForeignKey("CategoryId")]
        public virtual FaqCategory FaqCategory { get; set; }
    }

    public class DTOFaq
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
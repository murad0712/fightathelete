﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Currency")]
    public class Currency
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public virtual ICollection<MembershipType> MembershipType { get; set; }
    }
}
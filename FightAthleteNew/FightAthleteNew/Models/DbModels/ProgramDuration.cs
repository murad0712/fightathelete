﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Program_Duration")]
    public class ProgramDuration
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int DurationId { get; set; }
    }
}
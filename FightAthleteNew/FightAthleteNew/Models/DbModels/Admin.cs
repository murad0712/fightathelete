﻿using System.Collections.Generic;

namespace FightAthleteNew.Models.DbModels
{
    public class Admin
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class AdminDatabase : List<Admin>
    {
        public AdminDatabase()
        {
            Add(new Admin() { Id = 101, Name = "admin"});
        }
    }
}
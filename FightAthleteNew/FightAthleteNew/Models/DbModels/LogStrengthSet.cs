﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("LogStrengthSet")]
    public class LogStrengthSet
    {
        public int Id { get; set; }
        public int SetNumber { get; set; }
        public double Weight { get; set; }
        public double Reps { get; set; }
        public int LogStrengthExerciseId { get; set; }
    }
}
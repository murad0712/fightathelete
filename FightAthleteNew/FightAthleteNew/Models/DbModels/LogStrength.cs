﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("LogStrength")]
    public class LogStrength
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Type { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int Point { get; set; }
        public DateTime Date { get; set; }


        public int? ObjectiveId { get; set; }
        public int? LevelId { get; set; }
        public int? DurationId { get; set; }
        public int? FrequencyId { get; set; }
        public int Cycle { get; set; }
        public int? ProgramId { get; set; }

    }
}
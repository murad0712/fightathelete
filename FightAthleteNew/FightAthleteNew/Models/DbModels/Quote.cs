﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Quote")]
    public class Quote
    {
        public int Id { get; set; }
        [Required]
        public string Tekst { get; set; }
        [Required]
        public string Author { get; set; }
    }
}
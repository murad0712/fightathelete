﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Exercise_Level")]
    public class ExerciseLevel
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int LevelId { get; set; }
    }
}
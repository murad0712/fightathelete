﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("FavouriteProgram")]
    public class FavouriteProgram
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }

        public int? LevelId { get; set; }
        public int? DurationId { get; set; }
        public int? FrequencyId { get; set; }
    }
}
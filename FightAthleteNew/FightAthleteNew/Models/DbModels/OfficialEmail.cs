﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthleteNew.Models.DbModels
{
    [Table("OfficialEmail")]
    public class OfficialEmail
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
}
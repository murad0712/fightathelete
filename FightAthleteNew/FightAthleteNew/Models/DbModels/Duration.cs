﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Duration")]
    public class Duration
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Duration (In Week)")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Duration must be numeric")]
        public string Name { get; set; }
    }
}
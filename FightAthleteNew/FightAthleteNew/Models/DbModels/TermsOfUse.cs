﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("TermsOfUse")]
    public class TermsOfUse
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Program_Tool")]
    public class ProgramTool
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int ToolId { get; set; }
    }
}
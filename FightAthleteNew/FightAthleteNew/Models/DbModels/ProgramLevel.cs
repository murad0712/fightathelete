﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Program_Level")]
    public class ProgramLevel
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int LevelId { get; set; }
    }
}
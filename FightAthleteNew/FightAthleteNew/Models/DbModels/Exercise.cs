﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Exercise")]
    public class Exercise
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        public int IsRestExercise { get; set; }
        public int IsBasicVersion { get; set; }
        public int IsNew { get; set; }
        public int IsActive { get; set; }
        public string Duration { get; set; }
        /*public string Photo { get; set; }*/
        public string Video { get; set; }
        public string Thumbnail { get; set; }
        public int IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ExerciseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Video { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<MuscleGroup> MuscleGroups { get; set; }
        public List<Tool> Tools { get; set; }
        public int IsNew { get; set; }
    }
}
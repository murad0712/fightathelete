﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("MembershipType")]
    public class MembershipType
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Duration (In Month)")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Duration must be numeric")]
        public int Duration { get; set; }
        [Required]
        public double? Price { get; set; }
        public int CurrencyId { get; set; }
        public int Discount { get; set; }


        [ForeignKey("CurrencyId")]
        public virtual Currency Currencies { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
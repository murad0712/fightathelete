﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Exercise_MuscleGroup")]
    public class ExerciseMuscleGroup
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int MuscleGroupId { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Exercise_Tool")]
    public class ExerciseTool
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int ToolId { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("LogStrengthExercise")]
    public class LogStrengthExercise
    {
        public int Id { get; set; }
        public int LogStrengthId { get; set; }
        public string ExerciseName { get; set; }
    }
}
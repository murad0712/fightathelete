﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Program_Frequency")]
    public class ProgramFrequency
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int FrequencyId { get; set; }
    }
}
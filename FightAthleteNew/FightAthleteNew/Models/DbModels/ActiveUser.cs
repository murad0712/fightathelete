﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("ActiveUser")]
    public class ActiveUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime LastLogin { get; set; }
    }
}
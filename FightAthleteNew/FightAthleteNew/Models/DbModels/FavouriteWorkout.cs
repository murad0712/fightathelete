﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("FavouriteWorkout")]
    public class FavouriteWorkout
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public string WorkoutName { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }

        public int? ObjectiveId { get; set; }
        public int? LevelId { get; set; }
    }
}
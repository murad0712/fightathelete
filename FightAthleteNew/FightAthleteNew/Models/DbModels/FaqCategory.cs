﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("FaqCategory")]
    public class FaqCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
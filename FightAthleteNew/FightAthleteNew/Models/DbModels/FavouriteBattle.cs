﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("FavouriteBattle")]
    public class FavouriteBattle
    {
        public int Id { get; set; }
        public int BattleId { get; set; }
        public string BattleName { get; set; }
        public int UserId { get; set; }
        public DateTime DateTime { get; set; }

        public int? ObjectiveId { get; set; }
        public int? LevelId { get; set; }
    }
}
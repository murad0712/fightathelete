﻿using System.ComponentModel.DataAnnotations.Schema;

namespace FightAthleteNew.Models.DbModels
{
    [Table("Login")]
    public class Login
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
﻿using System.Collections.Generic;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Models.ViewModels
{
    public class StrengthWorkoutExerciseViewModel
    {
        public int Id { get; set; }
        public int LogStrengthId { get; set; }
        public string ExerciseName { get; set; }
        public List<LogStrengthSet> Sets { get; set; }
    }
}
﻿using System.Collections.Generic;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Models.ViewModels
{
    public class ProgramWorkoutViewModel
    {
        public int WorkoutId { get; set; }
        public string WorkoutName { get; set; }
        public int IsBattle { get; set; }
        public int Day { get; set; }
        public List<Level> Levels { get; set; }
        public List<Objective> Objectives { get; set; }
    }
}
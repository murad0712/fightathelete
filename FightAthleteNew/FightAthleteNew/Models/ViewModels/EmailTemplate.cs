﻿namespace FightAthleteNew.Models.ViewModels
{
    public class EmailTemplate
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public string To { get; set; }
    }
}
﻿namespace FightAthleteNew.Models.ViewModels
{
    public class FaqViewModel
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
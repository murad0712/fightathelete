﻿namespace FightAthleteNew.Models.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public string Birthdate { get; set; }
        public string Country { get; set; }
        public string EmailAddress { get; set; }
        public string MembershipSince { get; set; }
        public string MembershipType { get; set; }
        public string Photo { get; set; }
    }
}
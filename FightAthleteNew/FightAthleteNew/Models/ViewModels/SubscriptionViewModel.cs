﻿using System;

namespace FightAthleteNew.Models.ViewModels
{
    public class SubscriptionViewModel
    {
        public string TypeOfMembership { get; set; }
        public DateTime StartMembership { get; set; }
        public int Duration { get; set; }
    }
}
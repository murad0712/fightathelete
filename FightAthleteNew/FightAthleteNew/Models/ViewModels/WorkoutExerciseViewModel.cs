﻿using System.Collections.Generic;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Models.ViewModels
{
    public class WorkoutExerciseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IsRestExercise { get; set; }
        public string Video { get; set; }
        public string Reps { get; set; }
        public int RepHigh { get; set; }
        public string Sets { get; set; }
        public string Tempo { get; set; }
        public string Rests { get; set; }
        public string Duration { get; set; }
        public string Distance { get; set; }
        public string Weight { get; set; }

        public List<Level> Levels { get; set; }
        public List<MuscleGroup> MuscleGroups { get; set; }
        public List<Tool> Tools { get; set; }

    }
}
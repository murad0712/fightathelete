﻿namespace FightAthleteNew.Models.ViewModels
{
    public class CircuitWorkoutViewModel
    {
        public int ExerciseId { get; set; }
        public string ExerciseName { get; set; }
        public string Reps { get; set; }
        public int RepHigh { get; set; }
        public string Distance { get; set; }
        public string Weight { get; set; }


        public int IsRestExercise { get; set; }
        public string Duration { get; set; }
    }
}
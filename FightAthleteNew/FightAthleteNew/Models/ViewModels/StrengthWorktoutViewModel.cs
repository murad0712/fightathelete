﻿using System;
using System.Collections.Generic;

namespace FightAthleteNew.Models.ViewModels
{
    public class StrengthWorktoutViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Type { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public int Point { get; set; }
        public DateTime Date { get; set; }
        public int? ObjectiveId { get; set; }
        public int? LevelId { get; set; }
        public int Cycle { get; set; }
        public List<StrengthWorkoutExerciseViewModel> WorkoutExercises { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class LogStrengthSetsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/LogStrengthSets
        public IQueryable<LogStrengthSet> GetLogStrengthSets()
        {
            return db.LogStrengthSets;
        }

        // GET: api/LogStrengthSets/5
        [ResponseType(typeof(LogStrengthSet))]
        public IHttpActionResult GetLogStrengthSet(int id)
        {
            LogStrengthSet logStrengthSet = db.LogStrengthSets.Find(id);
            if (logStrengthSet == null)
            {
                return NotFound();
            }

            return Ok(logStrengthSet);
        }

        // GET: api/LogStrengthSets/GetLogStrengthSetByExerciseId/5
        [ResponseType(typeof(LogStrengthSet))]
        public List<LogStrengthSet> GetLogStrengthSetByExerciseId(int id)
        {
            List<LogStrengthSet> logStrengthSet = db.LogStrengthSets.Where(_=>_.LogStrengthExerciseId == id).ToList();

            return logStrengthSet;
        }

        // PUT: api/LogStrengthSets/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLogStrengthSet(int id, LogStrengthSet logStrengthSet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != logStrengthSet.Id)
            {
                return BadRequest();
            }

            db.Entry(logStrengthSet).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogStrengthSetExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LogStrengthSets
        [ResponseType(typeof(LogStrengthSet))]
        public IHttpActionResult PostLogStrengthSet(LogStrengthSet logStrengthSet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LogStrengthSets.Add(logStrengthSet);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = logStrengthSet.Id }, logStrengthSet);
        }

        // DELETE: api/LogStrengthSets/5
        [ResponseType(typeof(LogStrengthSet))]
        public IHttpActionResult DeleteLogStrengthSet(int id)
        {
            LogStrengthSet logStrengthSet = db.LogStrengthSets.Find(id);
            if (logStrengthSet == null)
            {
                return NotFound();
            }

            db.LogStrengthSets.Remove(logStrengthSet);
            db.SaveChanges();

            return Ok(logStrengthSet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogStrengthSetExists(int id)
        {
            return db.LogStrengthSets.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class HallOfFamesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/HallOfFames
        public IQueryable<HallOfFame> GetHallOfFames()
        {
            return db.HallOfFames;
        }

        // GET: api/HallOfFames/GetHallOfFamesByUserId
        public IQueryable<HallOfFame> GetHallOfFamesByUserId(int id)
        {
            return db.HallOfFames.Where(_ => _.UserId == id && _.IsDeleted == 0);
        }

        // GET: api/HallOfFames/DeleteHallOfFamesById
        public bool DeleteHallOfFamesById(int id)
        {
            HallOfFame hallOfFame = db.HallOfFames.Find(id);
            if (hallOfFame == null)
            {
                return false;
            }
            hallOfFame.IsDeleted = 1;
            db.Entry(hallOfFame).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HallOfFameExists(id))
                {
                    return false;
                }
                else
                {
                    throw;
                }
            }
            return true;
        }

        // GET: api/HallOfFames/GetHallOfFamesByBattleId?userId=18&battleId=3
        public IQueryable<HallOfFame> GetHallOfFamesByBattleId(int userId, int battleId)
        {
            return db.HallOfFames.Where(_ => _.UserId == userId && _.BattleId == battleId && _.IsDeleted == 0);
        }

        // GET: api/HallOfFames/5
        [ResponseType(typeof(HallOfFame))]
        public IHttpActionResult GetHallOfFame(int id)
        {
            HallOfFame hallOfFame = db.HallOfFames.Find(id);
            if (hallOfFame == null)
            {
                return NotFound();
            }

            return Ok(hallOfFame);
        }

        // PUT: api/HallOfFames/PutHallOfFame/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutHallOfFame(int id, HallOfFame hallOfFame)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hallOfFame.Id)
            {
                return BadRequest();
            }

            db.Entry(hallOfFame).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HallOfFameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/HallOfFames/PostHallOfFame
        [ResponseType(typeof(HallOfFame))]
        public IHttpActionResult PostHallOfFame(HallOfFame hallOfFame)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.HallOfFames.Add(hallOfFame);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = hallOfFame.Id }, hallOfFame);
        }

        // DELETE: api/HallOfFames/DeleteHallOfFame/5
        [ResponseType(typeof(HallOfFame))]
        public IHttpActionResult DeleteHallOfFame(int id)
        {
            HallOfFame hallOfFame = db.HallOfFames.Find(id);
            if (hallOfFame == null)
            {
                return NotFound();
            }

            db.HallOfFames.Remove(hallOfFame);
            db.SaveChanges();

            return Ok(hallOfFame);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HallOfFameExists(int id)
        {
            return db.HallOfFames.Count(e => e.Id == id) > 0;
        }
    }
}
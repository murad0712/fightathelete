﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    /*[Authorize]*/
    public class UsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Users/GetUserses
        public IQueryable<User> GetUserses()
        {
            return db.Userses;
        }

        // GET: api/Users/GetUserByEmail
        public IQueryable<User> GetUserByEmail(string email)
        {
            return db.Userses.Where(_ => _.EmailAddress == email);
        }

        // GET: api/Users/GetUserById
        public User GetUserById(int id)
        {
            return db.Userses.FirstOrDefault(_ => _.Id == id);
        }

        // GET: api/Users/GetUser/5
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = db.Userses.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        // PUT: api/Users/PutUser/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(int id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.Id)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users/UploadPhoto/5
        public IHttpActionResult UploadPhoto(int id)
        {
            //Check if valid user
            var user = db.Userses.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            // Check if request has file
            var files = HttpContext.Current.Request.Files;
            if (files.Count > 0 && files[0].ContentLength > 0)
            {
                var rootPath = HttpContext.Current.Server.MapPath("~/Upload/Users");
                var file = files[0];

                //Check directory exists for user
                if (!Directory.Exists(rootPath + $"/{id}"))
                {
                    Directory.CreateDirectory(rootPath + $"/{id}");
                }

                //Check if file already exists
                var filePath = rootPath + $"/{id}/{Path.GetFileName(file.FileName)}";

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                file.SaveAs(filePath);

                //Save file relative path in db
                var relativePath = $"/Upload/Users/{id}/{Path.GetFileName(file.FileName)}";
                user.Photo = relativePath;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return Ok(relativePath);
            }

            return BadRequest();
        }

        // POST: api/Users
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Userses.Add(user);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = user.Id }, user);
        }

        // GET: api/Users/UpdateLastLogin/5
        public IHttpActionResult UpdateLastLogin(int id)
        {
            var user = db.Userses.Find(id);

            if (user != null)
            {
                user.LastLogin = DateTime.UtcNow;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return Ok();
            }

            return NotFound();
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(int id)
        {
            User user = db.Userses.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Userses.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(int id)
        {
            return db.Userses.Count(e => e.Id == id) > 0;
        }
    }
}
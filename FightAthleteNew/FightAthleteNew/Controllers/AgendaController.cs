﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class AgendaController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Agenda
        public IQueryable<Agenda> GetAgendas()
        {
            return db.Agendas;
        }

        // GET: api/Agenda/GetAgendasByUserId
        public IQueryable<Agenda> GetAgendasByUserId(int id)
        {
            return db.Agendas.Where(_=>_.User_Id == id);
        }

        // GET: api/Agenda/GetAgendasByDateRange
        public IQueryable<Agenda> GetAgendasByDateRange(int id, string date1, string date2)
        {
            DateTime start = DateTime.Parse(date1);
            DateTime end = DateTime.Parse(date2);
            var agenda = (from agend in db.Agendas
                where agend.Date >= start && agend.Date <= end && agend.User_Id == id
                orderby agend.Date ascending 
                select agend);
            return agenda;
        }

        // GET: api/Agenda/5
        [ResponseType(typeof(Agenda))]
        public IHttpActionResult GetAgenda(int id)
        {
            Agenda agenda = db.Agendas.Find(id);
            if (agenda == null)
            {
                return NotFound();
            }

            return Ok(agenda);
        }

        // PUT: api/Agenda/PutAgenda/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAgenda(int id, Agenda agenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != agenda.Id)
            {
                return BadRequest();
            }

            db.Entry(agenda).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AgendaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Agenda
        [ResponseType(typeof(Agenda))]
        public IHttpActionResult PostAgenda(Agenda agenda)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Agendas.Add(agenda);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = agenda.Id }, agenda);
        }

        // DELETE: api/Agenda/DeleteAgenda/5
        [ResponseType(typeof(Agenda))]
        public IHttpActionResult DeleteAgenda(int id)
        {
            Agenda agenda = db.Agendas.Find(id);
            if (agenda == null)
            {
                return NotFound();
            }

            db.Agendas.Remove(agenda);
            db.SaveChanges();

            return Ok(agenda);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AgendaExists(int id)
        {
            return db.Agendas.Count(e => e.Id == id) > 0;
        }
    }
}
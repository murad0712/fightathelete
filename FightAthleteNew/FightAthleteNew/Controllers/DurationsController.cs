﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class DurationsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Durations
        public IQueryable<Duration> GetDurations()
        {
            return db.Durations;
        }

        // GET: api/Durations/5
        [ResponseType(typeof(Duration))]
        public IHttpActionResult GetDuration(int id)
        {
            Duration duration = db.Durations.Find(id);
            if (duration == null)
            {
                return NotFound();
            }

            return Ok(duration);
        }

        // PUT: api/Durations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDuration(int id, Duration duration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != duration.Id)
            {
                return BadRequest();
            }

            db.Entry(duration).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DurationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Durations
        [ResponseType(typeof(Duration))]
        public IHttpActionResult PostDuration(Duration duration)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Durations.Add(duration);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = duration.Id }, duration);
        }

        // DELETE: api/Durations/5
        [ResponseType(typeof(Duration))]
        public IHttpActionResult DeleteDuration(int id)
        {
            Duration duration = db.Durations.Find(id);
            if (duration == null)
            {
                return NotFound();
            }

            db.Durations.Remove(duration);
            db.SaveChanges();

            return Ok(duration);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DurationExists(int id)
        {
            return db.Durations.Count(e => e.Id == id) > 0;
        }
    }
}
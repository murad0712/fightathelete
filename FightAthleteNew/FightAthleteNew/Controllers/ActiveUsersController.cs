﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class ActiveUsersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ActiveUsers/GetActiveUsers
        public IQueryable<ActiveUser> GetActiveUsers()
        {
            return db.ActiveUsers;
        }

        // GET: api/ActiveUsers/GetActiveUsersById
        public ActiveUser GetActiveUsersById(int id)
        {
            return db.ActiveUsers.FirstOrDefault(_=>_.UserId == id);
        }

        // GET: api/ActiveUsers/5
        [ResponseType(typeof(ActiveUser))]
        public IHttpActionResult GetActiveUser(int id)
        {
            ActiveUser activeUser = db.ActiveUsers.Find(id);
            if (activeUser == null)
            {
                return NotFound();
            }

            return Ok(activeUser);
        }

        // PUT: api/ActiveUsers/PutActiveUser/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutActiveUser(int id, ActiveUser activeUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != activeUser.Id)
            {
                return BadRequest();
            }

            db.Entry(activeUser).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActiveUserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ActiveUsers
        [ResponseType(typeof(ActiveUser))]
        public IHttpActionResult PostActiveUser(ActiveUser activeUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ActiveUsers.Add(activeUser);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = activeUser.Id }, activeUser);
        }

        // DELETE: api/ActiveUsers/5
        [ResponseType(typeof(ActiveUser))]
        public IHttpActionResult DeleteActiveUser(int id)
        {
            ActiveUser activeUser = db.ActiveUsers.Find(id);
            if (activeUser == null)
            {
                return NotFound();
            }

            db.ActiveUsers.Remove(activeUser);
            db.SaveChanges();

            return Ok(activeUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ActiveUserExists(int id)
        {
            return db.ActiveUsers.Count(e => e.Id == id) > 0;
        }
    }
}
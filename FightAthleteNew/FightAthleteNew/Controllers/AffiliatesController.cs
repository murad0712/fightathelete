﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class AffiliatesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Affiliates
        public IQueryable<Affiliate> GetAffiliates()
        {
            return db.Affiliates;
        }

        // GET: api/Affiliates/5
        [ResponseType(typeof(Affiliate))]
        public IHttpActionResult GetAffiliate(int id)
        {
            Affiliate affiliate = db.Affiliates.Find(id);
            if (affiliate == null)
            {
                return NotFound();
            }

            return Ok(affiliate);
        }

        // PUT: api/Affiliates/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAffiliate(int id, Affiliate affiliate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != affiliate.Id)
            {
                return BadRequest();
            }

            db.Entry(affiliate).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AffiliateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Affiliates
        [ResponseType(typeof(Affiliate))]
        public IHttpActionResult PostAffiliate(Affiliate affiliate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Affiliates.Add(affiliate);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = affiliate.Id }, affiliate);
        }

        // DELETE: api/Affiliates/5
        [ResponseType(typeof(Affiliate))]
        public IHttpActionResult DeleteAffiliate(int id)
        {
            Affiliate affiliate = db.Affiliates.Find(id);
            if (affiliate == null)
            {
                return NotFound();
            }

            db.Affiliates.Remove(affiliate);
            db.SaveChanges();

            return Ok(affiliate);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AffiliateExists(int id)
        {
            return db.Affiliates.Count(e => e.Id == id) > 0;
        }

        // POST: api/Affiliates/SendEmailViaWebApi
        private void SendEmailViaWebApi()
        {
            string subject = "Test Mail";
            string body = "This is for testing SMTP mail from GMAIL";
            string FromMail = "asifimtiaz29@gmail.com";
            string emailTo = "asif.imtiaz@bs-23.net";
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient();
            mail.From = new MailAddress(FromMail);
            mail.To.Add(emailTo);
            mail.Subject = subject;
            mail.Body = body;
            SmtpServer.Port = 25;
            SmtpServer.Credentials = new System.Net.NetworkCredential("asifimtiaz29@gmail.com", "asif0302");
            SmtpServer.EnableSsl = false;
            SmtpServer.Send(mail);
        }
    }
}
﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class FavouriteExercisesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/FavouriteExercises
        public IQueryable<FavouriteExercise> GetFavouriteExercises()
        {
            return db.FavouriteExercises;
        }

        // GET: api/FavouriteExercises/GetFavouriteExercisesByUserId
        public IQueryable<FavouriteExercise> GetFavouriteExercisesByUserId(int id)
        {
            return db.FavouriteExercises.Where(_=>_.UserId == id).OrderByDescending(_=>_.DateTime);
        }

        // GET: api/FavouriteExercises/5
        [ResponseType(typeof(FavouriteExercise))]
        public IHttpActionResult GetFavouriteExercise(int id)
        {
            FavouriteExercise favouriteExercise = db.FavouriteExercises.Find(id);
            if (favouriteExercise == null)
            {
                return NotFound();
            }

            return Ok(favouriteExercise);
        }

        // PUT: api/FavouriteExercises/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFavouriteExercise(int id, FavouriteExercise favouriteExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != favouriteExercise.Id)
            {
                return BadRequest();
            }

            db.Entry(favouriteExercise).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavouriteExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FavouriteExercises/PostFavouriteExercise
        [ResponseType(typeof(FavouriteExercise))]
        public IHttpActionResult PostFavouriteExercise(FavouriteExercise favouriteExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            favouriteExercise.DateTime = DateTime.Now.Date;

            db.FavouriteExercises.Add(favouriteExercise);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = favouriteExercise.Id }, favouriteExercise);
        }

        // DELETE: api/FavouriteExercises/5
        [ResponseType(typeof(FavouriteExercise))]
        public IHttpActionResult DeleteFavouriteExercise(int id)
        {
            FavouriteExercise favouriteExercise = db.FavouriteExercises.Find(id);
            if (favouriteExercise == null)
            {
                return NotFound();
            }

            db.FavouriteExercises.Remove(favouriteExercise);
            db.SaveChanges();

            return Ok(favouriteExercise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FavouriteExerciseExists(int id)
        {
            return db.FavouriteExercises.Count(e => e.Id == id) > 0;
        }
    }
}
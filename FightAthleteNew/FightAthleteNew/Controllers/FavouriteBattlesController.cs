﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class FavouriteBattlesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/FavouriteBattles
        public IQueryable<FavouriteBattle> GetFavouriteBattles()
        {
            return db.FavouriteBattles;
        }

        // GET: api/FavouriteBattles/GetFavouriteBattlesByUserId
        public IQueryable<FavouriteBattle> GetFavouriteBattlesByUserId(int id)
        {
            return db.FavouriteBattles.Where(_ => _.UserId == id).OrderByDescending(_ => _.DateTime);
        }

        // GET: api/FavouriteBattles/5
        [ResponseType(typeof(FavouriteBattle))]
        public IHttpActionResult GetFavouriteBattle(int id)
        {
            FavouriteBattle favouriteBattle = db.FavouriteBattles.Find(id);
            if (favouriteBattle == null)
            {
                return NotFound();
            }

            return Ok(favouriteBattle);
        }

        // PUT: api/FavouriteBattles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFavouriteBattle(int id, FavouriteBattle favouriteBattle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != favouriteBattle.Id)
            {
                return BadRequest();
            }

            db.Entry(favouriteBattle).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavouriteBattleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FavouriteBattles/PostFavouriteBattle
        [ResponseType(typeof(FavouriteBattle))]
        public IHttpActionResult PostFavouriteBattle(FavouriteBattle favouriteBattle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            favouriteBattle.DateTime = DateTime.Now.Date;

            db.FavouriteBattles.Add(favouriteBattle);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = favouriteBattle.Id }, favouriteBattle);
        }

        // DELETE: api/FavouriteBattles/5
        [ResponseType(typeof(FavouriteBattle))]
        public IHttpActionResult DeleteFavouriteBattle(int id)
        {
            FavouriteBattle favouriteBattle = db.FavouriteBattles.Find(id);
            if (favouriteBattle == null)
            {
                return NotFound();
            }

            db.FavouriteBattles.Remove(favouriteBattle);
            db.SaveChanges();

            return Ok(favouriteBattle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FavouriteBattleExists(int id)
        {
            return db.FavouriteBattles.Count(e => e.Id == id) > 0;
        }
    }
}
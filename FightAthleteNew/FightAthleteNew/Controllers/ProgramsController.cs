﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;
using FightAthleteNew.Services;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class ProgramsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ProgramService _programService = new ProgramService();

        // GET: api/Programs/GetPrograms
        public IQueryable<Program> GetPrograms()
        {
            return db.Programs;
        }

        // GET: api/Programs/GetProgram/5
        [ResponseType(typeof(Program))]
        public IHttpActionResult GetProgram(int id)
        {
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return NotFound();
            }

            return Ok(program);
        }

        // PUT: api/Programs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProgram(int id, Program program)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != program.Id)
            {
                return BadRequest();
            }

            db.Entry(program).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgramExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Programs
        [ResponseType(typeof(Program))]
        public IHttpActionResult PostProgram(Program program)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Programs.Add(program);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = program.Id }, program);
        }

        // DELETE: api/Programs/5
        [ResponseType(typeof(Program))]
        public IHttpActionResult DeleteProgram(int id)
        {
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return NotFound();
            }

            db.Programs.Remove(program);
            db.SaveChanges();

            return Ok(program);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgramExists(int id)
        {
            return db.Programs.Count(e => e.Id == id) > 0;
        }

        // GET: api/Programs/GetAllPrograms
        public List<ProgramViewModel> GetAllPrograms()
        {
            return _programService.GetAllPrograms().Where(_=>_.IsDeleted == 0 && _.IsActive == 1).ToList();
        }

        // GET: api/Programs/GetProgramsById/5
        public ProgramViewModel GetProgramsById(int id)
        {
            return _programService.GetAllPrograms().FirstOrDefault(_=>_.Id == id);
        }

        //Program List Page
        // GET: api/Programs/GetProgramByLevel/5
        public List<ProgramDTO> GetProgramByLevel(int id)
        {
            return _programService.GetProgramByLevel(id);
        }
    }
}
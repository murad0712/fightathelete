﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class ObjectivesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Objectives/GetObjectives
        public IQueryable<Objective> GetObjectives()
        {
            return db.Objectives;
        }
        
        // GET: api/Objectives/GetObjectivesByWorkoutId?workoutId=18
        public IQueryable<Objective> GetObjectivesByWorkoutId(int workoutId)
        {
            var objectiveList = (from objective in db.Objectives
                join workoutObjective in db.WorkoutObjectives on objective.Id equals workoutObjective.ObjectiveId
                where workoutObjective.WorkoutId == workoutId
            select objective
            );
            return objectiveList;
        }

        // GET: api/Objectives/GetObjective/5
        [ResponseType(typeof(Objective))]
        public IHttpActionResult GetObjective(int id)
        {
            Objective objective = db.Objectives.Find(id);
            if (objective == null)
            {
                return NotFound();
            }

            return Ok(objective);
        }

        // PUT: api/Objectives/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutObjective(int id, Objective objective)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != objective.Id)
            {
                return BadRequest();
            }

            db.Entry(objective).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjectiveExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Objectives
        [ResponseType(typeof(Objective))]
        public IHttpActionResult PostObjective(Objective objective)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Objectives.Add(objective);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = objective.Id }, objective);
        }

        // DELETE: api/Objectives/5
        [ResponseType(typeof(Objective))]
        public IHttpActionResult DeleteObjective(int id)
        {
            Objective objective = db.Objectives.Find(id);
            if (objective == null)
            {
                return NotFound();
            }

            db.Objectives.Remove(objective);
            db.SaveChanges();

            return Ok(objective);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObjectiveExists(int id)
        {
            return db.Objectives.Count(e => e.Id == id) > 0;
        }

        // GET: api/Objectives/GetObjectivesOfWorkout
        public IQueryable<Objective> GetObjectivesOfWorkout()
        {
            var objectiveList = (from objective in db.Objectives
                                 join workoutObjective in db.WorkoutObjectives on objective.Id equals workoutObjective.ObjectiveId
                                 join workout in db.Workouts on workoutObjective.WorkoutId equals workout.Id
            
                                 where workout.IsActive == 1 && workout.IsDeleted == 0 &&
                                        workout.IsBattleWorkout == 1
                                 select objective
                                ).Distinct();
            return objectiveList;
        }

        // GET: api/Objectives/GetObjectivesOfHallOfFame/2
        public IQueryable<Objective> GetObjectivesOfHallOfFame(int userId)
        {
            var objectiveList = (from objective in db.Objectives
                                 join hallOfFame in db.HallOfFames on objective.Id equals hallOfFame.ObjectiveId

                                 where hallOfFame.UserId == userId
                                 select objective
                                ).Distinct();
            return objectiveList;
        }
    }
}
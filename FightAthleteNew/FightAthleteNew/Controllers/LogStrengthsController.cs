﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class LogStrengthsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/LogStrengths
        public IQueryable<LogStrength> GetLogStrengths()
        {
            return db.LogStrengths;
        }

        // GET: api/LogStrengths/GetLogStrengthsByUserId?userId=18&type=Workout
        public IQueryable<LogStrength> GetLogStrengthsByUserId(int userId, string type)
        {
            var logs = (from strength in db.LogStrengths
                        where strength.UserId == userId && strength.Type == type
                        orderby strength.Date descending 
                        select strength);
            return logs;
        }

        // GET: api/LogStrengths/5
        [ResponseType(typeof (LogStrength))]
        public IHttpActionResult GetLogStrength(int id)
        {
            LogStrength logStrength = db.LogStrengths.Find(id);
            if (logStrength == null)
            {
                return NotFound();
            }

            return Ok(logStrength);
        }

        // PUT: api/LogStrengths/5
        [ResponseType(typeof (void))]
        public IHttpActionResult PutLogStrength(int id, LogStrength logStrength)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != logStrength.Id)
            {
                return BadRequest();
            }

            db.Entry(logStrength).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogStrengthExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LogStrengths/PostLogStrength
        [ResponseType(typeof (LogStrength))]
        public IHttpActionResult PostLogStrength(LogStrength logStrength)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LogStrengths.Add(logStrength);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new {id = logStrength.Id}, logStrength);
        }

        // DELETE: api/LogStrengths/5
        [ResponseType(typeof (LogStrength))]
        public IHttpActionResult DeleteLogStrength(int id)
        {
            LogStrength logStrength = db.LogStrengths.Find(id);
            if (logStrength == null)
            {
                return NotFound();
            }

            db.LogStrengths.Remove(logStrength);
            db.SaveChanges();

            return Ok(logStrength);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogStrengthExists(int id)
        {
            return db.LogStrengths.Count(e => e.Id == id) > 0;
        }


        // GET: api/LogStrengths/GetAllStrengthWorkouts?userId=18&workoutId=2&type=Workout
        /*public IQueryable<StrengthWorktoutViewModel> GetAllStrengthWorkouts(int userId, int workoutId, string type)
        {
            var strengthWorkoutVm = (from logStrength in db.LogStrengths
                                     where logStrength.UserId == userId && logStrength.TypeId == workoutId && logStrength.Type == type
                select new StrengthWorktoutViewModel()
                {
                    Id = logStrength.Id,
                    UserId = logStrength.UserId,
                    Type = logStrength.Type,
                    TypeId = logStrength.TypeId,
                    TypeName = logStrength.TypeName,
                    Date = logStrength.Date,
                    Point = logStrength.Point,
                    WorkoutExercises = (from logStrengthExercise in db.LogStrengthExercises
                        join logStrength1 in db.LogStrengths on logStrengthExercise.LogStrengthId equals logStrength1.Id
                        select new StrengthWorkoutExerciseViewModel()
                        {
                            Id = logStrengthExercise.Id,
                            ExerciseName = logStrengthExercise.ExerciseName,
                            LogStrengthId = logStrength.Id,
                            Sets = (from logStrengthSet in db.LogStrengthSets
                                join logStrengthExercise1 in db.LogStrengthExercises on logStrengthSet.LogStrengthExerciseId equals logStrengthExercise1.Id
                                select logStrengthSet
                             ).ToList()
                        }
                     ).ToList()
                });
            return strengthWorkoutVm;
        }*/


        LogStrengthExercisesController _strengthExercises = new LogStrengthExercisesController();
        // GET: api/LogStrengths/GetAllStrengthWorkouts?userId=18&workoutId=2&type=Workout
        public List<StrengthWorktoutViewModel> GetAllStrengthWorkouts(int userId, int workoutId, string type)
        {
            var strengthList = db.LogStrengths.Where(_ => _.UserId == userId && _.TypeId == workoutId && _.Type == type).OrderByDescending(_=>_.Date).ToList();
            List <StrengthWorktoutViewModel> strengthWorktoutList = new List<StrengthWorktoutViewModel>();


            foreach (var logStrength in strengthList)
            {
                StrengthWorktoutViewModel strengthWorktout = new StrengthWorktoutViewModel()
                {
                    Type = logStrength.Type,
                    TypeId = logStrength.TypeId,
                    TypeName = logStrength.TypeName,
                    Point = logStrength.Point,
                    UserId = logStrength.UserId,
                    Date = logStrength.Date,
                    ObjectiveId = logStrength.ObjectiveId,
                    LevelId = logStrength.LevelId,
                    Cycle = logStrength.Cycle,
                    WorkoutExercises = _strengthExercises.GetLogStrengthExerciseByStrengthId(logStrength.Id)
                };
                strengthWorktoutList.Add(strengthWorktout);
            }
            return strengthWorktoutList;

        }

        // GET: api/LogStrengths/GetAllStrengthWorkouts?userId=18&workoutId=2&programId=2&type=Workout
        public List<StrengthWorktoutViewModel> GetAllStrengthWorkouts(int userId, int workoutId, int programId, string type)
        {
            var strengthList = db.LogStrengths.Where(_ => _.UserId == userId && _.TypeId == workoutId && _.Type == type && _.ProgramId == programId).OrderByDescending(_ => _.Date).ToList();
            List<StrengthWorktoutViewModel> strengthWorktoutList = new List<StrengthWorktoutViewModel>();


            foreach (var logStrength in strengthList)
            {
                StrengthWorktoutViewModel strengthWorktout = new StrengthWorktoutViewModel()
                {
                    Type = logStrength.Type,
                    TypeId = logStrength.TypeId,
                    TypeName = logStrength.TypeName,
                    Point = logStrength.Point,
                    UserId = logStrength.UserId,
                    Date = logStrength.Date,
                    ObjectiveId = logStrength.ObjectiveId,
                    LevelId = logStrength.LevelId,
                    Cycle = logStrength.Cycle,
                    WorkoutExercises = _strengthExercises.GetLogStrengthExerciseByStrengthId(logStrength.Id)
                };
                strengthWorktoutList.Add(strengthWorktout);
            }
            return strengthWorktoutList;

        }

        // GET: api/LogStrengths/GetStrengthWorkoutsForProgram?userId=18&workoutId=2&programId=2
        public List<StrengthWorktoutViewModel> GetStrengthWorkoutsForProgram(int userId, int workoutId, int programId)
        {
            var strengthList = db.LogStrengths.Where(_ => _.UserId == userId && _.TypeId == workoutId && _.ProgramId == programId).OrderByDescending(_ => _.Date).ToList();
            List<StrengthWorktoutViewModel> strengthWorktoutList = new List<StrengthWorktoutViewModel>();


            foreach (var logStrength in strengthList)
            {
                StrengthWorktoutViewModel strengthWorktout = new StrengthWorktoutViewModel()
                {
                    Type = logStrength.Type,
                    TypeId = logStrength.TypeId,
                    TypeName = logStrength.TypeName,
                    Point = logStrength.Point,
                    UserId = logStrength.UserId,
                    Date = logStrength.Date,
                    ObjectiveId = logStrength.ObjectiveId,
                    LevelId = logStrength.LevelId,
                    Cycle = logStrength.Cycle,
                    WorkoutExercises = _strengthExercises.GetLogStrengthExerciseByStrengthId(logStrength.Id)
                };
                strengthWorktoutList.Add(strengthWorktout);
            }
            return strengthWorktoutList;

        }

    }

}
﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class FavouriteWorkoutsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/FavouriteWorkouts
        public IQueryable<FavouriteWorkout> GetFavouriteWorkouts()
        {
            return db.FavouriteWorkouts;
        }

        // GET: api/FavouriteWorkouts/GetFavouriteWorkoutsByUserId
        public IQueryable<FavouriteWorkout> GetFavouriteWorkoutsByUserId(int id)
        {
            return db.FavouriteWorkouts.Where(_ => _.UserId == id).OrderByDescending(_ => _.DateTime);
        }

        // GET: api/FavouriteWorkouts/5
        [ResponseType(typeof(FavouriteWorkout))]
        public IHttpActionResult GetFavouriteWorkout(int id)
        {
            FavouriteWorkout favouriteWorkout = db.FavouriteWorkouts.Find(id);
            if (favouriteWorkout == null)
            {
                return NotFound();
            }

            return Ok(favouriteWorkout);
        }

        // PUT: api/FavouriteWorkouts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFavouriteWorkout(int id, FavouriteWorkout favouriteWorkout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != favouriteWorkout.Id)
            {
                return BadRequest();
            }

            db.Entry(favouriteWorkout).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavouriteWorkoutExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FavouriteWorkouts/PostFavouriteWorkout
        [ResponseType(typeof(FavouriteWorkout))]
        public IHttpActionResult PostFavouriteWorkout(FavouriteWorkout favouriteWorkout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            favouriteWorkout.DateTime = DateTime.Now.Date;

            db.FavouriteWorkouts.Add(favouriteWorkout);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = favouriteWorkout.Id }, favouriteWorkout);
        }

        // DELETE: api/FavouriteWorkouts/5
        [ResponseType(typeof(FavouriteWorkout))]
        public IHttpActionResult DeleteFavouriteWorkout(int id)
        {
            FavouriteWorkout favouriteWorkout = db.FavouriteWorkouts.Find(id);
            if (favouriteWorkout == null)
            {
                return NotFound();
            }

            db.FavouriteWorkouts.Remove(favouriteWorkout);
            db.SaveChanges();

            return Ok(favouriteWorkout);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FavouriteWorkoutExists(int id)
        {
            return db.FavouriteWorkouts.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class FavouriteProgramsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/FavouritePrograms
        public IQueryable<FavouriteProgram> GetFavouritePrograms()
        {
            return db.FavouritePrograms;
        }

        // GET: api/FavouritePrograms/GetFavouriteProgramsByUserId
        public IQueryable<FavouriteProgram> GetFavouriteProgramsByUserId(int id)
        {
            return db.FavouritePrograms.Where(_ => _.UserId == id).OrderByDescending(_ => _.DateTime);
        }

        // GET: api/FavouritePrograms/5
        [ResponseType(typeof(FavouriteProgram))]
        public IHttpActionResult GetFavouriteProgram(int id)
        {
            FavouriteProgram favouriteProgram = db.FavouritePrograms.Find(id);
            if (favouriteProgram == null)
            {
                return NotFound();
            }

            return Ok(favouriteProgram);
        }

        // PUT: api/FavouritePrograms/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFavouriteProgram(int id, FavouriteProgram favouriteProgram)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != favouriteProgram.Id)
            {
                return BadRequest();
            }

            db.Entry(favouriteProgram).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavouriteProgramExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/FavouritePrograms/PostFavouriteProgram
        [ResponseType(typeof(FavouriteProgram))]
        public IHttpActionResult PostFavouriteProgram(FavouriteProgram favouriteProgram)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            favouriteProgram.DateTime = DateTime.Now.Date;

            db.FavouritePrograms.Add(favouriteProgram);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = favouriteProgram.Id }, favouriteProgram);
        }

        // DELETE: api/FavouritePrograms/5
        [ResponseType(typeof(FavouriteProgram))]
        public IHttpActionResult DeleteFavouriteProgram(int id)
        {
            FavouriteProgram favouriteProgram = db.FavouritePrograms.Find(id);
            if (favouriteProgram == null)
            {
                return NotFound();
            }

            db.FavouritePrograms.Remove(favouriteProgram);
            db.SaveChanges();

            return Ok(favouriteProgram);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FavouriteProgramExists(int id)
        {
            return db.FavouritePrograms.Count(e => e.Id == id) > 0;
        }
    }
}
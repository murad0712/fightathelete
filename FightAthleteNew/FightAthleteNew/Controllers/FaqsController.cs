﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class FaqsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Faqs/GetFaqs
        public IQueryable<FaqViewModel> GetFaqs()
        {
            var faqs = (from faq in db.Faqs
                join faqCategory in db.FaqCategories on faq.CategoryId equals faqCategory.Id
                select new FaqViewModel()
                {
                    Id = faq.Id,
                    Category = faqCategory.Name,
                    Answer = faq.Answer,
                    Question = faq.Question
                }
                );
            return faqs;
        }

        // GET: api/Faqs/GetFaqCategories
        public IQueryable<FaqCategory> GetFaqCategories()
        {
            return db.FaqCategories;
        }

        // GET: api/Faqs/GetFaqByCategory?categoryId=5
        public IQueryable<DTOFaq> GetFaqByCategory(int categoryId)
        {
            var faqlist = (from faq in db.Faqs
                           where faq.CategoryId == categoryId
                           select new DTOFaq()
                           {
                                Id = faq.Id,
                                Question = faq.Question,
                                Answer = faq.Answer
                            });
            return faqlist;
        }

        // GET: api/Faqs/GetFaq/5
        public DTOFaq GetFaq(int id)
        {
            var faq = db.Faqs.FirstOrDefault(_=>_.Id == id);
            DTOFaq faqDTO = new DTOFaq()
            {
                Id = faq.Id,
                Answer = faq.Answer,
                Question = faq.Question
            };
            return faqDTO;
        }

        // PUT: api/Faqs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFaq(int id, Faq faq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != faq.Id)
            {
                return BadRequest();
            }

            db.Entry(faq).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FaqExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Faqs
        [ResponseType(typeof(Faq))]
        public IHttpActionResult PostFaq(Faq faq)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Faqs.Add(faq);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = faq.Id }, faq);
        }

        // DELETE: api/Faqs/5
        [ResponseType(typeof(Faq))]
        public IHttpActionResult DeleteFaq(int id)
        {
            Faq faq = db.Faqs.Find(id);
            if (faq == null)
            {
                return NotFound();
            }

            db.Faqs.Remove(faq);
            db.SaveChanges();

            return Ok(faq);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FaqExists(int id)
        {
            return db.Faqs.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;
using FightAthleteNew.Services;

namespace FightAthleteNew.Controllers
{
    //[Authorize]
    public class WorkoutsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly WorkoutService _workoutService;

        public WorkoutsController()
        {
            _workoutService = new WorkoutService();
        }

        // GET: api/Workouts/GetWorkouts
        public IQueryable<Workout> GetWorkouts()
        {
            return db.Workouts.Where(_=>_.IsDeleted==0).Where(_=>_.IsBattleWorkout == 0);
        }

        // GET: api/Workouts/GetWorkouts
        public IQueryable<Workout> GetActiveWorkouts()
        {
            return db.Workouts.Where(_ => _.IsDeleted == 0).Where(_ => _.IsBattleWorkout == 0 && _.IsActive == 1);
        }

        // GET: api/Workouts/GetWorkout/5
        [ResponseType(typeof(Workout))]
        public IHttpActionResult GetWorkout(int id)
        {
            Workout workout = db.Workouts.Find(id);
            if (workout == null)
            {
                return NotFound();
            }

            return Ok(workout);
        }

        // PUT: api/Workouts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutWorkout(int id, Workout workout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != workout.Id)
            {
                return BadRequest();
            }

            db.Entry(workout).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkoutExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Workouts
        [ResponseType(typeof(Workout))]
        public IHttpActionResult PostWorkout(Workout workout)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Workouts.Add(workout);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = workout.Id }, workout);
        }

        // DELETE: api/Workouts/5
        [ResponseType(typeof(Workout))]
        public IHttpActionResult DeleteWorkout(int id)
        {
            Workout workout = db.Workouts.Find(id);
            if (workout == null)
            {
                return NotFound();
            }

            db.Workouts.Remove(workout);
            db.SaveChanges();

            return Ok(workout);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WorkoutExists(int id)
        {
            return db.Workouts.Count(e => e.Id == id) > 0;
        }

        // GET: api/Workouts/GetAllWorkoutsNBattles
        public List<WorkoutViewModel> GetAllWorkoutsNBattles()
        {
            return _workoutService.GetAllWorkoutsNBattles();
        }

        // GET: api/Workouts/GetAllWorkouts
        public List<WorkoutViewModel> GetAllWorkouts()
        {
            return _workoutService.GetAllWorkouts().Where(_ => _.IsDeleted == 0 && _.IsActive == 1).ToList();
        }

        // GET: api/Workouts/GetWorkoutById/5
        public WorkoutViewModel GetWorkoutById(int id)
        {
            return _workoutService.GetAllWorkouts().FirstOrDefault(_ => _.WorkoutId == id);
        }

        // GET: api/Workouts/GetAllBattles
        public List<WorkoutViewModel> GetAllBattles()
        {
            return _workoutService.GetAllBattles().Where(_ => _.IsDeleted == 0 && _.IsActive == 1).ToList();
        }

        // GET: api/Workouts/GetBattleById/5
        public WorkoutViewModel GetBattleById(int id)
        {
            return _workoutService.GetAllBattles().FirstOrDefault(_ => _.WorkoutId == id);
        }

        //for different user type
        // GET: api/Workouts/GetAllWorkoutsByObjLvl?level=1&objective=1&userId=51
        public List<WorkoutDTO> GetAllWorkoutsByObjLvl(int level, int objective, int userId)
        {
            return _workoutService.GetAllWorkoutsByObjLvl(level, objective, userId).ToList();
        }

        //for all users
        // GET: api/Workouts/GetAllWorkoutsByObjLvl?level=1&objective=1
        public List<WorkoutDTO> GetAllWorkoutsByObjLvl(int level, int objective)
        {
            return _workoutService.GetAllWorkoutsByObjLvl(level, objective).ToList();
        }

        // GET: api/Workouts/GetAllBattlesByObjLvl?level=1&objective=1
        public List<WorkoutDTO> GetAllBattlesByObjLvl(int level, int objective)
        {
            return _workoutService.GetAllBattlesByObjLvl(level, objective).ToList();
        }

        // GET: api/Workouts/GetWorkoutsByProgramId/5
        public List<ProgramWorkoutViewModel> GetWorkoutsByProgramId(int id)
        {
            return _workoutService.GetWorkoutsByProgramId(id);
        }
    }
}
﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;
using FightAthleteNew.Services;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class ExercisesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private readonly ExerciseService _exerciseService;

        public ExercisesController()
        {
            _exerciseService = new ExerciseService();
        }

        // GET: api/Exercises/GetAllExercise
        /// <summary>
        /// Gets all exercise.
        /// </summary>
        /// <returns></returns>
        public List<ExerciseViewModel> GetAllExercise()
        {
            return _exerciseService.GetAllExercise().Where(_=>_.IsDeleted == 0 && _.IsActive == 1).ToList();
        }

        // GET: api/Exercises/GetExercises
        public IQueryable<Exercise> GetExercises()
        {
            return db.Exercises.Where(_=>_.IsDeleted== 0 && _.IsActive == 1);
        }

        // GET: api/Exercises/GetExercise/5
        [ResponseType(typeof(Exercise))]
        public IHttpActionResult GetExercise(int id)
        {
            Exercise exercise = db.Exercises.Find(id);
            if (exercise == null)
            {
                return NotFound();
            }

            return Ok(exercise);
        }

        // PUT: api/Exercises/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutExercise(int id, Exercise exercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != exercise.Id)
            {
                return BadRequest();
            }

            db.Entry(exercise).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Exercises
        [ResponseType(typeof(Exercise))]
        public IHttpActionResult PostExercise(Exercise exercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Exercises.Add(exercise);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = exercise.Id }, exercise);
        }

        // DELETE: api/Exercises/5
        [ResponseType(typeof(Exercise))]
        public IHttpActionResult DeleteExercise(int id)
        {
            Exercise exercise = db.Exercises.Find(id);
            if (exercise == null)
            {
                return NotFound();
            }

            db.Exercises.Remove(exercise);
            db.SaveChanges();

            return Ok(exercise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ExerciseExists(int id)
        {
            return db.Exercises.Count(e => e.Id == id) > 0;
        }

        // GET: api/Exercises/GetExerciseByLevel/5
        public List<ExerciseDTO> GetExerciseByLevel(int id)
        {
            return _exerciseService.GetExerciseByLevel(id);
        }

        // GET: api/Exercises/GetExerciseByTarget/5
        public List<ExerciseDTO> GetExerciseByTarget(int id)
        {
            return _exerciseService.GetExerciseByMusclegroup(id);
        }

        // GET: api/Exercises/GetExerciseByFilter/5
        public List<ExerciseDTO> GetExerciseByFilter(int level, int muscle, int tool)
        {
            return _exerciseService.GetExerciseByFilter(level, muscle, tool);
        }

        // GET: api/Exercises/GetExerciseByWorkout/5
        public List<ExerciseDTO> GetExerciseByWorkout(int id)
        {
            return _exerciseService.GetExerciseByWorkout(id);
        }

        // GET: api/Exercises/GetCircuitExerciseByWorkoutId/1013
        public List<CircuitWorkoutViewModel> GetCircuitExerciseByWorkoutId(int id)
        {
            return _exerciseService.GetCircuitExerciseByWorkoutId(id);
        }
    }
}
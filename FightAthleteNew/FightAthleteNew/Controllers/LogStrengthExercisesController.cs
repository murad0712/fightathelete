﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class LogStrengthExercisesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/LogStrengthExercises
        public IQueryable<LogStrengthExercise> GetLogStrengthExercises()
        {
            return db.LogStrengthExercises;
        }

        // GET: api/LogStrengthExercises/GetLogStrengthByStrengthId/5
        [ResponseType(typeof(LogStrengthExercise))]
        public List<LogStrengthExercise> GetLogStrengthByStrengthId(int id)
        {
            List<LogStrengthExercise> logStrengthExercise = db.LogStrengthExercises.Where(_ => _.LogStrengthId == id).ToList();

            return logStrengthExercise;
        }

        LogStrengthSetsController _logStrengthSetsController = new LogStrengthSetsController();

        // GET: api/LogStrengthExercises/GetLogStrengthExerciseByStrengthId/5
        [ResponseType(typeof(LogStrengthExercise))]
        public List<StrengthWorkoutExerciseViewModel> GetLogStrengthExerciseByStrengthId(int id)
        {
            List<StrengthWorkoutExerciseViewModel> exerciseViewModelsList = new List<StrengthWorkoutExerciseViewModel>();

            List<LogStrengthExercise> logStrengthExercise = db.LogStrengthExercises.Where(_ => _.LogStrengthId == id).ToList();

            foreach (var strengthExercise in logStrengthExercise)
            {
                StrengthWorkoutExerciseViewModel exerciseViewModel = new StrengthWorkoutExerciseViewModel()
                {
                    ExerciseName = strengthExercise.ExerciseName,
                    LogStrengthId = strengthExercise.LogStrengthId,
                    Sets = _logStrengthSetsController.GetLogStrengthSetByExerciseId(strengthExercise.Id)
                };
                exerciseViewModelsList.Add(exerciseViewModel);
            }
            

            return exerciseViewModelsList;
        }

        // GET: api/LogStrengthExercises/5
        [ResponseType(typeof(LogStrengthExercise))]
        public IHttpActionResult GetLogStrengthExercise(int id)
        {
            LogStrengthExercise logStrengthExercise = db.LogStrengthExercises.Find(id);
            if (logStrengthExercise == null)
            {
                return NotFound();
            }

            return Ok(logStrengthExercise);
        }

        // PUT: api/LogStrengthExercises/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLogStrengthExercise(int id, LogStrengthExercise logStrengthExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != logStrengthExercise.Id)
            {
                return BadRequest();
            }

            db.Entry(logStrengthExercise).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogStrengthExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LogStrengthExercises/PostLogStrengthExercise
        [ResponseType(typeof(LogStrengthExercise))]
        public IHttpActionResult PostLogStrengthExercise(LogStrengthExercise logStrengthExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LogStrengthExercises.Add(logStrengthExercise);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = logStrengthExercise.Id }, logStrengthExercise);
        }

        // DELETE: api/LogStrengthExercises/5
        [ResponseType(typeof(LogStrengthExercise))]
        public IHttpActionResult DeleteLogStrengthExercise(int id)
        {
            LogStrengthExercise logStrengthExercise = db.LogStrengthExercises.Find(id);
            if (logStrengthExercise == null)
            {
                return NotFound();
            }

            db.LogStrengthExercises.Remove(logStrengthExercise);
            db.SaveChanges();

            return Ok(logStrengthExercise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogStrengthExerciseExists(int id)
        {
            return db.LogStrengthExercises.Count(e => e.Id == id) > 0;
        }
    }
}
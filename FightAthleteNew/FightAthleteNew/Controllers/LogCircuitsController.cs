﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class LogCircuitsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/LogCircuits
        public IQueryable<LogCircuit> GetLogCircuits()
        {
            return db.LogCircuits;
        }

        // GET: api/LogCircuits/GetLogCircuitsByUserId?userId=18&type=Workout
        public IQueryable<LogCircuit> GetLogCircuitsByUserId(int userId, string type)
        {
            var logs = (from circuit in db.LogCircuits
                where circuit.UserId == userId && circuit.Type == type
                orderby circuit.Date descending 
                select circuit);
            return logs;
        }

        // GET: api/LogCircuits/GetLogCircuitsByUserId
        public IQueryable<LogCircuit> GetLogCircuitsByTypeId(int userId, int typeId, string type)
        {
            var logs = (from circuit in db.LogCircuits
                        where circuit.UserId == userId && circuit.Type == type && circuit.TypeId == typeId
                        orderby circuit.Date descending 
                        select circuit);
            return logs;
        }

        // GET: api/LogCircuits/GetLogCircuitsByUserId
        public IQueryable<LogCircuit> GetLogCircuitsByTypeId(int userId, int typeId, int programId, string type)
        {
            var logs = (from circuit in db.LogCircuits
                        where circuit.UserId == userId && circuit.Type == type && circuit.TypeId == typeId && circuit.ProgramId == programId
                        orderby circuit.Date descending
                        select circuit);
            return logs;
        }

        // GET: api/LogCircuits/GetCircuitsForProgram
        public IQueryable<LogCircuit> GetCircuitsForProgram(int userId, int typeId, int programId)
        {
            var logs = (from circuit in db.LogCircuits
                        where circuit.UserId == userId && circuit.TypeId == typeId && circuit.ProgramId == programId
                        orderby circuit.Date descending
                        select circuit);
            return logs;
        }

        // GET: api/LogCircuits/5
        [ResponseType(typeof(LogCircuit))]
        public IHttpActionResult GetLogCircuit(int id)
        {
            LogCircuit logCircuit = db.LogCircuits.Find(id);
            if (logCircuit == null)
            {
                return NotFound();
            }

            return Ok(logCircuit);
        }

        // PUT: api/LogCircuits/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutLogCircuit(int id, LogCircuit logCircuit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != logCircuit.Id)
            {
                return BadRequest();
            }

            db.Entry(logCircuit).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LogCircuitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/LogCircuits/PostLogCircuit
        [ResponseType(typeof(LogCircuit))]
        public IHttpActionResult PostLogCircuit(LogCircuit logCircuit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.LogCircuits.Add(logCircuit);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = logCircuit.Id }, logCircuit);
        }

        // DELETE: api/LogCircuits/5
        [ResponseType(typeof(LogCircuit))]
        public IHttpActionResult DeleteLogCircuit(int id)
        {
            LogCircuit logCircuit = db.LogCircuits.Find(id);
            if (logCircuit == null)
            {
                return NotFound();
            }

            db.LogCircuits.Remove(logCircuit);
            db.SaveChanges();

            return Ok(logCircuit);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool LogCircuitExists(int id)
        {
            return db.LogCircuits.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System.Linq;
using System.Web.Http;
using FightAthleteNew.Email;
using FightAthleteNew.Models;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Controllers
{
    //[Authorize]
    public class EmailController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // POST: api/Email/SendEmail
        public IHttpActionResult SendEmail(EmailTemplate emailTemplate)
        {
            emailTemplate.To = emailTemplate.To ?? db.OfficialEmails.FirstOrDefault()?.Email;

            if (string.IsNullOrEmpty(emailTemplate.To))
            {
                return Ok();
            }

            var emailClient = new EmailClient();
            emailClient.SendEmail(emailTemplate);
            return Ok();
        }
    }
}

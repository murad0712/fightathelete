﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    /*[Authorize]*/
    public class WorkoutExercisesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/WorkoutExercises
        public IQueryable<WorkoutExercise> GetWorkoutExercises()
        {
            return db.WorkoutExercises;
        }

        // GET: api/WorkoutExercises/GetWorkoutExercisesByWorkoutId
        public IQueryable<WorkoutExercise> GetWorkoutExercisesByWorkoutId(int id)
        {
            return db.WorkoutExercises.Where(_=>_.WorkoutId == id);
        }

        // GET: api/WorkoutExercises/5
        [ResponseType(typeof(WorkoutExercise))]
        public IHttpActionResult GetWorkoutExercise(int id)
        {
            WorkoutExercise workoutExercise = db.WorkoutExercises.Find(id);
            if (workoutExercise == null)
            {
                return NotFound();
            }

            return Ok(workoutExercise);
        }

        // PUT: api/WorkoutExercises/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutWorkoutExercise(int id, WorkoutExercise workoutExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != workoutExercise.Id)
            {
                return BadRequest();
            }

            db.Entry(workoutExercise).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkoutExerciseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/WorkoutExercises
        [ResponseType(typeof(WorkoutExercise))]
        public IHttpActionResult PostWorkoutExercise(WorkoutExercise workoutExercise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.WorkoutExercises.Add(workoutExercise);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = workoutExercise.Id }, workoutExercise);
        }

        // DELETE: api/WorkoutExercises/5
        [ResponseType(typeof(WorkoutExercise))]
        public IHttpActionResult DeleteWorkoutExercise(int id)
        {
            WorkoutExercise workoutExercise = db.WorkoutExercises.Find(id);
            if (workoutExercise == null)
            {
                return NotFound();
            }

            db.WorkoutExercises.Remove(workoutExercise);
            db.SaveChanges();

            return Ok(workoutExercise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WorkoutExerciseExists(int id)
        {
            return db.WorkoutExercises.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using System.Linq;
using System.Web.Http;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;
using FightAthleteNew.Models.ViewModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class SettingsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Settings/GetTermsOfUses
        public IQueryable<TermsOfUse> GetTermsOfUses()
        {
            return db.TermsOfUses;
        }

        // GET: api/Settings/GetPrivacies
        public IQueryable<Privacy> GetPrivacies()
        {
            return db.Privacies;
        }

        // GET: api/Settings/GetFaqs
        public IQueryable<FaqViewModel> GetFaqs()
        {
            var faqs = (from faq in db.Faqs
                        join faqCategory in db.FaqCategories on faq.CategoryId equals faqCategory.Id
                        select new FaqViewModel()
                        {
                            Id = faq.Id,
                            Category = faqCategory.Name,
                            Answer = faq.Answer,
                            Question = faq.Question
                        }
                );
            return faqs;
        }

        // GET: api/Settings/GetSubscriptions?userId=5
        public IQueryable<SubscriptionViewModel> GetSubscriptions(int userId)
        {
            var subscriptions = (from user in db.Userses
                join membership in db.MembershipTypes on user.MembershipTypeId equals membership.Id
                where user.Id == userId
                select new SubscriptionViewModel()
                {
                    TypeOfMembership = membership.Name,
                    StartMembership = user.MembershipSince,
                    Duration = membership.Duration
                });
            return subscriptions;
        }
    }
}

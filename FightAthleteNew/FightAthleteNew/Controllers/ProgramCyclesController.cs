﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using FightAthleteNew.Models;
using FightAthleteNew.Models.DbModels;

namespace FightAthleteNew.Controllers
{
    [Authorize]
    public class ProgramCyclesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/ProgramCycles
        public IQueryable<ProgramCycle> GetProgramCycles()
        {
            return db.ProgramCycles;
        }

        // GET: api/ProgramCycles/GetProgramCyclesByUserNProgramId?userId=18&programId=2
        public IQueryable<ProgramCycle> GetProgramCyclesByUserNProgramId(int userId, int programId)
        {
            return db.ProgramCycles.Where(_=>_.UserId == userId && _.ProgramId == programId);
        }

        // GET: api/ProgramCycles/GetProgramCyclesByUserId?userId=18
        public List<ProgramCycle> GetProgramCyclesByUserId(int userId)
        {
            return db.ProgramCycles.Where(_ => _.UserId == userId).ToList();
        }

        // GET: api/ProgramCycles/5
        [ResponseType(typeof(ProgramCycle))]
        public IHttpActionResult GetProgramCycle(int id)
        {
            ProgramCycle programCycle = db.ProgramCycles.Find(id);
            if (programCycle == null)
            {
                return NotFound();
            }

            return Ok(programCycle);
        }

        // PUT: api/ProgramCycles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProgramCycle(int id, ProgramCycle programCycle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != programCycle.Id)
            {
                return BadRequest();
            }

            db.Entry(programCycle).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProgramCycleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProgramCycles/PostProgramCycle
        [ResponseType(typeof(ProgramCycle))]
        public IHttpActionResult PostProgramCycle(ProgramCycle programCycle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ProgramCycles.Add(programCycle);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = programCycle.Id }, programCycle);
        }

        // DELETE: api/ProgramCycles/5
        [ResponseType(typeof(ProgramCycle))]
        public IHttpActionResult DeleteProgramCycle(int id)
        {
            ProgramCycle programCycle = db.ProgramCycles.Find(id);
            if (programCycle == null)
            {
                return NotFound();
            }

            db.ProgramCycles.Remove(programCycle);
            db.SaveChanges();

            return Ok(programCycle);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProgramCycleExists(int id)
        {
            return db.ProgramCycles.Count(e => e.Id == id) > 0;
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(FightAthleteNew.Startup))]

namespace FightAthleteNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

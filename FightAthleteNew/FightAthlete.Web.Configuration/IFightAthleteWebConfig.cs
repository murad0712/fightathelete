﻿namespace FightAthlete.Web.Configuration
{
   public interface IFightAthleteWebConfig
    {
        string FacebookAppId { get; }
        string FacebookAppSecret { get; }
        string FacebookUserInformationEndpoint { get; }
        string FacebookUserProfileScope { get; }
        string GcmApiKey { get; }
        string GcmUrl { get; }
    }
}

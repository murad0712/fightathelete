﻿using System;
using System.Web.Configuration;

namespace FightAthlete.Web.Configuration
{
    public class FightAthleteWebConfig: IFightAthleteWebConfig
    {
        private static readonly Lazy<FightAthleteWebConfig> Config = new Lazy<FightAthleteWebConfig>(() => new FightAthleteWebConfig());
        public static FightAthleteWebConfig Current => Config.Value;
        public string FacebookAppId => WebConfigurationManager.AppSettings[nameof(FacebookAppId)];

        public string FacebookAppSecret => WebConfigurationManager.AppSettings[nameof(FacebookAppSecret)];

        public string FacebookUserInformationEndpoint => WebConfigurationManager.AppSettings[nameof(FacebookUserInformationEndpoint)];

        public string FacebookUserProfileScope => WebConfigurationManager.AppSettings[nameof(FacebookUserProfileScope)];

        public string DefaultEmail => WebConfigurationManager.AppSettings[nameof(DefaultEmail)];
        public string DefaultEmailPassword => WebConfigurationManager.AppSettings[nameof(DefaultEmailPassword)];
        public string SmtpServer => WebConfigurationManager.AppSettings[nameof(SmtpServer)];
        public string GcmApiKey => WebConfigurationManager.AppSettings[nameof(GcmApiKey)];

        public string GcmUrl => WebConfigurationManager.AppSettings[nameof(GcmUrl)];
        public string SmtpPort => WebConfigurationManager.AppSettings[nameof(SmtpPort)];
        public string SmtpServerAdmin => WebConfigurationManager.AppSettings[nameof(SmtpServerAdmin)];
        public string SmtpServerAdminPassword => WebConfigurationManager.AppSettings[nameof(SmtpServerAdminPassword)];
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Repository;

namespace FightAthlete.Services
{
    public class ProgramService
    {
        private readonly ProgramRepository _programRepository;
        private readonly ProgramLevelRepo _programLevelRepo;
        private readonly ProgramToolRepo _programToolRepo;
        private readonly ProgramWorkoutRepository _programWorkoutRepository;
        private readonly ApplicationDbContext _db;

        public ProgramService()
        {
            _programToolRepo = new ProgramToolRepo();
            _programLevelRepo = new ProgramLevelRepo();
            _programWorkoutRepository = new ProgramWorkoutRepository();
            _programRepository = new ProgramRepository();
            _db = new ApplicationDbContext();
        }

        public void AddNewProgram(ProgramViewModel programVm)
        {
            Program program = new Program
            {
                Name = programVm.Name,
                IsBasic = programVm.IsBasic,
                FrequencyId = programVm.FrequencyId,
                DurationId = programVm.DurationId,
                IsDeleted = 0,
                IsNew = programVm.IsNew,
                IsActive = programVm.IsActive,
                Point = programVm.Point,
                CreatedOn = DateTime.UtcNow
            };
            _programRepository.Add(program);
            var programId = program.Id;

            AddLevelTool(programVm, programId);
            AddProgramWorkout(programVm, programId);
        }

        private void AddProgramWorkout(ProgramViewModel programVm, int programId)
        {
            ProgramWorkout programWorkout = new ProgramWorkout();

            if (programVm.ProgramWorkouts != null)
            {
                foreach (var workoutData in programVm.ProgramWorkouts)
                {
                    programWorkout.WorkoutId = workoutData.WorkoutId;
                    programWorkout.ProgramId = programId;
                    programWorkout.Day = workoutData.Day;
                    programWorkout.Type = workoutData.Type;
                    _programWorkoutRepository.Add(programWorkout);
                }
            }
        }

        private void AddLevelTool(ProgramViewModel programVm, int programId)
        {
            ProgramLevel programLevel = new ProgramLevel();

            if (programVm.Levels != null)
            {
                foreach (var levelData in programVm.Levels)
                {
                    programLevel.LevelId = levelData.Id;
                    programLevel.ProgramId = programId;
                    _programLevelRepo.Add(programLevel);
                }
            }

            ProgramTool programTool = new ProgramTool();

            if (programVm.Tools != null)
            {
                foreach (var toollData in programVm.Tools)
                {
                    programTool.ToolId = toollData.Id;
                    programTool.ProgramId = programId;
                    _programToolRepo.Add(programTool);
                }
            }
        }

        public IQueryable<ProgramViewModel> GetAllPrograms()
        {
            return _programRepository.GetAllPrograms();
        }

        public void Delete(int id)
        {
            _programRepository.DeleteProgram(id);
        }

        public void EditProgram(ProgramViewModel programVm)
        {
            Program program = new Program
            {
                Id = programVm.Id,
                Name = programVm.Name,
                IsBasic = programVm.IsBasic,
                FrequencyId = programVm.FrequencyId,
                DurationId = programVm.DurationId,
                IsDeleted = 0,
                IsNew = programVm.IsNew,
                IsActive = programVm.IsActive,
                Point = programVm.Point,
                CreatedOn = DateTime.UtcNow,
            };
            _programRepository.Update(program);

            var programLevel = _db.ProgramLevels.Where(_ => _.ProgramId == program.Id);
            if (programLevel.Any())
            {
                foreach (var level in programLevel)
                {
                    _programLevelRepo.Delete(level.Id);
                }
            }

            var programTool = _db.ProgramTools.Where(_ => _.ProgramId == program.Id);
            if (programTool.Any())
            {
                foreach (var tool in programTool)
                {
                    _programToolRepo.Delete(tool.Id);
                }
            }

            AddLevelTool(programVm, program.Id);

            var programWorkout = _db.ProgramWorkouts.Where(_ => _.ProgramId == program.Id);
            if (programWorkout.Any())
            {
                foreach (var workout in programWorkout)
                {
                    _programWorkoutRepository.Delete(workout.Id);
                }
            }

            AddProgramWorkout(programVm,program.Id);
        }
    }
}
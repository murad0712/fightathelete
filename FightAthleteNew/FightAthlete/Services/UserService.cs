﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;

namespace FightAthlete.Services
{
    public class UserService
    {
        protected readonly ApplicationDbContext _db = new ApplicationDbContext();

        public List<UserViewModel> GetAllUser()
        {
            List<UserViewModel> userList = new List<UserViewModel>();
            var users = _db.Userses.ToList();
            var membership = _db.MembershipTypes.ToList();
            foreach (var user in users)
            {
                UserViewModel newUser = new UserViewModel();
                newUser.Id = user.Id;
                newUser.Name = user.Name;
                newUser.Surname = user.Surname;
                newUser.Gender = user.Gender;
                newUser.Birthdate = user.Birthdate.ToString("d");
                newUser.Country = user.Country;
                newUser.EmailAddress = user.EmailAddress;
                newUser.MembershipSince = user.MembershipSince.ToString("d");
                newUser.Photo = user.Photo;

                foreach (var membershipType in membership)
                {
                    if (user.MembershipTypeId == membershipType.Id)
                    {
                        newUser.MembershipType = membershipType.Name;
                    }
                }
                userList.Add(newUser);
            }
            return userList;
        } 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Repository;

namespace FightAthlete.Services
{
    public class ExerciseService
    {
        private ExerciseRepository _exerciseRepository;
        private ExerciseLevelRepo _exerciseLevelRepo;
        private ExerciseMuscleGroupRepo _exerciseMuscleGroupRepo;
        private ExerciseToolRepo _exerciseToolRepo;
        protected readonly ApplicationDbContext _context;
        public ExerciseService()
        {
            _exerciseRepository = new ExerciseRepository();
            _exerciseLevelRepo = new ExerciseLevelRepo();
            _exerciseMuscleGroupRepo = new ExerciseMuscleGroupRepo();
            _exerciseToolRepo = new ExerciseToolRepo();
            _context = ApplicationDbContext.Create();
        }

        public IQueryable<ExerciseViewModel> GetAllExercise()
        {
            return _exerciseRepository.GetAllExercise();
        }

        public void AddNewExercise(ExerciseViewModel exerciseVm)
        {
            var exercise = new Exercise
            {
                Name = exerciseVm.Name,
                Description = exerciseVm.Description,
                IsRestExercise = exerciseVm.IsRestExercise,
                IsBasicVersion = exerciseVm.IsBasicVersion,
                Duration = exerciseVm.Duration,
                Video = exerciseVm.Video,
                Thumbnail = exerciseVm.Thumbnail,
                IsActive = exerciseVm.IsActive,
                IsNew = exerciseVm.IsNew,
                IsDeleted = 0,
                CreatedDate = DateTime.UtcNow
            };
            var exerciseId = _exerciseRepository.Create(exercise);

            AddLevelMuscleTool(exerciseVm,exerciseId);
        }

        public void AddLevelMuscleTool(ExerciseViewModel exerciseVm,int exerciseId)
        {
            ExerciseLevel exerciseLevel = new ExerciseLevel();
            ExerciseMuscleGroup exerciseMuscleGroup = new ExerciseMuscleGroup();
            ExerciseTool exerciseTool = new ExerciseTool();

            if (exerciseVm.Levels != null)
            {
                foreach (var exerciseLevelData in exerciseVm.Levels)
                {
                    exerciseLevel.ExerciseId = exerciseId;
                    exerciseLevel.LevelId = exerciseLevelData.Id;
                    _exerciseLevelRepo.Add(exerciseLevel);
                }
            }

            if (exerciseVm.MuscleGroups != null)
            {
                foreach (var exerciseMuscleGroupData in exerciseVm.MuscleGroups)
                {
                    exerciseMuscleGroup.ExerciseId = exerciseId;
                    exerciseMuscleGroup.MuscleGroupId = exerciseMuscleGroupData.Id;
                    _exerciseMuscleGroupRepo.Add(exerciseMuscleGroup);
                }
            }

            if (exerciseVm.Tools != null)
            {
                foreach (var exerciseToolData in exerciseVm.Tools)
                {
                    exerciseTool.ExerciseId = exerciseId;
                    exerciseTool.ToolId = exerciseToolData.Id;
                    _exerciseToolRepo.Add(exerciseTool);
                }
            }

        }

        public void Delete(int id)
        {
            _exerciseRepository.DeleteExercise(id);
        }

        public void EditExercise(ExerciseViewModel exerciseVm)
        {
            Exercise exercise = new Exercise
            {
                Id = exerciseVm.Id,
                Name = exerciseVm.Name,
                Description = exerciseVm.Description,
                IsRestExercise = exerciseVm.IsRestExercise,
                IsBasicVersion = exerciseVm.IsBasicVersion,
                Duration = exerciseVm.Duration,
                IsActive = exerciseVm.IsActive,
                IsNew = exerciseVm.IsNew,
                Video = exerciseVm.Video,
                Thumbnail = exerciseVm.Thumbnail,
                IsDeleted = 0,
                CreatedDate = DateTime.UtcNow
            };
            _exerciseRepository.Update(exercise);


            var deleteExerciseLevel =
                _context.ExerciseLevels.Where(levels => levels.ExerciseId == exercise.Id);
            if (deleteExerciseLevel.Any())
            {
                foreach (var level in deleteExerciseLevel)
                {
                    _exerciseLevelRepo.Delete(level.Id);
                }
            }

            var deleteExerciseMuscleGroup =
               from muscles in _context.ExerciseMuscleGroups
               where muscles.ExerciseId == exercise.Id
               select muscles;
            if (deleteExerciseMuscleGroup.Any())
            {
                foreach (var muscle in deleteExerciseMuscleGroup)
                {
                    _exerciseMuscleGroupRepo.Delete(muscle.Id);
                }
            }
            
            var deleteTool =
               from tools in _context.ExerciseTools
               where tools.ExerciseId == exercise.Id
               select tools;
            if (deleteTool.Any())
            {
                foreach (var tool in deleteTool)
                {
                    _exerciseToolRepo.Delete(tool.Id);
                }
            }

            AddLevelMuscleTool(exerciseVm, exercise.Id);

            
        }
    }
}
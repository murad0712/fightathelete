﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Repository;

namespace FightAthlete.Services
{
    public class WorkoutService
    {
        private WorkoutExerciseRepository _workoutExerciseRepository;
        private WorkoutRepository _workoutRepository;
        private WorkoutLevelRepo _workoutLevelRepo;
        private WorkoutObjectiveRepo _workoutObjectiveRepo;
        private WorkoutToolRepo _workoutToolRepo;
        private ApplicationDbContext _db;

        public WorkoutService()
        {
            _workoutExerciseRepository = new WorkoutExerciseRepository();
            _workoutRepository = new WorkoutRepository();
            _workoutLevelRepo = new WorkoutLevelRepo();
            _workoutObjectiveRepo = new WorkoutObjectiveRepo();
            _workoutToolRepo = new WorkoutToolRepo();
            _db = new ApplicationDbContext();
        }


        public void AddNewWorkout(WorkoutViewModel workoutViewModel)
        {
            Workout workout = new Workout
            {
                Name = workoutViewModel.Name,
                WorkoutType = workoutViewModel.Type,
                GoalDescription = workoutViewModel.Goal,
                Points = workoutViewModel.Points,
                IsBattleWorkout = workoutViewModel.IsBattleWorkout,
                IsOnBasicVersion = workoutViewModel.IsBasicVersion,
                IsActive = workoutViewModel.IsActive,
                IsNew = workoutViewModel.IsNew,
                Photo = workoutViewModel.Photo,
                CreatedOn = DateTime.UtcNow,
                IsDeleted = 0
            };
            var workoutId = _workoutRepository.Create(workout);

            AddLevelObjectiveTool(workoutViewModel, workoutId);
            AddWorkoutExercise(workoutViewModel, workoutId);
        }

        private void AddLevelObjectiveTool(WorkoutViewModel workoutViewModel, int workoutId)
        {
            WorkoutLevel workoutLevel = new WorkoutLevel();
            WorkoutObjective workoutObjective = new WorkoutObjective();
            WorkoutTool workoutTool = new WorkoutTool();

            if (workoutViewModel.Levels != null)
            {
                foreach (var levelData in workoutViewModel.Levels)
                {
                    workoutLevel.LevelId = levelData.Id;
                    workoutLevel.WorkoutId = workoutId;
                    _workoutLevelRepo.Add(workoutLevel);
                }
            }

            if (workoutViewModel.Objectives != null)
            {
                foreach (var objectiveData in workoutViewModel.Objectives)
                {
                    workoutObjective.ObjectiveId = objectiveData.Id;
                    workoutObjective.WorkoutId = workoutId;
                    _workoutObjectiveRepo.Add(workoutObjective);
                }
            }

            if (workoutViewModel.Tools != null)
            {
                foreach (var toolData in workoutViewModel.Tools)
                {
                    workoutTool.ToolId = toolData.Id;
                    workoutTool.WorkoutId = workoutId;
                    _workoutToolRepo.Add(workoutTool);
                }
            }

        }

        public void AddWorkoutExercise(WorkoutViewModel workoutVm, int workoutId)
        {
            WorkoutExercise workoutExercise = new WorkoutExercise();

            if (workoutVm.Exercises != null)
            {
                foreach (var exercise in workoutVm.Exercises)
                {
                    workoutExercise.WorkoutId = workoutId;
                    workoutExercise.ExerciseId = exercise.Id;
                    workoutExercise.Reps = exercise.Reps;
                    workoutExercise.RepHigh = exercise.RepHigh;
                    workoutExercise.Sets = exercise.Sets;
                    workoutExercise.Tempo = exercise.Tempo;
                    workoutExercise.Rest = exercise.Rests;
                    workoutExercise.Duration = exercise.Duration;
                    workoutExercise.Distance = exercise.Distance;
                    workoutExercise.Weight = exercise.Weight;

                    _workoutExerciseRepository.Add(workoutExercise);
                }
            }
            
        }

        public IQueryable<WorkoutViewModel> GetAllWorkouts()
        {
            return _workoutRepository.GetAllWorkouts();
        }

        public Workout GetWorkoutById(int id)
        {
            return _workoutRepository.Get(id);
        }

        public void Delete(int id)
        {
            _workoutRepository.DeleteWorkout(id);
        }

        public void EditWorkout(WorkoutViewModel workoutVm)
        {
            Workout workout = new Workout
            {
                Id = workoutVm.WorkoutId,
                Name = workoutVm.Name,
                GoalDescription = workoutVm.Goal,
                WorkoutType = workoutVm.Type,
                Points = workoutVm.Points,
                IsActive = workoutVm.IsActive,
                IsNew = workoutVm.IsNew,
                IsBattleWorkout = workoutVm.IsBattleWorkout,
                IsOnBasicVersion = workoutVm.IsBasicVersion,
                Photo = workoutVm.Photo,
                CreatedOn = DateTime.UtcNow,
                IsDeleted = 0
            };
            _workoutRepository.Update(workout);

            var workoutExercise = _db.WorkoutExercises.Where(_ => _.WorkoutId == workout.Id);
            if (workoutExercise.Any())
            {
                foreach (var exercise in workoutExercise)
                {
                    _workoutExerciseRepository.Delete(exercise.Id);
                }
            }
            
            AddWorkoutExercise(workoutVm, workout.Id);

            var workoutLevel = _db.WorkoutLevels.Where(_ => _.WorkoutId == workout.Id);
            if (workoutLevel.Any())
            {
                foreach (var level in workoutLevel)
                {
                    _workoutLevelRepo.Delete(level.Id);
                }
            }
            
            var workoutObjective = _db.WorkoutObjectives.Where(_ => _.WorkoutId == workout.Id);
            if (workoutObjective.Any())
            {
                foreach (var objective in workoutObjective)
                {
                    _workoutObjectiveRepo.Delete(objective.Id);
                }
            }

            var workoutTool = _db.WorkoutTools.Where(_ => _.WorkoutId == workout.Id);
            if (workoutTool.Any())
            {
                foreach (var tool in workoutTool)
                {
                    _workoutToolRepo.Delete(tool.Id);
                }
            }

            AddLevelObjectiveTool(workoutVm,workout.Id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models.DbModels;
using FightAthlete.Repository;

namespace FightAthlete.Services
{
    public class ObjectsService
    {
        private LevelRepository _levelRepository;

        public ObjectsService()
        {
            this._levelRepository = new LevelRepository();
        }

        public List<Level> GetLevels()
        {
            return _levelRepository.GetAll().ToList();
        }
    }
}
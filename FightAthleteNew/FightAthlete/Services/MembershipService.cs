﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.ViewModels;

namespace FightAthlete.Services
{
    public class MembershipService
    {
        protected readonly ApplicationDbContext _db = new ApplicationDbContext();

        public List<MembershipViewModel> GetAllMembership()
        {
            List<MembershipViewModel> memberList = new List<MembershipViewModel>();
            var members = _db.MembershipTypes.ToList();
            var currency = _db.Currencies.ToList();
            foreach (var member in members)
            {
                MembershipViewModel newMember = new MembershipViewModel();
                newMember.Id = member.Id;
                newMember.Name = member.Name;
                newMember.Duration = member.Duration;
                newMember.Price = member.Price;
                newMember.Discount = member.Discount;

                foreach (var cur in currency)
                {
                    if (member.CurrencyId == cur.Id)
                    {
                        newMember.Currency = cur.Name;
                    }
                }
                memberList.Add(newMember);
            }
            return memberList;
        } 
    }
}
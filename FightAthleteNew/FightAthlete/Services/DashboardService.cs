﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Services
{
    public class DashboardService
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public int GetBasicUser()
        {
            //var membership = db.MembershipTypes;
            var user = db.Userses.Where(_=>_.MembershipTypeId == 1);

            return user.Count();
        }

        public int GetExclusiveUser()
        {
            var user = db.Userses.Where(_ => _.MembershipTypeId == 2);

            return user.Count();
        }

        public int GetTotalActiveUser()
        {
            var active = db.ActiveUsers;
            List<ActiveUser> activeList = new List<ActiveUser>();
            foreach (var activeUser in active)
            {
                if (activeUser.LastLogin > DateTime.UtcNow.AddMonths(-1))
                {
                    activeList.Add(activeUser);
                }
            }

            return activeList.Count();
        }

        public int GetAllUser()
        {
            return db.Userses.Count();
        }

        public int GetAllAbandonment()
        {
            int abandonment = GetAllUser() - GetTotalActiveUser();
            return abandonment;
        }

        public double AbandonmentPercent()
        {
            int abandonment = GetAllAbandonment();
            int all = GetAllUser();

            var value = ((double)abandonment / all) * 100;
            var percentage = Convert.ToInt32(Math.Round(value, 0));

            return percentage;
        }

        public double ChurnPercent()
        {
            int active = GetTotalActiveUser();
            int all = GetAllUser();

            var value = ((double)active / all) * 100;
            var percentage = Convert.ToInt32(Math.Round(value, 0));

            return percentage;
        }
    }
}
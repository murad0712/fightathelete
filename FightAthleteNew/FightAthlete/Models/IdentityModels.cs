﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using FightAthlete.Models.DbModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace FightAthlete.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Level> Levels { get; set; }
        public DbSet<Objective> Objectives { get; set; }
        public DbSet<Tool> Tools { get; set; }
        public DbSet<MuscleGroup> MuscleGroups { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<ExerciseMuscleGroup> ExerciseMuscleGroups { get; set; }
        public DbSet<ExerciseTool> ExerciseTools { get; set; }
        public DbSet<ExerciseLevel> ExerciseLevels { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<MembershipType> MembershipTypes { get; set; }
        public DbSet<User> Userses { get; set; }
        public DbSet<Frequency> Frequencies { get; set; }
        public DbSet<Duration> Durations { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<WorkoutExercise> WorkoutExercises { get; set; }
        public DbSet<WorkoutLevel> WorkoutLevels { get; set; }
        public DbSet<WorkoutObjective> WorkoutObjectives { get; set; }
        public DbSet<WorkoutTool> WorkoutTools { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkouts { get; set; }
        public DbSet<ProgramDuration> ProgramDurations { get; set; }
        public DbSet<ProgramFrequency> ProgramFrequencies { get; set; }
        public DbSet<ProgramLevel> ProgramLevels { get; set; }
        public DbSet<ProgramTool> ProgramTools { get; set; }
        public DbSet<Affiliate> Affiliates { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<TermsOfUse> TermsOfUses { get; set; }
        public DbSet<Privacy> Privacies { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<FaqCategory> FaqCategories { get; set; }
        public DbSet<ActiveUser> ActiveUsers { get; set; }
        public DbSet<OfficialEmail> OfficialEmails { get; set; }
    }
}
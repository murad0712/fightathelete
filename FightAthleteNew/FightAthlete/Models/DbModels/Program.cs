﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Program")]
    public class Program
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public int IsBasic { get; set; }
        public int IsDeleted { get; set; }
        public int FrequencyId { get; set; }
        public int DurationId { get; set; }
        public int IsActive { get; set; }
        public int IsNew { get; set; }
        public int Point { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("ProgramWorkout")]
    public class ProgramWorkout
    {
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int WorkoutId { get; set; }
        public int Day { get; set; }
        public string Type { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Workout_Level")]
    public class WorkoutLevel
    {
        public int Id { get; set; }
        public int WorkoutId { get; set; }
        public int LevelId { get; set; }
    }
}
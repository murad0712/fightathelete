﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("WorkoutExercise")]
    public class WorkoutExercise
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int WorkoutId { get; set; }
        public string Reps { get; set; }
        public int? RepHigh { get; set; }
        public string Sets { get; set; }
        public string Tempo { get; set; }
        public string Rest { get; set; }
        public string Duration { get; set; }
        public string Distance { get; set; }
        public string Weight { get; set; }
    }
}
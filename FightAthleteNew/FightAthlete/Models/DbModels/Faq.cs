﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("FAQ")]
    public class Faq
    {
        public int Id { get; set; }
        [Display(Name = "Category)")]
        public int CategoryId { get; set; }
        [Required]
        public string Question { get; set; }
        [Required]
        public string Answer { get; set; }


        [ForeignKey("CategoryId")]
        public virtual FaqCategory FaqCategory { get; set; }
    }
}
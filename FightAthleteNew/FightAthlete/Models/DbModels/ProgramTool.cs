﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Program_Tool")]
    public class ProgramTool
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int ToolId { get; set; }
    }
}
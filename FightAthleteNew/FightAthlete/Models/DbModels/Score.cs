﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Score")]
    public class Score
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "From (Points)")]
        public int Start { get; set; }
        [Required]
        [Display(Name = "To (Points)")]
        public int Finish { get; set; }
    }
}
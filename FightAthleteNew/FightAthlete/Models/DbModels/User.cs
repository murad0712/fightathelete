﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Permissions;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("User")]
    public class User
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }
        public string Country { get; set; }
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "The email address is required")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Email is not valid")]
        public string EmailAddress { get; set; }
        [Display(Name = "Membership Since")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime MembershipSince { get; set; }
        [Display(Name = "Membership")]
        public int MembershipTypeId { get; set; }
        public string Photo { get; set; }


        [ForeignKey("MembershipTypeId")]
        public virtual MembershipType MembershipType { get; set; }
    }
}
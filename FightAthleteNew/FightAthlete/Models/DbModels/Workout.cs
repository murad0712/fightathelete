﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Workout")]
    public class Workout
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string WorkoutType { get; set; }
        public string GoalDescription { get; set; }
        [Required]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Points must be numeric")]
        public int Points { get; set; }
        public int IsBattleWorkout { get; set; }
        public int IsOnBasicVersion { get; set; }
        public int IsActive { get; set; }
        public int IsNew { get; set; }
        public string Photo { get; set; }
        public int IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
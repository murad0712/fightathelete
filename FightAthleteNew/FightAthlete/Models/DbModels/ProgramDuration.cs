﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Program_Duration")]
    public class ProgramDuration
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int DurationId { get; set; }
    }
}
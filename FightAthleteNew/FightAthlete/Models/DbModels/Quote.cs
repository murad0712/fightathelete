﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Quote")]
    public class Quote
    {
        public int Id { get; set; }
        [Required]
        public string Tekst { get; set; }
        [Required]
        public string Author { get; set; }
    }
}
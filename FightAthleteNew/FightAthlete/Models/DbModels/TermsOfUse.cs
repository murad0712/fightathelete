﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("TermsOfUse")]
    public class TermsOfUse
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
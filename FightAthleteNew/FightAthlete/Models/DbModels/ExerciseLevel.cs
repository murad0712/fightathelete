﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Exercise_Level")]
    public class ExerciseLevel
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int LevelId { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Exercise_Tool")]
    public class ExerciseTool
    {
        public int Id { get; set; }
        public int ExerciseId { get; set; }
        public int ToolId { get; set; }
    }
}
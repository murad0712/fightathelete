﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Exercise")]
    public class Exercise
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        public int IsRestExercise { get; set; }
        public int IsBasicVersion { get; set; }

        public int IsNew { get; set; }
        public int IsActive { get; set; }
        public string Duration { get; set; }
        /*public string Photo { get; set; }*/
        public string Video { get; set; }
        public string Thumbnail { get; set; }
        public int IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
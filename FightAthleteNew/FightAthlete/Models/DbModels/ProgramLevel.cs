﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Program_Level")]
    public class ProgramLevel
    {

        public int Id { get; set; }
        public int ProgramId { get; set; }
        public int LevelId { get; set; }
    }
}
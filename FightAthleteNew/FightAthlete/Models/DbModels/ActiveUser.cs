﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("ActiveUser")]
    public class ActiveUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime LastLogin { get; set; }
    }
}
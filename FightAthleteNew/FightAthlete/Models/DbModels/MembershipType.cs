﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("MembershipType")]
    public class MembershipType
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Duration (In Month)")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Duration must be positive")]
        /*[Range(Int32.MinValue, 0)]*/
        public int Duration { get; set; }
        [Required]
        public double? Price { get; set; }
        public int CurrencyId { get; set; }
        public int Discount { get; set; }


        [ForeignKey("CurrencyId")]
        [Display(Name = "Currency)")]
        public virtual Currency Currencies { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
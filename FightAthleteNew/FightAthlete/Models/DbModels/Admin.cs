﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    public class Admin
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class AdminDatabase : List<Admin>
    {
        public AdminDatabase()
        {
            Add(new Admin() { Id = 101, Name = "admin"});
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.DbModels
{
    [Table("Notification")]
    public class Notification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tekst { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Models.ViewModels
{
    public class WorkoutViewModel
    {
        public int WorkoutId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Goal { get; set; }
        public int Points { get; set; }
        public int IsBattleWorkout { get; set; }
        public int IsRestDay { get; set; }
        public int IsBasicVersion { get; set; }
        public int IsActive { get; set; }
        public int IsNew { get; set; }
        public string Photo { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<Level> Levels { get; set; }
        public List<Objective> Objectives { get; set; }
        public List<Tool> Tools { get; set; }
        public List<WorkoutExerciseViewModel> Exercises { get; set; }
    }
}
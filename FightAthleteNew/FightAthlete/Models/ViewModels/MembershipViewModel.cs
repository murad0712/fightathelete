﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.ViewModels
{
    public class MembershipViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Duration { get; set; }
        public double? Price { get; set; }
        public string Currency { get; set; }
        public int Discount { get; set; }
    }
}
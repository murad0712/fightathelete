﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Models.ViewModels
{
    public class ExerciseViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IsRestExercise { get; set; }
        public int IsBasicVersion { get; set; }
        public int IsNew { get; set; }
        public int IsActive { get; set; }
        public string Duration { get; set; }
        public List<MuscleGroup> MuscleGroups { get; set; }
        public List<Tool> Tools { get; set; }
        public List<Level> Levels { get; set; }
        public string Video { get; set; }
        public string Thumbnail { get; set; }
        public DateTime CreatedDate { get; set; }
        public HttpPostedFileBase VideoFile { get; set; }
        public HttpPostedFileBase ThumbnailFile { get; set; }
    }
}
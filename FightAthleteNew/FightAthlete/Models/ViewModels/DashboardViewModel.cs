﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FightAthlete.Models.ViewModels
{
    public class DashboardViewModel
    {
        public int BasicUser { get; set; }
        public int ExclusiveUser { get; set; }
        public int TotalUser { get; set; }
        public int TotalAbandonment { get; set; }
        public int TotalRetention { get; set; }
        public int TotalChurn { get; set; }
        public double AbandonmentPercent { get; set; }
        public double RetentionPercent { get; set; }
        public double ChurnPercent { get; set; }
    }
}
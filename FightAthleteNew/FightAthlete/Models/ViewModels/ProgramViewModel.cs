﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Models.ViewModels
{
    public class ProgramViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int IsBasic { get; set; }
        public int FrequencyId { get; set; }
        public int DurationId { get; set; }
        public int IsActive { get; set; }
        public int IsNew { get; set; }
        public int Point { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<Level> Levels { get; set; }
        public List<Tool> Tools { get; set; }
        public List<ProgramWorkout> ProgramWorkouts { get; set; }
    }
}
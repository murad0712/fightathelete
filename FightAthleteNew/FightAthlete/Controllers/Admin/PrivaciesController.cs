﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Controllers.Admin
{
    public class PrivaciesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Privacies
        public ActionResult Index()
        {
            return View(db.Privacies.ToList());
        }

        // GET: Privacies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privacy privacy = db.Privacies.Find(id);
            if (privacy == null)
            {
                return HttpNotFound();
            }
            return View(privacy);
        }

        // GET: Privacies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Privacies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description")] Privacy privacy)
        {
            if (ModelState.IsValid)
            {
                db.Privacies.Add(privacy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(privacy);
        }

        // GET: Privacies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privacy privacy = db.Privacies.Find(id);
            if (privacy == null)
            {
                return HttpNotFound();
            }
            return View(privacy);
        }

        // POST: Privacies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description")] Privacy privacy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(privacy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(privacy);
        }

        // GET: Privacies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Privacy privacy = db.Privacies.Find(id);
            if (privacy == null)
            {
                return HttpNotFound();
            }
            return View(privacy);
        }

        // POST: Privacies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Privacy privacy = db.Privacies.Find(id);
            db.Privacies.Remove(privacy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

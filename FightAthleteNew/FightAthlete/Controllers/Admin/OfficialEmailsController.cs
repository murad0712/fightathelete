﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Controllers.Admin
{
    public class OfficialEmailsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: OfficialEmails
        public ActionResult Index()
        {
            return View(db.OfficialEmails.ToList());
        }

        // GET: OfficialEmails/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfficialEmail officialEmail = db.OfficialEmails.Find(id);
            if (officialEmail == null)
            {
                return HttpNotFound();
            }
            return View(officialEmail);
        }

        // GET: OfficialEmails/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: OfficialEmails/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email")] OfficialEmail officialEmail)
        {
            if (ModelState.IsValid)
            {
                db.OfficialEmails.Add(officialEmail);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(officialEmail);
        }

        // GET: OfficialEmails/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfficialEmail officialEmail = db.OfficialEmails.Find(id);
            if (officialEmail == null)
            {
                return HttpNotFound();
            }
            return View(officialEmail);
        }

        // POST: OfficialEmails/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email")] OfficialEmail officialEmail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(officialEmail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(officialEmail);
        }

        // GET: OfficialEmails/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OfficialEmail officialEmail = db.OfficialEmails.Find(id);
            if (officialEmail == null)
            {
                return HttpNotFound();
            }
            return View(officialEmail);
        }

        // POST: OfficialEmails/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OfficialEmail officialEmail = db.OfficialEmails.Find(id);
            db.OfficialEmails.Remove(officialEmail);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

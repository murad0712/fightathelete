﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.ViewModels;
using FightAthlete.Services;

namespace FightAthlete.Controllers.Admin
{
    public class AdminController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        DashboardService _dashboardService = new DashboardService();

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            /*int a = _dashboardService.GetBasicUser();
            int b = _dashboardService.GetExclusiveUser();
            int c = _dashboardService.GetAllUser();
            int d = _dashboardService.GetTotalActiveUser();
            int e = _dashboardService.GetAllAbandonment();

            var user = db.Userses;*/
            var stat = new DashboardViewModel()
            {
                BasicUser = _dashboardService.GetBasicUser(),
                ExclusiveUser = _dashboardService.GetExclusiveUser(),
                TotalUser = _dashboardService.GetAllUser(),
                TotalAbandonment = _dashboardService.GetAllAbandonment(),
                TotalRetention = _dashboardService.GetTotalActiveUser(),
                ChurnPercent = _dashboardService.ChurnPercent(),
                AbandonmentPercent = _dashboardService.AbandonmentPercent()
            };
            return View(stat);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Services;

namespace FightAthlete.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class ProgramsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private ProgramService _programService = new ProgramService();

        // GET: Programs
        public ActionResult Index()
        {
            return View(db.Programs.ToList());
        }

        public ActionResult GetAllPrograms()
        {
            List<ProgramViewModel> programViewModels = _programService.GetAllPrograms().ToList();
            return Json(programViewModels, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllActivePrograms()
        {
            List<ProgramViewModel> programViewModels = _programService.GetAllPrograms().Where(_=>_.IsActive == 1).ToList();
            return Json(programViewModels, JsonRequestBehavior.AllowGet);
        }

        // GET: Programs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return HttpNotFound();
            }
            return View(program);
        }

        // GET: Programs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Programs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Create(ProgramViewModel programViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _programService.AddNewProgram(programViewModel);
                    return RedirectToAction("Index");
                }

                return Json("Success", "1");
            }
            catch
            {
                return Json("Error", "1");
            }
            
        }

        // GET: Programs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Program program = db.Programs.Find(id);
            if (program == null)
            {
                return HttpNotFound();
            }
            return View(program);
        }

        // POST: Programs/Edit/5
        [HttpPost]
        public ActionResult Edit(ProgramViewModel programVm)
        {
            try
            {
                _programService.EditProgram(programVm);
                return Json("Success", "1");
            }
            catch (Exception)
            {
                return Json("Error", "1");
            }
        }

        // GET: Programs/DeleteProgram/5
        public string DeleteProgram(int id)
        {
            try
            {
                _programService.Delete(id);
                return "Success";
            }
            catch (Exception)
            {

                return "Error";
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Controllers.Admin
{
    public class TermsOfUsesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TermsOfUses
        public ActionResult Index()
        {
            return View(db.TermsOfUses.ToList());
        }

        // GET: TermsOfUses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TermsOfUse termsOfUse = db.TermsOfUses.Find(id);
            if (termsOfUse == null)
            {
                return HttpNotFound();
            }
            return View(termsOfUse);
        }

        // GET: TermsOfUses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TermsOfUses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description")] TermsOfUse termsOfUse)
        {
            if (ModelState.IsValid)
            {
                db.TermsOfUses.Add(termsOfUse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(termsOfUse);
        }

        // GET: TermsOfUses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TermsOfUse termsOfUse = db.TermsOfUses.Find(id);
            if (termsOfUse == null)
            {
                return HttpNotFound();
            }
            return View(termsOfUse);
        }

        // POST: TermsOfUses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description")] TermsOfUse termsOfUse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(termsOfUse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(termsOfUse);
        }

        // GET: TermsOfUses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TermsOfUse termsOfUse = db.TermsOfUses.Find(id);
            if (termsOfUse == null)
            {
                return HttpNotFound();
            }
            return View(termsOfUse);
        }

        // POST: TermsOfUses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TermsOfUse termsOfUse = db.TermsOfUses.Find(id);
            db.TermsOfUses.Remove(termsOfUse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

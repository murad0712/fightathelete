﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Services;

namespace FightAthlete.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class WorkoutsController : Controller
    {
        private readonly WorkoutService _workoutService;

        public WorkoutsController()
        {
            _workoutService = new WorkoutService();
        }
        // GET: Workout
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllWorkouts()
        {
            List<WorkoutViewModel> workoutViewModels = _workoutService.GetAllWorkouts().ToList();
            return Json(workoutViewModels, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllActiveWorkouts()
        {
            List<WorkoutViewModel> workoutViewModels = _workoutService.GetAllWorkouts().Where(_=>_.IsActive == 1).ToList();
            return Json(workoutViewModels, JsonRequestBehavior.AllowGet);
        }

        public Workout GetWorkout(int workoutId)
        {
            return _workoutService.GetWorkoutById(workoutId);
        }
        
        // GET: Workout/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Workout/Create
        [HttpPost]
        public ActionResult CreateWorkout(WorkoutViewModel workoutViewModel)
        {
            try
            {
                _workoutService.AddNewWorkout(workoutViewModel);

                return RedirectToAction("Index");
            }
            catch
            {
                return Json("Error", "1");
            }
        }

        // POST: Workouts/Edit/5
        [HttpPost]
        public ActionResult Edit(WorkoutViewModel workoutVm)
        {
            try
            {
                _workoutService.EditWorkout(workoutVm);
                return Json("Success", "1");
            }
            catch (Exception)
            {
                return Json("Error", "1");
            }
        }

        // GET: Workouts/DeleteWorkout/5
        public string DeleteWorkout(int id)
        {
            try
            {
                _workoutService.Delete(id);
                return "Success";
            }
            catch (Exception)
            {

                return "Error";
            }
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class ToolsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Tools
        public ActionResult Index()
        {
            return View(db.Tools.ToList());
        }

        // GET: Tools/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tool tool = db.Tools.Find(id);
            if (tool == null)
            {
                return HttpNotFound();
            }
            return View(tool);
        }

        // GET: Tools/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tools/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,ToolPic")] Tool tool, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                var path = SaveFileToDirectory(Request.Files["ToolPic"]);
                tool.Photo = path;
                db.Tools.Add(tool);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tool);
        }

        private string SaveFileToDirectory(HttpPostedFileBase file)
        {
            string path = "Unavailable";
            if (file != null && file.ContentLength > 0)
            {
                if (file.ContentLength > 1 * 1024 * 1024)
                {
                    return "File size exceed 1024 kb";
                }
                if (!file.ContentType.Contains("image"))
                {
                    return "Not an image type file";
                }
                path = "/Upload/" + file.FileName;
                var filePath = Server.MapPath(path);
                file.SaveAs(filePath);
            }
            return path;
        }

        // GET: Tools/Edit/5
        public ActionResult Edit(int? id, HttpPostedFileBase file)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tool tool = db.Tools.Find(id);
            if (tool == null)
            {
                return HttpNotFound();
            }
            return View(tool);
        }

        // POST: Tools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,ToolPic")] Tool tool)
        {
            if (ModelState.IsValid)
            {
                var path = SaveFileToDirectory(Request.Files["ToolPic"]);
                tool.Photo = path;
                db.Entry(tool).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tool);
        }

        // GET: Tools/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tool tool = db.Tools.Find(id);
            if (tool == null)
            {
                return HttpNotFound();
            }
            return View(tool);
        }

        // POST: Tools/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tool tool = db.Tools.Find(id);
            db.Tools.Remove(tool);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult GetAllTools()
        {
            var tools = db.Tools.ToList();
            return Json(tools, JsonRequestBehavior.AllowGet);
        }
    }
}

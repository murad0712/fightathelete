﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Services;

namespace FightAthlete.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private readonly UserService _userService = new UserService();

        // GET: Users
        public ActionResult Index()
        {
            var userses = db.Userses.Include(u => u.MembershipType);
            return View(userses.ToList());
        }

        public ActionResult GetAllUsers()
        {
            List<UserViewModel> users = _userService.GetAllUser();
            return Json(users, JsonRequestBehavior.AllowGet);
        } 

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Userses.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.MembershipTypeId = new SelectList(db.MembershipTypes, "Id", "Name");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,Gender,Birthdate,Country,EmailAddress,MembershipSince,MembershipTypeId,UserPic")] User user, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                user.MembershipSince = DateTime.Now.Date;
                var path = SaveFileToDirectory(Request.Files["UserPic"]);
                user.Photo = path;
                db.Userses.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MembershipTypeId = new SelectList(db.MembershipTypes, "Id", "Name", user.MembershipTypeId);
            return View(user);
        }

        private string SaveFileToDirectory(HttpPostedFileBase file)
        {
            string path = "Unavailable";
            if (file != null && file.ContentLength > 0)
            {
                if (file.ContentLength > 1 * 1024 * 512)
                {
                    return "File size exceed 512kb";
                }
                if (!file.ContentType.Contains("image"))
                {
                    return "Not an image type file";
                }
                path = "/Upload/" + file.FileName;
                var filePath = Server.MapPath(path);
                file.SaveAs(filePath);
            }
            return path;
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Userses.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.MembershipTypeId = new SelectList(db.MembershipTypes, "Id", "Name", user.MembershipTypeId);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,Gender,Birthdate,Country,EmailAddress,MembershipSince,MembershipTypeId,UserPic")] User user)
        {
            if (ModelState.IsValid)
            {
                var path = SaveFileToDirectory(Request.Files["UserPic"]);
                user.Photo = path;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MembershipTypeId = new SelectList(db.MembershipTypes, "Id", "Name", user.MembershipTypeId);
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Userses.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Userses.Find(id);
            db.Userses.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;
using FightAthlete.Services;

namespace FightAthlete.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class ExerciseController : Controller
    {
        private readonly ExerciseService _exerciseService;

        public ExerciseController()
        {
            _exerciseService = new ExerciseService();
        }
        // GET: Exercise
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllExercise()
        {
            List<ExerciseViewModel> exerciseViewModels = _exerciseService.GetAllExercise().ToList();
            return Json(exerciseViewModels, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllActiveExercise()
        {
            List<ExerciseViewModel> exerciseViewModels = _exerciseService.GetAllExercise().Where(_ => _.IsActive == 1).ToList();
            return Json(exerciseViewModels, JsonRequestBehavior.AllowGet);
        }

        // GET: Exercise/Details/5
        /*public ActionResult Details(int id)
        {
            return View();
        }*/

        // GET: Exercise/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Exercise/Create
        [HttpPost]
        public ActionResult Create(ExerciseViewModel exerciseVm)
        {
            try
            {
                _exerciseService.AddNewExercise(exerciseVm);

                return Json("Success", "1");
            }
            catch
            {
                return Json("error", "1");
            }
        }

        // POST: Exercise/SaveFileToDirectory
        [HttpPost]
        public string SaveFileToDirectory(HttpPostedFileBase file)
        {
            var path = "Unavailable";
            var exerciseName = Request["exerciseName"];
            if (file != null && file.ContentLength > 0)
            {
                if (file.ContentLength > 15 * 1024 * 1024)
                {
                    return "File size exceed 15360 kb";
                }
                path = $"/Upload/{exerciseName}_{file.FileName}";
                var filePath = Server.MapPath(path);
                file.SaveAs(filePath);
            }
            return path;
        }

        private string SaveFileToDirectory(string exerciseName, HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
            {
                if (file.ContentLength > 15 * 1024 * 1024)
                {
                    return "File size exceed 15360 kb";
                }
                var path = $"/Upload/{exerciseName}_{file.FileName}";
                var filePath = Server.MapPath(path);
                file.SaveAs(filePath);
                return path;
            }
            return null;
        }

        // POST: Exercise/Edit/5
        [HttpPost]
        public ActionResult Edit(ExerciseViewModel exerciseVm)
        {
            try
            {
                if (exerciseVm.ThumbnailFile != null)
                {
                    exerciseVm.Thumbnail = SaveFileToDirectory(exerciseVm.Name, exerciseVm.ThumbnailFile);
                }

                if (exerciseVm.VideoFile != null)
                {
                    exerciseVm.Video = SaveFileToDirectory(exerciseVm.Name, exerciseVm.VideoFile);
                }

                _exerciseService.EditExercise(exerciseVm);
                return Json("Success", "1");
            }
            catch (Exception)
            {
                return Json("Error", "1");
            }
        }

        // GET: Exercise/Delete/5
        public string DeleteExercise(int id)
        {
            try
            {
                _exerciseService.Delete(id);
                return "Success";
            }
            catch (Exception)
            {

                return "Error";
            }
        }

    }
}

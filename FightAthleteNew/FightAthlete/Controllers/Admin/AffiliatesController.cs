﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;

namespace FightAthlete.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    public class AffiliatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Affiliates
        public ActionResult Index()
        {
            return View(db.Affiliates.ToList());
        }

        // GET: Affiliates/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Affiliate affiliate = db.Affiliates.Find(id);
            if (affiliate == null)
            {
                return HttpNotFound();
            }
            return View(affiliate);
        }

        // GET: Affiliates/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Affiliates/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,AffiliatePic")] Affiliate affiliate)
        {
            if (ModelState.IsValid)
            {
                var path = SaveFileToDirectory(Request.Files["AffiliatePic"]);
                affiliate.Photo = path;
                db.Affiliates.Add(affiliate);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(affiliate);
        }

        private string SaveFileToDirectory(HttpPostedFileBase file)
        {
            string path = "Unavailable";
            if (file != null && file.ContentLength > 0)
            {
                if (file.ContentLength > 1 * 1024 * 512)
                {
                    return "File size exceed 512kb";
                }
                if (!file.ContentType.Contains("image"))
                {
                    return "Not an image type file";
                }
                path = "/Upload/" + file.FileName;
                var filePath = Server.MapPath(path);
                file.SaveAs(filePath);
            }
            return path;
        }

        // GET: Affiliates/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Affiliate affiliate = db.Affiliates.Find(id);
            if (affiliate == null)
            {
                return HttpNotFound();
            }
            return View(affiliate);
        }

        // POST: Affiliates/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,AffiliatePic")] Affiliate affiliate)
        {
            if (ModelState.IsValid)
            {
                var path = SaveFileToDirectory(Request.Files["AffiliatePic"]);
                affiliate.Photo = path;
                db.Entry(affiliate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(affiliate);
        }

        // GET: Affiliates/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Affiliate affiliate = db.Affiliates.Find(id);
            if (affiliate == null)
            {
                return HttpNotFound();
            }
            return View(affiliate);
        }

        // POST: Affiliates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Affiliate affiliate = db.Affiliates.Find(id);
            db.Affiliates.Remove(affiliate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;

namespace FightAthlete.Repository
{
    public class ProgramRepository : Repository<Program>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        public IQueryable<ProgramViewModel> GetAllPrograms()
        {
            var programVm = (from program in _db.Programs
                where program.IsDeleted == 0
                select new ProgramViewModel
                {
                    Id = program.Id,
                    Name = program.Name,
                    IsBasic = program.IsBasic,
                    IsActive = program.IsActive,
                    IsNew = program.IsNew,
                    FrequencyId = program.FrequencyId,
                    DurationId = program.DurationId,
                    Point = program.Point,
                    CreatedOn = program.CreatedOn,
                    Levels = (from programLevel in _db.ProgramLevels
                        join level in _db.Levels on programLevel.LevelId equals level.Id
                        where programLevel.ProgramId == program.Id
                        select level
                        ).ToList(),
                    Tools = (from programTools in _db.ProgramTools
                        join tool in _db.Tools on programTools.ToolId equals tool.Id
                        where programTools.ProgramId == program.Id
                        select tool
                        ).ToList(),
                    ProgramWorkouts = (from programWorkout in _db.ProgramWorkouts
                        join workout in _db.Workouts on programWorkout.WorkoutId equals workout.Id
                        where programWorkout.ProgramId == program.Id
                        select programWorkout
                        ).ToList()
                });
            return programVm;
        }

        public void DeleteProgram(int id)
        {
            _db.Programs.Find(id).IsDeleted = 1;
            _db.SaveChanges();
        }
    }
}
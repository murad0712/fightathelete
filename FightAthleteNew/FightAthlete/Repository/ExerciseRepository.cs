﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc.Html;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;

namespace FightAthlete.Repository
{
    public class ExerciseRepository:Repository<Exercise>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        public int Create(Exercise exercise)
        {
            _db.Exercises.Add(exercise);
            _db.SaveChanges();
            return exercise.Id;
        }

        public IQueryable<ExerciseViewModel> GetAllExercise()
        {
            var exercisesVm = (from exercise in _db.Exercises
                               where exercise.IsDeleted == 0
                             select new ExerciseViewModel
                             {
                                 Id = exercise.Id,
                                 Name = exercise.Name,
                                 Description = exercise.Description,
                                 IsRestExercise = exercise.IsRestExercise,
                                 IsBasicVersion = exercise.IsBasicVersion,
                                 IsNew = exercise.IsNew,
                                 IsActive = exercise.IsActive,
                                 Duration = exercise.Duration,
                                 CreatedDate = exercise.CreatedDate,
                                 Levels = (from exerciseLevel in _db.ExerciseLevels
                                     join level in _db.Levels on exerciseLevel.LevelId equals level.Id
                                     where exerciseLevel.ExerciseId == exercise.Id
                                     select level
                                  ).ToList(),
                                 MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                           join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                           where exerciseMuscleGroup.ExerciseId == exercise.Id
                                           select muscleGroup
                                  ).ToList(),
                                 Tools = (from exerciseTool in _db.ExerciseTools
                                     join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                                     where exerciseTool.ExerciseId == exercise.Id
                                     select tool
                                  ).ToList(),
                                 Video = exercise.Video,
                                 Thumbnail = exercise.Thumbnail
                             }
                );
            return exercisesVm;
        }

        public void DeleteExercise(int id)
        {
            _db.Exercises.Find(id).IsDeleted = 1;
            _db.SaveChanges();
        }
    }
}
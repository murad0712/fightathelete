﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FightAthlete.Models;
using FightAthlete.Models.DbModels;
using FightAthlete.Models.ViewModels;

namespace FightAthlete.Repository
{
    public class WorkoutRepository : Repository<Workout>
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        public int Create(Workout workout)
        {
            _db.Workouts.Add(workout);
            _db.SaveChanges();
            return workout.Id;
        }

        public IQueryable<WorkoutViewModel> GetAllWorkouts()
        {
            var workoutVm = (from workout in _db.Workouts
                where workout.IsDeleted == 0
                select new WorkoutViewModel
                {
                    WorkoutId = workout.Id,
                    Name = workout.Name,
                    Type = workout.WorkoutType,
                    Goal = workout.GoalDescription,
                    Points = workout.Points,
                    IsActive = workout.IsActive,
                    IsNew = workout.IsNew,
                    IsBattleWorkout = workout.IsBattleWorkout,
                    IsBasicVersion = workout.IsOnBasicVersion,
                    Photo = workout.Photo,
                    CreatedOn = workout.CreatedOn,
                    Levels = (from workoutLevel in _db.WorkoutLevels
                        join level in _db.Levels on workoutLevel.LevelId equals level.Id
                        where workoutLevel.WorkoutId == workout.Id
                        select level
                     ).ToList(),
                    Objectives = (from workoutObjective in _db.WorkoutObjectives
                        join objective in _db.Objectives on workoutObjective.ObjectiveId equals objective.Id
                        where workoutObjective.WorkoutId == workout.Id
                        select objective
                     ).ToList(),
                    Tools = (from workoutTool in _db.WorkoutTools
                                  join tool in _db.Tools on workoutTool.ToolId equals tool.Id
                                  where workoutTool.WorkoutId == workout.Id
                                  select tool
                     ).ToList(),
                    Exercises = (from workoutExercise in _db.WorkoutExercises
                        join exercise in _db.Exercises on workoutExercise.ExerciseId equals exercise.Id
                        where workoutExercise.WorkoutId == workout.Id
                        select new WorkoutExerciseViewModel
                        {
                            Id = exercise.Id,
                            Name = exercise.Name,
                            Description = exercise.Description,
                            IsRestExercise = exercise.IsRestExercise,
                            Video = exercise.Video,
                            IsActive = exercise.IsActive,
                            IsNew = exercise.IsNew,
                            Reps = workoutExercise.Reps,
                            Sets = workoutExercise.Sets,
                            Tempo = workoutExercise.Tempo,
                            Rests = workoutExercise.Rest,
                            RepHigh = workoutExercise.RepHigh,
                            Duration = workoutExercise.Duration,
                            Distance = workoutExercise.Distance,
                            Weight = workoutExercise.Weight,
                            Levels = (from exerciseLevel in _db.ExerciseLevels
                                      join level in _db.Levels on exerciseLevel.LevelId equals level.Id
                                      where exerciseLevel.ExerciseId == exercise.Id
                                      select level
                                  ).ToList(),
                            MuscleGroups = (from exerciseMuscleGroup in _db.ExerciseMuscleGroups
                                            join muscleGroup in _db.MuscleGroups on exerciseMuscleGroup.MuscleGroupId equals muscleGroup.Id
                                            where exerciseMuscleGroup.ExerciseId == exercise.Id
                                            select muscleGroup
                                  ).ToList(),
                            Tools = (from exerciseTool in _db.ExerciseTools
                                     join tool in _db.Tools on exerciseTool.ToolId equals tool.Id
                                     where exerciseTool.ExerciseId == exercise.Id
                                     select tool
                                  ).ToList()
                        }).ToList()
    
                });
            return workoutVm;
        }

        public void DeleteWorkout(int id)
        {
            _db.Workouts.Find(id).IsDeleted = 1;
            _db.SaveChanges();
        }
    }
}
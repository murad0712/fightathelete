﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FightAthlete.Startup))]
namespace FightAthlete
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

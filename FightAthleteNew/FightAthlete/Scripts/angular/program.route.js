﻿"use strict";
(function () {
    angular.module("fight-app")
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
                when('/', {
                    templateUrl: '../Scripts/angular/Templates/HomeProgram.html',
                    controller: 'homeCtrl'
                }).
                when('/Edit/:Id', {
                	templateUrl: '../Scripts/angular/Templates/EditProgram.html',
                    controller: 'editCtrl'
                }).
                when('/View/:Id', {
                	templateUrl: '../Scripts/angular/Templates/ViewProgram.html',
                    controller: 'viewCtrl'
                });
    }]);
})();
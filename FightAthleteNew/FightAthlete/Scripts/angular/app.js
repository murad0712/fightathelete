﻿"use strict";
(function () {
    angular
        .module("fight-app", ["checklist-model", "ngRoute"])
        .filter("customExerciseFilter", function () {
            return function (items, input) {
                if (input.search) {
                    items = items.filter(function (item) {
                        return item.Name === input.search;
                    });
                }
                if (input.selectedLevel && input.selectedLevel !== "All") {
                    items = items.filter(function (item) {

                        var results = item.Levels.filter(function (it) {
                            return it.Name === input.selectedLevel;
                        });

                        return results.length > 0;
                    });
                }
                if (input.selectedGroup && input.selectedGroup !== "All") {
                    items = items.filter(function (item) {

                        var results = item.MuscleGroups.filter(function (it) {
                            return it.Name === input.selectedGroup;
                        });

                        return results.length > 0;
                    });
                }

                if (input.selectedTool && input.selectedTool !== "All") {
                    items = items.filter(function (item) {

                        var results = item.Tools.filter(function (it) {
                            return it.Name === input.selectedTool;
                        });

                        return results.length > 0;
                    });
                }
                return items;
            }
        });
})();
﻿"use strict";
(function () {
	angular
        .module("fight-app")
        .service("exerciseSvc", exerciseSvc);

	exerciseSvc.$inject = ["$http"];

	function exerciseSvc($http) {
	    var items = [];
		return {
			getItems: function () {
				return items;
			},
			getItem: function (id) {
				return items.filter(function (node) { return node.Id == id; });
			},
			removeItems: function (value) {
				items.splice(value, 1);
			},
			setItems: function (value) {
			    $http.get("/Exercise/GetAllExercise")
				.success(function(data){
					items = data;
				})
				.error(function (data) {
					return null;
				});
			    
			},
            getAllItems: function() {
                return $http.get("/Exercise/GetAllExercise");
            }
		};
	}
})();
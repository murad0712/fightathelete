﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .service("workoutSvc", workoutSvc);

    workoutSvc.$inject = ["$http"];

    function workoutSvc($http) {
        var items = [];
        return {
            getItems: function () {
                return items;
            },
            getItem: function (id) {
                return items.filter(function (node) { return node.WorkoutId == id; });
            },
            removeItems: function (value) {
                items.splice(value, 1);
            },
            setItems: function (value) {
                $http.get("/Workouts/GetAllWorkouts")
				.success(function (data) {
				    items = data;
				})
				.error(function (data) {
				    return null;
				});

            },
            getAllItems: function() {
                return $http.get("/Workouts/GetAllWorkouts");
            }
        };
    }
})();
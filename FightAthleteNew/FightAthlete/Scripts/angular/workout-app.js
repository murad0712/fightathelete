﻿"use strict";
(function () {
    angular.module("fight-app")
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
                when('/', {
                    templateUrl: '../Scripts/angular/Templates/HomeWorkout.html',
                    controller: 'homeCtrl'
                }).
                when('/Edit/:Id', {
                    templateUrl: '../Scripts/angular/Templates/EditWorkout.html',
                    controller: 'editCtrl'
                }).
                when('/View/:Id', {
                    templateUrl: '../Scripts/angular/Templates/ViewWorkout.html',
                    controller: 'viewCtrl'
                });
    }]);
})();
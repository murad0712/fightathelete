﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("viewCtrl", viewCtrl);

    viewCtrl.$inject = ["$scope", "$http", "workoutSvc", "$routeParams", '$timeout'];

    function viewCtrl($scope, $http, workoutSvc, $routeParams, $timeout) {
        //exerciseSvc.setItems();
        var id = $routeParams.Id;
        $scope.workout = workoutSvc.getItem(id)[0];
        if ($scope.workout === undefined) {
            workoutSvc.setItems();
            $timeout(function () {
                $scope.workout = workoutSvc.getItem(id)[0];
            }, 2000);
        }
    }
})();
﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("createWorkoutCtrl", createWorkoutCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    createWorkoutCtrl.$inject = ["$scope", "$http", "$window", "exerciseSvc", "$timeout", "$filter"];

    function createWorkoutCtrl($scope, $http, $window, exerciseSvc, $timeout, $filter) {
        $scope.workouts = [];
        $scope.workout = {
            Objectives: [],
            Levels: [],
            Exercises: [],
            IsBattleWorkout: false,
            IsBasicVersion: false,
            IsActive: true,
            IsNew: true
            /*Exercises: []*/
        };

        $http.get("/Workouts/GetAllWorkouts")
            .then(function (response) {
                $scope.workouts = response.data;
            });

        $http.get("/MuscleGroups/GetAllMuscleGroups")
            .then(function (response) {
                //console.log(response);
                $scope.muscleGroups = response.data;
            });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $http.get("/Objectives/GetAllObjectives")
            .then(function (response) {
                $scope.objectives = response.data;
            });

        $scope.nameExists = false;
        $scope.nameChanged = function () {
            var result = $scope.workouts.filter(function (node) {
                if (node.Name == $scope.workout.Name) {
                    return true;
                }
            });

            if (result.length > 0) {
                $scope.nameExists = true;
            }
            else {
                $scope.nameExists = false;
            }
        }

        $scope.data = [];
        exerciseSvc.setItems();
        $timeout(function () {
            $scope.data = exerciseSvc.getItems();
        }, 2000);

        $scope.currentPage = 0;
        $scope.pageSize = 5;
        $scope.sortType = 'Name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';     // set the default search/filter term
        $scope.index = 0;
        $scope.selectedLevel = "";
        $scope.selectedGroup = "";
        $scope.selectedTool = "";
        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.data, { Name: $scope.search })
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        var getTools = function () {
            $scope.workout.Tools = [];
            $scope.workout.Exercises.forEach(function (exercise) {
                exercise.Tools.forEach(function (tool) {
                    var alreadyIn = false;
                    for (var i = 0; i < $scope.workout.Tools.length;i++){
                        if (tool.Name == $scope.workout.Tools[i].Name) {
                            alreadyIn = true;
                            break;
                        }
                    }

                    if (alreadyIn == false) {
                        $scope.workout.Tools.push(tool);
                    }
                });
            });
        }

        $scope.addExercise = function (exercise) {
            exercise.Reps = 0;
            exercise.RepHigh = 0;
            exercise.Sets = 0;
            exercise.Tempo = 0;
            exercise.Rests = 0;
            exercise.Weight = 0;
            exercise.Distance = 0;
            var ex = {};
            angular.copy(exercise, ex);
            $scope.workout.Exercises.push(ex);
        }

        $scope.removeExercise = function (index) {
            $scope.index = 0;
            $scope.workout.Exercises.splice(index, 1);
        }

        $scope.changeIndexUp = function (index) {
            var temp = $scope.workout.Exercises[index];
            $scope.workout.Exercises[index] = $scope.workout.Exercises[index - 1];
            $scope.workout.Exercises[index - 1] = temp;
        }

        $scope.changeIndexDown = function (index) {
            var temp = $scope.workout.Exercises[index];
            $scope.workout.Exercises[index] = $scope.workout.Exercises[index + 1];
            $scope.workout.Exercises[index + 1] = temp;
        }

        $scope.changeIndex = function (change) {
            $scope.index = change;
        }

        $scope.submit = function () {
            getTools();
            var file = $scope.myFile;
            console.log(file);
            var fd = new FormData();
            fd.append('file', file);
            console.log(fd.getAll("file"));
            $http.post("/Exercise/SaveFileToDirectory", fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
            .success(function (data) {
                $scope.workout.Photo = data;
                console.log($scope.workout);
                $http.post("/Workouts/CreateWorkout", $scope.workout, {

                })
                .success(function (data) {
                    $window.location.href = "/Workouts/";
                })
                .error(function () {
                });
            })
            .error(function () {
            });

        };
        $scope.versionFilter = function () {
            if ($scope.selectedVersion == "true") {
                $scope.data = $scope.data.filter(function (node) {
                    return node.IsBasicVersion;
                })
            } else {
                $scope.data = exerciseSvc.getItems();
            }
        }

        $scope.activeFilter = function () {
            if ($scope.isActive == "true") {
                $scope.data = $scope.data.filter(function (node) {
                    return node.IsActive;
                })
            } else {
                $scope.data = exerciseSvc.getItems();
            }
        }

        $scope.changeExercise = function(item) {
            $scope.exercise = item;
        }
    }
})();
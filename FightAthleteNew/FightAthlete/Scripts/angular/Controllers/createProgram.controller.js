﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("createProgramCtrl", createProgramCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    createProgramCtrl.$inject = ["$scope", "$http", "$window", "$timeout", "$filter"];

    function createProgramCtrl($scope, $http, $window, $timeout, $filter) {
        $scope.programs = [];
        $scope.program = {
            IsBasic: false,
            IsNew: true,
            IsActive: true,
            Levels: [],
            ProgramWorkouts: []
        };

        $scope.duration = {
            Name:"0"
        };

        $http.get("/Programs/GetAllPrograms")
            .then(function (response) {
                $scope.programs = response.data;
            });

        $http.get("/Workouts/GetAllWorkouts")
            .then(function (response) {
                //console.log(response);
                $scope.data = response.data;
                $scope.workouts = $scope.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Objectives/GetAllObjectives")
            .then(function (response) {
                //console.log(response);
                $scope.objectives = response.data;
            });

        $http.get("/Frequencies/GetAllFrequencies")
            .then(function (response) {
                //console.log(response);
                $scope.frequencies = response.data;
            });

        $http.get("/Durations/GetAllDurations")
            .then(function (response) {
                //console.log(response);
                $scope.durations = response.data;
            });
        $scope.nameExists = false;
        $scope.nameChanged = function () {
            var result = $scope.programs.filter(function (node) {
                if(node.Name==$scope.program.Name){
                    return true;
                }
            });

            if(result.length>0){
                $scope.nameExists = true;
            }
            else {
                $scope.nameExists = false;
            }
        }

        $scope.submit = function () {
            console.log($scope.list1);
            getTools();
            getPoint();
            $scope.program.FrequencyId = $scope.frequency.Id;
            $scope.program.DurationId = $scope.duration.Id;

            console.log($scope.program);

            $http.post("/Programs/Create", $scope.program, {
                
            })
            .success(function (data) {
                console.log(data);
                $window.location.href = "/Programs/";
            })
            .error(function () {
            });
        }

        $scope.durationChanged = function(){
            $scope.list1 = [];

            var weeks = parseInt($scope.duration.Name);

            for (var i = 0; i < 7*weeks; i++) {
                $scope.list1.push([]);
            }
        }

        $scope.workouts = [];
        $scope.currentPage = 0;
        $scope.search = '';     // set the default search/filter term
        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.workouts, { Name: $scope.search });
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / 10);
        }

        var getTools = function () {
            $scope.program.ProgramWorkouts = [];
            $scope.program.Tools = [];
            $scope.list1.forEach(function (workouts, index) {
                workouts.forEach(function (workout) {
                    workout.Tools.forEach(function (tool) {
                        var alreadyIn = false;
                        for (var i = 0; i < $scope.program.Tools.length; i++) {
                            if (tool.Name == $scope.program.Tools[i].Name) {
                                alreadyIn = true;
                                break;
                            }
                        }

                        if (alreadyIn == false) {
                            $scope.program.Tools.push(tool);
                        }
                    });

                    var programWorkout = {
                        WorkoutId: workout.WorkoutId,
                        Day: index + 1,
                        Type: workout.Type
                    }

                    $scope.program.ProgramWorkouts.push(programWorkout);
                });
                
            });
        }

        var getPoint = function () {
            $scope.program.Point = 0;
            $scope.list1.forEach(function (workouts, index) {
                workouts.forEach(function (workout) {
                    $scope.program.Point += workout.Points;
                });

            });
        }

        $scope.remove = function (arr, index) {
            arr.splice(index,1);
        }

        $scope.versionFilter = function () {
            if ($scope.selectedVersion == "true") {
                $scope.workouts = $scope.workouts.filter(function (node) {
                    return node.IsBasicVersion==1;
                });
            } else {
                $scope.workouts = $scope.data;
            }
        }
        $scope.activeFilter = function () {
            if ($scope.isActive == "true") {
                $scope.workouts = $scope.workouts.filter(function (node) {
                    return node.IsActive==1;
                });
            } else {
                $scope.workouts = $scope.data;
            }
        }

        $scope.changeWorkout = function (item) {
            $scope.workout = item;
        }
    }
})();
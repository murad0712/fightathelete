﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("homeCtrl", homeCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    homeCtrl.$inject = ["$scope", "$http", "exerciseSvc", "$timeout", "$filter", "$window"];

    function homeCtrl($scope, $http, exerciseSvc, $timeout, $filter, $window) {
        $scope.data = [];
        exerciseSvc.getAllItems()
            .then(function(response) {
                $scope.data = response.data;
                $scope.data.forEach(function (node) {
                    var d = node.CreatedDate.split("(")[1];
                    var nd = d.split(")")[0];
                    node.CreatedDate = nd;
                });
            }, function(error) {

            });
        
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.sortType = 'Name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';     // set the default search/filter term
        $scope.selectedLevel = "";
        $scope.selectedGroup = "";
        $scope.selectedTool = "";
        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.data, { Name: $scope.search });
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $scope.delete = function (id) {
            var r = confirm("Are you sure?");
            if (r == true) {
                $http.get("/Exercise/DeleteExercise/" + id)
                .then(function (response) {
                    $window.location.href = "/Exercise/";
                });
            }
        }

        $http.get("/MuscleGroups/GetAllMuscleGroups")
                    .then(function (response) {
                        $scope.muscleGroups = response.data;
                    });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $scope.versionFilter = function() {
            if ($scope.selectedVersion == "true") {
                $scope.data = $scope.data.filter(function(node) {
                    return node.IsBasicVersion;
                });
            } else {
                $scope.data = exerciseSvc.getItems();
            }
        }

        $scope.activeFilter = function () {
            if ($scope.isActive == "true") {
                $scope.data = $scope.data.filter(function(node) {
                    return node.IsActive;
                });
            } else {
                $scope.data = exerciseSvc.getItems();
            }
        }
    }
})();
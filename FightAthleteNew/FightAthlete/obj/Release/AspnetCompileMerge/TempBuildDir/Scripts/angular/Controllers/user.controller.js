﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("homeCtrl", homeCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    homeCtrl.$inject = ["$scope", "$http", "$filter", "$window"];

    function homeCtrl($scope, $http, $filter, $window) {
        $scope.data = [];

        function loadData() {
            $http.get("/Users/GetAllUsers")
                .then(function (response) {
                    //console.log(response);
                    $scope.data = response.data;
                });
        }
        loadData();
        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.sortType = 'Name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';     // set the default search/filter term

        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            var filter = $filter('filter');

            // First filter the non-exact
            var filtered = filter($scope.data, { Name: $scope.search, Country: $scope.country, MembershipType: $scope.membership });

            // than the exact filters
            var genderFiltered = filter(filtered, { Gender: $scope.gender }, true);

            return $filter('orderBy')(genderFiltered, $scope.sortType, $scope.sortReverse);
        }

        $scope.exportData = function () {
            alasql('SELECT * INTO XLSX("fightAthlete.xlsx",{headers:true}) FROM ?', [$scope.getData()]);
        };

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $http.get("/MembershipTypes/GetAllMembership")
            .then(function (response) {
                //console.log(response);
                $scope.memberships = [];
                var result = response.data;
                for (var i = 0; i < result.length; i++) {
                    $scope.memberships.push(result[i].Name);
                }
            });
    }
})();
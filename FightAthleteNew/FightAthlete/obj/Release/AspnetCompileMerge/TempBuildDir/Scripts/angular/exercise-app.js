﻿"use strict";
(function () {
    angular.module("fight-app")
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.
                when('/', {
                    templateUrl: '../Scripts/angular/Templates/HomeExercise.html',
                    controller: 'homeCtrl'
                }).
                when('/Edit/:Id', {
                    templateUrl: '../Scripts/angular/Templates/EditExercise.html',
                    controller: 'editCtrl'
                }).
                when('/View/:Id', {
                    templateUrl: '../Scripts/angular/Templates/ViewExercise.html',
                    controller: 'viewCtrl'
                });
    }]);
})();
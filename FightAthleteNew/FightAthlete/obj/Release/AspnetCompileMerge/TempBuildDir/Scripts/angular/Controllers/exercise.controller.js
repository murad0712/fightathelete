﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("exerciseCtrl", exerciseCtrl);

    exerciseCtrl.$inject = ["$scope", "$http", "$window", "$q"];

    function exerciseCtrl($scope, $http, $window, $q) {
        $scope.exercises = [];
        $scope.exercise = {
            MuscleGroups: [],
            Tools: [],
            Levels: [],
            IsRestExercise: 0,
            IsBasicVersion: false,
            IsNew: true,
            IsActive: true,
            Duration: 0
        };
        $http.get("/Exercise/GetAllExercise")
            .then(function (response) {
                //console.log(response);
                $scope.exercises = response.data;
            });

        $http.get("/MuscleGroups/GetAllMuscleGroups")
            .then(function (response) {
                //console.log(response);
                $scope.muscleGroups = response.data;
            });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $scope.setIsRestExercise = function (isRestExercise) {
            $scope.exercise.IsRestExercise = isRestExercise;
        };

        $scope.nameExists = false;
        $scope.nameChanged = function () {
            var result = $scope.exercises.filter(function (node) {
                if (node.Name == $scope.exercise.Name) {
                    return true;
                }
            });

            if (result.length > 0) {
                $scope.nameExists = true;
            }
            else {
                $scope.nameExists = false;
            }
        }

        $scope.submit = function () {

            //upload video
            var video = $scope.myFile;
            var videoFormData = new FormData();
            videoFormData.append('file', video);
            videoFormData.append('exerciseName', $scope.exercise.Name);

            var filePromises = [2];

            filePromises[0] = $http.post("/Exercise/SaveFileToDirectory",
                videoFormData,
                {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                });

            //upload thumbnail
            var thumbnail = $scope.thumbnail;
            var thumbnailFormData = new FormData();
            thumbnailFormData.append('file', thumbnail);
            thumbnailFormData.append('exerciseName', $scope.exercise.Name);

            filePromises[1] = $http.post("/Exercise/SaveFileToDirectory",
                thumbnailFormData,
                {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                });

            $q.all(filePromises)
                .then(function (responses) {

                    $scope.exercise.Video = responses[0].data;
                    $scope.exercise.Thumbnail = responses[0].data;

                    $http.post("/Exercise/Create", $scope.exercise, {})
                        .success(function (data) {
                            $window.location.href = "/Exercise/";
                        })
                        .error(function () {
                        });
                },
                function (error) {
                    console.log(error);
                });
        };
    }
})();
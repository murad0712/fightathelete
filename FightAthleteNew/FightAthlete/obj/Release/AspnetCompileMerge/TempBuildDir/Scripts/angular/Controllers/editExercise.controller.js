﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("editCtrl", editCtrl);

    editCtrl.$inject = ["$scope", "$http", "exerciseSvc", "$routeParams", '$timeout', '$window'];

    function editCtrl($scope, $http, exerciseSvc, $routeParams, $timeout, $window) {
        //exerciseSvc.setItems();
        var id = $routeParams.Id;
        $scope.exercise = exerciseSvc.getItem(id)[0];
        if ($scope.exercise === undefined) {
            exerciseSvc.setItems();
            $timeout(function () {
                $scope.exercise = exerciseSvc.getItem(id)[0];
            }, 2000);
        }

        $timeout(function () {
            if ($scope.exercise.IsRestExercise == 1) $scope.exercise.IsRestExercise = true;
            if ($scope.exercise.IsBasicVersion == 1) $scope.exercise.IsBasicVersion = true;
            if ($scope.exercise.IsNew == 1) $scope.exercise.IsNew = true;
            if ($scope.exercise.IsActive == 1) $scope.exercise.IsActive = true;
        }, 2000);

        $http.get("/MuscleGroups/GetAllMuscleGroups")
            .then(function (response) {
                //console.log(response);
                $scope.muscleGroups = response.data;
            });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $scope.setIsRestExercise = function (isRestExercise) {
            $scope.exercise.IsRestExercise = isRestExercise;
        };
        var objectToFormData = function (obj, form, namespace) {
            var fd = form || new FormData();
            var formKey;

            for (var property in obj) {
                if (obj.hasOwnProperty(property) && obj[property]) {
                    if (namespace) {
                        formKey = namespace + '[' + property + ']';
                    } else {
                        formKey = property;
                    }

                    // if the property is an object, but not a File, use recursivity.
                    if (obj[property] instanceof Date) {
                        fd.append(formKey, obj[property].toISOString());
                    }
                    else if (typeof obj[property] === 'boolean') {
                        if (obj[property]) {
                            fd.append(formKey, 1);
                        } else {
                            fd.append(formKey, 0);
                        }
                    }
                    else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                        objectToFormData(obj[property], fd, formKey);
                    } else { // if it's a string or a File object
                        fd.append(formKey, obj[property]);
                    }
                }
            }

            return fd;
        }

        $scope.submit = function () {
            var formData = objectToFormData($scope.exercise);

            var video = $scope.myFile;
            if (video !== 'undefined') {
                formData.append('VideoFile', video);
            }

            var thumbnail = $scope.thumbnail;

            if (thumbnail !== 'undefined') {
                formData.append('ThumbnailFile', thumbnail);
            }

            $http.post("/Exercise/Edit", formData,
                {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
                .success(function (data) {
                    $window.location.href = "/Exercise/";
                })
                .error(function () {
                });
        };
    }
})();
﻿"use strict";
(function () {
    angular.module("fight-app", ["checklist-model", "ngRoute", "ngDragDrop"]);
})();
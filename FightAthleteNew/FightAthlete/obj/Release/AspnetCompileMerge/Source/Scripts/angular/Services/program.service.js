﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .service("programSvc", programSvc);

    programSvc.$inject = ["$http"];

    function programSvc($http) {
        var items = [];
        var workouts = [];
        var durations = [];
        return {
            getItems: function () {
                return items;
            },
            getItem: function (id) {
                return items.filter(function (node) { return node.Id == id; });
            },
            removeItems: function (value) {
                items.splice(value, 1);
            },
            setItems: function (value) {
                $http.get("/Programs/GetAllPrograms")
				.success(function (data) {
				    items = data;
				})
				.error(function (data) {
				    items = [];
				});

                $http.get("/Workouts/GetAllWorkouts")
				.success(function (data) {
				    workouts = data;
				})
				.error(function (data) {
				    return null;
				});

                $http.get("/Durations/GetAllDurations")
				.success(function (data) {
				    durations = data;
				})
				.error(function (data) {
				    return null;
				});
            },
            getAllItems: function () {
                return $http.get("/Programs/GetAllPrograms");
            },
            getWorkouts: function () {
                return workouts;
            },
            getWorkout: function (id) {
                return workouts.filter(function (node) { return node.WorkoutId == id; });
            },
            getDurations: function () {
                return durations;
            },
            getDurationName: function (id) {
                return durations.filter(function (node) { return node.Id == id; })[0];
            }
        };
    }
})();
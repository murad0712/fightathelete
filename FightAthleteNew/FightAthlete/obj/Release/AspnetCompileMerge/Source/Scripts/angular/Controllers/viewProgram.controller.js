﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("viewCtrl", viewCtrl);

    viewCtrl.$inject = ["$scope", "$http", "programSvc", "$routeParams", '$timeout'];

    function viewCtrl($scope, $http, programSvc, $routeParams, $timeout) {
        var id = $routeParams.Id;
        $scope.list1 = [];
        $scope.program = programSvc.getItem(id)[0];
        if ($scope.program === undefined) {
            programSvc.setItems();
            $timeout(function () {
                $scope.program = programSvc.getItem(id)[0];
            }, 1000);
        }

        $timeout(function () {

            $scope.list1 = [];

            $scope.duration = programSvc.getDurationName($scope.program.DurationId);

            var weeks = parseInt($scope.duration.Name);

            for (var i = 0; i < 7 * weeks; i++) {
                $scope.list1.push([]);
            }

            for (var i = 0; i < $scope.program.ProgramWorkouts.length; i++){
                var workout = $scope.program.ProgramWorkouts[i];
                $scope.list1[workout.Day - 1].push(programSvc.getWorkout(workout.WorkoutId)[0]);
            }

            console.log($scope.list1);
        }, 2000);
    }
})();
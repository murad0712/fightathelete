﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("viewCtrl", viewCtrl);

    viewCtrl.$inject = ["$scope", "$http", "exerciseSvc", "$routeParams", '$timeout'];

    function viewCtrl($scope, $http, exerciseSvc, $routeParams, $timeout) {
        //exerciseSvc.setItems();
        var id = $routeParams.Id;
        $scope.exercise = exerciseSvc.getItem(id)[0];
        if ($scope.exercise === undefined) {
            exerciseSvc.setItems();
            $timeout(function () {
                $scope.exercise = exerciseSvc.getItem(id)[0];
            }, 2000);
        }
    }
})();
﻿app.controller('adminController', function ($scope, adminservice) {
    $scope.Admin = [];

    $scope.Message = "";
    $scope.userName = sessionStorage.getItem('userName');


    loadAdmin();

    function loadAdmin() {

        var promise = adminservice.get();
        promise.then(function (resp) {
            $scope.Admin = resp.data;
            $scope.Message = "Call Completed Successfully";
        }, function (err) {
            $scope.Message = "Error!!! " + err.status;
        });
    };


    $scope.logout = function () {

        sessionStorage.removeItem('accessToken');
        window.location.href = '/';
    };
});
﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("editCtrl", editCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    editCtrl.$inject = ["$scope", "$http", "workoutSvc", "exerciseSvc", "$routeParams", '$timeout','$window', '$filter'];

    function editCtrl($scope, $http, workoutSvc, exerciseSvc, $routeParams, $timeout, $window, $filter) {
        //exerciseSvc.setItems();
        var id = $routeParams.Id;
        $scope.workout = workoutSvc.getItem(id)[0];
        if ($scope.workout === undefined) {
            workoutSvc.setItems();
            $timeout(function () {
                $scope.workout = workoutSvc.getItem(id)[0];
                if ($scope.workout.IsBattleWorkout == 1) {
                    $scope.workout.IsBattleWorkout = true;
                }
                else {
                    $scope.workout.IsBattleWorkout = false;
                }

                if ($scope.workout.IsRestDay == 1) {
                    $scope.workout.IsRestDay = true;
                }
                else {
                    $scope.workout.IsRestDay = false;
                }

                if ($scope.workout.IsBasicVersion == 1) {
                    $scope.workout.IsBasicVersion = true;
                }
                else {
                    $scope.workout.IsBasicVersion = false;
                }

                if ($scope.workout.IsActive == 1) {
                    $scope.workout.IsActive = true;
                }
                else {
                    $scope.workout.IsActive = false;
                }
                if ($scope.workout.IsNew == 1) {
                    $scope.workout.IsNew = true;
                }
                else {
                    $scope.workout.IsNew = false;
                }
            }, 2000);
        }
        else {
            if ($scope.workout.IsBattleWorkout == 1) {
                $scope.workout.IsBattleWorkout = true;
            }
            else {
                $scope.workout.IsBattleWorkout = false;
            }

            if ($scope.workout.IsRestDay == 1) {
                $scope.workout.IsRestDay = true;
            }
            else {
                $scope.workout.IsRestDay = false;
            }

            if ($scope.workout.IsBasicVersion == 1) {
                $scope.workout.IsBasicVersion = true;
            }
            else {
                $scope.workout.IsBasicVersion = false;
            }

            if ($scope.workout.IsActive == 1) {
                $scope.workout.IsActive = true;
            }
            else {
                $scope.workout.IsActive = false;
            }
            if ($scope.workout.IsNew == 1) {
                $scope.workout.IsNew = true;
            }
            else {
                $scope.workout.IsNew = false;
            }
        }

        

        $http.get("/MuscleGroups/GetAllMuscleGroups")
            .then(function (response) {
                //console.log(response);
                $scope.muscleGroups = response.data;
            });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $http.get("/Objectives/GetAllObjectives")
            .then(function (response) {
                $scope.objectives = response.data;
            });

        $scope.data = [];
        exerciseSvc.setItems();
        $timeout(function () {
            $scope.data = exerciseSvc.getItems();
        }, 2000);

        $scope.currentPage = 0;
        $scope.pageSize = 5;
        $scope.sortType = 'a'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';     // set the default search/filter term
        $scope.index = 0;
        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.data, { Name: $scope.search })
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $scope.filterByLevel = function () {
            $scope.selectedGroup = {};
            $scope.selectedTool = {};
            $scope.data = exerciseSvc.getItems();
            $scope.data = $scope.data.filter(function (node) {
                var a = false;
                node.Levels.forEach(function (level) {
                    if (level.Name == $scope.selectedLevel.Name) {
                        a = true;
                    }
                });
                return a;
            });

            console.log($scope.data);
        }

        $scope.changeIndexUp = function (index) {
            var temp = $scope.workout.Exercises[index];
            $scope.workout.Exercises[index] = $scope.workout.Exercises[index - 1];
            $scope.workout.Exercises[index - 1] = temp;
        }

        $scope.changeIndexDown = function (index) {
            var temp = $scope.workout.Exercises[index];
            $scope.workout.Exercises[index] = $scope.workout.Exercises[index + 1];
            $scope.workout.Exercises[index + 1] = temp;
        }

        $scope.changeIndex = function (change) {
            $scope.index = change;
        }

        $scope.filterByGroup = function () {
            $scope.selectedLevel = {};
            $scope.selectedTool = {};
            $scope.data = exerciseSvc.getItems();
            $scope.data = $scope.data.filter(function (node) {
                var a = false;
                node.MuscleGroups.forEach(function (group) {
                    if (group.Name == $scope.selectedGroup.Name) {
                        a = true;
                    }
                });
                return a;
            });

            console.log($scope.data);
        }

        $scope.filterByTool = function () {
            $scope.selectedLevel = {};
            $scope.selectedGroup = {};
            $scope.data = exerciseSvc.getItems();
            $scope.data = $scope.data.filter(function (node) {
                var a = false;
                node.Tools.forEach(function (tool) {
                    if (tool.Name == $scope.selectedTool.Name) {
                        a = true;
                    }
                });
                return a;
            });

            console.log($scope.data);
        }

        $scope.addExercise = function (exercise) {
            exercise.Reps = 0;
            exercise.RepHigh = 0;
            exercise.Sets = 0;
            exercise.Tempo = 0;
            exercise.Rests = 0;
            exercise.Weight = 0;
            exercise.Distance = 0;
            $scope.workout.Exercises.push(exercise);
        }

        $scope.removeExercise = function (index) {
            $scope.workout.Exercises.splice(index, 1);
        }

        var getTools = function () {
            $scope.workout.Tools = [];
            $scope.workout.Exercises.forEach(function (exercise) {
                exercise.Tools.forEach(function (tool) {
                    var alreadyIn = false;
                    for (var i = 0; i < $scope.workout.Tools.length; i++) {
                        if (tool.Name == $scope.workout.Tools[i].Name) {
                            alreadyIn = true;
                            break;
                        }
                    }

                    if (alreadyIn == false) {
                        $scope.workout.Tools.push(tool);
                    }
                });
            });
        }

        $scope.submit = function () {
            getTools();

            $http.post("/Workouts/Edit", $scope.workout, {

            })
            .success(function (data) {
                $window.location.href = "/Workouts/";
            })
            .error(function () {
            });

        };

        $scope.versionFilter = function () {
            if ($scope.selectedVersion == "true") {
                $scope.data = $scope.data.filter(function (node) {
                    return node.IsBasicVersion;
                })
            } else {
                $scope.data = exerciseSvc.getItems();
            }
        }
    }
})();
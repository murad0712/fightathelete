/// <autosync enabled="true" />
/// <reference path="alasql.min.js" />
/// <reference path="angular.min.js" />
/// <reference path="angular/admin/AdminController.js" />
/// <reference path="angular/admin/AdminService.js" />
/// <reference path="angular/admin/LoginLogic.js" />
/// <reference path="angular/admin/Module.js" />
/// <reference path="angular/app.js" />
/// <reference path="angular/controllers/createProgram.controller.js" />
/// <reference path="angular/controllers/createWorkout.controller.js" />
/// <reference path="angular/controllers/editExercise.controller.js" />
/// <reference path="angular/controllers/editProgram.controller.js" />
/// <reference path="angular/controllers/editWorkout.controller.js" />
/// <reference path="angular/controllers/exercise.controller.js" />
/// <reference path="angular/controllers/homeExercise.controller.js" />
/// <reference path="angular/controllers/homeProgram.controller.js" />
/// <reference path="angular/controllers/homeWorkout.controller.js" />
/// <reference path="angular/controllers/user.controller.js" />
/// <reference path="angular/controllers/viewExercise.controller.js" />
/// <reference path="angular/controllers/viewProgram.controller.js" />
/// <reference path="angular/controllers/viewWorkout.controller.js" />
/// <reference path="angular/directives/file.directive.js" />
/// <reference path="angular/exercise-app.js" />
/// <reference path="angular/program.route.js" />
/// <reference path="angular/program-app.js" />
/// <reference path="angular/services/exercise.service.js" />
/// <reference path="angular/services/program.service.js" />
/// <reference path="angular/services/workout.service.js" />
/// <reference path="angular/workout-app.js" />
/// <reference path="angular-dragdrop.min.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="xlsx.core.min.js" />

﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("editCtrl", editCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    editCtrl.$inject = ["$scope", "$http", "programSvc", "$window", "$timeout", "$routeParams", "$filter"];

    function editCtrl($scope, $http, programSvc, $window, $timeout, $routeParams, $filter) {
        var id = $routeParams.Id;
        $scope.list1 = [];
        $scope.program = programSvc.getItem(id)[0];
        if ($scope.program === undefined) {
            programSvc.setItems();
            $timeout(function () {
                $scope.program = programSvc.getItem(id)[0];
            }, 2000);
        }

        $timeout(function () {
            if ($scope.program.IsBasic == 1) {
                $scope.program.IsBasic = true;
            }
            if ($scope.program.IsNew == 1) {
                $scope.program.IsNew = true;
            }
            if ($scope.program.IsActive == 1) {
                $scope.program.IsActive = true;
            }
            $scope.list1 = [];
            $scope.duration = programSvc.getDurationName($scope.program.DurationId);

            var weeks = parseInt($scope.duration.Name);

            for (var i = 0; i < 7 * weeks; i++) {
                $scope.list1.push([]);
            }

            for (var i = 0; i < $scope.program.ProgramWorkouts.length; i++) {
                var workout = $scope.program.ProgramWorkouts[i];
                $scope.list1[workout.Day - 1].push(programSvc.getWorkout(workout.WorkoutId)[0]);
            }
        }, 2200);

        $http.get("/Workouts/GetAllWorkouts")
            .then(function (response) {
                //console.log(response);
                $scope.data = response.data;
                $scope.workouts = $scope.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $http.get("/Objectives/GetAllObjectives")
            .then(function (response) {
                //console.log(response);
                $scope.objectives = response.data;
            });

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                //console.log(response);
                $scope.tools = response.data;
            });

        $http.get("/Frequencies/GetAllFrequencies")
            .then(function (response) {
                //console.log(response);
                $scope.frequencies = response.data;

                $timeout(function () {
                    $scope.frequencies.forEach(function (node) {
                        if (node.Id == $scope.program.FrequencyId) {
                            $scope.frequency = node;
                        }
                    });
                }, 2000);
            });

        $http.get("/Durations/GetAllDurations")
            .then(function (response) {
                //console.log(response);
                $scope.durations = response.data;

                $timeout(function () {
                    $scope.durations.forEach(function (node) {
                        if (node.Id == $scope.program.DurationId) {
                            $scope.duration = node;
                        }
                    });
                }, 2000);
            });

        $scope.submit = function () {
            getTools();
            getPoint();
            $scope.program.FrequencyId = $scope.frequency.Id;
            $scope.program.DurationId = $scope.duration.Id;

            console.log($scope.program);

            $http.post("/Programs/Edit", $scope.program, {

            })
            .success(function (data) {
                console.log(data);
                $window.location.href = "/Programs/";
            })
            .error(function () {
            });
        }

        $scope.durationChanged = function () {
            $scope.list1 = [];

            var weeks = parseInt($scope.duration.Name)

            for (var i = 0; i < 7 * weeks; i++) {
                $scope.list1.push({});
            }
        }

        $scope.workouts = [];
        $scope.currentPage = 0;
        $scope.search = '';     // set the default search/filter term
        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.workouts, { Name: $scope.search })
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / 5);
        }

        

        var getTools = function () {
            $scope.program.ProgramWorkouts = [];
            $scope.program.Tools = [];
            $scope.list1.forEach(function (workouts, index) {
                workouts.forEach(function (workout) {
                    workout.Tools.forEach(function (tool) {
                        var alreadyIn = false;
                        for (var i = 0; i < $scope.program.Tools.length; i++) {
                            if (tool.Name == $scope.program.Tools[i].Name) {
                                alreadyIn = true;
                                break;
                            }
                        }

                        if (alreadyIn == false) {
                            $scope.program.Tools.push(tool);
                        }
                    });
                    console.log(workout);
                    var programWorkout = {
                        WorkoutId: workout.WorkoutId,
                        Day: index + 1,
                        Type: workout.Type
                    }

                    $scope.program.ProgramWorkouts.push(programWorkout);
                });

            });
        }

        var getPoint = function () {
            $scope.program.Point = 0;
            $scope.list1.forEach(function (workouts, index) {
                workouts.forEach(function (workout) {
                    $scope.program.Point += workout.Points;
                });

            });
        }

        $scope.remove = function (arr, index) {
            arr.splice(index, 1);
        }

        $scope.versionFilter = function () {
            if ($scope.selectedVersion == "true") {
                $scope.workouts = $scope.data.filter(function (node) {
                    return node.IsBasicVersion==1;
                });
            } else {
                $scope.workouts = $scope.data;
            }
        }
    }
})();
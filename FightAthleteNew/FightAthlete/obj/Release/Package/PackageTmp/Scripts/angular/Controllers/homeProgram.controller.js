﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("homeCtrl", homeCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    homeCtrl.$inject = ["$scope", "$http", "programSvc", "$timeout", "$filter", "$window"];

    function homeCtrl($scope, $http, programSvc, $timeout, $filter, $window) {
        $scope.data = [];
        programSvc.getAllItems()
            .then(function (response) {
                $scope.data = response.data;
                $scope.data.forEach(function (node) {
                    var d = node.CreatedOn.split("(")[1];
                    var nd = d.split(")")[0];
                    node.CreatedOn = nd;
                });
            }, function (error) {

            });

        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.sortType = 'Name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';     // set the default search/filter term

        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.data, { Name: $scope.search })
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $scope.delete = function (id) {
            var r = confirm("Are you sure?");
            if (r == true) {
                $http.get("/Programs/DeleteProgram/" + id)
                .then(function (response) {
                    $window.location.href = "/Programs/";
                });
            }
        }

        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $http.get("/Durations/GetAllDurations")
			.then(function (response) {
			    $scope.durations = response.data;
			});

        $http.get("/Frequencies/GetAllFrequencies")
			.then(function (response) {
			    $scope.frequencies = response.data;
			});

        $scope.filterId = function (actual, expected) {
            return actual == expected;
        }

        $scope.versionFilter = function () {
            if ($scope.selectedVersion == "true") {
                $scope.data = $scope.data.filter(function (node) {
                    return node.IsBasic;
                });
            } else {
                $scope.data = programSvc.getItems();
            }
        }

        $scope.activeFilter = function () {
            if ($scope.isActive == "true") {
                $scope.data = $scope.data.filter(function (node) {
                    return node.IsActive;
                })
            } else {
                $scope.data = programSvc.getItems();
            }
        }
    }
})();
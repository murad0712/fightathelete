﻿"use strict";
(function () {
    angular
        .module("fight-app")
        .controller("homeCtrl", homeCtrl)
        .filter('startFrom', function () {
            return function (input, start) {
                start = +start; //parse to int
                return input.slice(start);
            }
        });

    homeCtrl.$inject = ["$scope", "$http", "workoutSvc", "$timeout", "$filter", "$window"];

    function homeCtrl($scope, $http, workoutSvc, $timeout, $filter, $window) {
        $scope.data = [];
        workoutSvc.getAllItems()
            .then(function (response) {
                $scope.data = response.data;
                $scope.data.forEach(function (node) {
                    var d = node.CreatedOn.split("(")[1];
                    var nd = d.split(")")[0];
                    node.CreatedOn = nd;
                });
            }, function (error) {

            });

        $scope.currentPage = 0;
        $scope.pageSize = 10;
        $scope.sortType = 'Name'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.search = '';     // set the default search/filter term

        $scope.getData = function () {
            //console.log($scope.selectedLevel);
            // needed for the pagination calc
            // https://docs.angularjs.org/api/ng/filter/filter
            return $filter('filter')($scope.data, { Name: $scope.search })
        }

        $scope.numberOfPages = function () {
            return Math.ceil($scope.getData().length / $scope.pageSize);
        }

        $scope.filterByLevel = function () {
            $scope.selectedObjective = {};
            $scope.selectedTool = {};
            $scope.data = workoutSvc.getItems();
            $scope.data = $scope.data.filter(function (node) {
                var a = false;
                node.Levels.forEach(function (level) {
                    if (level.Name == $scope.selectedLevel.Name) {
                        a = true;
                    }
                });
                return a;
            });

            //console.log($scope.data);
        }

        $scope.filterByObjective = function () {
            $scope.selectedLevel = {};
            $scope.selectedTool = {};
            $scope.data = workoutSvc.getItems();
            $scope.data = $scope.data.filter(function (node) {
                var a = false;
                node.Objectives.forEach(function (group) {
                    if (group.Name == $scope.selectedObjective.Name) {
                        a = true;
                    }
                });
                return a;
            });

            //console.log($scope.data);
        }

        $scope.filterByTool = function () {
            $scope.selectedLevel = {};
            $scope.selectedObjective = {};
            $scope.data = workoutSvc.getItems();
            $scope.data = $scope.data.filter(function (node) {
                var a = false;
                node.Tools.forEach(function (tool) {
                    if (tool.Name == $scope.selectedTool.Name) {
                        a = true;
                    }
                });
                return a;
            });

            //console.log($scope.data);
        }

        $scope.delete = function (id) {
            var r = confirm("Are you sure?");
            if (r == true) {
                $http.get("/Workouts/DeleteWorkout/" + id)
                .then(function (response) {
                    $window.location.href = "/Workouts/";
                });
            }
        }

        $http.get("/Objectives/GetAllObjectives")
            .then(function (response) {
                $scope.objectives = response.data;
            });
        $http.get("/Tools/GetAllTools")
            .then(function (response) {
                $scope.tools = response.data;
            });

        $http.get("/Levels/GetAllLevels")
            .then(function (response) {
                //console.log(response);
                $scope.levels = response.data;
            });

        $scope.versionFilter = function () {
            if ($scope.selectedVersion == "true") {
                $scope.data = $scope.data.filter(function(node) {
                    return node.IsBasicVersion;
                });
            } else {
                $scope.data = workoutSvc.getItems();
            }
        }
        $scope.activeFilter = function () {
            if ($scope.isActive == "true") {
                $scope.data = $scope.data.filter(function (node) {
                    return node.IsActive;
                })
            } else {
                $scope.data = workoutSvc.getItems();
            }
        }
    }
})();
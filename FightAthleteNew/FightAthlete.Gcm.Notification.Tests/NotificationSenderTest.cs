﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace FightAthlete.Gcm.Notification.Tests
{
    [TestClass]
    public class NotificationSenderTest
    {
        private const string ApiKey = "AAAAlviiSeE:APA91bG3v8qq-Iy4DYkke0Biq5CbAQlk-Wm-qgZDQYCFSZAkP8IjWXzB8ZD7IHwZ9re9u2rVng9wHWNc4HZifAlsq9dgSckjHrATc6Eoj04d_pRihAkbO8VY2s8fSPIoZWCkikQiTv44";
        private const string GcmUrl = "https://gcm-http.googleapis.com/gcm/send";
        [TestMethod]
        public async Task NotificationSenderTest_Send()
        {
            var to = "c9sHPGMVtAI:APA91bGjUdy16TjgkZeZhKcFtdYSM2OAcaxUP3VGDnqhiyUQfZO1krF1lR7L_OOZuAVVMODd_-v2y7e3KpWGvZ46XuV4kQ3cgCAClUOxxgHUnpwiEbFgT6ttL9izh_Cm4CzW6EIWAizg";

            var notificationSender = new NotificationSender(ApiKey, GcmUrl);
            var message = new JObject {{"message", "test notification11"}};

            await notificationSender.Send(message, to);
        }

        [TestMethod]
        public async Task NotificationSenderTest_SendToAll()
        {
            var notificationSender = new NotificationSender(ApiKey, GcmUrl);
            var message = new JObject { { "message", "Send to all" } };

            await notificationSender.SendToAll(message);
        }
    }
}

﻿namespace FightAthlete.Notification.Scheduler.Models
{
    public class AgendaNotification
    {
        public int User_Id { get; set; }
        public string GcmRegistrationToken { get; set; }
    }
}
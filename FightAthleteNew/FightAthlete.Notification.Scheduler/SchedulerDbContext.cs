﻿using System.Configuration;
using System.Data.Entity;

namespace FightAthlete.Notification.Scheduler
{
    public class SchedulerDbContext : DbContext
    {
        public SchedulerDbContext()
            : base(ConfigurationManager.AppSettings["dbConnection"])
        {

        }
    }
}
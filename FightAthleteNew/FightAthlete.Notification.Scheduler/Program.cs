﻿using System;
using System.Configuration;
using System.Linq;
using FightAthlete.Gcm.Notification;
using FightAthlete.Notification.Scheduler.Models;
using Newtonsoft.Json.Linq;

namespace FightAthlete.Notification.Scheduler
{
    class Program
    {
        static void Main(string[] args)
        {
            var apiKey = ConfigurationManager.AppSettings["apiKey"];
            var gcmUrl = ConfigurationManager.AppSettings["gcmUrl"];
            var dbContext = new SchedulerDbContext();
            var notificationSender = new NotificationSender(apiKey, gcmUrl);

            // Send agenda notification
            try
            {
                var agendaDate = DateTime.Today;
                if (args.Length > 0)
                {
                    DateTime.TryParse(args[0], out agendaDate);
                }

                var query = "select [User_Id], [GcmRegistrationToken] from [fightathlete].[dbo].[User] u, " +
                            $"[fightathlete].[dbo].[Agenda] a where u.Id = a.[User_Id] and [Date] = '{agendaDate.Date:yyyy-MM-dd HH:mm:ss.fff}' and u.[GcmRegistrationToken] != ''";

                var agendaNotifications = dbContext.Database.SqlQuery<AgendaNotification>(query).ToList();

                foreach (var agendaNotification in agendaNotifications)
                {
                    var message = new JObject { { "message", ConfigurationManager.AppSettings["agendaMessage"] } };
                    notificationSender.Send(message, agendaNotification.GcmRegistrationToken).GetAwaiter().GetResult();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            //Send last login notification
            try
            {
                var lastLoginQuery =
                    "select [GcmRegistrationToken] from [fightathlete].[dbo].[User] where DATEDIFF(wk, [LastLogin], CAST(GETDATE() AS DATE)) = 4 and [GcmRegistrationToken] != ''";

                var gcmTokens = dbContext.Database.SqlQuery<string>(lastLoginQuery).ToList();
                foreach (var gcmToken in gcmTokens)
                {
                    var message = new JObject { { "message", ConfigurationManager.AppSettings["lastLoginMessage"] } };
                    notificationSender.Send(message, gcmToken).GetAwaiter().GetResult();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
